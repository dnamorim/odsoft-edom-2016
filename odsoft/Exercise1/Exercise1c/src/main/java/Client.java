package example;


public class Client {
	private String myName;
	private String myEmail;
    private String myPhone;
    private String myTin;
    
    public Client () {}
    
    protected Client(String strName, String strEmail, String strPhone, String strTin) {
        setName(strName);
        setEmail(strEmail);
        setPhone(strPhone);
        setTin(strTin);
    }
    
    public String getName() {
        return this.myName;
    }
    
        
    public String getEmail() {
        return this.myEmail;
    }
    
    public String getPhone() {
        return this.myPhone;
    }
    
    public String getTin() {
        return this.myTin;
    }
    
    public void setName(String strName) {
        this.myName = strName;
    }

    public void setEmail(String strEmail)  {
        this.myEmail = strEmail;
    }

    public void setPhone(String strPhone) {
        this.myPhone = strPhone;
    }
    
    public void setTin(String strTin) {
        this.myTin = strTin;
    }
    
    
    public boolean validate() {
    	return true;
    }
    
    @Override
    public String toString()   {
        String str = "Client:\n";
        str += "\tName: " + this.myName + "\n";
        str += "\tEmail: " + this.myEmail + "\n";
        str += "\tPhone: " + this.myPhone + "\n";
        str += "\tTin: " + this.myTin + "\n";

        return str;
    }
    
    @Override
    public Client clone()  {
        return new Client(this.getName(),this.getEmail(), this.getPhone(), this.getTin());
    }


}
