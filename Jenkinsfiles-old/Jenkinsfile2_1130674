#!groovy
node {

git url: 'https://dnamorim@bitbucket.org/mei-isep/odsoft-2016-ifp-g217.git'
 
 stage ('Checkout'){
	echo 'Checkout stage started...'
	checkout scm
    echo 'Checkout stage finished.'
 }

 
 stage ('Build JAR'){
 	echo 'Clear previous test results...'
	dir ("${workspace}/ofbiz"){
    	if (isUnix()) {
    		sh  './gradlew cleanTest'
    	} else {
    		bat 'gradlew cleanTest'
    	}
	}
	echo 'Previous test results cleared.'
	echo 'Build JAR stage started...'
	def workspace = pwd() 
    dir ("${workspace}/ofbiz"){
    	if (isUnix()) {
    		sh  './gradlew build --info'
    	} else {
    		bat 'gradlew build --info'
    	}
	}
	echo 'JAR built successfully.'
 }
 
 parallel(archiveTask: {
	 stage ('Archive JAR'){
		echo 'Archive JAR stage started...'
		dir ("${workspace}/ofbiz/build/libs"){
			archive 'ofbiz.jar'
		}	
		echo 'Ofbiz JAR archieved.'
	 }
 })
 
 stage ('Unit Tests'){
	echo 'Unit tests stage started...'

	dir ("${workspace}/"){
		junit allowEmptyResults: true, testResults: 'ofbiz/build/test-results/*.xml'
	}
	
	echo 'Unit tests completed.'
	
 }

 stage ('Test Coverage'){
 	echo '[exercise7] Test Coverage with Jacoco stage started...'
    dir('ofbiz') {
        if (isUnix()) {
            sh './gradlew jacoco'
        } else {
            bat 'gradlew jacoco'
        }
        publishHTML([allowMissing: true, alwaysLinkToLastBuild: true, keepAll: true, reportDir: 'build/reports/jacoco/test/html', reportFiles: 'index.html', reportName: 'HTML Report']) 
    }  
 		
 	echo '[exercise7] Test Coverage with Jacoco completed.'
 }
 
  stage ('Integration Tests'){
 	echo '[Exercise8] Ofbiz integration tests with Jacoco started...'
    dir('ofbiz') {
        if (isUnix()) {
            sh './gradlew ofbizIntegrationTests'
        } else {
            bat 'gradlew ofbizIntegrationTests'
        }
    }   		
 	echo '[Exercise8] Ofbiz integration tests with Jacoco completed.'
 }
 
  stage ('Publish Integration Tests Results'){
 	echo '[Exercise8] Starting publishing Ofbiz integration tests into an HTML file...'
    dir('ofbiz') {
        if (isUnix()) {
            sh './gradlew ofbizCodeCoverageReport '
        } else {
            bat 'gradlew ofbizCodeCoverageReport '
        }
    }  
 		
 	echo '[Exercise8] All the integration test results were published.'
 }
 
}