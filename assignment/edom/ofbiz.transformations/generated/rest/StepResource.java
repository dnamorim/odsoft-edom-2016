package restcomponent;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

@Path("/recipesteps")
public class StepResource {
	@Context
	HttpServletRequest httpRequest;
	
	@GET
	@Produces("application/json")
	public Response getStep(@PathParam("recipeId") String recipeId, @PathParam("userId") String userId, @PathParam("password") String password) {
		String username = null;
		String password = null;

		try {
			username = httpRequest.getHeader("login.username");
			password = httpRequest.getHeader("login.password");
		} catch (NullPointerException e) {
			return Response.serverError().entity("Problem reading http header(s): login.username or login.password")
					.build();
		}

		if (username == null || password == null) {
			return Response.serverError().entity("Problem reading http header(s): login.username or login.password")
					.build();
		}

		GenericDelegator delegator = (GenericDelegator) DelegatorFactory.getDelegator("default");

		// TODO Add REST method code here...
	}

	@POST
	@Produces("application/json")
	public Response postStep(@PathParam("stepNumber") String stepNumber, @PathParam("recipeId") String recipeId, @PathParam("description") String description, @PathParam("imageURL") String imageURL, @PathParam("userId") String userId, @PathParam("password") String password) {
		String username = null;
		String password = null;

		try {
			username = httpRequest.getHeader("login.username");
			password = httpRequest.getHeader("login.password");
		} catch (NullPointerException e) {
			return Response.serverError().entity("Problem reading http header(s): login.username or login.password")
					.build();
		}

		if (username == null || password == null) {
			return Response.serverError().entity("Problem reading http header(s): login.username or login.password")
					.build();
		}

		GenericDelegator delegator = (GenericDelegator) DelegatorFactory.getDelegator("default");

		// TODO Add REST method code here...
	}

	@PUT
	@Produces("application/json")
	public Response putStep(@PathParam("recipeStepId") String recipeStepId, @PathParam("stepNumber") String stepNumber, @PathParam("recipeId") String recipeId, @PathParam("description") String description, @PathParam("imageURL") String imageURL, @PathParam("userId") String userId, @PathParam("password") String password) {
		String username = null;
		String password = null;

		try {
			username = httpRequest.getHeader("login.username");
			password = httpRequest.getHeader("login.password");
		} catch (NullPointerException e) {
			return Response.serverError().entity("Problem reading http header(s): login.username or login.password")
					.build();
		}

		if (username == null || password == null) {
			return Response.serverError().entity("Problem reading http header(s): login.username or login.password")
					.build();
		}

		GenericDelegator delegator = (GenericDelegator) DelegatorFactory.getDelegator("default");

		// TODO Add REST method code here...
	}

	@DELETE
	@Produces("application/json")
	public Response deleteStep(@PathParam("recipeStepId") String recipeStepId, @PathParam("userId") String userId, @PathParam("password") String password) {
		String username = null;
		String password = null;

		try {
			username = httpRequest.getHeader("login.username");
			password = httpRequest.getHeader("login.password");
		} catch (NullPointerException e) {
			return Response.serverError().entity("Problem reading http header(s): login.username or login.password")
					.build();
		}

		if (username == null || password == null) {
			return Response.serverError().entity("Problem reading http header(s): login.username or login.password")
					.build();
		}

		GenericDelegator delegator = (GenericDelegator) DelegatorFactory.getDelegator("default");

		// TODO Add REST method code here...
	}

}
