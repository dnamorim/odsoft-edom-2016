/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.analysis;

public class EntityPrimKeyReferenceResolver implements ofbiz.resource.ofbiz.IOfbizReferenceResolver<ofbiz.Entity, ofbiz.Field> {
	
	private ofbiz.resource.ofbiz.analysis.OfbizDefaultResolverDelegate<ofbiz.Entity, ofbiz.Field> delegate = new ofbiz.resource.ofbiz.analysis.OfbizDefaultResolverDelegate<ofbiz.Entity, ofbiz.Field>();
	
	public void resolve(String identifier, ofbiz.Entity container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final ofbiz.resource.ofbiz.IOfbizReferenceResolveResult<ofbiz.Field> result) {
		delegate.resolve(identifier, container, reference, position, resolveFuzzy, result);
	}
	
	public String deResolve(ofbiz.Field element, ofbiz.Entity container, org.eclipse.emf.ecore.EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
