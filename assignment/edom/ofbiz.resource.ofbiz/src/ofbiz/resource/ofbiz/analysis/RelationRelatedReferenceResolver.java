/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.analysis;

public class RelationRelatedReferenceResolver implements ofbiz.resource.ofbiz.IOfbizReferenceResolver<ofbiz.Relation, ofbiz.Entity> {
	
	private ofbiz.resource.ofbiz.analysis.OfbizDefaultResolverDelegate<ofbiz.Relation, ofbiz.Entity> delegate = new ofbiz.resource.ofbiz.analysis.OfbizDefaultResolverDelegate<ofbiz.Relation, ofbiz.Entity>();
	
	public void resolve(String identifier, ofbiz.Relation container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final ofbiz.resource.ofbiz.IOfbizReferenceResolveResult<ofbiz.Entity> result) {
		delegate.resolve(identifier, container, reference, position, resolveFuzzy, result);
	}
	
	public String deResolve(ofbiz.Entity element, ofbiz.Relation container, org.eclipse.emf.ecore.EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
