/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz;

public interface IOfbizProblem {
	public String getMessage();
	public ofbiz.resource.ofbiz.OfbizEProblemSeverity getSeverity();
	public ofbiz.resource.ofbiz.OfbizEProblemType getType();
	public java.util.Collection<ofbiz.resource.ofbiz.IOfbizQuickFix> getQuickFixes();
}
