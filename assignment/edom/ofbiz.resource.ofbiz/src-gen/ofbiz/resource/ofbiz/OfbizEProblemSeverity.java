/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz;

public enum OfbizEProblemSeverity {
	WARNING, ERROR;
}
