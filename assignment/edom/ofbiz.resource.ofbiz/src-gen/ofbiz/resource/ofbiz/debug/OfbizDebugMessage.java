/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.debug;

/**
 * DebugMessages are exchanged between the debug server (the Eclipse debug
 * framework) and the debug client (a running process or interpreter). To exchange
 * messages they are serialized and sent over sockets.
 */
public class OfbizDebugMessage {
	
	private static final char DELIMITER = ':';
	private ofbiz.resource.ofbiz.debug.EOfbizDebugMessageTypes messageType;
	private String[] arguments;
	
	public OfbizDebugMessage(ofbiz.resource.ofbiz.debug.EOfbizDebugMessageTypes messageType, String[] arguments) {
		super();
		this.messageType = messageType;
		this.arguments = arguments;
	}
	
	public OfbizDebugMessage(ofbiz.resource.ofbiz.debug.EOfbizDebugMessageTypes messageType, java.util.List<String> arguments) {
		super();
		this.messageType = messageType;
		this.arguments = new String[arguments.size()];
		for (int i = 0; i < arguments.size(); i++) {
			this.arguments[i] = arguments.get(i);
		}
	}
	
	public ofbiz.resource.ofbiz.debug.EOfbizDebugMessageTypes getMessageType() {
		return messageType;
	}
	
	public String[] getArguments() {
		return arguments;
	}
	
	public String serialize() {
		java.util.List<String> parts = new java.util.ArrayList<String>();
		parts.add(messageType.name());
		for (String argument : arguments) {
			parts.add(argument);
		}
		return ofbiz.resource.ofbiz.util.OfbizStringUtil.encode(DELIMITER, parts);
	}
	
	public static OfbizDebugMessage deserialize(String response) {
		java.util.List<String> parts = ofbiz.resource.ofbiz.util.OfbizStringUtil.decode(response, DELIMITER);
		String messageType = parts.get(0);
		String[] arguments = new String[parts.size() - 1];
		for (int i = 1; i < parts.size(); i++) {
			arguments[i - 1] = parts.get(i);
		}
		ofbiz.resource.ofbiz.debug.EOfbizDebugMessageTypes type = ofbiz.resource.ofbiz.debug.EOfbizDebugMessageTypes.valueOf(messageType);
		OfbizDebugMessage message = new OfbizDebugMessage(type, arguments);
		return message;
	}
	
	public boolean hasType(ofbiz.resource.ofbiz.debug.EOfbizDebugMessageTypes type) {
		return this.messageType == type;
	}
	
	public String getArgument(int index) {
		return getArguments()[index];
	}
	
	public String toString() {
		return this.getClass().getSimpleName() + "[" + messageType.name() + ": " + ofbiz.resource.ofbiz.util.OfbizStringUtil.explode(arguments, ", ") + "]";
	}
	
}
