/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.debug;

/**
 * The DebugProxy allows to communicate between the interpreter, which runs in a
 * separate thread or process and the Eclipse Debug framework (i.e., the
 * DebugTarget class).
 */
public class OfbizDebugProxy {
	
	public static final int STARTUP_DELAY = 1000;
	
	private java.io.PrintStream output;
	
	private java.io.BufferedReader reader;
	
	private ofbiz.resource.ofbiz.debug.OfbizDebugTarget debugTarget;
	
	private ofbiz.resource.ofbiz.debug.OfbizDebugCommunicationHelper communicationHelper = new ofbiz.resource.ofbiz.debug.OfbizDebugCommunicationHelper();
	
	public OfbizDebugProxy(ofbiz.resource.ofbiz.debug.OfbizDebugTarget debugTarget, int requestPort) throws java.net.UnknownHostException, java.io.IOException {
		this.debugTarget = debugTarget;
		// give interpreter a chance to start
		try {
			Thread.sleep(STARTUP_DELAY);
		} catch (InterruptedException e) {
		}
		startSocket(requestPort);
	}
	
	private void startSocket(int requestPort) throws java.net.UnknownHostException, java.io.IOException {
		// creating client proxy socket (trying to connect)...
		java.net.Socket client = new java.net.Socket("localhost", requestPort);
		// creating client proxy socket - done. (connected)
		try {
			java.io.BufferedInputStream input = new java.io.BufferedInputStream(client.getInputStream());
			reader = new java.io.BufferedReader(new java.io.InputStreamReader(input));
		} catch (java.io.IOException e) {
			System.out.println(e);
		}
		try {
			output = new java.io.PrintStream(client.getOutputStream());
		} catch (java.io.IOException e) {
			System.out.println(e);
		}
	}
	
	public void resume() {
		sendCommand(ofbiz.resource.ofbiz.debug.EOfbizDebugMessageTypes.RESUME);
	}
	
	public void stepOver() {
		sendCommand(ofbiz.resource.ofbiz.debug.EOfbizDebugMessageTypes.STEP_OVER);
	}
	
	public void stepInto() {
		sendCommand(ofbiz.resource.ofbiz.debug.EOfbizDebugMessageTypes.STEP_INTO);
	}
	
	public void stepReturn() {
		sendCommand(ofbiz.resource.ofbiz.debug.EOfbizDebugMessageTypes.STEP_RETURN);
	}
	
	public void terminate() {
		sendCommand(ofbiz.resource.ofbiz.debug.EOfbizDebugMessageTypes.EXIT);
	}
	
	public ofbiz.resource.ofbiz.debug.OfbizDebugMessage getStack() {
		return sendCommandAndRead(ofbiz.resource.ofbiz.debug.EOfbizDebugMessageTypes.GET_STACK);
	}
	
	public void addLineBreakpoint(String location, int line) {
		ofbiz.resource.ofbiz.debug.OfbizDebugMessage message = new ofbiz.resource.ofbiz.debug.OfbizDebugMessage(ofbiz.resource.ofbiz.debug.EOfbizDebugMessageTypes.ADD_LINE_BREAKPOINT, new String[] {location, Integer.toString(line)});
		communicationHelper.sendEvent(message, output);
	}
	
	public void removeLineBreakpoint(String location, int line) {
		ofbiz.resource.ofbiz.debug.OfbizDebugMessage message = new ofbiz.resource.ofbiz.debug.OfbizDebugMessage(ofbiz.resource.ofbiz.debug.EOfbizDebugMessageTypes.REMOVE_LINE_BREAKPOINT, new String[] {location, Integer.toString(line)});
		communicationHelper.sendEvent(message, output);
	}
	
	public org.eclipse.debug.core.model.IVariable[] getStackVariables(String stackFrame) {
		ofbiz.resource.ofbiz.debug.OfbizDebugMessage response = sendCommandAndRead(ofbiz.resource.ofbiz.debug.EOfbizDebugMessageTypes.GET_FRAME_VARIABLES, new String[] {stackFrame});
		String[] ids = response.getArguments();
		// fetch all variables
		org.eclipse.debug.core.model.IVariable[] variables = getVariables(ids);
		return variables;
	}
	
	public org.eclipse.debug.core.model.IVariable[] getVariables(String... requestedIDs) {
		ofbiz.resource.ofbiz.debug.OfbizDebugMessage response = sendCommandAndRead(ofbiz.resource.ofbiz.debug.EOfbizDebugMessageTypes.GET_VARIABLES, requestedIDs);
		String[] varStrings = response.getArguments();
		ofbiz.resource.ofbiz.debug.OfbizDebugVariable[] variables  = new ofbiz.resource.ofbiz.debug.OfbizDebugVariable[varStrings.length];
		int i = 0;
		for (String varString : varStrings) {
			java.util.Map<String, String> properties = ofbiz.resource.ofbiz.util.OfbizStringUtil.convertFromString(varString);
			
			// convert varString to variables and values
			String valueString = properties.get("!valueString");
			String valueRefType = "valueRefType";
			java.util.Map<String, Long> childVariables = new java.util.TreeMap<String, Long>(new java.util.Comparator<String>() {
				public int compare(String s1, String s2) {
					return s1.compareToIgnoreCase(s2);
				}
			});
			for (String property : properties.keySet()) {
				// ignore special properties - they are not children
				if (property.startsWith("!")) {
					continue;
				}
				childVariables.put(property, Long.parseLong(properties.get(property)));
			}
			String id = properties.get("!id");
			org.eclipse.debug.core.model.IValue value = new ofbiz.resource.ofbiz.debug.OfbizDebugValue(debugTarget, id, valueString, valueRefType, childVariables);
			
			String variableName = properties.get("!name");
			String variableRefType = properties.get("!type");
			
			ofbiz.resource.ofbiz.debug.OfbizDebugVariable variable = new ofbiz.resource.ofbiz.debug.OfbizDebugVariable(debugTarget, variableName, value, variableRefType);
			variables[i++] = variable;
		}
		return variables;
	}
	
	private void sendCommand(ofbiz.resource.ofbiz.debug.EOfbizDebugMessageTypes command, String... parameters) {
		ofbiz.resource.ofbiz.debug.OfbizDebugMessage message = new ofbiz.resource.ofbiz.debug.OfbizDebugMessage(command, parameters);
		communicationHelper.sendEvent(message, output);
	}
	
	private ofbiz.resource.ofbiz.debug.OfbizDebugMessage sendCommandAndRead(ofbiz.resource.ofbiz.debug.EOfbizDebugMessageTypes command, String... parameters) {
		ofbiz.resource.ofbiz.debug.OfbizDebugMessage message = new ofbiz.resource.ofbiz.debug.OfbizDebugMessage(command, parameters);
		return communicationHelper.sendAndReceive(message, output, reader);
	}
	
}
