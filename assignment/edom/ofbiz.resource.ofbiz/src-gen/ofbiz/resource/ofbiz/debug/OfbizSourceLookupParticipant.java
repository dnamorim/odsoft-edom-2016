/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.debug;

public class OfbizSourceLookupParticipant extends org.eclipse.debug.core.sourcelookup.AbstractSourceLookupParticipant {
	
	public String getSourceName(Object object) throws org.eclipse.core.runtime.CoreException {
		if (object instanceof ofbiz.resource.ofbiz.debug.OfbizStackFrame) {
			ofbiz.resource.ofbiz.debug.OfbizStackFrame frame = (ofbiz.resource.ofbiz.debug.OfbizStackFrame) object;
			return frame.getResourceURI();
		}
		return null;
	}
	
}
