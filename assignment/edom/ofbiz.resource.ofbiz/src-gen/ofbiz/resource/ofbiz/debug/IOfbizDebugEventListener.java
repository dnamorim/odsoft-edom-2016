/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.debug;

public interface IOfbizDebugEventListener {
	
	/**
	 * Notification that the given event occurred in the while debugging.
	 */
	public void handleMessage(ofbiz.resource.ofbiz.debug.OfbizDebugMessage message);
}
