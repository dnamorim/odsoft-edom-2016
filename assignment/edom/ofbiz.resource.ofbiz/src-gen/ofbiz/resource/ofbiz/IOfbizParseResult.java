/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz;

/**
 * An interface used to access the result of parsing a document.
 */
public interface IOfbizParseResult {
	
	/**
	 * Returns the root object of the document.
	 */
	public org.eclipse.emf.ecore.EObject getRoot();
	
	/**
	 * Returns a list of commands that must be executed after parsing the document.
	 */
	public java.util.Collection<ofbiz.resource.ofbiz.IOfbizCommand<ofbiz.resource.ofbiz.IOfbizTextResource>> getPostParseCommands();
	
}
