/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.util;

/**
 * This class provides basic infrastructure to interpret models. To implement
 * concrete interpreters, subclass this abstract interpreter and override the
 * interprete_* methods. The interpretation can be customized by binding the two
 * type parameters (ResultType, ContextType). The former is returned by all
 * interprete_* methods, while the latter is passed from method to method while
 * traversing the model. The concrete traversal strategy can also be exchanged.
 * One can use a static traversal strategy by pushing all objects to interpret on
 * the interpretation stack (using addObjectToInterprete()) before calling
 * interprete(). Alternatively, the traversal strategy can be dynamic by pushing
 * objects on the interpretation stack during interpretation.
 */
public class AbstractOfbizInterpreter<ResultType, ContextType> {
	
	private java.util.Stack<org.eclipse.emf.ecore.EObject> interpretationStack = new java.util.Stack<org.eclipse.emf.ecore.EObject>();
	private java.util.List<ofbiz.resource.ofbiz.IOfbizInterpreterListener> listeners = new java.util.ArrayList<ofbiz.resource.ofbiz.IOfbizInterpreterListener>();
	private org.eclipse.emf.ecore.EObject nextObjectToInterprete;
	private Object currentContext;
	
	public ResultType interprete(ContextType context) {
		ResultType result = null;
		org.eclipse.emf.ecore.EObject next = null;
		currentContext = context;
		while (!interpretationStack.empty()) {
			try {
				next = interpretationStack.pop();
			} catch (java.util.EmptyStackException ese) {
				// this can happen when the interpreter was terminated between the call to empty()
				// and pop()
				break;
			}
			nextObjectToInterprete = next;
			notifyListeners(next);
			result = interprete(next, context);
			if (!continueInterpretation(context, result)) {
				break;
			}
		}
		currentContext = null;
		return result;
	}
	
	/**
	 * Override this method to stop the overall interpretation depending on the result
	 * of the interpretation of a single model elements.
	 */
	public boolean continueInterpretation(ContextType context, ResultType result) {
		return true;
	}
	
	public ResultType interprete(org.eclipse.emf.ecore.EObject object, ContextType context) {
		ResultType result = null;
		if (object instanceof ofbiz.OfbizComponent) {
			result = interprete_ofbiz_OfbizComponent((ofbiz.OfbizComponent) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof ofbiz.EntityModel) {
			result = interprete_ofbiz_EntityModel((ofbiz.EntityModel) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof ofbiz.Entity) {
			result = interprete_ofbiz_Entity((ofbiz.Entity) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof ofbiz.Field) {
			result = interprete_ofbiz_Field((ofbiz.Field) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof ofbiz.Relation) {
			result = interprete_ofbiz_Relation((ofbiz.Relation) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof ofbiz.KeyMap) {
			result = interprete_ofbiz_KeyMap((ofbiz.KeyMap) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof ofbiz.Services) {
			result = interprete_ofbiz_Services((ofbiz.Services) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof ofbiz.Service) {
			result = interprete_ofbiz_Service((ofbiz.Service) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof ofbiz.Attribute) {
			result = interprete_ofbiz_Attribute((ofbiz.Attribute) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof ofbiz.RestService) {
			result = interprete_ofbiz_RestService((ofbiz.RestService) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof ofbiz.RestRequest) {
			result = interprete_ofbiz_RestRequest((ofbiz.RestRequest) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof ofbiz.RestParameter) {
			result = interprete_ofbiz_RestParameter((ofbiz.RestParameter) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof ofbiz.EntityResource) {
			result = interprete_ofbiz_EntityResource((ofbiz.EntityResource) object, context);
		}
		if (result != null) {
			return result;
		}
		return result;
	}
	
	public ResultType interprete_ofbiz_OfbizComponent(ofbiz.OfbizComponent ofbizComponent, ContextType context) {
		return null;
	}
	
	public ResultType interprete_ofbiz_EntityModel(ofbiz.EntityModel entityModel, ContextType context) {
		return null;
	}
	
	public ResultType interprete_ofbiz_Entity(ofbiz.Entity entity, ContextType context) {
		return null;
	}
	
	public ResultType interprete_ofbiz_Field(ofbiz.Field field, ContextType context) {
		return null;
	}
	
	public ResultType interprete_ofbiz_Relation(ofbiz.Relation relation, ContextType context) {
		return null;
	}
	
	public ResultType interprete_ofbiz_KeyMap(ofbiz.KeyMap keyMap, ContextType context) {
		return null;
	}
	
	public ResultType interprete_ofbiz_Services(ofbiz.Services services, ContextType context) {
		return null;
	}
	
	public ResultType interprete_ofbiz_Service(ofbiz.Service service, ContextType context) {
		return null;
	}
	
	public ResultType interprete_ofbiz_Attribute(ofbiz.Attribute attribute, ContextType context) {
		return null;
	}
	
	public ResultType interprete_ofbiz_RestService(ofbiz.RestService restService, ContextType context) {
		return null;
	}
	
	public ResultType interprete_ofbiz_RestRequest(ofbiz.RestRequest restRequest, ContextType context) {
		return null;
	}
	
	public ResultType interprete_ofbiz_RestParameter(ofbiz.RestParameter restParameter, ContextType context) {
		return null;
	}
	
	public ResultType interprete_ofbiz_EntityResource(ofbiz.EntityResource entityResource, ContextType context) {
		return null;
	}
	
	private void notifyListeners(org.eclipse.emf.ecore.EObject element) {
		for (ofbiz.resource.ofbiz.IOfbizInterpreterListener listener : listeners) {
			listener.handleInterpreteObject(element);
		}
	}
	
	/**
	 * Adds the given object to the interpretation stack. Attention: Objects that are
	 * added first, are interpret last.
	 */
	public void addObjectToInterprete(org.eclipse.emf.ecore.EObject object) {
		interpretationStack.push(object);
	}
	
	/**
	 * Adds the given collection of objects to the interpretation stack. Attention:
	 * Collections that are added first, are interpret last.
	 */
	public void addObjectsToInterprete(java.util.Collection<? extends org.eclipse.emf.ecore.EObject> objects) {
		for (org.eclipse.emf.ecore.EObject object : objects) {
			addObjectToInterprete(object);
		}
	}
	
	/**
	 * Adds the given collection of objects in reverse order to the interpretation
	 * stack.
	 */
	public void addObjectsToInterpreteInReverseOrder(java.util.Collection<? extends org.eclipse.emf.ecore.EObject> objects) {
		java.util.List<org.eclipse.emf.ecore.EObject> reverse = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>(objects.size());
		reverse.addAll(objects);
		java.util.Collections.reverse(reverse);
		addObjectsToInterprete(reverse);
	}
	
	/**
	 * Adds the given object and all its children to the interpretation stack such
	 * that they are interpret in top down order.
	 */
	public void addObjectTreeToInterpreteTopDown(org.eclipse.emf.ecore.EObject root) {
		java.util.List<org.eclipse.emf.ecore.EObject> objects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
		objects.add(root);
		java.util.Iterator<org.eclipse.emf.ecore.EObject> it = root.eAllContents();
		while (it.hasNext()) {
			org.eclipse.emf.ecore.EObject eObject = (org.eclipse.emf.ecore.EObject) it.next();
			objects.add(eObject);
		}
		addObjectsToInterpreteInReverseOrder(objects);
	}
	
	public void addListener(ofbiz.resource.ofbiz.IOfbizInterpreterListener newListener) {
		listeners.add(newListener);
	}
	
	public boolean removeListener(ofbiz.resource.ofbiz.IOfbizInterpreterListener listener) {
		return listeners.remove(listener);
	}
	
	public org.eclipse.emf.ecore.EObject getNextObjectToInterprete() {
		return nextObjectToInterprete;
	}
	
	public java.util.Stack<org.eclipse.emf.ecore.EObject> getInterpretationStack() {
		return interpretationStack;
	}
	
	public void terminate() {
		interpretationStack.clear();
	}
	
	public Object getCurrentContext() {
		return currentContext;
	}
	
}
