/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.util;

/**
 * Class OfbizTextResourceUtil can be used to perform common tasks on text
 * resources, such as loading and saving resources, as well as, checking them for
 * errors. This class is deprecated and has been replaced by
 * ofbiz.resource.ofbiz.util.OfbizResourceUtil.
 */
public class OfbizTextResourceUtil {
	
	/**
	 * Use ofbiz.resource.ofbiz.util.OfbizResourceUtil.getResource() instead.
	 */
	@Deprecated	
	public static ofbiz.resource.ofbiz.mopp.OfbizResource getResource(org.eclipse.core.resources.IFile file) {
		return new ofbiz.resource.ofbiz.util.OfbizEclipseProxy().getResource(file);
	}
	
	/**
	 * Use ofbiz.resource.ofbiz.util.OfbizResourceUtil.getResource() instead.
	 */
	@Deprecated	
	public static ofbiz.resource.ofbiz.mopp.OfbizResource getResource(java.io.File file, java.util.Map<?,?> options) {
		return ofbiz.resource.ofbiz.util.OfbizResourceUtil.getResource(file, options);
	}
	
	/**
	 * Use ofbiz.resource.ofbiz.util.OfbizResourceUtil.getResource() instead.
	 */
	@Deprecated	
	public static ofbiz.resource.ofbiz.mopp.OfbizResource getResource(org.eclipse.emf.common.util.URI uri) {
		return ofbiz.resource.ofbiz.util.OfbizResourceUtil.getResource(uri);
	}
	
	/**
	 * Use ofbiz.resource.ofbiz.util.OfbizResourceUtil.getResource() instead.
	 */
	@Deprecated	
	public static ofbiz.resource.ofbiz.mopp.OfbizResource getResource(org.eclipse.emf.common.util.URI uri, java.util.Map<?,?> options) {
		return ofbiz.resource.ofbiz.util.OfbizResourceUtil.getResource(uri, options);
	}
	
}
