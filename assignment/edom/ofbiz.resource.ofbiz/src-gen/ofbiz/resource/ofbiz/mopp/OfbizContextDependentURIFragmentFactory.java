/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.mopp;

/**
 * A factory for ContextDependentURIFragments. Given a feasible reference
 * resolver, this factory returns a matching fragment that used the resolver to
 * resolver proxy objects.
 * 
 * @param <ContainerType> the type of the class containing the reference to be
 * resolved
 * @param <ReferenceType> the type of the reference to be resolved
 */
public class OfbizContextDependentURIFragmentFactory<ContainerType extends org.eclipse.emf.ecore.EObject, ReferenceType extends org.eclipse.emf.ecore.EObject>  implements ofbiz.resource.ofbiz.IOfbizContextDependentURIFragmentFactory<ContainerType, ReferenceType> {
	
	private final ofbiz.resource.ofbiz.IOfbizReferenceResolver<ContainerType, ReferenceType> resolver;
	
	public OfbizContextDependentURIFragmentFactory(ofbiz.resource.ofbiz.IOfbizReferenceResolver<ContainerType, ReferenceType> resolver) {
		this.resolver = resolver;
	}
	
	public ofbiz.resource.ofbiz.IOfbizContextDependentURIFragment<?> create(String identifier, ContainerType container, org.eclipse.emf.ecore.EReference reference, int positionInReference, org.eclipse.emf.ecore.EObject proxy) {
		
		return new ofbiz.resource.ofbiz.mopp.OfbizContextDependentURIFragment<ContainerType, ReferenceType>(identifier, container, reference, positionInReference, proxy) {
			public ofbiz.resource.ofbiz.IOfbizReferenceResolver<ContainerType, ReferenceType> getResolver() {
				return resolver;
			}
		};
	}
}
