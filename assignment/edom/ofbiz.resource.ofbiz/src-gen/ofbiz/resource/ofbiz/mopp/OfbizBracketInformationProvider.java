/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.mopp;

public class OfbizBracketInformationProvider {
	
	public class BracketPair implements ofbiz.resource.ofbiz.IOfbizBracketPair {
		
		private String opening;
		private String closing;
		private boolean closingEnabledInside;
		
		public BracketPair(String opening, String closing, boolean closingEnabledInside) {
			super();
			this.opening = opening;
			this.closing = closing;
			this.closingEnabledInside = closingEnabledInside;
		}
		
		public String getOpeningBracket() {
			return opening;
		}
		
		public String getClosingBracket() {
			return closing;
		}
		
		public boolean isClosingEnabledInside() {
			return closingEnabledInside;
		}
	}
	
	public java.util.Collection<ofbiz.resource.ofbiz.IOfbizBracketPair> getBracketPairs() {
		java.util.Collection<ofbiz.resource.ofbiz.IOfbizBracketPair> result = new java.util.ArrayList<ofbiz.resource.ofbiz.IOfbizBracketPair>();
		result.add(new BracketPair("{", "}", true));
		result.add(new BracketPair("\"", "\"", false));
		return result;
	}
	
}
