/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.mopp;

/**
 * A representation for a range in a document where a keyword (i.e., a static
 * string) is expected.
 */
public class OfbizExpectedCsString extends ofbiz.resource.ofbiz.mopp.OfbizAbstractExpectedElement {
	
	private ofbiz.resource.ofbiz.grammar.OfbizKeyword keyword;
	
	public OfbizExpectedCsString(ofbiz.resource.ofbiz.grammar.OfbizKeyword keyword) {
		super(keyword.getMetaclass());
		this.keyword = keyword;
	}
	
	public String getValue() {
		return keyword.getValue();
	}
	
	/**
	 * Returns the expected keyword.
	 */
	public ofbiz.resource.ofbiz.grammar.OfbizSyntaxElement getSymtaxElement() {
		return keyword;
	}
	
	public java.util.Set<String> getTokenNames() {
		return java.util.Collections.singleton("'" + getValue() + "'");
	}
	
	public String toString() {
		return "CsString \"" + getValue() + "\"";
	}
	
	public boolean equals(Object o) {
		if (o instanceof OfbizExpectedCsString) {
			return getValue().equals(((OfbizExpectedCsString) o).getValue());
		}
		return false;
	}
	
	@Override	
	public int hashCode() {
		return getValue().hashCode();
	}
	
}
