/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.mopp;

public class OfbizSyntaxCoverageInformationProvider {
	
	public org.eclipse.emf.ecore.EClass[] getClassesWithSyntax() {
		return new org.eclipse.emf.ecore.EClass[] {
			ofbiz.OfbizPackage.eINSTANCE.getOfbizComponent(),
			ofbiz.OfbizPackage.eINSTANCE.getEntityModel(),
			ofbiz.OfbizPackage.eINSTANCE.getEntity(),
			ofbiz.OfbizPackage.eINSTANCE.getField(),
			ofbiz.OfbizPackage.eINSTANCE.getRelation(),
			ofbiz.OfbizPackage.eINSTANCE.getKeyMap(),
			ofbiz.OfbizPackage.eINSTANCE.getServices(),
			ofbiz.OfbizPackage.eINSTANCE.getService(),
			ofbiz.OfbizPackage.eINSTANCE.getAttribute(),
			ofbiz.OfbizPackage.eINSTANCE.getRestService(),
			ofbiz.OfbizPackage.eINSTANCE.getRestRequest(),
			ofbiz.OfbizPackage.eINSTANCE.getRestParameter(),
			ofbiz.OfbizPackage.eINSTANCE.getEntityResource(),
		};
	}
	
	public org.eclipse.emf.ecore.EClass[] getStartSymbols() {
		return new org.eclipse.emf.ecore.EClass[] {
			ofbiz.OfbizPackage.eINSTANCE.getOfbizComponent(),
		};
	}
	
}
