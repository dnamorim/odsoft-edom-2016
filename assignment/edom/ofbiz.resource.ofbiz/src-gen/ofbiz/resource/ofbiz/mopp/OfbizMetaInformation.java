/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.mopp;

public class OfbizMetaInformation implements ofbiz.resource.ofbiz.IOfbizMetaInformation {
	
	public String getSyntaxName() {
		return "ofbiz";
	}
	
	public String getURI() {
		return "http://www.emftext.org/language/ofbiz";
	}
	
	public ofbiz.resource.ofbiz.IOfbizTextScanner createLexer() {
		return new ofbiz.resource.ofbiz.mopp.OfbizAntlrScanner(new ofbiz.resource.ofbiz.mopp.OfbizLexer());
	}
	
	public ofbiz.resource.ofbiz.IOfbizTextParser createParser(java.io.InputStream inputStream, String encoding) {
		return new ofbiz.resource.ofbiz.mopp.OfbizParser().createInstance(inputStream, encoding);
	}
	
	public ofbiz.resource.ofbiz.IOfbizTextPrinter createPrinter(java.io.OutputStream outputStream, ofbiz.resource.ofbiz.IOfbizTextResource resource) {
		return new ofbiz.resource.ofbiz.mopp.OfbizPrinter2(outputStream, resource);
	}
	
	public org.eclipse.emf.ecore.EClass[] getClassesWithSyntax() {
		return new ofbiz.resource.ofbiz.mopp.OfbizSyntaxCoverageInformationProvider().getClassesWithSyntax();
	}
	
	public org.eclipse.emf.ecore.EClass[] getStartSymbols() {
		return new ofbiz.resource.ofbiz.mopp.OfbizSyntaxCoverageInformationProvider().getStartSymbols();
	}
	
	public ofbiz.resource.ofbiz.IOfbizReferenceResolverSwitch getReferenceResolverSwitch() {
		return new ofbiz.resource.ofbiz.mopp.OfbizReferenceResolverSwitch();
	}
	
	public ofbiz.resource.ofbiz.IOfbizTokenResolverFactory getTokenResolverFactory() {
		return new ofbiz.resource.ofbiz.mopp.OfbizTokenResolverFactory();
	}
	
	public String getPathToCSDefinition() {
		return "ofbiz-dsl/metamodel/ofbiz.cs";
	}
	
	public String[] getTokenNames() {
		return ofbiz.resource.ofbiz.mopp.OfbizParser.tokenNames;
	}
	
	public ofbiz.resource.ofbiz.IOfbizTokenStyle getDefaultTokenStyle(String tokenName) {
		return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyleInformationProvider().getDefaultTokenStyle(tokenName);
	}
	
	public java.util.Collection<ofbiz.resource.ofbiz.IOfbizBracketPair> getBracketPairs() {
		return new ofbiz.resource.ofbiz.mopp.OfbizBracketInformationProvider().getBracketPairs();
	}
	
	public org.eclipse.emf.ecore.EClass[] getFoldableClasses() {
		return new ofbiz.resource.ofbiz.mopp.OfbizFoldingInformationProvider().getFoldableClasses();
	}
	
	public org.eclipse.emf.ecore.resource.Resource.Factory createResourceFactory() {
		return new ofbiz.resource.ofbiz.mopp.OfbizResourceFactory();
	}
	
	public ofbiz.resource.ofbiz.mopp.OfbizNewFileContentProvider getNewFileContentProvider() {
		return new ofbiz.resource.ofbiz.mopp.OfbizNewFileContentProvider();
	}
	
	public void registerResourceFactory() {
		org.eclipse.emf.ecore.resource.Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(getSyntaxName(), new ofbiz.resource.ofbiz.mopp.OfbizResourceFactory());
	}
	
	/**
	 * Returns the key of the option that can be used to register a preprocessor that
	 * is used as a pipe when loading resources. This key is language-specific. To
	 * register one preprocessor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getInputStreamPreprocessorProviderOptionKey() {
		return getSyntaxName() + "_" + "INPUT_STREAM_PREPROCESSOR_PROVIDER";
	}
	
	/**
	 * Returns the key of the option that can be used to register a post-processors
	 * that are invoked after loading resources. This key is language-specific. To
	 * register one post-processor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getResourcePostProcessorProviderOptionKey() {
		return getSyntaxName() + "_" + "RESOURCE_POSTPROCESSOR_PROVIDER";
	}
	
	public String getLaunchConfigurationType() {
		return "ofbiz.resource.ofbiz.ui.launchConfigurationType";
	}
	
	public ofbiz.resource.ofbiz.IOfbizNameProvider createNameProvider() {
		return new ofbiz.resource.ofbiz.analysis.OfbizDefaultNameProvider();
	}
	
	public String[] getSyntaxHighlightableTokenNames() {
		ofbiz.resource.ofbiz.mopp.OfbizAntlrTokenHelper tokenHelper = new ofbiz.resource.ofbiz.mopp.OfbizAntlrTokenHelper();
		java.util.List<String> highlightableTokens = new java.util.ArrayList<String>();
		String[] parserTokenNames = getTokenNames();
		for (int i = 0; i < parserTokenNames.length; i++) {
			// If ANTLR is used we need to normalize the token names
			if (!tokenHelper.canBeUsedForSyntaxHighlighting(i)) {
				continue;
			}
			String tokenName = tokenHelper.getTokenName(parserTokenNames, i);
			if (tokenName == null) {
				continue;
			}
			highlightableTokens.add(tokenName);
		}
		highlightableTokens.add(ofbiz.resource.ofbiz.mopp.OfbizTokenStyleInformationProvider.TASK_ITEM_TOKEN_NAME);
		return highlightableTokens.toArray(new String[highlightableTokens.size()]);
	}
	
}
