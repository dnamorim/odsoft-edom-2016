/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.mopp;

/**
 * The OfbizTokenResolverFactory class provides access to all generated token
 * resolvers. By giving the name of a defined token, the corresponding resolve can
 * be obtained. Despite the fact that this class is called TokenResolverFactory is
 * does NOT create new token resolvers whenever a client calls methods to obtain a
 * resolver. Rather, this class maintains a map of all resolvers and creates each
 * resolver at most once.
 */
public class OfbizTokenResolverFactory implements ofbiz.resource.ofbiz.IOfbizTokenResolverFactory {
	
	private java.util.Map<String, ofbiz.resource.ofbiz.IOfbizTokenResolver> tokenName2TokenResolver;
	private java.util.Map<String, ofbiz.resource.ofbiz.IOfbizTokenResolver> featureName2CollectInTokenResolver;
	private static ofbiz.resource.ofbiz.IOfbizTokenResolver defaultResolver = new ofbiz.resource.ofbiz.analysis.OfbizDefaultTokenResolver();
	
	public OfbizTokenResolverFactory() {
		tokenName2TokenResolver = new java.util.LinkedHashMap<String, ofbiz.resource.ofbiz.IOfbizTokenResolver>();
		featureName2CollectInTokenResolver = new java.util.LinkedHashMap<String, ofbiz.resource.ofbiz.IOfbizTokenResolver>();
		registerTokenResolver("TEXT", new ofbiz.resource.ofbiz.analysis.OfbizTEXTTokenResolver());
		registerTokenResolver("QUOTED_34_34", new ofbiz.resource.ofbiz.analysis.OfbizQUOTED_34_34TokenResolver());
	}
	
	public ofbiz.resource.ofbiz.IOfbizTokenResolver createTokenResolver(String tokenName) {
		return internalCreateResolver(tokenName2TokenResolver, tokenName);
	}
	
	public ofbiz.resource.ofbiz.IOfbizTokenResolver createCollectInTokenResolver(String featureName) {
		return internalCreateResolver(featureName2CollectInTokenResolver, featureName);
	}
	
	protected boolean registerTokenResolver(String tokenName, ofbiz.resource.ofbiz.IOfbizTokenResolver resolver){
		return internalRegisterTokenResolver(tokenName2TokenResolver, tokenName, resolver);
	}
	
	protected boolean registerCollectInTokenResolver(String featureName, ofbiz.resource.ofbiz.IOfbizTokenResolver resolver){
		return internalRegisterTokenResolver(featureName2CollectInTokenResolver, featureName, resolver);
	}
	
	protected ofbiz.resource.ofbiz.IOfbizTokenResolver deRegisterTokenResolver(String tokenName){
		return tokenName2TokenResolver.remove(tokenName);
	}
	
	private ofbiz.resource.ofbiz.IOfbizTokenResolver internalCreateResolver(java.util.Map<String, ofbiz.resource.ofbiz.IOfbizTokenResolver> resolverMap, String key) {
		if (resolverMap.containsKey(key)){
			return resolverMap.get(key);
		} else {
			return defaultResolver;
		}
	}
	
	private boolean internalRegisterTokenResolver(java.util.Map<String, ofbiz.resource.ofbiz.IOfbizTokenResolver> resolverMap, String key, ofbiz.resource.ofbiz.IOfbizTokenResolver resolver) {
		if (!resolverMap.containsKey(key)) {
			resolverMap.put(key,resolver);
			return true;
		}
		return false;
	}
	
}
