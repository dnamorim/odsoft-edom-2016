/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.mopp;

public class OfbizResourceFactory implements org.eclipse.emf.ecore.resource.Resource.Factory {
	
	public OfbizResourceFactory() {
		super();
	}
	
	public org.eclipse.emf.ecore.resource.Resource createResource(org.eclipse.emf.common.util.URI uri) {
		return new ofbiz.resource.ofbiz.mopp.OfbizResource(uri);
	}
	
}
