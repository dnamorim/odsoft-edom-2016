/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.mopp;

public class OfbizParseResult implements ofbiz.resource.ofbiz.IOfbizParseResult {
	
	private org.eclipse.emf.ecore.EObject root;
	private java.util.Collection<ofbiz.resource.ofbiz.IOfbizCommand<ofbiz.resource.ofbiz.IOfbizTextResource>> commands = new java.util.ArrayList<ofbiz.resource.ofbiz.IOfbizCommand<ofbiz.resource.ofbiz.IOfbizTextResource>>();
	
	public OfbizParseResult() {
		super();
	}
	
	public void setRoot(org.eclipse.emf.ecore.EObject root) {
		this.root = root;
	}
	
	public org.eclipse.emf.ecore.EObject getRoot() {
		return root;
	}
	
	public java.util.Collection<ofbiz.resource.ofbiz.IOfbizCommand<ofbiz.resource.ofbiz.IOfbizTextResource>> getPostParseCommands() {
		return commands;
	}
	
}
