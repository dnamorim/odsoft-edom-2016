grammar Ofbiz;

options {
	superClass = OfbizANTLRParserBase;
	backtrack = true;
	memoize = true;
}

@lexer::header {
	package ofbiz.resource.ofbiz.mopp;
}

@lexer::members {
	public java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions  = new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>();
	public java.util.List<Integer> lexerExceptionsPosition = new java.util.ArrayList<Integer>();
	
	public void reportError(org.antlr.runtime3_4_0.RecognitionException e) {
		lexerExceptions.add(e);
		lexerExceptionsPosition.add(((org.antlr.runtime3_4_0.ANTLRStringStream) input).index());
	}
}
@header{
	package ofbiz.resource.ofbiz.mopp;
}

@members{
	private ofbiz.resource.ofbiz.IOfbizTokenResolverFactory tokenResolverFactory = new ofbiz.resource.ofbiz.mopp.OfbizTokenResolverFactory();
	
	/**
	 * the index of the last token that was handled by collectHiddenTokens()
	 */
	private int lastPosition;
	
	/**
	 * A flag that indicates whether the parser should remember all expected elements.
	 * This flag is set to true when using the parse for code completion. Otherwise it
	 * is set to false.
	 */
	private boolean rememberExpectedElements = false;
	
	private Object parseToIndexTypeObject;
	private int lastTokenIndex = 0;
	
	/**
	 * A list of expected elements the were collected while parsing the input stream.
	 * This list is only filled if <code>rememberExpectedElements</code> is set to
	 * true.
	 */
	private java.util.List<ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal> expectedElements = new java.util.ArrayList<ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal>();
	
	private int mismatchedTokenRecoveryTries = 0;
	/**
	 * A helper list to allow a lexer to pass errors to its parser
	 */
	protected java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions = java.util.Collections.synchronizedList(new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>());
	
	/**
	 * Another helper list to allow a lexer to pass positions of errors to its parser
	 */
	protected java.util.List<Integer> lexerExceptionsPosition = java.util.Collections.synchronizedList(new java.util.ArrayList<Integer>());
	
	/**
	 * A stack for incomplete objects. This stack is used filled when the parser is
	 * used for code completion. Whenever the parser starts to read an object it is
	 * pushed on the stack. Once the element was parser completely it is popped from
	 * the stack.
	 */
	java.util.List<org.eclipse.emf.ecore.EObject> incompleteObjects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
	
	private int stopIncludingHiddenTokens;
	private int stopExcludingHiddenTokens;
	private int tokenIndexOfLastCompleteElement;
	
	private int expectedElementsIndexOfLastCompleteElement;
	
	/**
	 * The offset indicating the cursor position when the parser is used for code
	 * completion by calling parseToExpectedElements().
	 */
	private int cursorOffset;
	
	/**
	 * The offset of the first hidden token of the last expected element. This offset
	 * is used to discard expected elements, which are not needed for code completion.
	 */
	private int lastStartIncludingHidden;
	
	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
		postParseCommands.add(new ofbiz.resource.ofbiz.IOfbizCommand<ofbiz.resource.ofbiz.IOfbizTextResource>() {
			public boolean execute(ofbiz.resource.ofbiz.IOfbizTextResource resource) {
				if (resource == null) {
					// the resource can be null if the parser is used for code completion
					return true;
				}
				resource.addProblem(new ofbiz.resource.ofbiz.IOfbizProblem() {
					public ofbiz.resource.ofbiz.OfbizEProblemSeverity getSeverity() {
						return ofbiz.resource.ofbiz.OfbizEProblemSeverity.ERROR;
					}
					public ofbiz.resource.ofbiz.OfbizEProblemType getType() {
						return ofbiz.resource.ofbiz.OfbizEProblemType.SYNTAX_ERROR;
					}
					public String getMessage() {
						return errorMessage;
					}
					public java.util.Collection<ofbiz.resource.ofbiz.IOfbizQuickFix> getQuickFixes() {
						return null;
					}
				}, column, line, startIndex, stopIndex);
				return true;
			}
		});
	}
	
	public void addExpectedElement(org.eclipse.emf.ecore.EClass eClass, int[] ids) {
		if (!this.rememberExpectedElements) {
			return;
		}
		int terminalID = ids[0];
		int followSetID = ids[1];
		ofbiz.resource.ofbiz.IOfbizExpectedElement terminal = ofbiz.resource.ofbiz.grammar.OfbizFollowSetProvider.TERMINALS[terminalID];
		ofbiz.resource.ofbiz.mopp.OfbizContainedFeature[] containmentFeatures = new ofbiz.resource.ofbiz.mopp.OfbizContainedFeature[ids.length - 2];
		for (int i = 2; i < ids.length; i++) {
			containmentFeatures[i - 2] = ofbiz.resource.ofbiz.grammar.OfbizFollowSetProvider.LINKS[ids[i]];
		}
		ofbiz.resource.ofbiz.grammar.OfbizContainmentTrace containmentTrace = new ofbiz.resource.ofbiz.grammar.OfbizContainmentTrace(eClass, containmentFeatures);
		org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
		ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal expectedElement = new ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal(container, terminal, followSetID, containmentTrace);
		setPosition(expectedElement, input.index());
		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
		if (lastStartIncludingHidden >= 0 && lastStartIncludingHidden < startIncludingHiddenTokens && cursorOffset > startIncludingHiddenTokens) {
			// clear list of expected elements
			this.expectedElements.clear();
			this.expectedElementsIndexOfLastCompleteElement = 0;
		}
		lastStartIncludingHidden = startIncludingHiddenTokens;
		this.expectedElements.add(expectedElement);
	}
	
	protected void collectHiddenTokens(org.eclipse.emf.ecore.EObject element) {
	}
	
	protected void copyLocalizationInfos(final org.eclipse.emf.ecore.EObject source, final org.eclipse.emf.ecore.EObject target) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new ofbiz.resource.ofbiz.IOfbizCommand<ofbiz.resource.ofbiz.IOfbizTextResource>() {
			public boolean execute(ofbiz.resource.ofbiz.IOfbizTextResource resource) {
				ofbiz.resource.ofbiz.IOfbizLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				locationMap.setCharStart(target, locationMap.getCharStart(source));
				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
				locationMap.setColumn(target, locationMap.getColumn(source));
				locationMap.setLine(target, locationMap.getLine(source));
				return true;
			}
		});
	}
	
	protected void copyLocalizationInfos(final org.antlr.runtime3_4_0.CommonToken source, final org.eclipse.emf.ecore.EObject target) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new ofbiz.resource.ofbiz.IOfbizCommand<ofbiz.resource.ofbiz.IOfbizTextResource>() {
			public boolean execute(ofbiz.resource.ofbiz.IOfbizTextResource resource) {
				ofbiz.resource.ofbiz.IOfbizLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				if (source == null) {
					return true;
				}
				locationMap.setCharStart(target, source.getStartIndex());
				locationMap.setCharEnd(target, source.getStopIndex());
				locationMap.setColumn(target, source.getCharPositionInLine());
				locationMap.setLine(target, source.getLine());
				return true;
			}
		});
	}
	
	/**
	 * Sets the end character index and the last line for the given object in the
	 * location map.
	 */
	protected void setLocalizationEnd(java.util.Collection<ofbiz.resource.ofbiz.IOfbizCommand<ofbiz.resource.ofbiz.IOfbizTextResource>> postParseCommands , final org.eclipse.emf.ecore.EObject object, final int endChar, final int endLine) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new ofbiz.resource.ofbiz.IOfbizCommand<ofbiz.resource.ofbiz.IOfbizTextResource>() {
			public boolean execute(ofbiz.resource.ofbiz.IOfbizTextResource resource) {
				ofbiz.resource.ofbiz.IOfbizLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				locationMap.setCharEnd(object, endChar);
				locationMap.setLine(object, endLine);
				return true;
			}
		});
	}
	
	public ofbiz.resource.ofbiz.IOfbizTextParser createInstance(java.io.InputStream actualInputStream, String encoding) {
		try {
			if (encoding == null) {
				return new OfbizParser(new org.antlr.runtime3_4_0.CommonTokenStream(new OfbizLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream))));
			} else {
				return new OfbizParser(new org.antlr.runtime3_4_0.CommonTokenStream(new OfbizLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream, encoding))));
			}
		} catch (java.io.IOException e) {
			new ofbiz.resource.ofbiz.util.OfbizRuntimeUtil().logError("Error while creating parser.", e);
			return null;
		}
	}
	
	/**
	 * This default constructor is only used to call createInstance() on it.
	 */
	public OfbizParser() {
		super(null);
	}
	
	protected org.eclipse.emf.ecore.EObject doParse() throws org.antlr.runtime3_4_0.RecognitionException {
		this.lastPosition = 0;
		// required because the lexer class can not be subclassed
		((OfbizLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
		((OfbizLexer) getTokenStream().getTokenSource()).lexerExceptionsPosition = lexerExceptionsPosition;
		Object typeObject = getTypeObject();
		if (typeObject == null) {
			return start();
		} else if (typeObject instanceof org.eclipse.emf.ecore.EClass) {
			org.eclipse.emf.ecore.EClass type = (org.eclipse.emf.ecore.EClass) typeObject;
			if (type.getInstanceClass() == ofbiz.OfbizComponent.class) {
				return parse_ofbiz_OfbizComponent();
			}
			if (type.getInstanceClass() == ofbiz.EntityModel.class) {
				return parse_ofbiz_EntityModel();
			}
			if (type.getInstanceClass() == ofbiz.Entity.class) {
				return parse_ofbiz_Entity();
			}
			if (type.getInstanceClass() == ofbiz.Field.class) {
				return parse_ofbiz_Field();
			}
			if (type.getInstanceClass() == ofbiz.Relation.class) {
				return parse_ofbiz_Relation();
			}
			if (type.getInstanceClass() == ofbiz.KeyMap.class) {
				return parse_ofbiz_KeyMap();
			}
			if (type.getInstanceClass() == ofbiz.Services.class) {
				return parse_ofbiz_Services();
			}
			if (type.getInstanceClass() == ofbiz.Service.class) {
				return parse_ofbiz_Service();
			}
			if (type.getInstanceClass() == ofbiz.Attribute.class) {
				return parse_ofbiz_Attribute();
			}
			if (type.getInstanceClass() == ofbiz.RestService.class) {
				return parse_ofbiz_RestService();
			}
			if (type.getInstanceClass() == ofbiz.RestRequest.class) {
				return parse_ofbiz_RestRequest();
			}
			if (type.getInstanceClass() == ofbiz.RestParameter.class) {
				return parse_ofbiz_RestParameter();
			}
			if (type.getInstanceClass() == ofbiz.EntityResource.class) {
				return parse_ofbiz_EntityResource();
			}
		}
		throw new ofbiz.resource.ofbiz.mopp.OfbizUnexpectedContentTypeException(typeObject);
	}
	
	public int getMismatchedTokenRecoveryTries() {
		return mismatchedTokenRecoveryTries;
	}
	
	public Object getMissingSymbol(org.antlr.runtime3_4_0.IntStream arg0, org.antlr.runtime3_4_0.RecognitionException arg1, int arg2, org.antlr.runtime3_4_0.BitSet arg3) {
		mismatchedTokenRecoveryTries++;
		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
	}
	
	public Object getParseToIndexTypeObject() {
		return parseToIndexTypeObject;
	}
	
	protected Object getTypeObject() {
		Object typeObject = getParseToIndexTypeObject();
		if (typeObject != null) {
			return typeObject;
		}
		java.util.Map<?,?> options = getOptions();
		if (options != null) {
			typeObject = options.get(ofbiz.resource.ofbiz.IOfbizOptions.RESOURCE_CONTENT_TYPE);
		}
		return typeObject;
	}
	
	/**
	 * Implementation that calls {@link #doParse()} and handles the thrown
	 * RecognitionExceptions.
	 */
	public ofbiz.resource.ofbiz.IOfbizParseResult parse() {
		terminateParsing = false;
		postParseCommands = new java.util.ArrayList<ofbiz.resource.ofbiz.IOfbizCommand<ofbiz.resource.ofbiz.IOfbizTextResource>>();
		ofbiz.resource.ofbiz.mopp.OfbizParseResult parseResult = new ofbiz.resource.ofbiz.mopp.OfbizParseResult();
		try {
			org.eclipse.emf.ecore.EObject result =  doParse();
			if (lexerExceptions.isEmpty()) {
				parseResult.setRoot(result);
			}
		} catch (org.antlr.runtime3_4_0.RecognitionException re) {
			reportError(re);
		} catch (java.lang.IllegalArgumentException iae) {
			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
				// can be caused if a null is set on EMF models where not allowed. this will just
				// happen if other errors occurred before
			} else {
				iae.printStackTrace();
			}
		}
		for (org.antlr.runtime3_4_0.RecognitionException re : lexerExceptions) {
			reportLexicalError(re);
		}
		parseResult.getPostParseCommands().addAll(postParseCommands);
		return parseResult;
	}
	
	public java.util.List<ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal> parseToExpectedElements(org.eclipse.emf.ecore.EClass type, ofbiz.resource.ofbiz.IOfbizTextResource dummyResource, int cursorOffset) {
		this.rememberExpectedElements = true;
		this.parseToIndexTypeObject = type;
		this.cursorOffset = cursorOffset;
		this.lastStartIncludingHidden = -1;
		final org.antlr.runtime3_4_0.CommonTokenStream tokenStream = (org.antlr.runtime3_4_0.CommonTokenStream) getTokenStream();
		ofbiz.resource.ofbiz.IOfbizParseResult result = parse();
		for (org.eclipse.emf.ecore.EObject incompleteObject : incompleteObjects) {
			org.antlr.runtime3_4_0.Lexer lexer = (org.antlr.runtime3_4_0.Lexer) tokenStream.getTokenSource();
			int endChar = lexer.getCharIndex();
			int endLine = lexer.getLine();
			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
		}
		if (result != null) {
			org.eclipse.emf.ecore.EObject root = result.getRoot();
			if (root != null) {
				dummyResource.getContentsInternal().add(root);
			}
			for (ofbiz.resource.ofbiz.IOfbizCommand<ofbiz.resource.ofbiz.IOfbizTextResource> command : result.getPostParseCommands()) {
				command.execute(dummyResource);
			}
		}
		// remove all expected elements that were added after the last complete element
		expectedElements = expectedElements.subList(0, expectedElementsIndexOfLastCompleteElement + 1);
		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
		java.util.Set<ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal> currentFollowSet = new java.util.LinkedHashSet<ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal>();
		java.util.List<ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal> newFollowSet = new java.util.ArrayList<ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal>();
		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
			ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal expectedElementI = expectedElements.get(i);
			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
				currentFollowSet.add(expectedElementI);
			} else {
				break;
			}
		}
		int followSetID = 180;
		int i;
		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
			org.antlr.runtime3_4_0.CommonToken nextToken = (org.antlr.runtime3_4_0.CommonToken) tokenStream.get(i);
			if (nextToken.getType() < 0) {
				break;
			}
			if (nextToken.getChannel() == 99) {
				// hidden tokens do not reduce the follow set
			} else {
				// now that we have found the next visible token the position for that expected
				// terminals can be set
				for (ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal nextFollow : newFollowSet) {
					lastTokenIndex = 0;
					setPosition(nextFollow, i);
				}
				newFollowSet.clear();
				// normal tokens do reduce the follow set - only elements that match the token are
				// kept
				for (ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal nextFollow : currentFollowSet) {
					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
						// keep this one - it matches
						java.util.Collection<ofbiz.resource.ofbiz.util.OfbizPair<ofbiz.resource.ofbiz.IOfbizExpectedElement, ofbiz.resource.ofbiz.mopp.OfbizContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
						for (ofbiz.resource.ofbiz.util.OfbizPair<ofbiz.resource.ofbiz.IOfbizExpectedElement, ofbiz.resource.ofbiz.mopp.OfbizContainedFeature[]> newFollowerPair : newFollowers) {
							ofbiz.resource.ofbiz.IOfbizExpectedElement newFollower = newFollowerPair.getLeft();
							org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
							ofbiz.resource.ofbiz.grammar.OfbizContainmentTrace containmentTrace = new ofbiz.resource.ofbiz.grammar.OfbizContainmentTrace(null, newFollowerPair.getRight());
							ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal newFollowTerminal = new ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal(container, newFollower, followSetID, containmentTrace);
							newFollowSet.add(newFollowTerminal);
							expectedElements.add(newFollowTerminal);
						}
					}
				}
				currentFollowSet.clear();
				currentFollowSet.addAll(newFollowSet);
			}
			followSetID++;
		}
		// after the last token in the stream we must set the position for the elements
		// that were added during the last iteration of the loop
		for (ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal nextFollow : newFollowSet) {
			lastTokenIndex = 0;
			setPosition(nextFollow, i);
		}
		return this.expectedElements;
	}
	
	public void setPosition(ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal expectedElement, int tokenIndex) {
		int currentIndex = Math.max(0, tokenIndex);
		for (int index = lastTokenIndex; index < currentIndex; index++) {
			if (index >= input.size()) {
				break;
			}
			org.antlr.runtime3_4_0.CommonToken tokenAtIndex = (org.antlr.runtime3_4_0.CommonToken) input.get(index);
			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			}
		}
		lastTokenIndex = Math.max(0, currentIndex);
		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
	}
	
	public Object recoverFromMismatchedToken(org.antlr.runtime3_4_0.IntStream input, int ttype, org.antlr.runtime3_4_0.BitSet follow) throws org.antlr.runtime3_4_0.RecognitionException {
		if (!rememberExpectedElements) {
			return super.recoverFromMismatchedToken(input, ttype, follow);
		} else {
			return null;
		}
	}
	
	/**
	 * Translates errors thrown by the parser into human readable messages.
	 */
	public void reportError(final org.antlr.runtime3_4_0.RecognitionException e)  {
		String message = e.getMessage();
		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
			String expectedTokenName = formatTokenName(mte.expecting);
			String actualTokenName = formatTokenName(e.token.getType());
			message = "Syntax error on token \"" + e.token.getText() + " (" + actualTokenName + ")\", \"" + expectedTokenName + "\" expected";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedTreeNodeException) {
			org.antlr.runtime3_4_0.MismatchedTreeNodeException mtne = (org.antlr.runtime3_4_0.MismatchedTreeNodeException) e;
			String expectedTokenName = formatTokenName(mtne.expecting);
			message = "mismatched tree node: " + "xxx" + "; tokenName " + expectedTokenName;
		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
			message = "Syntax error on token \"" + e.token.getText() + "\", check following tokens";
		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
			message = "Syntax error on token \"" + e.token.getText() + "\", delete this token";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
			message = "mismatched token: " + e.token + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
			message = "mismatched token: " +  e.token + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
			message = "rule " + fpe.ruleName + " failed predicate: {" +  fpe.predicateText + "}?";
		}
		// the resource may be null if the parser is used for code completion
		final String finalMessage = message;
		if (e.token instanceof org.antlr.runtime3_4_0.CommonToken) {
			final org.antlr.runtime3_4_0.CommonToken ct = (org.antlr.runtime3_4_0.CommonToken) e.token;
			addErrorToResource(finalMessage, ct.getCharPositionInLine(), ct.getLine(), ct.getStartIndex(), ct.getStopIndex());
		} else {
			addErrorToResource(finalMessage, e.token.getCharPositionInLine(), e.token.getLine(), 1, 5);
		}
	}
	
	/**
	 * Translates errors thrown by the lexer into human readable messages.
	 */
	public void reportLexicalError(final org.antlr.runtime3_4_0.RecognitionException e)  {
		String message = "";
		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
			message = "Syntax error on token \"" + ((char) e.c) + "\", \"" + (char) mte.expecting + "\" expected";
		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
			message = "Syntax error on token \"" + ((char) e.c) + "\", delete this token";
		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
			org.antlr.runtime3_4_0.EarlyExitException eee = (org.antlr.runtime3_4_0.EarlyExitException) e;
			message = "required (...)+ loop (decision=" + eee.decisionNumber + ") did not match anything; on line " + e.line + ":" + e.charPositionInLine + " char=" + ((char) e.c) + "'";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedRangeException) {
			org.antlr.runtime3_4_0.MismatchedRangeException mre = (org.antlr.runtime3_4_0.MismatchedRangeException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set '" + (char) mre.a + "'..'" + (char) mre.b + "'";
		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
			message = "rule " + fpe.ruleName + " failed predicate: {" + fpe.predicateText + "}?";
		}
		addErrorToResource(message, e.charPositionInLine, e.line, lexerExceptionsPosition.get(lexerExceptions.indexOf(e)), lexerExceptionsPosition.get(lexerExceptions.indexOf(e)));
	}
	
	private void startIncompleteElement(Object object) {
		if (object instanceof org.eclipse.emf.ecore.EObject) {
			this.incompleteObjects.add((org.eclipse.emf.ecore.EObject) object);
		}
	}
	
	private void completedElement(Object object, boolean isContainment) {
		if (isContainment && !this.incompleteObjects.isEmpty()) {
			boolean exists = this.incompleteObjects.remove(object);
			if (!exists) {
			}
		}
		if (object instanceof org.eclipse.emf.ecore.EObject) {
			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
		}
	}
	
	private org.eclipse.emf.ecore.EObject getLastIncompleteElement() {
		if (incompleteObjects.isEmpty()) {
			return null;
		}
		return incompleteObjects.get(incompleteObjects.size() - 1);
	}
	
}

start returns [ org.eclipse.emf.ecore.EObject element = null]
:
	{
		// follow set for start rule(s)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[0]);
		expectedElementsIndexOfLastCompleteElement = 0;
	}
	(
		c0 = parse_ofbiz_OfbizComponent{ element = c0; }
	)
	EOF	{
		retrieveLayoutInformation(element, null, null, false);
	}
	
;

parse_ofbiz_OfbizComponent returns [ofbiz.OfbizComponent element = null]
@init{
}
:
	a0 = 'OfbizComponent' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[1]);
	}
	
	a1 = '{' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[2]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[3]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[4]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[5]);
	}
	
	(
		(
			a2 = 'entitymodel' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_2_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[6]);
			}
			
			a3 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_2_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getOfbizComponent(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[7]);
			}
			
			(
				a4_0 = parse_ofbiz_EntityModel				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
						startIncompleteElement(element);
					}
					if (a4_0 != null) {
						if (a4_0 != null) {
							Object value = a4_0;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.OFBIZ_COMPONENT__ENTITYMODEL), value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_2_0_0_2, a4_0, true);
						copyLocalizationInfos(a4_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[8]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[9]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[10]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[11]);
			}
			
			
			|			a5 = 'services' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_2_0_1_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[12]);
			}
			
			a6 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_2_0_1_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getOfbizComponent(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[13]);
			}
			
			(
				a7_0 = parse_ofbiz_Services				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
						startIncompleteElement(element);
					}
					if (a7_0 != null) {
						if (a7_0 != null) {
							Object value = a7_0;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.OFBIZ_COMPONENT__SERVICES), value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_2_0_1_2, a7_0, true);
						copyLocalizationInfos(a7_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[14]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[15]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[16]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[17]);
			}
			
			
			|			a8 = 'rest' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_2_0_2_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[18]);
			}
			
			a9 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_2_0_2_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getOfbizComponent(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[19]);
			}
			
			(
				a10_0 = parse_ofbiz_RestService				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
						startIncompleteElement(element);
					}
					if (a10_0 != null) {
						if (a10_0 != null) {
							Object value = a10_0;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.OFBIZ_COMPONENT__REST), value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_2_0_2_2, a10_0, true);
						copyLocalizationInfos(a10_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[20]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[21]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[22]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[23]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[24]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[25]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[26]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[27]);
	}
	
	a11 = '}' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
	}
	{
		// expected elements (follow set)
	}
	
;

parse_ofbiz_EntityModel returns [ofbiz.EntityModel element = null]
@init{
}
:
	a0 = 'EntityModel' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[28]);
	}
	
	a1 = '{' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[29]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[30]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[31]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[32]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[33]);
	}
	
	(
		(
			a2 = 'title' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[34]);
			}
			
			a3 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[35]);
			}
			
			(
				a4 = QUOTED_34_34				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
						startIncompleteElement(element);
					}
					if (a4 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__TITLE), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__TITLE), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_0_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[36]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[37]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[38]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[39]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[40]);
			}
			
			
			|			a5 = 'description' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_1_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[41]);
			}
			
			a6 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_1_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[42]);
			}
			
			(
				a7 = QUOTED_34_34				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
						startIncompleteElement(element);
					}
					if (a7 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__DESCRIPTION), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__DESCRIPTION), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_1_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[43]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[44]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[45]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[46]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[47]);
			}
			
			
			|			a8 = 'version' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_2_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[48]);
			}
			
			a9 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_2_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[49]);
			}
			
			(
				a10 = QUOTED_34_34				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
						startIncompleteElement(element);
					}
					if (a10 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a10.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__VERSION), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a10).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a10).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a10).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a10).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__VERSION), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_2_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a10, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[50]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[51]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[52]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[53]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[54]);
			}
			
			
			|			a11 = 'entities' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_3_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[55]);
			}
			
			a12 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_3_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getEntityModel(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[56]);
			}
			
			(
				a13_0 = parse_ofbiz_Entity				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
						startIncompleteElement(element);
					}
					if (a13_0 != null) {
						if (a13_0 != null) {
							Object value = a13_0;
							addObjectToList(element, ofbiz.OfbizPackage.ENTITY_MODEL__ENTITIES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_3_2, a13_0, true);
						copyLocalizationInfos(a13_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[57]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[58]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[59]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[60]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[61]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[62]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[63]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[64]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[65]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[66]);
	}
	
	a14 = '}' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a14, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[67]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[68]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[69]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[70]);
	}
	
;

parse_ofbiz_Entity returns [ofbiz.Entity element = null]
@init{
}
:
	a0 = 'Entity' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[71]);
	}
	
	a1 = '{' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[72]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[73]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[74]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[75]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[76]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[77]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[78]);
	}
	
	(
		(
			a2 = 'entityName' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[79]);
			}
			
			a3 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[80]);
			}
			
			(
				a4 = QUOTED_34_34				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
						startIncompleteElement(element);
					}
					if (a4 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__ENTITY_NAME), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__ENTITY_NAME), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_0_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[81]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[82]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[83]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[84]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[85]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[86]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[87]);
			}
			
			
			|			a5 = 'packageName' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_1_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[88]);
			}
			
			a6 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_1_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[89]);
			}
			
			(
				a7 = QUOTED_34_34				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
						startIncompleteElement(element);
					}
					if (a7 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__PACKAGE_NAME), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__PACKAGE_NAME), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_1_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[90]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[91]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[92]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[93]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[94]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[95]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[96]);
			}
			
			
			|			a8 = 'title' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_2_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[97]);
			}
			
			a9 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_2_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[98]);
			}
			
			(
				a10 = QUOTED_34_34				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
						startIncompleteElement(element);
					}
					if (a10 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a10.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__TITLE), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a10).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a10).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a10).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a10).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__TITLE), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_2_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a10, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[99]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[100]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[101]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[102]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[103]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[104]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[105]);
			}
			
			
			|			a11 = 'primKey' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_3_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[106]);
			}
			
			a12 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_3_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[107]);
			}
			
			(
				a13 = TEXT				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
						startIncompleteElement(element);
					}
					if (a13 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a13.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__PRIM_KEY), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a13).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a13).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a13).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a13).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						ofbiz.Field proxy = ofbiz.OfbizFactory.eINSTANCE.createField();
						collectHiddenTokens(element);
						registerContextDependentProxy(new ofbiz.resource.ofbiz.mopp.OfbizContextDependentURIFragmentFactory<ofbiz.Entity, ofbiz.Field>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEntityPrimKeyReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__PRIM_KEY), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							addObjectToList(element, ofbiz.OfbizPackage.ENTITY__PRIM_KEY, value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_3_2, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a13, element);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a13, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[108]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[109]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[110]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[111]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[112]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[113]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[114]);
			}
			
			
			|			a14 = 'fields' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_4_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a14, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[115]);
			}
			
			a15 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_4_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a15, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getEntity(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[116]);
			}
			
			(
				a16_0 = parse_ofbiz_Field				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
						startIncompleteElement(element);
					}
					if (a16_0 != null) {
						if (a16_0 != null) {
							Object value = a16_0;
							addObjectToList(element, ofbiz.OfbizPackage.ENTITY__FIELDS, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_4_2, a16_0, true);
						copyLocalizationInfos(a16_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[117]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[118]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[119]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[120]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[121]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[122]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[123]);
			}
			
			
			|			a17 = 'relations' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_5_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a17, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[124]);
			}
			
			a18 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_5_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a18, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getEntity(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[125]);
			}
			
			(
				a19_0 = parse_ofbiz_Relation				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
						startIncompleteElement(element);
					}
					if (a19_0 != null) {
						if (a19_0 != null) {
							Object value = a19_0;
							addObjectToList(element, ofbiz.OfbizPackage.ENTITY__RELATIONS, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_5_2, a19_0, true);
						copyLocalizationInfos(a19_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[126]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[127]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[128]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[129]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[130]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[131]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[132]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[133]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[134]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[135]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[136]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[137]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[138]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[139]);
	}
	
	a20 = '}' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a20, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[140]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[141]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[142]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[143]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[144]);
	}
	
;

parse_ofbiz_Field returns [ofbiz.Field element = null]
@init{
}
:
	a0 = 'Field' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createField();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[145]);
	}
	
	a1 = '{' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createField();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[146]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[147]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[148]);
	}
	
	(
		(
			a2 = 'name' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createField();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[149]);
			}
			
			a3 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createField();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[150]);
			}
			
			(
				a4 = QUOTED_34_34				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createField();
						startIncompleteElement(element);
					}
					if (a4 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__NAME), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__NAME), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_0_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[151]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[152]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[153]);
			}
			
			
			|			a5 = 'type' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createField();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[154]);
			}
			
			a6 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createField();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[155]);
			}
			
			(
				(
					a7 = 'object' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.OBJECT_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a8 = 'blob' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.BLOB_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a9 = 'byte-array' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.BYTE_ARRAY_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a10 = 'date-time' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.DATE_TIME_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a11 = 'date' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.DATE_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a12 = 'time' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.TIME_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a13 = 'currency-ammount' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a13, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.CURRENCY_AMMOUNT_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a14 = 'currency-precise' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a14, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.CURRENCY_PRECISE_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a15 = 'fixed-point' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a15, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.FIXED_POINT_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a16 = 'floating-point' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a16, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.FLOATING_POINT_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a17 = 'numeric' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a17, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.NUMERIC_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a18 = 'id' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a18, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.ID_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a19 = 'id-long' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a19, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.ID_LONG_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a20 = 'id-vlong' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a20, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.ID_VLONG_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a21 = 'indicator' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a21, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.INDICATOR_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a22 = 'very-short' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a22, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.VERY_SHORT_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a23 = 'short-varchar' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a23, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.SHORT_VARCHAR_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a24 = 'long-varchar' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a24, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.LONG_VARCHAR_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a25 = 'very-long' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a25, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.VERY_LONG_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a26 = 'comment' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a26, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.COMMENT_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a27 = 'description' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a27, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.DESCRIPTION_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a28 = 'name' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a28, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.NAME_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a29 = 'value' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a29, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.VALUE_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a30 = 'credit-card-number' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a30, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.CREDIT_CARD_NUMBER_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a31 = 'credit-card-date' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a31, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.CREDIT_CARD_DATE_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a32 = 'email' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a32, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.EMAIL_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a33 = 'url' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a33, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.URL_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a34 = 'id-ne' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a34, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.ID_NE_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a35 = 'id-long-ne' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a35, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.ID_LONG_NE_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a36 = 'id-vlong-ne' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a36, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.ID_VLONG_NE_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
					|					a37 = 'tel-number' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createField();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a37, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.TEL_NUMBER_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
						completedElement(value, false);
					}
				)
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[156]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[157]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[158]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[159]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[160]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[161]);
	}
	
	a40 = '}' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createField();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a40, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[162]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[163]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[164]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[165]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[166]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[167]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[168]);
	}
	
;

parse_ofbiz_Relation returns [ofbiz.Relation element = null]
@init{
}
:
	a0 = 'Relation' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[169]);
	}
	
	a1 = '{' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[170]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[171]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[172]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[173]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[174]);
	}
	
	(
		(
			a2 = 'fkName' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[175]);
			}
			
			a3 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[176]);
			}
			
			(
				a4 = QUOTED_34_34				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
						startIncompleteElement(element);
					}
					if (a4 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__FK_NAME), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__FK_NAME), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_0_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[177]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[178]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[179]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[180]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[181]);
			}
			
			
			|			a5 = 'type' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_1_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[182]);
			}
			
			a6 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_1_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[183]);
			}
			
			(
				(
					a7 = 'ONE' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getRelationType().getEEnumLiteral(ofbiz.RelationType.ONE_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__TYPE), value);
						completedElement(value, false);
					}
					|					a8 = 'MANY' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getRelationType().getEEnumLiteral(ofbiz.RelationType.MANY_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__TYPE), value);
						completedElement(value, false);
					}
				)
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[184]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[185]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[186]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[187]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[188]);
			}
			
			
			|			a11 = 'keyMap' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_2_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[189]);
			}
			
			a12 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_2_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getRelation(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[190]);
			}
			
			(
				a13_0 = parse_ofbiz_KeyMap				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
						startIncompleteElement(element);
					}
					if (a13_0 != null) {
						if (a13_0 != null) {
							Object value = a13_0;
							addObjectToList(element, ofbiz.OfbizPackage.RELATION__KEY_MAP, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_2_2, a13_0, true);
						copyLocalizationInfos(a13_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[191]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[192]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[193]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[194]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[195]);
			}
			
			
			|			a14 = 'related' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_3_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a14, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[196]);
			}
			
			a15 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_3_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a15, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[197]);
			}
			
			(
				a16 = TEXT				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
						startIncompleteElement(element);
					}
					if (a16 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a16.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__RELATED), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a16).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a16).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a16).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a16).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						ofbiz.Entity proxy = ofbiz.OfbizFactory.eINSTANCE.createEntity();
						collectHiddenTokens(element);
						registerContextDependentProxy(new ofbiz.resource.ofbiz.mopp.OfbizContextDependentURIFragmentFactory<ofbiz.Relation, ofbiz.Entity>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getRelationRelatedReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__RELATED), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__RELATED), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_3_2, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a16, element);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a16, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[198]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[199]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[200]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[201]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[202]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[203]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[204]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[205]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[206]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[207]);
	}
	
	a17 = '}' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a17, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[208]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[209]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[210]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[211]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[212]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[213]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[214]);
	}
	
;

parse_ofbiz_KeyMap returns [ofbiz.KeyMap element = null]
@init{
}
:
	a0 = 'KeyMap' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createKeyMap();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_5_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[215]);
	}
	
	a1 = '{' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createKeyMap();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_5_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[216]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[217]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[218]);
	}
	
	(
		(
			a2 = 'origin' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createKeyMap();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_5_0_0_2_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[219]);
			}
			
			a3 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createKeyMap();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_5_0_0_2_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[220]);
			}
			
			(
				a4 = TEXT				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createKeyMap();
						startIncompleteElement(element);
					}
					if (a4 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__ORIGIN), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						ofbiz.Field proxy = ofbiz.OfbizFactory.eINSTANCE.createField();
						collectHiddenTokens(element);
						registerContextDependentProxy(new ofbiz.resource.ofbiz.mopp.OfbizContextDependentURIFragmentFactory<ofbiz.KeyMap, ofbiz.Field>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getKeyMapOriginReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__ORIGIN), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__ORIGIN), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_5_0_0_2_0_0_2, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[221]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[222]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[223]);
			}
			
			
			|			a5 = 'related' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createKeyMap();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_5_0_0_2_0_1_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[224]);
			}
			
			a6 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createKeyMap();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_5_0_0_2_0_1_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[225]);
			}
			
			(
				a7 = TEXT				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createKeyMap();
						startIncompleteElement(element);
					}
					if (a7 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__RELATED), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						ofbiz.Field proxy = ofbiz.OfbizFactory.eINSTANCE.createField();
						collectHiddenTokens(element);
						registerContextDependentProxy(new ofbiz.resource.ofbiz.mopp.OfbizContextDependentURIFragmentFactory<ofbiz.KeyMap, ofbiz.Field>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getKeyMapRelatedReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__RELATED), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__RELATED), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_5_0_0_2_0_1_2, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[226]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[227]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[228]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[229]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[230]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[231]);
	}
	
	a8 = '}' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createKeyMap();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_5_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[232]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[233]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[234]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[235]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[236]);
	}
	
;

parse_ofbiz_Services returns [ofbiz.Services element = null]
@init{
}
:
	a0 = 'Services' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createServices();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[237]);
	}
	
	a1 = '{' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createServices();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[238]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[239]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[240]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[241]);
	}
	
	(
		(
			a2 = 'description' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createServices();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_2_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[242]);
			}
			
			a3 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createServices();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_2_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[243]);
			}
			
			(
				a4 = QUOTED_34_34				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createServices();
						startIncompleteElement(element);
					}
					if (a4 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICES__DESCRIPTION), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICES__DESCRIPTION), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_2_0_0_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[244]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[245]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[246]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[247]);
			}
			
			
			|			a5 = 'version' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createServices();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_2_0_1_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[248]);
			}
			
			a6 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createServices();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_2_0_1_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[249]);
			}
			
			(
				a7 = QUOTED_34_34				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createServices();
						startIncompleteElement(element);
					}
					if (a7 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICES__VERSION), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICES__VERSION), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_2_0_1_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[250]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[251]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[252]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[253]);
			}
			
			
			|			a8 = 'services' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createServices();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_2_0_2_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[254]);
			}
			
			a9 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createServices();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_2_0_2_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getServices(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[255]);
			}
			
			(
				a10_0 = parse_ofbiz_Service				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createServices();
						startIncompleteElement(element);
					}
					if (a10_0 != null) {
						if (a10_0 != null) {
							Object value = a10_0;
							addObjectToList(element, ofbiz.OfbizPackage.SERVICES__SERVICES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_2_0_2_2, a10_0, true);
						copyLocalizationInfos(a10_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[256]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[257]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[258]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[259]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[260]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[261]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[262]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[263]);
	}
	
	a11 = '}' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createServices();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[264]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[265]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[266]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[267]);
	}
	
;

parse_ofbiz_Service returns [ofbiz.Service element = null]
@init{
}
:
	a0 = 'Service' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createService();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[268]);
	}
	
	a1 = '{' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createService();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[269]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[270]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[271]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[272]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[273]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[274]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[275]);
	}
	
	(
		(
			a2 = 'name' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createService();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[276]);
			}
			
			a3 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createService();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[277]);
			}
			
			(
				a4 = QUOTED_34_34				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createService();
						startIncompleteElement(element);
					}
					if (a4 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__NAME), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__NAME), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_0_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[278]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[279]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[280]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[281]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[282]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[283]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[284]);
			}
			
			
			|			a5 = 'engine' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createService();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[285]);
			}
			
			a6 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createService();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[286]);
			}
			
			(
				(
					a7 = 'entity-auto' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createService();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.ENTITY_AUTO_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
						completedElement(value, false);
					}
					|					a8 = 'group' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createService();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.GROUP_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
						completedElement(value, false);
					}
					|					a9 = 'interface' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createService();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.INTERFACE_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
						completedElement(value, false);
					}
					|					a10 = 'java' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createService();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.JAVA_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
						completedElement(value, false);
					}
					|					a11 = 'simple' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createService();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.SIMPLE_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
						completedElement(value, false);
					}
					|					a12 = 'script' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createService();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.SCRIPT_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
						completedElement(value, false);
					}
					|					a13 = 'groovy' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createService();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a13, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.GROOVY_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
						completedElement(value, false);
					}
					|					a14 = 'jacl' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createService();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a14, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.JACL_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
						completedElement(value, false);
					}
					|					a15 = 'javascript' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createService();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a15, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.JAVASCRIPT_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
						completedElement(value, false);
					}
					|					a16 = 'jpython' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createService();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a16, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.JPYTHON_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
						completedElement(value, false);
					}
					|					a17 = 'route' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createService();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a17, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.ROUTE_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
						completedElement(value, false);
					}
					|					a18 = 'http' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createService();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a18, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.HTTP_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
						completedElement(value, false);
					}
					|					a19 = 'jms' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createService();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a19, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.JMS_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
						completedElement(value, false);
					}
					|					a20 = 'rmi' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createService();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a20, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.RMI_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
						completedElement(value, false);
					}
					|					a21 = 'soap' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createService();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a21, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.SOAP_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
						completedElement(value, false);
					}
				)
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[287]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[288]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[289]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[290]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[291]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[292]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[293]);
			}
			
			
			|			a24 = 'location' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createService();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_2_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a24, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[294]);
			}
			
			a25 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createService();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_2_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a25, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[295]);
			}
			
			(
				a26 = QUOTED_34_34				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createService();
						startIncompleteElement(element);
					}
					if (a26 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a26.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__LOCATION), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a26).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a26).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a26).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a26).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__LOCATION), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_2_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a26, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[296]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[297]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[298]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[299]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[300]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[301]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[302]);
			}
			
			
			|			a27 = 'invoke' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createService();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_3_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a27, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[303]);
			}
			
			a28 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createService();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_3_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a28, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[304]);
			}
			
			(
				a29 = QUOTED_34_34				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createService();
						startIncompleteElement(element);
					}
					if (a29 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a29.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__INVOKE), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a29).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a29).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a29).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a29).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__INVOKE), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_3_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a29, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[305]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[306]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[307]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[308]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[309]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[310]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[311]);
			}
			
			
			|			a30 = 'description' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createService();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_4_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a30, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[312]);
			}
			
			a31 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createService();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_4_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a31, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[313]);
			}
			
			(
				a32 = QUOTED_34_34				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createService();
						startIncompleteElement(element);
					}
					if (a32 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a32.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__DESCRIPTION), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a32).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a32).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a32).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a32).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__DESCRIPTION), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_4_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a32, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[314]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[315]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[316]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[317]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[318]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[319]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[320]);
			}
			
			
			|			a33 = 'attributes' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createService();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_5_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a33, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[321]);
			}
			
			a34 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createService();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_5_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a34, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getService(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[322]);
				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getService(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[323]);
			}
			
			(
				a35_0 = parse_ofbiz_Attribute				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createService();
						startIncompleteElement(element);
					}
					if (a35_0 != null) {
						if (a35_0 != null) {
							Object value = a35_0;
							addObjectToList(element, ofbiz.OfbizPackage.SERVICE__ATTRIBUTES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_5_2, a35_0, true);
						copyLocalizationInfos(a35_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[324]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[325]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[326]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[327]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[328]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[329]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[330]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[331]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[332]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[333]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[334]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[335]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[336]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[337]);
	}
	
	a36 = '}' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createService();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a36, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[338]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[339]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[340]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[341]);
	}
	
;

parse_ofbiz_Attribute returns [ofbiz.Attribute element = null]
@init{
}
:
	(
		(
			a0 = 'optional' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
					startIncompleteElement(element);
					// initialize boolean attribute
					{
						Object value = false;
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
					}
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_0, true, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
				// set value of boolean attribute
				Object value = true;
				element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
				completedElement(value, false);
			}
		)?	)
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[342]);
	}
	
	a3 = 'Attribute' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
			startIncompleteElement(element);
			// initialize boolean attribute
			{
				Object value = false;
				element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
			}
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[343]);
	}
	
	a4 = '{' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
			startIncompleteElement(element);
			// initialize boolean attribute
			{
				Object value = false;
				element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
			}
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[344]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[345]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[346]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[347]);
	}
	
	(
		(
			a5 = 'name' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
					startIncompleteElement(element);
					// initialize boolean attribute
					{
						Object value = false;
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
					}
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[348]);
			}
			
			a6 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
					startIncompleteElement(element);
					// initialize boolean attribute
					{
						Object value = false;
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
					}
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[349]);
			}
			
			(
				a7 = QUOTED_34_34				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
						startIncompleteElement(element);
						// initialize boolean attribute
						{
							Object value = false;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
						}
					}
					if (a7 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__NAME), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__NAME), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_0_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[350]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[351]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[352]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[353]);
			}
			
			
			|			a8 = 'type' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
					startIncompleteElement(element);
					// initialize boolean attribute
					{
						Object value = false;
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
					}
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_1_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[354]);
			}
			
			a9 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
					startIncompleteElement(element);
					// initialize boolean attribute
					{
						Object value = false;
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
					}
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_1_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[355]);
			}
			
			(
				(
					a10 = 'String' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
							startIncompleteElement(element);
							// initialize boolean attribute
							{
								Object value = false;
								element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
							}
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceDatatype().getEEnumLiteral(ofbiz.ServiceDatatype.STRING_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__TYPE), value);
						completedElement(value, false);
					}
					|					a11 = 'Boolean' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
							startIncompleteElement(element);
							// initialize boolean attribute
							{
								Object value = false;
								element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
							}
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceDatatype().getEEnumLiteral(ofbiz.ServiceDatatype.BOOLEAN_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__TYPE), value);
						completedElement(value, false);
					}
					|					a12 = 'BigDecimal' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
							startIncompleteElement(element);
							// initialize boolean attribute
							{
								Object value = false;
								element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
							}
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceDatatype().getEEnumLiteral(ofbiz.ServiceDatatype.BIG_DECIMAL_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__TYPE), value);
						completedElement(value, false);
					}
					|					a13 = 'Timestamp' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
							startIncompleteElement(element);
							// initialize boolean attribute
							{
								Object value = false;
								element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
							}
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a13, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceDatatype().getEEnumLiteral(ofbiz.ServiceDatatype.TIMESTAMP_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__TYPE), value);
						completedElement(value, false);
					}
					|					a14 = 'List' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
							startIncompleteElement(element);
							// initialize boolean attribute
							{
								Object value = false;
								element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
							}
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a14, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceDatatype().getEEnumLiteral(ofbiz.ServiceDatatype.LIST_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__TYPE), value);
						completedElement(value, false);
					}
					|					a15 = 'Map' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
							startIncompleteElement(element);
							// initialize boolean attribute
							{
								Object value = false;
								element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
							}
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_1_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a15, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceDatatype().getEEnumLiteral(ofbiz.ServiceDatatype.MAP_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__TYPE), value);
						completedElement(value, false);
					}
				)
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[356]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[357]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[358]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[359]);
			}
			
			
			|			a18 = 'mode' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
					startIncompleteElement(element);
					// initialize boolean attribute
					{
						Object value = false;
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
					}
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_2_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a18, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[360]);
			}
			
			a19 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
					startIncompleteElement(element);
					// initialize boolean attribute
					{
						Object value = false;
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
					}
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_2_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a19, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[361]);
			}
			
			(
				(
					a20 = 'IN' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
							startIncompleteElement(element);
							// initialize boolean attribute
							{
								Object value = false;
								element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
							}
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_2_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a20, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getAttributeMode().getEEnumLiteral(ofbiz.AttributeMode.IN_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__MODE), value);
						completedElement(value, false);
					}
					|					a21 = 'OUT' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
							startIncompleteElement(element);
							// initialize boolean attribute
							{
								Object value = false;
								element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
							}
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_2_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a21, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getAttributeMode().getEEnumLiteral(ofbiz.AttributeMode.OUT_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__MODE), value);
						completedElement(value, false);
					}
				)
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[362]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[363]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[364]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[365]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[366]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[367]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[368]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[369]);
	}
	
	a24 = '}' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
			startIncompleteElement(element);
			// initialize boolean attribute
			{
				Object value = false;
				element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
			}
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a24, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[370]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[371]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[372]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[373]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[374]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[375]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[376]);
	}
	
;

parse_ofbiz_RestService returns [ofbiz.RestService element = null]
@init{
}
:
	a0 = 'RestService' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[377]);
	}
	
	a1 = '{' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[378]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[379]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[380]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[381]);
	}
	
	(
		(
			a2 = 'name' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_2_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[382]);
			}
			
			a3 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_2_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[383]);
			}
			
			(
				a4 = QUOTED_34_34				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
						startIncompleteElement(element);
					}
					if (a4 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_SERVICE__NAME), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_SERVICE__NAME), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_2_0_0_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[384]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[385]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[386]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[387]);
			}
			
			
			|			a5 = 'version' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_2_0_1_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[388]);
			}
			
			a6 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_2_0_1_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[389]);
			}
			
			(
				a7 = QUOTED_34_34				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
						startIncompleteElement(element);
					}
					if (a7 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_SERVICE__VERSION), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_SERVICE__VERSION), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_2_0_1_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[390]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[391]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[392]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[393]);
			}
			
			
			|			a8 = 'resources' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_2_0_2_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[394]);
			}
			
			a9 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_2_0_2_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getRestService(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[395]);
			}
			
			(
				a10_0 = parse_ofbiz_EntityResource				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
						startIncompleteElement(element);
					}
					if (a10_0 != null) {
						if (a10_0 != null) {
							Object value = a10_0;
							addObjectToList(element, ofbiz.OfbizPackage.REST_SERVICE__RESOURCES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_2_0_2_2, a10_0, true);
						copyLocalizationInfos(a10_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[396]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[397]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[398]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[399]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[400]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[401]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[402]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[403]);
	}
	
	a11 = '}' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[404]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[405]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[406]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[407]);
	}
	
;

parse_ofbiz_RestRequest returns [ofbiz.RestRequest element = null]
@init{
}
:
	a0 = 'RestRequest' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[408]);
	}
	
	a1 = '{' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[409]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[410]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[411]);
	}
	
	(
		(
			a2 = 'action' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_2_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[412]);
			}
			
			a3 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_2_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[413]);
			}
			
			(
				(
					a4 = 'GET' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_2_0_0_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getRestMethod().getEEnumLiteral(ofbiz.RestMethod.GET_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_REQUEST__ACTION), value);
						completedElement(value, false);
					}
					|					a5 = 'PUT' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_2_0_0_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getRestMethod().getEEnumLiteral(ofbiz.RestMethod.PUT_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_REQUEST__ACTION), value);
						completedElement(value, false);
					}
					|					a6 = 'POST' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_2_0_0_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getRestMethod().getEEnumLiteral(ofbiz.RestMethod.POST_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_REQUEST__ACTION), value);
						completedElement(value, false);
					}
					|					a7 = 'DELETE' {
						if (element == null) {
							element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_2_0_0_2, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
						// set value of enumeration attribute
						Object value = ofbiz.OfbizPackage.eINSTANCE.getRestMethod().getEEnumLiteral(ofbiz.RestMethod.DELETE_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_REQUEST__ACTION), value);
						completedElement(value, false);
					}
				)
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[414]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[415]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[416]);
			}
			
			
			|			a10 = 'parameters' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_2_0_1_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[417]);
			}
			
			a11 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_2_0_1_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getRestRequest(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[418]);
			}
			
			(
				a12_0 = parse_ofbiz_RestParameter				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
						startIncompleteElement(element);
					}
					if (a12_0 != null) {
						if (a12_0 != null) {
							Object value = a12_0;
							addObjectToList(element, ofbiz.OfbizPackage.REST_REQUEST__PARAMETERS, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_2_0_1_2, a12_0, true);
						copyLocalizationInfos(a12_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[419]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[420]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[421]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[422]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[423]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[424]);
	}
	
	a13 = '}' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a13, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[425]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[426]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[427]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[428]);
	}
	
;

parse_ofbiz_RestParameter returns [ofbiz.RestParameter element = null]
@init{
}
:
	a0 = 'RestParameter' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createRestParameter();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_11_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[429]);
	}
	
	a1 = '{' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createRestParameter();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_11_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[430]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[431]);
	}
	
	(
		(
			a2 = 'name' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createRestParameter();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_11_0_0_2_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[432]);
			}
			
			a3 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createRestParameter();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_11_0_0_2_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[433]);
			}
			
			(
				a4 = QUOTED_34_34				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createRestParameter();
						startIncompleteElement(element);
					}
					if (a4 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_PARAMETER__NAME), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_PARAMETER__NAME), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_11_0_0_2_0_0_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[434]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[435]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[436]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[437]);
	}
	
	a5 = '}' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createRestParameter();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_11_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[438]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[439]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[440]);
	}
	
;

parse_ofbiz_EntityResource returns [ofbiz.EntityResource element = null]
@init{
}
:
	a0 = 'EntityResource' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[441]);
	}
	
	a1 = '{' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[442]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[443]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[444]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[445]);
	}
	
	(
		(
			a2 = 'entity' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_2_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[446]);
			}
			
			a3 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_2_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[447]);
			}
			
			(
				a4 = TEXT				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
						startIncompleteElement(element);
					}
					if (a4 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__ENTITY), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						ofbiz.Entity proxy = ofbiz.OfbizFactory.eINSTANCE.createEntity();
						collectHiddenTokens(element);
						registerContextDependentProxy(new ofbiz.resource.ofbiz.mopp.OfbizContextDependentURIFragmentFactory<ofbiz.EntityResource, ofbiz.Entity>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEntityResourceEntityReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__ENTITY), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__ENTITY), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_2_0_0_2, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[448]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[449]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[450]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[451]);
			}
			
			
			|			a5 = 'requests' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_2_0_1_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[452]);
			}
			
			a6 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_2_0_1_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getEntityResource(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[453]);
			}
			
			(
				a7_0 = parse_ofbiz_RestRequest				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
						startIncompleteElement(element);
					}
					if (a7_0 != null) {
						if (a7_0 != null) {
							Object value = a7_0;
							addObjectToList(element, ofbiz.OfbizPackage.ENTITY_RESOURCE__REQUESTS, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_2_0_1_2, a7_0, true);
						copyLocalizationInfos(a7_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[454]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[455]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[456]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[457]);
			}
			
			
			|			a8 = 'urlPath' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_2_0_2_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[458]);
			}
			
			a9 = ':' {
				if (element == null) {
					element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_2_0_2_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[459]);
			}
			
			(
				a10 = QUOTED_34_34				
				{
					if (terminateParsing) {
						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
					}
					if (element == null) {
						element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
						startIncompleteElement(element);
					}
					if (a10 != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						tokenResolver.setOptions(getOptions());
						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a10.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__URL_PATH), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a10).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a10).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a10).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a10).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__URL_PATH), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_2_0_2_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a10, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[460]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[461]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[462]);
				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[463]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[464]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[465]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[466]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[467]);
	}
	
	a11 = '}' {
		if (element == null) {
			element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[468]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[469]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[470]);
		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[471]);
	}
	
;

COMMENT:
	('//'(~('\n'|'\r'|'\uffff'))*)
	{ _channel = 99; }
;
INTEGER:
	(('-')?('1'..'9')('0'..'9')*|'0')
	{ _channel = 99; }
;
FLOAT:
	(('-')?(('1'..'9') ('0'..'9')* | '0') '.' ('0'..'9')+ )
	{ _channel = 99; }
;
TEXT:
	(('A'..'Z' | 'a'..'z' | '0'..'9' | '_' | '-' )+)
;
WHITESPACE:
	((' ' | '\t' | '\f'))
	{ _channel = 99; }
;
LINEBREAK:
	(('\r\n' | '\r' | '\n'))
	{ _channel = 99; }
;
QUOTED_34_34:
	(('"')(~('"'))*('"'))
;

