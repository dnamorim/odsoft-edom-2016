/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.mopp;

public class OfbizReferenceResolverSwitch implements ofbiz.resource.ofbiz.IOfbizReferenceResolverSwitch {
	
	/**
	 * This map stores a copy of the options the were set for loading the resource.
	 */
	private java.util.Map<Object, Object> options;
	
	protected ofbiz.resource.ofbiz.analysis.EntityPrimKeyReferenceResolver entityPrimKeyReferenceResolver = new ofbiz.resource.ofbiz.analysis.EntityPrimKeyReferenceResolver();
	protected ofbiz.resource.ofbiz.analysis.RelationRelatedReferenceResolver relationRelatedReferenceResolver = new ofbiz.resource.ofbiz.analysis.RelationRelatedReferenceResolver();
	protected ofbiz.resource.ofbiz.analysis.KeyMapOriginReferenceResolver keyMapOriginReferenceResolver = new ofbiz.resource.ofbiz.analysis.KeyMapOriginReferenceResolver();
	protected ofbiz.resource.ofbiz.analysis.KeyMapRelatedReferenceResolver keyMapRelatedReferenceResolver = new ofbiz.resource.ofbiz.analysis.KeyMapRelatedReferenceResolver();
	protected ofbiz.resource.ofbiz.analysis.EntityResourceEntityReferenceResolver entityResourceEntityReferenceResolver = new ofbiz.resource.ofbiz.analysis.EntityResourceEntityReferenceResolver();
	
	public ofbiz.resource.ofbiz.IOfbizReferenceResolver<ofbiz.Entity, ofbiz.Field> getEntityPrimKeyReferenceResolver() {
		return getResolverChain(ofbiz.OfbizPackage.eINSTANCE.getEntity_PrimKey(), entityPrimKeyReferenceResolver);
	}
	
	public ofbiz.resource.ofbiz.IOfbizReferenceResolver<ofbiz.Relation, ofbiz.Entity> getRelationRelatedReferenceResolver() {
		return getResolverChain(ofbiz.OfbizPackage.eINSTANCE.getRelation_Related(), relationRelatedReferenceResolver);
	}
	
	public ofbiz.resource.ofbiz.IOfbizReferenceResolver<ofbiz.KeyMap, ofbiz.Field> getKeyMapOriginReferenceResolver() {
		return getResolverChain(ofbiz.OfbizPackage.eINSTANCE.getKeyMap_Origin(), keyMapOriginReferenceResolver);
	}
	
	public ofbiz.resource.ofbiz.IOfbizReferenceResolver<ofbiz.KeyMap, ofbiz.Field> getKeyMapRelatedReferenceResolver() {
		return getResolverChain(ofbiz.OfbizPackage.eINSTANCE.getKeyMap_Related(), keyMapRelatedReferenceResolver);
	}
	
	public ofbiz.resource.ofbiz.IOfbizReferenceResolver<ofbiz.EntityResource, ofbiz.Entity> getEntityResourceEntityReferenceResolver() {
		return getResolverChain(ofbiz.OfbizPackage.eINSTANCE.getEntityResource_Entity(), entityResourceEntityReferenceResolver);
	}
	
	public void setOptions(java.util.Map<?, ?> options) {
		if (options != null) {
			this.options = new java.util.LinkedHashMap<Object, Object>();
			this.options.putAll(options);
		}
		entityPrimKeyReferenceResolver.setOptions(options);
		relationRelatedReferenceResolver.setOptions(options);
		keyMapOriginReferenceResolver.setOptions(options);
		keyMapRelatedReferenceResolver.setOptions(options);
		entityResourceEntityReferenceResolver.setOptions(options);
	}
	
	public void resolveFuzzy(String identifier, org.eclipse.emf.ecore.EObject container, org.eclipse.emf.ecore.EReference reference, int position, ofbiz.resource.ofbiz.IOfbizReferenceResolveResult<org.eclipse.emf.ecore.EObject> result) {
		if (container == null) {
			return;
		}
		if (ofbiz.OfbizPackage.eINSTANCE.getEntity().isInstance(container)) {
			OfbizFuzzyResolveResult<ofbiz.Field> frr = new OfbizFuzzyResolveResult<ofbiz.Field>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("primKey")) {
				entityPrimKeyReferenceResolver.resolve(identifier, (ofbiz.Entity) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (ofbiz.OfbizPackage.eINSTANCE.getRelation().isInstance(container)) {
			OfbizFuzzyResolveResult<ofbiz.Entity> frr = new OfbizFuzzyResolveResult<ofbiz.Entity>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("related")) {
				relationRelatedReferenceResolver.resolve(identifier, (ofbiz.Relation) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (ofbiz.OfbizPackage.eINSTANCE.getKeyMap().isInstance(container)) {
			OfbizFuzzyResolveResult<ofbiz.Field> frr = new OfbizFuzzyResolveResult<ofbiz.Field>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("origin")) {
				keyMapOriginReferenceResolver.resolve(identifier, (ofbiz.KeyMap) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (ofbiz.OfbizPackage.eINSTANCE.getKeyMap().isInstance(container)) {
			OfbizFuzzyResolveResult<ofbiz.Field> frr = new OfbizFuzzyResolveResult<ofbiz.Field>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("related")) {
				keyMapRelatedReferenceResolver.resolve(identifier, (ofbiz.KeyMap) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (ofbiz.OfbizPackage.eINSTANCE.getEntityResource().isInstance(container)) {
			OfbizFuzzyResolveResult<ofbiz.Entity> frr = new OfbizFuzzyResolveResult<ofbiz.Entity>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("entity")) {
				entityResourceEntityReferenceResolver.resolve(identifier, (ofbiz.EntityResource) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
	}
	
	public ofbiz.resource.ofbiz.IOfbizReferenceResolver<? extends org.eclipse.emf.ecore.EObject, ? extends org.eclipse.emf.ecore.EObject> getResolver(org.eclipse.emf.ecore.EStructuralFeature reference) {
		if (reference == ofbiz.OfbizPackage.eINSTANCE.getEntity_PrimKey()) {
			return getResolverChain(reference, entityPrimKeyReferenceResolver);
		}
		if (reference == ofbiz.OfbizPackage.eINSTANCE.getRelation_Related()) {
			return getResolverChain(reference, relationRelatedReferenceResolver);
		}
		if (reference == ofbiz.OfbizPackage.eINSTANCE.getKeyMap_Origin()) {
			return getResolverChain(reference, keyMapOriginReferenceResolver);
		}
		if (reference == ofbiz.OfbizPackage.eINSTANCE.getKeyMap_Related()) {
			return getResolverChain(reference, keyMapRelatedReferenceResolver);
		}
		if (reference == ofbiz.OfbizPackage.eINSTANCE.getEntityResource_Entity()) {
			return getResolverChain(reference, entityResourceEntityReferenceResolver);
		}
		return null;
	}
	
	@SuppressWarnings({"rawtypes", "unchecked"})	
	public <ContainerType extends org.eclipse.emf.ecore.EObject, ReferenceType extends org.eclipse.emf.ecore.EObject> ofbiz.resource.ofbiz.IOfbizReferenceResolver<ContainerType, ReferenceType> getResolverChain(org.eclipse.emf.ecore.EStructuralFeature reference, ofbiz.resource.ofbiz.IOfbizReferenceResolver<ContainerType, ReferenceType> originalResolver) {
		if (options == null) {
			return originalResolver;
		}
		Object value = options.get(ofbiz.resource.ofbiz.IOfbizOptions.ADDITIONAL_REFERENCE_RESOLVERS);
		if (value == null) {
			return originalResolver;
		}
		if (!(value instanceof java.util.Map)) {
			// send this to the error log
			new ofbiz.resource.ofbiz.util.OfbizRuntimeUtil().logWarning("Found value with invalid type for option " + ofbiz.resource.ofbiz.IOfbizOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + java.util.Map.class.getName() + ", but was " + value.getClass().getName() + ")", null);
			return originalResolver;
		}
		java.util.Map<?,?> resolverMap = (java.util.Map<?,?>) value;
		Object resolverValue = resolverMap.get(reference);
		if (resolverValue == null) {
			return originalResolver;
		}
		if (resolverValue instanceof ofbiz.resource.ofbiz.IOfbizReferenceResolver) {
			ofbiz.resource.ofbiz.IOfbizReferenceResolver replacingResolver = (ofbiz.resource.ofbiz.IOfbizReferenceResolver) resolverValue;
			if (replacingResolver instanceof ofbiz.resource.ofbiz.IOfbizDelegatingReferenceResolver) {
				// pass original resolver to the replacing one
				((ofbiz.resource.ofbiz.IOfbizDelegatingReferenceResolver) replacingResolver).setDelegate(originalResolver);
			}
			return replacingResolver;
		} else if (resolverValue instanceof java.util.Collection) {
			java.util.Collection replacingResolvers = (java.util.Collection) resolverValue;
			ofbiz.resource.ofbiz.IOfbizReferenceResolver replacingResolver = originalResolver;
			for (Object next : replacingResolvers) {
				if (next instanceof ofbiz.resource.ofbiz.IOfbizReferenceCache) {
					ofbiz.resource.ofbiz.IOfbizReferenceResolver nextResolver = (ofbiz.resource.ofbiz.IOfbizReferenceResolver) next;
					if (nextResolver instanceof ofbiz.resource.ofbiz.IOfbizDelegatingReferenceResolver) {
						// pass original resolver to the replacing one
						((ofbiz.resource.ofbiz.IOfbizDelegatingReferenceResolver) nextResolver).setDelegate(replacingResolver);
					}
					replacingResolver = nextResolver;
				} else {
					// The collection contains a non-resolver. Send a warning to the error log.
					new ofbiz.resource.ofbiz.util.OfbizRuntimeUtil().logWarning("Found value with invalid type in value map for option " + ofbiz.resource.ofbiz.IOfbizOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + ofbiz.resource.ofbiz.IOfbizDelegatingReferenceResolver.class.getName() + ", but was " + next.getClass().getName() + ")", null);
				}
			}
			return replacingResolver;
		} else {
			// The value for the option ADDITIONAL_REFERENCE_RESOLVERS has an unknown type.
			new ofbiz.resource.ofbiz.util.OfbizRuntimeUtil().logWarning("Found value with invalid type in value map for option " + ofbiz.resource.ofbiz.IOfbizOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + ofbiz.resource.ofbiz.IOfbizDelegatingReferenceResolver.class.getName() + ", but was " + resolverValue.getClass().getName() + ")", null);
			return originalResolver;
		}
	}
	
}
