/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.mopp;

public class OfbizProblem implements ofbiz.resource.ofbiz.IOfbizProblem {
	
	private String message;
	private ofbiz.resource.ofbiz.OfbizEProblemType type;
	private ofbiz.resource.ofbiz.OfbizEProblemSeverity severity;
	private java.util.Collection<ofbiz.resource.ofbiz.IOfbizQuickFix> quickFixes;
	
	public OfbizProblem(String message, ofbiz.resource.ofbiz.OfbizEProblemType type, ofbiz.resource.ofbiz.OfbizEProblemSeverity severity) {
		this(message, type, severity, java.util.Collections.<ofbiz.resource.ofbiz.IOfbizQuickFix>emptySet());
	}
	
	public OfbizProblem(String message, ofbiz.resource.ofbiz.OfbizEProblemType type, ofbiz.resource.ofbiz.OfbizEProblemSeverity severity, ofbiz.resource.ofbiz.IOfbizQuickFix quickFix) {
		this(message, type, severity, java.util.Collections.singleton(quickFix));
	}
	
	public OfbizProblem(String message, ofbiz.resource.ofbiz.OfbizEProblemType type, ofbiz.resource.ofbiz.OfbizEProblemSeverity severity, java.util.Collection<ofbiz.resource.ofbiz.IOfbizQuickFix> quickFixes) {
		super();
		this.message = message;
		this.type = type;
		this.severity = severity;
		this.quickFixes = new java.util.LinkedHashSet<ofbiz.resource.ofbiz.IOfbizQuickFix>();
		this.quickFixes.addAll(quickFixes);
	}
	
	public ofbiz.resource.ofbiz.OfbizEProblemType getType() {
		return type;
	}
	
	public ofbiz.resource.ofbiz.OfbizEProblemSeverity getSeverity() {
		return severity;
	}
	
	public String getMessage() {
		return message;
	}
	
	public java.util.Collection<ofbiz.resource.ofbiz.IOfbizQuickFix> getQuickFixes() {
		return quickFixes;
	}
	
}
