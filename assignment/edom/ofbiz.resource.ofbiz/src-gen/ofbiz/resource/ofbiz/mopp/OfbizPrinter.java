/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.mopp;

public class OfbizPrinter implements ofbiz.resource.ofbiz.IOfbizTextPrinter {
	
	protected ofbiz.resource.ofbiz.IOfbizTokenResolverFactory tokenResolverFactory = new ofbiz.resource.ofbiz.mopp.OfbizTokenResolverFactory();
	
	protected java.io.OutputStream outputStream;
	
	/**
	 * Holds the resource that is associated with this printer. This may be null if
	 * the printer is used stand alone.
	 */
	private ofbiz.resource.ofbiz.IOfbizTextResource resource;
	
	private java.util.Map<?, ?> options;
	private String encoding = System.getProperty("file.encoding");
	
	public OfbizPrinter(java.io.OutputStream outputStream, ofbiz.resource.ofbiz.IOfbizTextResource resource) {
		super();
		this.outputStream = outputStream;
		this.resource = resource;
	}
	
	protected int matchCount(java.util.Map<String, Integer> featureCounter, java.util.Collection<String> needed) {
		int pos = 0;
		int neg = 0;
		
		for (String featureName : featureCounter.keySet()) {
			if (needed.contains(featureName)) {
				int value = featureCounter.get(featureName);
				if (value == 0) {
					neg += 1;
				} else {
					pos += 1;
				}
			}
		}
		return neg > 0 ? -neg : pos;
	}
	
	protected void doPrint(org.eclipse.emf.ecore.EObject element, java.io.PrintWriter out, String globaltab) {
		if (element == null) {
			throw new java.lang.IllegalArgumentException("Nothing to write.");
		}
		if (out == null) {
			throw new java.lang.IllegalArgumentException("Nothing to write on.");
		}
		
		if (element instanceof ofbiz.OfbizComponent) {
			print_ofbiz_OfbizComponent((ofbiz.OfbizComponent) element, globaltab, out);
			return;
		}
		if (element instanceof ofbiz.EntityModel) {
			print_ofbiz_EntityModel((ofbiz.EntityModel) element, globaltab, out);
			return;
		}
		if (element instanceof ofbiz.Entity) {
			print_ofbiz_Entity((ofbiz.Entity) element, globaltab, out);
			return;
		}
		if (element instanceof ofbiz.Field) {
			print_ofbiz_Field((ofbiz.Field) element, globaltab, out);
			return;
		}
		if (element instanceof ofbiz.Relation) {
			print_ofbiz_Relation((ofbiz.Relation) element, globaltab, out);
			return;
		}
		if (element instanceof ofbiz.KeyMap) {
			print_ofbiz_KeyMap((ofbiz.KeyMap) element, globaltab, out);
			return;
		}
		if (element instanceof ofbiz.Services) {
			print_ofbiz_Services((ofbiz.Services) element, globaltab, out);
			return;
		}
		if (element instanceof ofbiz.Service) {
			print_ofbiz_Service((ofbiz.Service) element, globaltab, out);
			return;
		}
		if (element instanceof ofbiz.Attribute) {
			print_ofbiz_Attribute((ofbiz.Attribute) element, globaltab, out);
			return;
		}
		if (element instanceof ofbiz.RestService) {
			print_ofbiz_RestService((ofbiz.RestService) element, globaltab, out);
			return;
		}
		if (element instanceof ofbiz.RestRequest) {
			print_ofbiz_RestRequest((ofbiz.RestRequest) element, globaltab, out);
			return;
		}
		if (element instanceof ofbiz.RestParameter) {
			print_ofbiz_RestParameter((ofbiz.RestParameter) element, globaltab, out);
			return;
		}
		if (element instanceof ofbiz.EntityResource) {
			print_ofbiz_EntityResource((ofbiz.EntityResource) element, globaltab, out);
			return;
		}
		
		addWarningToResource("The printer can not handle " + element.eClass().getName() + " elements", element);
	}
	
	protected ofbiz.resource.ofbiz.mopp.OfbizReferenceResolverSwitch getReferenceResolverSwitch() {
		return (ofbiz.resource.ofbiz.mopp.OfbizReferenceResolverSwitch) new ofbiz.resource.ofbiz.mopp.OfbizMetaInformation().getReferenceResolverSwitch();
	}
	
	protected void addWarningToResource(final String errorMessage, org.eclipse.emf.ecore.EObject cause) {
		ofbiz.resource.ofbiz.IOfbizTextResource resource = getResource();
		if (resource == null) {
			// the resource can be null if the printer is used stand alone
			return;
		}
		resource.addProblem(new ofbiz.resource.ofbiz.mopp.OfbizProblem(errorMessage, ofbiz.resource.ofbiz.OfbizEProblemType.PRINT_PROBLEM, ofbiz.resource.ofbiz.OfbizEProblemSeverity.WARNING), cause);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		this.options = options;
	}
	
	public java.util.Map<?,?> getOptions() {
		return options;
	}
	
	public void setEncoding(String encoding) {
		if (encoding != null) {
			this.encoding = encoding;
		}
	}
	
	public String getEncoding() {
		return encoding;
	}
	
	public ofbiz.resource.ofbiz.IOfbizTextResource getResource() {
		return resource;
	}
	
	/**
	 * Calls {@link #doPrint(EObject, PrintWriter, String)} and writes the result to
	 * the underlying output stream.
	 */
	public void print(org.eclipse.emf.ecore.EObject element) throws java.io.IOException {
		java.io.PrintWriter out = new java.io.PrintWriter(new java.io.OutputStreamWriter(new java.io.BufferedOutputStream(outputStream), encoding));
		doPrint(element, out, "");
		out.flush();
		out.close();
	}
	
	public void print_ofbiz_OfbizComponent(ofbiz.OfbizComponent element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.OFBIZ_COMPONENT__ENTITYMODEL));
		printCountingMap.put("entitymodel", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.OFBIZ_COMPONENT__SERVICES));
		printCountingMap.put("services", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.OFBIZ_COMPONENT__REST));
		printCountingMap.put("rest", temp == null ? 0 : 1);
		// print collected hidden tokens
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("OfbizComponent");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_ofbiz_OfbizComponent_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_ofbiz_OfbizComponent_0(ofbiz.OfbizComponent element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		int alt = -1;
		alt = 0;
		int matches = 		matchCount(printCountingMap, java.util.Arrays.asList(		"entitymodel"		));
		int tempMatchCount;
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"services"		));
		if (tempMatchCount > matches) {
			alt = 1;
			matches = tempMatchCount;
		}
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"rest"		));
		if (tempMatchCount > matches) {
			alt = 2;
			matches = tempMatchCount;
		}
		switch(alt) {
			case 1:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("services");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (Containment)
				count = printCountingMap.get("services");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.OFBIZ_COMPONENT__SERVICES));
					if (o != null) {
						doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
					}
					printCountingMap.put("services", count - 1);
				}
			}
			break;
			case 2:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("rest");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (Containment)
				count = printCountingMap.get("rest");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.OFBIZ_COMPONENT__REST));
					if (o != null) {
						doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
					}
					printCountingMap.put("rest", count - 1);
				}
			}
			break;
			default:			// DEFINITION PART BEGINS (CsString)
			out.print("entitymodel");
			out.print(" ");
			// DEFINITION PART BEGINS (CsString)
			out.print(":");
			out.print(" ");
			// DEFINITION PART BEGINS (Containment)
			count = printCountingMap.get("entitymodel");
			if (count > 0) {
				Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.OFBIZ_COMPONENT__ENTITYMODEL));
				if (o != null) {
					doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
				}
				printCountingMap.put("entitymodel", count - 1);
			}
		}
	}
	
	
	public void print_ofbiz_EntityModel(ofbiz.EntityModel element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(4);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__TITLE));
		printCountingMap.put("title", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__DESCRIPTION));
		printCountingMap.put("description", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__VERSION));
		printCountingMap.put("version", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__ENTITIES));
		printCountingMap.put("entities", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("EntityModel");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_ofbiz_EntityModel_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_ofbiz_EntityModel_0(ofbiz.EntityModel element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		int alt = -1;
		alt = 0;
		int matches = 		matchCount(printCountingMap, java.util.Arrays.asList(		"title"		));
		int tempMatchCount;
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"description"		));
		if (tempMatchCount > matches) {
			alt = 1;
			matches = tempMatchCount;
		}
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"version"		));
		if (tempMatchCount > matches) {
			alt = 2;
			matches = tempMatchCount;
		}
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"entities"		));
		if (tempMatchCount > matches) {
			alt = 3;
			matches = tempMatchCount;
		}
		switch(alt) {
			case 1:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("description");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (PlaceholderInQuotes)
				count = printCountingMap.get("description");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__DESCRIPTION));
					if (o != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						resolver.setOptions(getOptions());
						out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__DESCRIPTION), element));
						out.print(" ");
					}
					printCountingMap.put("description", count - 1);
				}
			}
			break;
			case 2:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("version");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (PlaceholderInQuotes)
				count = printCountingMap.get("version");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__VERSION));
					if (o != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						resolver.setOptions(getOptions());
						out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__VERSION), element));
						out.print(" ");
					}
					printCountingMap.put("version", count - 1);
				}
			}
			break;
			case 3:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("entities");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (Containment)
				count = printCountingMap.get("entities");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__ENTITIES));
					java.util.List<?> list = (java.util.List<?>) o;
					int index = list.size() - count;
					if (index >= 0) {
						o = list.get(index);
					} else {
						o = null;
					}
					if (o != null) {
						doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
					}
					printCountingMap.put("entities", count - 1);
				}
			}
			break;
			default:			// DEFINITION PART BEGINS (CsString)
			out.print("title");
			out.print(" ");
			// DEFINITION PART BEGINS (CsString)
			out.print(":");
			out.print(" ");
			// DEFINITION PART BEGINS (PlaceholderInQuotes)
			count = printCountingMap.get("title");
			if (count > 0) {
				Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__TITLE));
				if (o != null) {
					ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
					resolver.setOptions(getOptions());
					out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__TITLE), element));
					out.print(" ");
				}
				printCountingMap.put("title", count - 1);
			}
		}
	}
	
	
	public void print_ofbiz_Entity(ofbiz.Entity element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(6);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__ENTITY_NAME));
		printCountingMap.put("entityName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__PACKAGE_NAME));
		printCountingMap.put("packageName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__TITLE));
		printCountingMap.put("title", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__PRIM_KEY));
		printCountingMap.put("primKey", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__FIELDS));
		printCountingMap.put("fields", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__RELATIONS));
		printCountingMap.put("relations", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("Entity");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_ofbiz_Entity_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_ofbiz_Entity_0(ofbiz.Entity element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		int alt = -1;
		alt = 0;
		int matches = 		matchCount(printCountingMap, java.util.Arrays.asList(		"entityName"		));
		int tempMatchCount;
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"packageName"		));
		if (tempMatchCount > matches) {
			alt = 1;
			matches = tempMatchCount;
		}
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"title"		));
		if (tempMatchCount > matches) {
			alt = 2;
			matches = tempMatchCount;
		}
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"primKey"		));
		if (tempMatchCount > matches) {
			alt = 3;
			matches = tempMatchCount;
		}
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"fields"		));
		if (tempMatchCount > matches) {
			alt = 4;
			matches = tempMatchCount;
		}
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"relations"		));
		if (tempMatchCount > matches) {
			alt = 5;
			matches = tempMatchCount;
		}
		switch(alt) {
			case 1:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("packageName");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (PlaceholderInQuotes)
				count = printCountingMap.get("packageName");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__PACKAGE_NAME));
					if (o != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						resolver.setOptions(getOptions());
						out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__PACKAGE_NAME), element));
						out.print(" ");
					}
					printCountingMap.put("packageName", count - 1);
				}
			}
			break;
			case 2:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("title");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (PlaceholderInQuotes)
				count = printCountingMap.get("title");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__TITLE));
					if (o != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						resolver.setOptions(getOptions());
						out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__TITLE), element));
						out.print(" ");
					}
					printCountingMap.put("title", count - 1);
				}
			}
			break;
			case 3:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("primKey");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
				count = printCountingMap.get("primKey");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__PRIM_KEY));
					java.util.List<?> list = (java.util.List<?>) o;
					int index = list.size() - count;
					if (index >= 0) {
						o = list.get(index);
					} else {
						o = null;
					}
					if (o != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
						resolver.setOptions(getOptions());
						out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEntityPrimKeyReferenceResolver().deResolve((ofbiz.Field) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__PRIM_KEY)), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__PRIM_KEY), element));
						out.print(" ");
					}
					printCountingMap.put("primKey", count - 1);
				}
			}
			break;
			case 4:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("fields");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (Containment)
				count = printCountingMap.get("fields");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__FIELDS));
					java.util.List<?> list = (java.util.List<?>) o;
					int index = list.size() - count;
					if (index >= 0) {
						o = list.get(index);
					} else {
						o = null;
					}
					if (o != null) {
						doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
					}
					printCountingMap.put("fields", count - 1);
				}
			}
			break;
			case 5:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("relations");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (Containment)
				count = printCountingMap.get("relations");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__RELATIONS));
					java.util.List<?> list = (java.util.List<?>) o;
					int index = list.size() - count;
					if (index >= 0) {
						o = list.get(index);
					} else {
						o = null;
					}
					if (o != null) {
						doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
					}
					printCountingMap.put("relations", count - 1);
				}
			}
			break;
			default:			// DEFINITION PART BEGINS (CsString)
			out.print("entityName");
			out.print(" ");
			// DEFINITION PART BEGINS (CsString)
			out.print(":");
			out.print(" ");
			// DEFINITION PART BEGINS (PlaceholderInQuotes)
			count = printCountingMap.get("entityName");
			if (count > 0) {
				Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__ENTITY_NAME));
				if (o != null) {
					ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
					resolver.setOptions(getOptions());
					out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__ENTITY_NAME), element));
					out.print(" ");
				}
				printCountingMap.put("entityName", count - 1);
			}
		}
	}
	
	
	public void print_ofbiz_Field(ofbiz.Field element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE));
		printCountingMap.put("type", temp == null ? 0 : 1);
		// print collected hidden tokens
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("Field");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_ofbiz_Field_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_ofbiz_Field_0(ofbiz.Field element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		int alt = -1;
		alt = 0;
		int matches = 		matchCount(printCountingMap, java.util.Arrays.asList(		"name"		));
		int tempMatchCount;
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"type"		));
		if (tempMatchCount > matches) {
			alt = 1;
			matches = tempMatchCount;
		}
		switch(alt) {
			case 1:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("type");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (EnumTerminal)
				count = printCountingMap.get("type");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE));
					if (o != null) {
					}
					printCountingMap.put("type", count - 1);
				}
			}
			break;
			default:			// DEFINITION PART BEGINS (CsString)
			out.print("name");
			out.print(" ");
			// DEFINITION PART BEGINS (CsString)
			out.print(":");
			out.print(" ");
			// DEFINITION PART BEGINS (PlaceholderInQuotes)
			count = printCountingMap.get("name");
			if (count > 0) {
				Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__NAME));
				if (o != null) {
					ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
					resolver.setOptions(getOptions());
					out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__NAME), element));
					out.print(" ");
				}
				printCountingMap.put("name", count - 1);
			}
		}
	}
	
	
	public void print_ofbiz_Relation(ofbiz.Relation element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(4);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__FK_NAME));
		printCountingMap.put("fkName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__TYPE));
		printCountingMap.put("type", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__KEY_MAP));
		printCountingMap.put("keyMap", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__RELATED));
		printCountingMap.put("related", temp == null ? 0 : 1);
		// print collected hidden tokens
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("Relation");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_ofbiz_Relation_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_ofbiz_Relation_0(ofbiz.Relation element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		int alt = -1;
		alt = 0;
		int matches = 		matchCount(printCountingMap, java.util.Arrays.asList(		"fkName"		));
		int tempMatchCount;
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"type"		));
		if (tempMatchCount > matches) {
			alt = 1;
			matches = tempMatchCount;
		}
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"keyMap"		));
		if (tempMatchCount > matches) {
			alt = 2;
			matches = tempMatchCount;
		}
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"related"		));
		if (tempMatchCount > matches) {
			alt = 3;
			matches = tempMatchCount;
		}
		switch(alt) {
			case 1:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("type");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (EnumTerminal)
				count = printCountingMap.get("type");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__TYPE));
					if (o != null) {
					}
					printCountingMap.put("type", count - 1);
				}
			}
			break;
			case 2:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("keyMap");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (Containment)
				count = printCountingMap.get("keyMap");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__KEY_MAP));
					java.util.List<?> list = (java.util.List<?>) o;
					int index = list.size() - count;
					if (index >= 0) {
						o = list.get(index);
					} else {
						o = null;
					}
					if (o != null) {
						doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
					}
					printCountingMap.put("keyMap", count - 1);
				}
			}
			break;
			case 3:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("related");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
				count = printCountingMap.get("related");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__RELATED));
					if (o != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
						resolver.setOptions(getOptions());
						out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getRelationRelatedReferenceResolver().deResolve((ofbiz.Entity) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__RELATED)), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__RELATED), element));
						out.print(" ");
					}
					printCountingMap.put("related", count - 1);
				}
			}
			break;
			default:			// DEFINITION PART BEGINS (CsString)
			out.print("fkName");
			out.print(" ");
			// DEFINITION PART BEGINS (CsString)
			out.print(":");
			out.print(" ");
			// DEFINITION PART BEGINS (PlaceholderInQuotes)
			count = printCountingMap.get("fkName");
			if (count > 0) {
				Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__FK_NAME));
				if (o != null) {
					ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
					resolver.setOptions(getOptions());
					out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__FK_NAME), element));
					out.print(" ");
				}
				printCountingMap.put("fkName", count - 1);
			}
		}
	}
	
	
	public void print_ofbiz_KeyMap(ofbiz.KeyMap element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__ORIGIN));
		printCountingMap.put("origin", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__RELATED));
		printCountingMap.put("related", temp == null ? 0 : 1);
		// print collected hidden tokens
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("KeyMap");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_ofbiz_KeyMap_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_ofbiz_KeyMap_0(ofbiz.KeyMap element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		int alt = -1;
		alt = 0;
		int matches = 		matchCount(printCountingMap, java.util.Arrays.asList(		"origin"		));
		int tempMatchCount;
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"related"		));
		if (tempMatchCount > matches) {
			alt = 1;
			matches = tempMatchCount;
		}
		switch(alt) {
			case 1:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("related");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
				count = printCountingMap.get("related");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__RELATED));
					if (o != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
						resolver.setOptions(getOptions());
						out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getKeyMapRelatedReferenceResolver().deResolve((ofbiz.Field) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__RELATED)), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__RELATED), element));
						out.print(" ");
					}
					printCountingMap.put("related", count - 1);
				}
			}
			break;
			default:			// DEFINITION PART BEGINS (CsString)
			out.print("origin");
			out.print(" ");
			// DEFINITION PART BEGINS (CsString)
			out.print(":");
			out.print(" ");
			// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
			count = printCountingMap.get("origin");
			if (count > 0) {
				Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__ORIGIN));
				if (o != null) {
					ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
					resolver.setOptions(getOptions());
					out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getKeyMapOriginReferenceResolver().deResolve((ofbiz.Field) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__ORIGIN)), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__ORIGIN), element));
					out.print(" ");
				}
				printCountingMap.put("origin", count - 1);
			}
		}
	}
	
	
	public void print_ofbiz_Services(ofbiz.Services element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICES__DESCRIPTION));
		printCountingMap.put("description", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICES__VERSION));
		printCountingMap.put("version", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICES__SERVICES));
		printCountingMap.put("services", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("Services");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_ofbiz_Services_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_ofbiz_Services_0(ofbiz.Services element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		int alt = -1;
		alt = 0;
		int matches = 		matchCount(printCountingMap, java.util.Arrays.asList(		"description"		));
		int tempMatchCount;
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"version"		));
		if (tempMatchCount > matches) {
			alt = 1;
			matches = tempMatchCount;
		}
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"services"		));
		if (tempMatchCount > matches) {
			alt = 2;
			matches = tempMatchCount;
		}
		switch(alt) {
			case 1:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("version");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (PlaceholderInQuotes)
				count = printCountingMap.get("version");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICES__VERSION));
					if (o != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						resolver.setOptions(getOptions());
						out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICES__VERSION), element));
						out.print(" ");
					}
					printCountingMap.put("version", count - 1);
				}
			}
			break;
			case 2:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("services");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (Containment)
				count = printCountingMap.get("services");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICES__SERVICES));
					java.util.List<?> list = (java.util.List<?>) o;
					int index = list.size() - count;
					if (index >= 0) {
						o = list.get(index);
					} else {
						o = null;
					}
					if (o != null) {
						doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
					}
					printCountingMap.put("services", count - 1);
				}
			}
			break;
			default:			// DEFINITION PART BEGINS (CsString)
			out.print("description");
			out.print(" ");
			// DEFINITION PART BEGINS (CsString)
			out.print(":");
			out.print(" ");
			// DEFINITION PART BEGINS (PlaceholderInQuotes)
			count = printCountingMap.get("description");
			if (count > 0) {
				Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICES__DESCRIPTION));
				if (o != null) {
					ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
					resolver.setOptions(getOptions());
					out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICES__DESCRIPTION), element));
					out.print(" ");
				}
				printCountingMap.put("description", count - 1);
			}
		}
	}
	
	
	public void print_ofbiz_Service(ofbiz.Service element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(6);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE));
		printCountingMap.put("engine", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__LOCATION));
		printCountingMap.put("location", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__INVOKE));
		printCountingMap.put("invoke", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__DESCRIPTION));
		printCountingMap.put("description", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ATTRIBUTES));
		printCountingMap.put("attributes", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("Service");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_ofbiz_Service_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_ofbiz_Service_0(ofbiz.Service element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		int alt = -1;
		alt = 0;
		int matches = 		matchCount(printCountingMap, java.util.Arrays.asList(		"name"		));
		int tempMatchCount;
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"engine"		));
		if (tempMatchCount > matches) {
			alt = 1;
			matches = tempMatchCount;
		}
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"location"		));
		if (tempMatchCount > matches) {
			alt = 2;
			matches = tempMatchCount;
		}
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"invoke"		));
		if (tempMatchCount > matches) {
			alt = 3;
			matches = tempMatchCount;
		}
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"description"		));
		if (tempMatchCount > matches) {
			alt = 4;
			matches = tempMatchCount;
		}
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"attributes"		));
		if (tempMatchCount > matches) {
			alt = 5;
			matches = tempMatchCount;
		}
		switch(alt) {
			case 1:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("engine");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (EnumTerminal)
				count = printCountingMap.get("engine");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE));
					if (o != null) {
					}
					printCountingMap.put("engine", count - 1);
				}
			}
			break;
			case 2:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("location");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (PlaceholderInQuotes)
				count = printCountingMap.get("location");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__LOCATION));
					if (o != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						resolver.setOptions(getOptions());
						out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__LOCATION), element));
						out.print(" ");
					}
					printCountingMap.put("location", count - 1);
				}
			}
			break;
			case 3:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("invoke");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (PlaceholderInQuotes)
				count = printCountingMap.get("invoke");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__INVOKE));
					if (o != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						resolver.setOptions(getOptions());
						out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__INVOKE), element));
						out.print(" ");
					}
					printCountingMap.put("invoke", count - 1);
				}
			}
			break;
			case 4:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("description");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (PlaceholderInQuotes)
				count = printCountingMap.get("description");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__DESCRIPTION));
					if (o != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						resolver.setOptions(getOptions());
						out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__DESCRIPTION), element));
						out.print(" ");
					}
					printCountingMap.put("description", count - 1);
				}
			}
			break;
			case 5:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("attributes");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (Containment)
				count = printCountingMap.get("attributes");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ATTRIBUTES));
					java.util.List<?> list = (java.util.List<?>) o;
					int index = list.size() - count;
					if (index >= 0) {
						o = list.get(index);
					} else {
						o = null;
					}
					if (o != null) {
						doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
					}
					printCountingMap.put("attributes", count - 1);
				}
			}
			break;
			default:			// DEFINITION PART BEGINS (CsString)
			out.print("name");
			out.print(" ");
			// DEFINITION PART BEGINS (CsString)
			out.print(":");
			out.print(" ");
			// DEFINITION PART BEGINS (PlaceholderInQuotes)
			count = printCountingMap.get("name");
			if (count > 0) {
				Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__NAME));
				if (o != null) {
					ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
					resolver.setOptions(getOptions());
					out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__NAME), element));
					out.print(" ");
				}
				printCountingMap.put("name", count - 1);
			}
		}
	}
	
	
	public void print_ofbiz_Attribute(ofbiz.Attribute element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(4);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__TYPE));
		printCountingMap.put("type", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__MODE));
		printCountingMap.put("mode", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL));
		printCountingMap.put("optional", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (BooleanTerminal)
		count = printCountingMap.get("optional");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL));
			if (o != null) {
			}
			printCountingMap.put("optional", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("Attribute");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_ofbiz_Attribute_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_ofbiz_Attribute_0(ofbiz.Attribute element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		int alt = -1;
		alt = 0;
		int matches = 		matchCount(printCountingMap, java.util.Arrays.asList(		"name"		));
		int tempMatchCount;
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"type"		));
		if (tempMatchCount > matches) {
			alt = 1;
			matches = tempMatchCount;
		}
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"mode"		));
		if (tempMatchCount > matches) {
			alt = 2;
			matches = tempMatchCount;
		}
		switch(alt) {
			case 1:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("type");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (EnumTerminal)
				count = printCountingMap.get("type");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__TYPE));
					if (o != null) {
					}
					printCountingMap.put("type", count - 1);
				}
			}
			break;
			case 2:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("mode");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (EnumTerminal)
				count = printCountingMap.get("mode");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__MODE));
					if (o != null) {
					}
					printCountingMap.put("mode", count - 1);
				}
			}
			break;
			default:			// DEFINITION PART BEGINS (CsString)
			out.print("name");
			out.print(" ");
			// DEFINITION PART BEGINS (CsString)
			out.print(":");
			out.print(" ");
			// DEFINITION PART BEGINS (PlaceholderInQuotes)
			count = printCountingMap.get("name");
			if (count > 0) {
				Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__NAME));
				if (o != null) {
					ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
					resolver.setOptions(getOptions());
					out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__NAME), element));
					out.print(" ");
				}
				printCountingMap.put("name", count - 1);
			}
		}
	}
	
	
	public void print_ofbiz_RestService(ofbiz.RestService element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_SERVICE__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_SERVICE__VERSION));
		printCountingMap.put("version", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_SERVICE__RESOURCES));
		printCountingMap.put("resources", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("RestService");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_ofbiz_RestService_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_ofbiz_RestService_0(ofbiz.RestService element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		int alt = -1;
		alt = 0;
		int matches = 		matchCount(printCountingMap, java.util.Arrays.asList(		"name"		));
		int tempMatchCount;
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"version"		));
		if (tempMatchCount > matches) {
			alt = 1;
			matches = tempMatchCount;
		}
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"resources"		));
		if (tempMatchCount > matches) {
			alt = 2;
			matches = tempMatchCount;
		}
		switch(alt) {
			case 1:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("version");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (PlaceholderInQuotes)
				count = printCountingMap.get("version");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_SERVICE__VERSION));
					if (o != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						resolver.setOptions(getOptions());
						out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_SERVICE__VERSION), element));
						out.print(" ");
					}
					printCountingMap.put("version", count - 1);
				}
			}
			break;
			case 2:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("resources");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (Containment)
				count = printCountingMap.get("resources");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_SERVICE__RESOURCES));
					java.util.List<?> list = (java.util.List<?>) o;
					int index = list.size() - count;
					if (index >= 0) {
						o = list.get(index);
					} else {
						o = null;
					}
					if (o != null) {
						doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
					}
					printCountingMap.put("resources", count - 1);
				}
			}
			break;
			default:			// DEFINITION PART BEGINS (CsString)
			out.print("name");
			out.print(" ");
			// DEFINITION PART BEGINS (CsString)
			out.print(":");
			out.print(" ");
			// DEFINITION PART BEGINS (PlaceholderInQuotes)
			count = printCountingMap.get("name");
			if (count > 0) {
				Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_SERVICE__NAME));
				if (o != null) {
					ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
					resolver.setOptions(getOptions());
					out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_SERVICE__NAME), element));
					out.print(" ");
				}
				printCountingMap.put("name", count - 1);
			}
		}
	}
	
	
	public void print_ofbiz_RestRequest(ofbiz.RestRequest element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_REQUEST__ACTION));
		printCountingMap.put("action", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_REQUEST__PARAMETERS));
		printCountingMap.put("parameters", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("RestRequest");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_ofbiz_RestRequest_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_ofbiz_RestRequest_0(ofbiz.RestRequest element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		int alt = -1;
		alt = 0;
		int matches = 		matchCount(printCountingMap, java.util.Arrays.asList(		"action"		));
		int tempMatchCount;
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"parameters"		));
		if (tempMatchCount > matches) {
			alt = 1;
			matches = tempMatchCount;
		}
		switch(alt) {
			case 1:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("parameters");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (Containment)
				count = printCountingMap.get("parameters");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_REQUEST__PARAMETERS));
					java.util.List<?> list = (java.util.List<?>) o;
					int index = list.size() - count;
					if (index >= 0) {
						o = list.get(index);
					} else {
						o = null;
					}
					if (o != null) {
						doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
					}
					printCountingMap.put("parameters", count - 1);
				}
			}
			break;
			default:			// DEFINITION PART BEGINS (CsString)
			out.print("action");
			out.print(" ");
			// DEFINITION PART BEGINS (CsString)
			out.print(":");
			out.print(" ");
			// DEFINITION PART BEGINS (EnumTerminal)
			count = printCountingMap.get("action");
			if (count > 0) {
				Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_REQUEST__ACTION));
				if (o != null) {
				}
				printCountingMap.put("action", count - 1);
			}
		}
	}
	
	
	public void print_ofbiz_RestParameter(ofbiz.RestParameter element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_PARAMETER__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		// print collected hidden tokens
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("RestParameter");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_ofbiz_RestParameter_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_ofbiz_RestParameter_0(ofbiz.RestParameter element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("name");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print(":");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderInQuotes)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_PARAMETER__NAME));
			if (o != null) {
				ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_PARAMETER__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
	}
	
	
	public void print_ofbiz_EntityResource(ofbiz.EntityResource element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__ENTITY));
		printCountingMap.put("entity", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__REQUESTS));
		printCountingMap.put("requests", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__URL_PATH));
		printCountingMap.put("urlPath", temp == null ? 0 : 1);
		// print collected hidden tokens
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("EntityResource");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_ofbiz_EntityResource_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_ofbiz_EntityResource_0(ofbiz.EntityResource element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		int alt = -1;
		alt = 0;
		int matches = 		matchCount(printCountingMap, java.util.Arrays.asList(		"entity"		));
		int tempMatchCount;
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"requests"		));
		if (tempMatchCount > matches) {
			alt = 1;
			matches = tempMatchCount;
		}
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"urlPath"		));
		if (tempMatchCount > matches) {
			alt = 2;
			matches = tempMatchCount;
		}
		switch(alt) {
			case 1:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("requests");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (Containment)
				count = printCountingMap.get("requests");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__REQUESTS));
					java.util.List<?> list = (java.util.List<?>) o;
					int index = list.size() - count;
					if (index >= 0) {
						o = list.get(index);
					} else {
						o = null;
					}
					if (o != null) {
						doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
					}
					printCountingMap.put("requests", count - 1);
				}
			}
			break;
			case 2:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("urlPath");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print(":");
				out.print(" ");
				// DEFINITION PART BEGINS (PlaceholderInQuotes)
				count = printCountingMap.get("urlPath");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__URL_PATH));
					if (o != null) {
						ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						resolver.setOptions(getOptions());
						out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__URL_PATH), element));
						out.print(" ");
					}
					printCountingMap.put("urlPath", count - 1);
				}
			}
			break;
			default:			// DEFINITION PART BEGINS (CsString)
			out.print("entity");
			out.print(" ");
			// DEFINITION PART BEGINS (CsString)
			out.print(":");
			out.print(" ");
			// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
			count = printCountingMap.get("entity");
			if (count > 0) {
				Object o = element.eGet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__ENTITY));
				if (o != null) {
					ofbiz.resource.ofbiz.IOfbizTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
					resolver.setOptions(getOptions());
					out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEntityResourceEntityReferenceResolver().deResolve((ofbiz.Entity) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__ENTITY)), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__ENTITY), element));
					out.print(" ");
				}
				printCountingMap.put("entity", count - 1);
			}
		}
	}
	
	
}
