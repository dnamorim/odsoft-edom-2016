/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.mopp;

/**
 * A basic implementation of the ofbiz.resource.ofbiz.IOfbizReferenceResolveResult
 * interface that collects mappings in a list.
 * 
 * @param <ReferenceType> the type of the references that can be contained in this
 * result
 */
public class OfbizReferenceResolveResult<ReferenceType> implements ofbiz.resource.ofbiz.IOfbizReferenceResolveResult<ReferenceType> {
	
	private java.util.Collection<ofbiz.resource.ofbiz.IOfbizReferenceMapping<ReferenceType>> mappings;
	private String errorMessage;
	private boolean resolveFuzzy;
	private java.util.Set<ofbiz.resource.ofbiz.IOfbizQuickFix> quickFixes;
	
	public OfbizReferenceResolveResult(boolean resolveFuzzy) {
		super();
		this.resolveFuzzy = resolveFuzzy;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public java.util.Collection<ofbiz.resource.ofbiz.IOfbizQuickFix> getQuickFixes() {
		if (quickFixes == null) {
			quickFixes = new java.util.LinkedHashSet<ofbiz.resource.ofbiz.IOfbizQuickFix>();
		}
		return java.util.Collections.unmodifiableSet(quickFixes);
	}
	
	public void addQuickFix(ofbiz.resource.ofbiz.IOfbizQuickFix quickFix) {
		if (quickFixes == null) {
			quickFixes = new java.util.LinkedHashSet<ofbiz.resource.ofbiz.IOfbizQuickFix>();
		}
		quickFixes.add(quickFix);
	}
	
	public java.util.Collection<ofbiz.resource.ofbiz.IOfbizReferenceMapping<ReferenceType>> getMappings() {
		return mappings;
	}
	
	public boolean wasResolved() {
		return mappings != null;
	}
	
	public boolean wasResolvedMultiple() {
		return mappings != null && mappings.size() > 1;
	}
	
	public boolean wasResolvedUniquely() {
		return mappings != null && mappings.size() == 1;
	}
	
	public void setErrorMessage(String message) {
		errorMessage = message;
	}
	
	public void addMapping(String identifier, ReferenceType target) {
		if (!resolveFuzzy && target == null) {
			throw new IllegalArgumentException("Mapping references to null is only allowed for fuzzy resolution.");
		}
		addMapping(identifier, target, null);
	}
	
	public void addMapping(String identifier, ReferenceType target, String warning) {
		if (mappings == null) {
			mappings = new java.util.ArrayList<ofbiz.resource.ofbiz.IOfbizReferenceMapping<ReferenceType>>(1);
		}
		mappings.add(new ofbiz.resource.ofbiz.mopp.OfbizElementMapping<ReferenceType>(identifier, target, warning));
		errorMessage = null;
	}
	
	public void addMapping(String identifier, org.eclipse.emf.common.util.URI uri) {
		addMapping(identifier, uri, null);
	}
	
	public void addMapping(String identifier, org.eclipse.emf.common.util.URI uri, String warning) {
		if (mappings == null) {
			mappings = new java.util.ArrayList<ofbiz.resource.ofbiz.IOfbizReferenceMapping<ReferenceType>>(1);
		}
		mappings.add(new ofbiz.resource.ofbiz.mopp.OfbizURIMapping<ReferenceType>(identifier, uri, warning));
	}
}
