/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.mopp;

/**
 * The OfbizTaskItemBuilder is used to find task items in text documents. The
 * current implementation uses the generated lexer and the TaskItemDetector to
 * detect task items. This class is called by the BuilderAdapter, which runs both
 * this builder and the default builder that is intended to be customized.
 */
public class OfbizTaskItemBuilder {
	
	public void build(org.eclipse.core.resources.IFile resource, org.eclipse.emf.ecore.resource.ResourceSet resourceSet, org.eclipse.core.runtime.IProgressMonitor monitor) {
		monitor.setTaskName("Searching for task items");
		new ofbiz.resource.ofbiz.mopp.OfbizMarkerHelper().removeAllMarkers(resource, org.eclipse.core.resources.IMarker.TASK);
		if (isInBinFolder(resource)) {
			return;
		}
		java.util.List<ofbiz.resource.ofbiz.mopp.OfbizTaskItem> taskItems = new java.util.ArrayList<ofbiz.resource.ofbiz.mopp.OfbizTaskItem>();
		ofbiz.resource.ofbiz.mopp.OfbizTaskItemDetector taskItemDetector = new ofbiz.resource.ofbiz.mopp.OfbizTaskItemDetector();
		try {
			java.io.InputStream inputStream = resource.getContents();
			String content = ofbiz.resource.ofbiz.util.OfbizStreamUtil.getContent(inputStream);
			ofbiz.resource.ofbiz.IOfbizTextScanner lexer = new ofbiz.resource.ofbiz.mopp.OfbizMetaInformation().createLexer();
			lexer.setText(content);
			
			ofbiz.resource.ofbiz.IOfbizTextToken nextToken = lexer.getNextToken();
			while (nextToken != null) {
				String text = nextToken.getText();
				taskItems.addAll(taskItemDetector.findTaskItems(text, nextToken.getLine(), nextToken.getOffset()));
				nextToken = lexer.getNextToken();
			}
		} catch (java.io.IOException e) {
			ofbiz.resource.ofbiz.mopp.OfbizPlugin.logError("Exception while searching for task items", e);
		} catch (org.eclipse.core.runtime.CoreException e) {
			ofbiz.resource.ofbiz.mopp.OfbizPlugin.logError("Exception while searching for task items", e);
		}
		
		for (ofbiz.resource.ofbiz.mopp.OfbizTaskItem taskItem : taskItems) {
			java.util.Map<String, Object> markerAttributes = new java.util.LinkedHashMap<String, Object>();
			markerAttributes.put(org.eclipse.core.resources.IMarker.USER_EDITABLE, false);
			markerAttributes.put(org.eclipse.core.resources.IMarker.DONE, false);
			markerAttributes.put(org.eclipse.core.resources.IMarker.LINE_NUMBER, taskItem.getLine());
			markerAttributes.put(org.eclipse.core.resources.IMarker.CHAR_START, taskItem.getCharStart());
			markerAttributes.put(org.eclipse.core.resources.IMarker.CHAR_END, taskItem.getCharEnd());
			markerAttributes.put(org.eclipse.core.resources.IMarker.MESSAGE, taskItem.getMessage());
			new ofbiz.resource.ofbiz.mopp.OfbizMarkerHelper().createMarker(resource, org.eclipse.core.resources.IMarker.TASK, markerAttributes);
		}
	}
	
	public String getBuilderMarkerId() {
		return org.eclipse.core.resources.IMarker.TASK;
	}
	
	public boolean isInBinFolder(org.eclipse.core.resources.IFile resource) {
		org.eclipse.core.resources.IContainer parent = resource.getParent();
		while (parent != null) {
			if ("bin".equals(parent.getName())) {
				return true;
			}
			parent = parent.getParent();
		}
		return false;
	}
	
}
