// $ANTLR 3.4

	package ofbiz.resource.ofbiz.mopp;


import org.antlr.runtime3_4_0.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class OfbizParser extends OfbizANTLRParserBase {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "COMMENT", "FLOAT", "INTEGER", "LINEBREAK", "QUOTED_34_34", "TEXT", "WHITESPACE", "':'", "'Attribute'", "'BigDecimal'", "'Boolean'", "'DELETE'", "'Entity'", "'EntityModel'", "'EntityResource'", "'Field'", "'GET'", "'IN'", "'KeyMap'", "'List'", "'MANY'", "'Map'", "'ONE'", "'OUT'", "'OfbizComponent'", "'POST'", "'PUT'", "'Relation'", "'RestParameter'", "'RestRequest'", "'RestService'", "'Service'", "'Services'", "'String'", "'Timestamp'", "'action'", "'attributes'", "'blob'", "'byte-array'", "'comment'", "'credit-card-date'", "'credit-card-number'", "'currency-ammount'", "'currency-precise'", "'date'", "'date-time'", "'description'", "'email'", "'engine'", "'entities'", "'entity'", "'entity-auto'", "'entityName'", "'entitymodel'", "'fields'", "'fixed-point'", "'fkName'", "'floating-point'", "'groovy'", "'group'", "'http'", "'id'", "'id-long'", "'id-long-ne'", "'id-ne'", "'id-vlong'", "'id-vlong-ne'", "'indicator'", "'interface'", "'invoke'", "'jacl'", "'java'", "'javascript'", "'jms'", "'jpython'", "'keyMap'", "'location'", "'long-varchar'", "'mode'", "'name'", "'numeric'", "'object'", "'optional'", "'origin'", "'packageName'", "'parameters'", "'primKey'", "'related'", "'relations'", "'requests'", "'resources'", "'rest'", "'rmi'", "'route'", "'script'", "'services'", "'short-varchar'", "'simple'", "'soap'", "'tel-number'", "'time'", "'title'", "'type'", "'url'", "'urlPath'", "'value'", "'version'", "'very-long'", "'very-short'", "'{'", "'}'"
    };

    public static final int EOF=-1;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__50=50;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__59=59;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__64=64;
    public static final int T__65=65;
    public static final int T__66=66;
    public static final int T__67=67;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int T__73=73;
    public static final int T__74=74;
    public static final int T__75=75;
    public static final int T__76=76;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int T__80=80;
    public static final int T__81=81;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int T__84=84;
    public static final int T__85=85;
    public static final int T__86=86;
    public static final int T__87=87;
    public static final int T__88=88;
    public static final int T__89=89;
    public static final int T__90=90;
    public static final int T__91=91;
    public static final int T__92=92;
    public static final int T__93=93;
    public static final int T__94=94;
    public static final int T__95=95;
    public static final int T__96=96;
    public static final int T__97=97;
    public static final int T__98=98;
    public static final int T__99=99;
    public static final int T__100=100;
    public static final int T__101=101;
    public static final int T__102=102;
    public static final int T__103=103;
    public static final int T__104=104;
    public static final int T__105=105;
    public static final int T__106=106;
    public static final int T__107=107;
    public static final int T__108=108;
    public static final int T__109=109;
    public static final int T__110=110;
    public static final int T__111=111;
    public static final int T__112=112;
    public static final int T__113=113;
    public static final int T__114=114;
    public static final int COMMENT=4;
    public static final int FLOAT=5;
    public static final int INTEGER=6;
    public static final int LINEBREAK=7;
    public static final int QUOTED_34_34=8;
    public static final int TEXT=9;
    public static final int WHITESPACE=10;

    // delegates
    public OfbizANTLRParserBase[] getDelegates() {
        return new OfbizANTLRParserBase[] {};
    }

    // delegators


    public OfbizParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public OfbizParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
        this.state.initializeRuleMemo(111 + 1);
         

    }

    public String[] getTokenNames() { return OfbizParser.tokenNames; }
    public String getGrammarFileName() { return "Ofbiz.g"; }


    	private ofbiz.resource.ofbiz.IOfbizTokenResolverFactory tokenResolverFactory = new ofbiz.resource.ofbiz.mopp.OfbizTokenResolverFactory();
    	
    	/**
    	 * the index of the last token that was handled by collectHiddenTokens()
    	 */
    	private int lastPosition;
    	
    	/**
    	 * A flag that indicates whether the parser should remember all expected elements.
    	 * This flag is set to true when using the parse for code completion. Otherwise it
    	 * is set to false.
    	 */
    	private boolean rememberExpectedElements = false;
    	
    	private Object parseToIndexTypeObject;
    	private int lastTokenIndex = 0;
    	
    	/**
    	 * A list of expected elements the were collected while parsing the input stream.
    	 * This list is only filled if <code>rememberExpectedElements</code> is set to
    	 * true.
    	 */
    	private java.util.List<ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal> expectedElements = new java.util.ArrayList<ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal>();
    	
    	private int mismatchedTokenRecoveryTries = 0;
    	/**
    	 * A helper list to allow a lexer to pass errors to its parser
    	 */
    	protected java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions = java.util.Collections.synchronizedList(new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>());
    	
    	/**
    	 * Another helper list to allow a lexer to pass positions of errors to its parser
    	 */
    	protected java.util.List<Integer> lexerExceptionsPosition = java.util.Collections.synchronizedList(new java.util.ArrayList<Integer>());
    	
    	/**
    	 * A stack for incomplete objects. This stack is used filled when the parser is
    	 * used for code completion. Whenever the parser starts to read an object it is
    	 * pushed on the stack. Once the element was parser completely it is popped from
    	 * the stack.
    	 */
    	java.util.List<org.eclipse.emf.ecore.EObject> incompleteObjects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
    	
    	private int stopIncludingHiddenTokens;
    	private int stopExcludingHiddenTokens;
    	private int tokenIndexOfLastCompleteElement;
    	
    	private int expectedElementsIndexOfLastCompleteElement;
    	
    	/**
    	 * The offset indicating the cursor position when the parser is used for code
    	 * completion by calling parseToExpectedElements().
    	 */
    	private int cursorOffset;
    	
    	/**
    	 * The offset of the first hidden token of the last expected element. This offset
    	 * is used to discard expected elements, which are not needed for code completion.
    	 */
    	private int lastStartIncludingHidden;
    	
    	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
    		postParseCommands.add(new ofbiz.resource.ofbiz.IOfbizCommand<ofbiz.resource.ofbiz.IOfbizTextResource>() {
    			public boolean execute(ofbiz.resource.ofbiz.IOfbizTextResource resource) {
    				if (resource == null) {
    					// the resource can be null if the parser is used for code completion
    					return true;
    				}
    				resource.addProblem(new ofbiz.resource.ofbiz.IOfbizProblem() {
    					public ofbiz.resource.ofbiz.OfbizEProblemSeverity getSeverity() {
    						return ofbiz.resource.ofbiz.OfbizEProblemSeverity.ERROR;
    					}
    					public ofbiz.resource.ofbiz.OfbizEProblemType getType() {
    						return ofbiz.resource.ofbiz.OfbizEProblemType.SYNTAX_ERROR;
    					}
    					public String getMessage() {
    						return errorMessage;
    					}
    					public java.util.Collection<ofbiz.resource.ofbiz.IOfbizQuickFix> getQuickFixes() {
    						return null;
    					}
    				}, column, line, startIndex, stopIndex);
    				return true;
    			}
    		});
    	}
    	
    	public void addExpectedElement(org.eclipse.emf.ecore.EClass eClass, int[] ids) {
    		if (!this.rememberExpectedElements) {
    			return;
    		}
    		int terminalID = ids[0];
    		int followSetID = ids[1];
    		ofbiz.resource.ofbiz.IOfbizExpectedElement terminal = ofbiz.resource.ofbiz.grammar.OfbizFollowSetProvider.TERMINALS[terminalID];
    		ofbiz.resource.ofbiz.mopp.OfbizContainedFeature[] containmentFeatures = new ofbiz.resource.ofbiz.mopp.OfbizContainedFeature[ids.length - 2];
    		for (int i = 2; i < ids.length; i++) {
    			containmentFeatures[i - 2] = ofbiz.resource.ofbiz.grammar.OfbizFollowSetProvider.LINKS[ids[i]];
    		}
    		ofbiz.resource.ofbiz.grammar.OfbizContainmentTrace containmentTrace = new ofbiz.resource.ofbiz.grammar.OfbizContainmentTrace(eClass, containmentFeatures);
    		org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
    		ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal expectedElement = new ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal(container, terminal, followSetID, containmentTrace);
    		setPosition(expectedElement, input.index());
    		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
    		if (lastStartIncludingHidden >= 0 && lastStartIncludingHidden < startIncludingHiddenTokens && cursorOffset > startIncludingHiddenTokens) {
    			// clear list of expected elements
    			this.expectedElements.clear();
    			this.expectedElementsIndexOfLastCompleteElement = 0;
    		}
    		lastStartIncludingHidden = startIncludingHiddenTokens;
    		this.expectedElements.add(expectedElement);
    	}
    	
    	protected void collectHiddenTokens(org.eclipse.emf.ecore.EObject element) {
    	}
    	
    	protected void copyLocalizationInfos(final org.eclipse.emf.ecore.EObject source, final org.eclipse.emf.ecore.EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new ofbiz.resource.ofbiz.IOfbizCommand<ofbiz.resource.ofbiz.IOfbizTextResource>() {
    			public boolean execute(ofbiz.resource.ofbiz.IOfbizTextResource resource) {
    				ofbiz.resource.ofbiz.IOfbizLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				locationMap.setCharStart(target, locationMap.getCharStart(source));
    				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
    				locationMap.setColumn(target, locationMap.getColumn(source));
    				locationMap.setLine(target, locationMap.getLine(source));
    				return true;
    			}
    		});
    	}
    	
    	protected void copyLocalizationInfos(final org.antlr.runtime3_4_0.CommonToken source, final org.eclipse.emf.ecore.EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new ofbiz.resource.ofbiz.IOfbizCommand<ofbiz.resource.ofbiz.IOfbizTextResource>() {
    			public boolean execute(ofbiz.resource.ofbiz.IOfbizTextResource resource) {
    				ofbiz.resource.ofbiz.IOfbizLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				if (source == null) {
    					return true;
    				}
    				locationMap.setCharStart(target, source.getStartIndex());
    				locationMap.setCharEnd(target, source.getStopIndex());
    				locationMap.setColumn(target, source.getCharPositionInLine());
    				locationMap.setLine(target, source.getLine());
    				return true;
    			}
    		});
    	}
    	
    	/**
    	 * Sets the end character index and the last line for the given object in the
    	 * location map.
    	 */
    	protected void setLocalizationEnd(java.util.Collection<ofbiz.resource.ofbiz.IOfbizCommand<ofbiz.resource.ofbiz.IOfbizTextResource>> postParseCommands , final org.eclipse.emf.ecore.EObject object, final int endChar, final int endLine) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new ofbiz.resource.ofbiz.IOfbizCommand<ofbiz.resource.ofbiz.IOfbizTextResource>() {
    			public boolean execute(ofbiz.resource.ofbiz.IOfbizTextResource resource) {
    				ofbiz.resource.ofbiz.IOfbizLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				locationMap.setCharEnd(object, endChar);
    				locationMap.setLine(object, endLine);
    				return true;
    			}
    		});
    	}
    	
    	public ofbiz.resource.ofbiz.IOfbizTextParser createInstance(java.io.InputStream actualInputStream, String encoding) {
    		try {
    			if (encoding == null) {
    				return new OfbizParser(new org.antlr.runtime3_4_0.CommonTokenStream(new OfbizLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream))));
    			} else {
    				return new OfbizParser(new org.antlr.runtime3_4_0.CommonTokenStream(new OfbizLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream, encoding))));
    			}
    		} catch (java.io.IOException e) {
    			new ofbiz.resource.ofbiz.util.OfbizRuntimeUtil().logError("Error while creating parser.", e);
    			return null;
    		}
    	}
    	
    	/**
    	 * This default constructor is only used to call createInstance() on it.
    	 */
    	public OfbizParser() {
    		super(null);
    	}
    	
    	protected org.eclipse.emf.ecore.EObject doParse() throws org.antlr.runtime3_4_0.RecognitionException {
    		this.lastPosition = 0;
    		// required because the lexer class can not be subclassed
    		((OfbizLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
    		((OfbizLexer) getTokenStream().getTokenSource()).lexerExceptionsPosition = lexerExceptionsPosition;
    		Object typeObject = getTypeObject();
    		if (typeObject == null) {
    			return start();
    		} else if (typeObject instanceof org.eclipse.emf.ecore.EClass) {
    			org.eclipse.emf.ecore.EClass type = (org.eclipse.emf.ecore.EClass) typeObject;
    			if (type.getInstanceClass() == ofbiz.OfbizComponent.class) {
    				return parse_ofbiz_OfbizComponent();
    			}
    			if (type.getInstanceClass() == ofbiz.EntityModel.class) {
    				return parse_ofbiz_EntityModel();
    			}
    			if (type.getInstanceClass() == ofbiz.Entity.class) {
    				return parse_ofbiz_Entity();
    			}
    			if (type.getInstanceClass() == ofbiz.Field.class) {
    				return parse_ofbiz_Field();
    			}
    			if (type.getInstanceClass() == ofbiz.Relation.class) {
    				return parse_ofbiz_Relation();
    			}
    			if (type.getInstanceClass() == ofbiz.KeyMap.class) {
    				return parse_ofbiz_KeyMap();
    			}
    			if (type.getInstanceClass() == ofbiz.Services.class) {
    				return parse_ofbiz_Services();
    			}
    			if (type.getInstanceClass() == ofbiz.Service.class) {
    				return parse_ofbiz_Service();
    			}
    			if (type.getInstanceClass() == ofbiz.Attribute.class) {
    				return parse_ofbiz_Attribute();
    			}
    			if (type.getInstanceClass() == ofbiz.RestService.class) {
    				return parse_ofbiz_RestService();
    			}
    			if (type.getInstanceClass() == ofbiz.RestRequest.class) {
    				return parse_ofbiz_RestRequest();
    			}
    			if (type.getInstanceClass() == ofbiz.RestParameter.class) {
    				return parse_ofbiz_RestParameter();
    			}
    			if (type.getInstanceClass() == ofbiz.EntityResource.class) {
    				return parse_ofbiz_EntityResource();
    			}
    		}
    		throw new ofbiz.resource.ofbiz.mopp.OfbizUnexpectedContentTypeException(typeObject);
    	}
    	
    	public int getMismatchedTokenRecoveryTries() {
    		return mismatchedTokenRecoveryTries;
    	}
    	
    	public Object getMissingSymbol(org.antlr.runtime3_4_0.IntStream arg0, org.antlr.runtime3_4_0.RecognitionException arg1, int arg2, org.antlr.runtime3_4_0.BitSet arg3) {
    		mismatchedTokenRecoveryTries++;
    		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
    	}
    	
    	public Object getParseToIndexTypeObject() {
    		return parseToIndexTypeObject;
    	}
    	
    	protected Object getTypeObject() {
    		Object typeObject = getParseToIndexTypeObject();
    		if (typeObject != null) {
    			return typeObject;
    		}
    		java.util.Map<?,?> options = getOptions();
    		if (options != null) {
    			typeObject = options.get(ofbiz.resource.ofbiz.IOfbizOptions.RESOURCE_CONTENT_TYPE);
    		}
    		return typeObject;
    	}
    	
    	/**
    	 * Implementation that calls {@link #doParse()} and handles the thrown
    	 * RecognitionExceptions.
    	 */
    	public ofbiz.resource.ofbiz.IOfbizParseResult parse() {
    		terminateParsing = false;
    		postParseCommands = new java.util.ArrayList<ofbiz.resource.ofbiz.IOfbizCommand<ofbiz.resource.ofbiz.IOfbizTextResource>>();
    		ofbiz.resource.ofbiz.mopp.OfbizParseResult parseResult = new ofbiz.resource.ofbiz.mopp.OfbizParseResult();
    		try {
    			org.eclipse.emf.ecore.EObject result =  doParse();
    			if (lexerExceptions.isEmpty()) {
    				parseResult.setRoot(result);
    			}
    		} catch (org.antlr.runtime3_4_0.RecognitionException re) {
    			reportError(re);
    		} catch (java.lang.IllegalArgumentException iae) {
    			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
    				// can be caused if a null is set on EMF models where not allowed. this will just
    				// happen if other errors occurred before
    			} else {
    				iae.printStackTrace();
    			}
    		}
    		for (org.antlr.runtime3_4_0.RecognitionException re : lexerExceptions) {
    			reportLexicalError(re);
    		}
    		parseResult.getPostParseCommands().addAll(postParseCommands);
    		return parseResult;
    	}
    	
    	public java.util.List<ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal> parseToExpectedElements(org.eclipse.emf.ecore.EClass type, ofbiz.resource.ofbiz.IOfbizTextResource dummyResource, int cursorOffset) {
    		this.rememberExpectedElements = true;
    		this.parseToIndexTypeObject = type;
    		this.cursorOffset = cursorOffset;
    		this.lastStartIncludingHidden = -1;
    		final org.antlr.runtime3_4_0.CommonTokenStream tokenStream = (org.antlr.runtime3_4_0.CommonTokenStream) getTokenStream();
    		ofbiz.resource.ofbiz.IOfbizParseResult result = parse();
    		for (org.eclipse.emf.ecore.EObject incompleteObject : incompleteObjects) {
    			org.antlr.runtime3_4_0.Lexer lexer = (org.antlr.runtime3_4_0.Lexer) tokenStream.getTokenSource();
    			int endChar = lexer.getCharIndex();
    			int endLine = lexer.getLine();
    			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
    		}
    		if (result != null) {
    			org.eclipse.emf.ecore.EObject root = result.getRoot();
    			if (root != null) {
    				dummyResource.getContentsInternal().add(root);
    			}
    			for (ofbiz.resource.ofbiz.IOfbizCommand<ofbiz.resource.ofbiz.IOfbizTextResource> command : result.getPostParseCommands()) {
    				command.execute(dummyResource);
    			}
    		}
    		// remove all expected elements that were added after the last complete element
    		expectedElements = expectedElements.subList(0, expectedElementsIndexOfLastCompleteElement + 1);
    		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
    		java.util.Set<ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal> currentFollowSet = new java.util.LinkedHashSet<ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal>();
    		java.util.List<ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal> newFollowSet = new java.util.ArrayList<ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal>();
    		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
    			ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal expectedElementI = expectedElements.get(i);
    			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
    				currentFollowSet.add(expectedElementI);
    			} else {
    				break;
    			}
    		}
    		int followSetID = 180;
    		int i;
    		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
    			org.antlr.runtime3_4_0.CommonToken nextToken = (org.antlr.runtime3_4_0.CommonToken) tokenStream.get(i);
    			if (nextToken.getType() < 0) {
    				break;
    			}
    			if (nextToken.getChannel() == 99) {
    				// hidden tokens do not reduce the follow set
    			} else {
    				// now that we have found the next visible token the position for that expected
    				// terminals can be set
    				for (ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal nextFollow : newFollowSet) {
    					lastTokenIndex = 0;
    					setPosition(nextFollow, i);
    				}
    				newFollowSet.clear();
    				// normal tokens do reduce the follow set - only elements that match the token are
    				// kept
    				for (ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal nextFollow : currentFollowSet) {
    					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
    						// keep this one - it matches
    						java.util.Collection<ofbiz.resource.ofbiz.util.OfbizPair<ofbiz.resource.ofbiz.IOfbizExpectedElement, ofbiz.resource.ofbiz.mopp.OfbizContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
    						for (ofbiz.resource.ofbiz.util.OfbizPair<ofbiz.resource.ofbiz.IOfbizExpectedElement, ofbiz.resource.ofbiz.mopp.OfbizContainedFeature[]> newFollowerPair : newFollowers) {
    							ofbiz.resource.ofbiz.IOfbizExpectedElement newFollower = newFollowerPair.getLeft();
    							org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
    							ofbiz.resource.ofbiz.grammar.OfbizContainmentTrace containmentTrace = new ofbiz.resource.ofbiz.grammar.OfbizContainmentTrace(null, newFollowerPair.getRight());
    							ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal newFollowTerminal = new ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal(container, newFollower, followSetID, containmentTrace);
    							newFollowSet.add(newFollowTerminal);
    							expectedElements.add(newFollowTerminal);
    						}
    					}
    				}
    				currentFollowSet.clear();
    				currentFollowSet.addAll(newFollowSet);
    			}
    			followSetID++;
    		}
    		// after the last token in the stream we must set the position for the elements
    		// that were added during the last iteration of the loop
    		for (ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal nextFollow : newFollowSet) {
    			lastTokenIndex = 0;
    			setPosition(nextFollow, i);
    		}
    		return this.expectedElements;
    	}
    	
    	public void setPosition(ofbiz.resource.ofbiz.mopp.OfbizExpectedTerminal expectedElement, int tokenIndex) {
    		int currentIndex = Math.max(0, tokenIndex);
    		for (int index = lastTokenIndex; index < currentIndex; index++) {
    			if (index >= input.size()) {
    				break;
    			}
    			org.antlr.runtime3_4_0.CommonToken tokenAtIndex = (org.antlr.runtime3_4_0.CommonToken) input.get(index);
    			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
    				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			}
    		}
    		lastTokenIndex = Math.max(0, currentIndex);
    		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
    	}
    	
    	public Object recoverFromMismatchedToken(org.antlr.runtime3_4_0.IntStream input, int ttype, org.antlr.runtime3_4_0.BitSet follow) throws org.antlr.runtime3_4_0.RecognitionException {
    		if (!rememberExpectedElements) {
    			return super.recoverFromMismatchedToken(input, ttype, follow);
    		} else {
    			return null;
    		}
    	}
    	
    	/**
    	 * Translates errors thrown by the parser into human readable messages.
    	 */
    	public void reportError(final org.antlr.runtime3_4_0.RecognitionException e)  {
    		String message = e.getMessage();
    		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
    			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
    			String expectedTokenName = formatTokenName(mte.expecting);
    			String actualTokenName = formatTokenName(e.token.getType());
    			message = "Syntax error on token \"" + e.token.getText() + " (" + actualTokenName + ")\", \"" + expectedTokenName + "\" expected";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedTreeNodeException) {
    			org.antlr.runtime3_4_0.MismatchedTreeNodeException mtne = (org.antlr.runtime3_4_0.MismatchedTreeNodeException) e;
    			String expectedTokenName = formatTokenName(mtne.expecting);
    			message = "mismatched tree node: " + "xxx" + "; tokenName " + expectedTokenName;
    		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
    			message = "Syntax error on token \"" + e.token.getText() + "\", check following tokens";
    		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
    			message = "Syntax error on token \"" + e.token.getText() + "\", delete this token";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
    			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
    			message = "mismatched token: " + e.token + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
    			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
    			message = "mismatched token: " +  e.token + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
    			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
    			message = "rule " + fpe.ruleName + " failed predicate: {" +  fpe.predicateText + "}?";
    		}
    		// the resource may be null if the parser is used for code completion
    		final String finalMessage = message;
    		if (e.token instanceof org.antlr.runtime3_4_0.CommonToken) {
    			final org.antlr.runtime3_4_0.CommonToken ct = (org.antlr.runtime3_4_0.CommonToken) e.token;
    			addErrorToResource(finalMessage, ct.getCharPositionInLine(), ct.getLine(), ct.getStartIndex(), ct.getStopIndex());
    		} else {
    			addErrorToResource(finalMessage, e.token.getCharPositionInLine(), e.token.getLine(), 1, 5);
    		}
    	}
    	
    	/**
    	 * Translates errors thrown by the lexer into human readable messages.
    	 */
    	public void reportLexicalError(final org.antlr.runtime3_4_0.RecognitionException e)  {
    		String message = "";
    		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
    			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
    			message = "Syntax error on token \"" + ((char) e.c) + "\", \"" + (char) mte.expecting + "\" expected";
    		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
    			message = "Syntax error on token \"" + ((char) e.c) + "\", delete this token";
    		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
    			org.antlr.runtime3_4_0.EarlyExitException eee = (org.antlr.runtime3_4_0.EarlyExitException) e;
    			message = "required (...)+ loop (decision=" + eee.decisionNumber + ") did not match anything; on line " + e.line + ":" + e.charPositionInLine + " char=" + ((char) e.c) + "'";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
    			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
    			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedRangeException) {
    			org.antlr.runtime3_4_0.MismatchedRangeException mre = (org.antlr.runtime3_4_0.MismatchedRangeException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set '" + (char) mre.a + "'..'" + (char) mre.b + "'";
    		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
    			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
    			message = "rule " + fpe.ruleName + " failed predicate: {" + fpe.predicateText + "}?";
    		}
    		addErrorToResource(message, e.charPositionInLine, e.line, lexerExceptionsPosition.get(lexerExceptions.indexOf(e)), lexerExceptionsPosition.get(lexerExceptions.indexOf(e)));
    	}
    	
    	private void startIncompleteElement(Object object) {
    		if (object instanceof org.eclipse.emf.ecore.EObject) {
    			this.incompleteObjects.add((org.eclipse.emf.ecore.EObject) object);
    		}
    	}
    	
    	private void completedElement(Object object, boolean isContainment) {
    		if (isContainment && !this.incompleteObjects.isEmpty()) {
    			boolean exists = this.incompleteObjects.remove(object);
    			if (!exists) {
    			}
    		}
    		if (object instanceof org.eclipse.emf.ecore.EObject) {
    			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
    			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
    		}
    	}
    	
    	private org.eclipse.emf.ecore.EObject getLastIncompleteElement() {
    		if (incompleteObjects.isEmpty()) {
    			return null;
    		}
    		return incompleteObjects.get(incompleteObjects.size() - 1);
    	}
    	



    // $ANTLR start "start"
    // Ofbiz.g:535:1: start returns [ org.eclipse.emf.ecore.EObject element = null] : (c0= parse_ofbiz_OfbizComponent ) EOF ;
    public final org.eclipse.emf.ecore.EObject start() throws RecognitionException {
        org.eclipse.emf.ecore.EObject element =  null;

        int start_StartIndex = input.index();

        ofbiz.OfbizComponent c0 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 1) ) { return element; }

            // Ofbiz.g:536:2: ( (c0= parse_ofbiz_OfbizComponent ) EOF )
            // Ofbiz.g:537:2: (c0= parse_ofbiz_OfbizComponent ) EOF
            {
            if ( state.backtracking==0 ) {
            		// follow set for start rule(s)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[0]);
            		expectedElementsIndexOfLastCompleteElement = 0;
            	}

            // Ofbiz.g:542:2: (c0= parse_ofbiz_OfbizComponent )
            // Ofbiz.g:543:3: c0= parse_ofbiz_OfbizComponent
            {
            pushFollow(FOLLOW_parse_ofbiz_OfbizComponent_in_start82);
            c0=parse_ofbiz_OfbizComponent();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c0; }

            }


            match(input,EOF,FOLLOW_EOF_in_start89); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		retrieveLayoutInformation(element, null, null, false);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 1, start_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "start"



    // $ANTLR start "parse_ofbiz_OfbizComponent"
    // Ofbiz.g:551:1: parse_ofbiz_OfbizComponent returns [ofbiz.OfbizComponent element = null] : a0= 'OfbizComponent' a1= '{' ( (a2= 'entitymodel' a3= ':' (a4_0= parse_ofbiz_EntityModel ) |a5= 'services' a6= ':' (a7_0= parse_ofbiz_Services ) |a8= 'rest' a9= ':' (a10_0= parse_ofbiz_RestService ) ) )* a11= '}' ;
    public final ofbiz.OfbizComponent parse_ofbiz_OfbizComponent() throws RecognitionException {
        ofbiz.OfbizComponent element =  null;

        int parse_ofbiz_OfbizComponent_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a5=null;
        Token a6=null;
        Token a8=null;
        Token a9=null;
        Token a11=null;
        ofbiz.EntityModel a4_0 =null;

        ofbiz.Services a7_0 =null;

        ofbiz.RestService a10_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 2) ) { return element; }

            // Ofbiz.g:554:2: (a0= 'OfbizComponent' a1= '{' ( (a2= 'entitymodel' a3= ':' (a4_0= parse_ofbiz_EntityModel ) |a5= 'services' a6= ':' (a7_0= parse_ofbiz_Services ) |a8= 'rest' a9= ':' (a10_0= parse_ofbiz_RestService ) ) )* a11= '}' )
            // Ofbiz.g:555:2: a0= 'OfbizComponent' a1= '{' ( (a2= 'entitymodel' a3= ':' (a4_0= parse_ofbiz_EntityModel ) |a5= 'services' a6= ':' (a7_0= parse_ofbiz_Services ) |a8= 'rest' a9= ':' (a10_0= parse_ofbiz_RestService ) ) )* a11= '}'
            {
            a0=(Token)match(input,28,FOLLOW_28_in_parse_ofbiz_OfbizComponent115); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[1]);
            	}

            a1=(Token)match(input,113,FOLLOW_113_in_parse_ofbiz_OfbizComponent129); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[2]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[3]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[4]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[5]);
            	}

            // Ofbiz.g:586:2: ( (a2= 'entitymodel' a3= ':' (a4_0= parse_ofbiz_EntityModel ) |a5= 'services' a6= ':' (a7_0= parse_ofbiz_Services ) |a8= 'rest' a9= ':' (a10_0= parse_ofbiz_RestService ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==57||LA2_0==95||LA2_0==99) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // Ofbiz.g:587:3: (a2= 'entitymodel' a3= ':' (a4_0= parse_ofbiz_EntityModel ) |a5= 'services' a6= ':' (a7_0= parse_ofbiz_Services ) |a8= 'rest' a9= ':' (a10_0= parse_ofbiz_RestService ) )
            	    {
            	    // Ofbiz.g:587:3: (a2= 'entitymodel' a3= ':' (a4_0= parse_ofbiz_EntityModel ) |a5= 'services' a6= ':' (a7_0= parse_ofbiz_Services ) |a8= 'rest' a9= ':' (a10_0= parse_ofbiz_RestService ) )
            	    int alt1=3;
            	    switch ( input.LA(1) ) {
            	    case 57:
            	        {
            	        alt1=1;
            	        }
            	        break;
            	    case 99:
            	        {
            	        alt1=2;
            	        }
            	        break;
            	    case 95:
            	        {
            	        alt1=3;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return element;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 1, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt1) {
            	        case 1 :
            	            // Ofbiz.g:588:4: a2= 'entitymodel' a3= ':' (a4_0= parse_ofbiz_EntityModel )
            	            {
            	            a2=(Token)match(input,57,FOLLOW_57_in_parse_ofbiz_OfbizComponent152); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_2_0_0_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[6]);
            	            			}

            	            a3=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_OfbizComponent172); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_2_0_0_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getOfbizComponent(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[7]);
            	            			}

            	            // Ofbiz.g:616:4: (a4_0= parse_ofbiz_EntityModel )
            	            // Ofbiz.g:617:5: a4_0= parse_ofbiz_EntityModel
            	            {
            	            pushFollow(FOLLOW_parse_ofbiz_EntityModel_in_parse_ofbiz_OfbizComponent198);
            	            a4_0=parse_ofbiz_EntityModel();

            	            state._fsp--;
            	            if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a4_0 != null) {
            	            						if (a4_0 != null) {
            	            							Object value = a4_0;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.OFBIZ_COMPONENT__ENTITYMODEL), value);
            	            							completedElement(value, true);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_2_0_0_2, a4_0, true);
            	            						copyLocalizationInfos(a4_0, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[8]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[9]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[10]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[11]);
            	            			}

            	            }
            	            break;
            	        case 2 :
            	            // Ofbiz.g:646:8: a5= 'services' a6= ':' (a7_0= parse_ofbiz_Services )
            	            {
            	            a5=(Token)match(input,99,FOLLOW_99_in_parse_ofbiz_OfbizComponent234); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_2_0_1_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[12]);
            	            			}

            	            a6=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_OfbizComponent254); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_2_0_1_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getOfbizComponent(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[13]);
            	            			}

            	            // Ofbiz.g:674:4: (a7_0= parse_ofbiz_Services )
            	            // Ofbiz.g:675:5: a7_0= parse_ofbiz_Services
            	            {
            	            pushFollow(FOLLOW_parse_ofbiz_Services_in_parse_ofbiz_OfbizComponent280);
            	            a7_0=parse_ofbiz_Services();

            	            state._fsp--;
            	            if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a7_0 != null) {
            	            						if (a7_0 != null) {
            	            							Object value = a7_0;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.OFBIZ_COMPONENT__SERVICES), value);
            	            							completedElement(value, true);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_2_0_1_2, a7_0, true);
            	            						copyLocalizationInfos(a7_0, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[14]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[15]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[16]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[17]);
            	            			}

            	            }
            	            break;
            	        case 3 :
            	            // Ofbiz.g:704:8: a8= 'rest' a9= ':' (a10_0= parse_ofbiz_RestService )
            	            {
            	            a8=(Token)match(input,95,FOLLOW_95_in_parse_ofbiz_OfbizComponent316); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_2_0_2_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[18]);
            	            			}

            	            a9=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_OfbizComponent336); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_2_0_2_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getOfbizComponent(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[19]);
            	            			}

            	            // Ofbiz.g:732:4: (a10_0= parse_ofbiz_RestService )
            	            // Ofbiz.g:733:5: a10_0= parse_ofbiz_RestService
            	            {
            	            pushFollow(FOLLOW_parse_ofbiz_RestService_in_parse_ofbiz_OfbizComponent362);
            	            a10_0=parse_ofbiz_RestService();

            	            state._fsp--;
            	            if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a10_0 != null) {
            	            						if (a10_0 != null) {
            	            							Object value = a10_0;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.OFBIZ_COMPONENT__REST), value);
            	            							completedElement(value, true);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_2_0_2_2, a10_0, true);
            	            						copyLocalizationInfos(a10_0, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[20]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[21]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[22]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[23]);
            	            			}

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[24]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[25]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[26]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[27]);
            	}

            a11=(Token)match(input,114,FOLLOW_114_in_parse_ofbiz_OfbizComponent403); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createOfbizComponent();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_0_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 2, parse_ofbiz_OfbizComponent_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_ofbiz_OfbizComponent"



    // $ANTLR start "parse_ofbiz_EntityModel"
    // Ofbiz.g:786:1: parse_ofbiz_EntityModel returns [ofbiz.EntityModel element = null] : a0= 'EntityModel' a1= '{' ( (a2= 'title' a3= ':' (a4= QUOTED_34_34 ) |a5= 'description' a6= ':' (a7= QUOTED_34_34 ) |a8= 'version' a9= ':' (a10= QUOTED_34_34 ) |a11= 'entities' a12= ':' (a13_0= parse_ofbiz_Entity ) ) )* a14= '}' ;
    public final ofbiz.EntityModel parse_ofbiz_EntityModel() throws RecognitionException {
        ofbiz.EntityModel element =  null;

        int parse_ofbiz_EntityModel_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;
        Token a9=null;
        Token a10=null;
        Token a11=null;
        Token a12=null;
        Token a14=null;
        ofbiz.Entity a13_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 3) ) { return element; }

            // Ofbiz.g:789:2: (a0= 'EntityModel' a1= '{' ( (a2= 'title' a3= ':' (a4= QUOTED_34_34 ) |a5= 'description' a6= ':' (a7= QUOTED_34_34 ) |a8= 'version' a9= ':' (a10= QUOTED_34_34 ) |a11= 'entities' a12= ':' (a13_0= parse_ofbiz_Entity ) ) )* a14= '}' )
            // Ofbiz.g:790:2: a0= 'EntityModel' a1= '{' ( (a2= 'title' a3= ':' (a4= QUOTED_34_34 ) |a5= 'description' a6= ':' (a7= QUOTED_34_34 ) |a8= 'version' a9= ':' (a10= QUOTED_34_34 ) |a11= 'entities' a12= ':' (a13_0= parse_ofbiz_Entity ) ) )* a14= '}'
            {
            a0=(Token)match(input,17,FOLLOW_17_in_parse_ofbiz_EntityModel432); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[28]);
            	}

            a1=(Token)match(input,113,FOLLOW_113_in_parse_ofbiz_EntityModel446); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[29]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[30]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[31]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[32]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[33]);
            	}

            // Ofbiz.g:822:2: ( (a2= 'title' a3= ':' (a4= QUOTED_34_34 ) |a5= 'description' a6= ':' (a7= QUOTED_34_34 ) |a8= 'version' a9= ':' (a10= QUOTED_34_34 ) |a11= 'entities' a12= ':' (a13_0= parse_ofbiz_Entity ) ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==50||LA4_0==53||LA4_0==105||LA4_0==110) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // Ofbiz.g:823:3: (a2= 'title' a3= ':' (a4= QUOTED_34_34 ) |a5= 'description' a6= ':' (a7= QUOTED_34_34 ) |a8= 'version' a9= ':' (a10= QUOTED_34_34 ) |a11= 'entities' a12= ':' (a13_0= parse_ofbiz_Entity ) )
            	    {
            	    // Ofbiz.g:823:3: (a2= 'title' a3= ':' (a4= QUOTED_34_34 ) |a5= 'description' a6= ':' (a7= QUOTED_34_34 ) |a8= 'version' a9= ':' (a10= QUOTED_34_34 ) |a11= 'entities' a12= ':' (a13_0= parse_ofbiz_Entity ) )
            	    int alt3=4;
            	    switch ( input.LA(1) ) {
            	    case 105:
            	        {
            	        alt3=1;
            	        }
            	        break;
            	    case 50:
            	        {
            	        alt3=2;
            	        }
            	        break;
            	    case 110:
            	        {
            	        alt3=3;
            	        }
            	        break;
            	    case 53:
            	        {
            	        alt3=4;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return element;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 3, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt3) {
            	        case 1 :
            	            // Ofbiz.g:824:4: a2= 'title' a3= ':' (a4= QUOTED_34_34 )
            	            {
            	            a2=(Token)match(input,105,FOLLOW_105_in_parse_ofbiz_EntityModel469); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_0_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[34]);
            	            			}

            	            a3=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_EntityModel489); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_0_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[35]);
            	            			}

            	            // Ofbiz.g:852:4: (a4= QUOTED_34_34 )
            	            // Ofbiz.g:853:5: a4= QUOTED_34_34
            	            {
            	            a4=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_ofbiz_EntityModel515); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a4 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__TITLE), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            	            						}
            	            						java.lang.String resolved = (java.lang.String) resolvedObject;
            	            						if (resolved != null) {
            	            							Object value = resolved;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__TITLE), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_0_2, resolved, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[36]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[37]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[38]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[39]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[40]);
            	            			}

            	            }
            	            break;
            	        case 2 :
            	            // Ofbiz.g:893:8: a5= 'description' a6= ':' (a7= QUOTED_34_34 )
            	            {
            	            a5=(Token)match(input,50,FOLLOW_50_in_parse_ofbiz_EntityModel556); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_1_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[41]);
            	            			}

            	            a6=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_EntityModel576); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_1_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[42]);
            	            			}

            	            // Ofbiz.g:921:4: (a7= QUOTED_34_34 )
            	            // Ofbiz.g:922:5: a7= QUOTED_34_34
            	            {
            	            a7=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_ofbiz_EntityModel602); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a7 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__DESCRIPTION), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
            	            						}
            	            						java.lang.String resolved = (java.lang.String) resolvedObject;
            	            						if (resolved != null) {
            	            							Object value = resolved;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__DESCRIPTION), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_1_2, resolved, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[43]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[44]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[45]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[46]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[47]);
            	            			}

            	            }
            	            break;
            	        case 3 :
            	            // Ofbiz.g:962:8: a8= 'version' a9= ':' (a10= QUOTED_34_34 )
            	            {
            	            a8=(Token)match(input,110,FOLLOW_110_in_parse_ofbiz_EntityModel643); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_2_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[48]);
            	            			}

            	            a9=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_EntityModel663); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_2_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[49]);
            	            			}

            	            // Ofbiz.g:990:4: (a10= QUOTED_34_34 )
            	            // Ofbiz.g:991:5: a10= QUOTED_34_34
            	            {
            	            a10=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_ofbiz_EntityModel689); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a10 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a10.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__VERSION), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a10).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a10).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a10).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a10).getStopIndex());
            	            						}
            	            						java.lang.String resolved = (java.lang.String) resolvedObject;
            	            						if (resolved != null) {
            	            							Object value = resolved;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__VERSION), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_2_2, resolved, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a10, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[50]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[51]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[52]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[53]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[54]);
            	            			}

            	            }
            	            break;
            	        case 4 :
            	            // Ofbiz.g:1031:8: a11= 'entities' a12= ':' (a13_0= parse_ofbiz_Entity )
            	            {
            	            a11=(Token)match(input,53,FOLLOW_53_in_parse_ofbiz_EntityModel730); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_3_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[55]);
            	            			}

            	            a12=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_EntityModel750); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_3_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getEntityModel(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[56]);
            	            			}

            	            // Ofbiz.g:1059:4: (a13_0= parse_ofbiz_Entity )
            	            // Ofbiz.g:1060:5: a13_0= parse_ofbiz_Entity
            	            {
            	            pushFollow(FOLLOW_parse_ofbiz_Entity_in_parse_ofbiz_EntityModel776);
            	            a13_0=parse_ofbiz_Entity();

            	            state._fsp--;
            	            if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a13_0 != null) {
            	            						if (a13_0 != null) {
            	            							Object value = a13_0;
            	            							addObjectToList(element, ofbiz.OfbizPackage.ENTITY_MODEL__ENTITIES, value);
            	            							completedElement(value, true);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_2_0_3_2, a13_0, true);
            	            						copyLocalizationInfos(a13_0, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[57]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[58]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[59]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[60]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[61]);
            	            			}

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[62]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[63]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[64]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[65]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[66]);
            	}

            a14=(Token)match(input,114,FOLLOW_114_in_parse_ofbiz_EntityModel817); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createEntityModel();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_1_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a14, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[67]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[68]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[69]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[70]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 3, parse_ofbiz_EntityModel_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_ofbiz_EntityModel"



    // $ANTLR start "parse_ofbiz_Entity"
    // Ofbiz.g:1119:1: parse_ofbiz_Entity returns [ofbiz.Entity element = null] : a0= 'Entity' a1= '{' ( (a2= 'entityName' a3= ':' (a4= QUOTED_34_34 ) |a5= 'packageName' a6= ':' (a7= QUOTED_34_34 ) |a8= 'title' a9= ':' (a10= QUOTED_34_34 ) |a11= 'primKey' a12= ':' (a13= TEXT ) |a14= 'fields' a15= ':' (a16_0= parse_ofbiz_Field ) |a17= 'relations' a18= ':' (a19_0= parse_ofbiz_Relation ) ) )* a20= '}' ;
    public final ofbiz.Entity parse_ofbiz_Entity() throws RecognitionException {
        ofbiz.Entity element =  null;

        int parse_ofbiz_Entity_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;
        Token a9=null;
        Token a10=null;
        Token a11=null;
        Token a12=null;
        Token a13=null;
        Token a14=null;
        Token a15=null;
        Token a17=null;
        Token a18=null;
        Token a20=null;
        ofbiz.Field a16_0 =null;

        ofbiz.Relation a19_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 4) ) { return element; }

            // Ofbiz.g:1122:2: (a0= 'Entity' a1= '{' ( (a2= 'entityName' a3= ':' (a4= QUOTED_34_34 ) |a5= 'packageName' a6= ':' (a7= QUOTED_34_34 ) |a8= 'title' a9= ':' (a10= QUOTED_34_34 ) |a11= 'primKey' a12= ':' (a13= TEXT ) |a14= 'fields' a15= ':' (a16_0= parse_ofbiz_Field ) |a17= 'relations' a18= ':' (a19_0= parse_ofbiz_Relation ) ) )* a20= '}' )
            // Ofbiz.g:1123:2: a0= 'Entity' a1= '{' ( (a2= 'entityName' a3= ':' (a4= QUOTED_34_34 ) |a5= 'packageName' a6= ':' (a7= QUOTED_34_34 ) |a8= 'title' a9= ':' (a10= QUOTED_34_34 ) |a11= 'primKey' a12= ':' (a13= TEXT ) |a14= 'fields' a15= ':' (a16_0= parse_ofbiz_Field ) |a17= 'relations' a18= ':' (a19_0= parse_ofbiz_Relation ) ) )* a20= '}'
            {
            a0=(Token)match(input,16,FOLLOW_16_in_parse_ofbiz_Entity846); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[71]);
            	}

            a1=(Token)match(input,113,FOLLOW_113_in_parse_ofbiz_Entity860); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[72]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[73]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[74]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[75]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[76]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[77]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[78]);
            	}

            // Ofbiz.g:1157:2: ( (a2= 'entityName' a3= ':' (a4= QUOTED_34_34 ) |a5= 'packageName' a6= ':' (a7= QUOTED_34_34 ) |a8= 'title' a9= ':' (a10= QUOTED_34_34 ) |a11= 'primKey' a12= ':' (a13= TEXT ) |a14= 'fields' a15= ':' (a16_0= parse_ofbiz_Field ) |a17= 'relations' a18= ':' (a19_0= parse_ofbiz_Relation ) ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==56||LA6_0==58||LA6_0==88||LA6_0==90||LA6_0==92||LA6_0==105) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // Ofbiz.g:1158:3: (a2= 'entityName' a3= ':' (a4= QUOTED_34_34 ) |a5= 'packageName' a6= ':' (a7= QUOTED_34_34 ) |a8= 'title' a9= ':' (a10= QUOTED_34_34 ) |a11= 'primKey' a12= ':' (a13= TEXT ) |a14= 'fields' a15= ':' (a16_0= parse_ofbiz_Field ) |a17= 'relations' a18= ':' (a19_0= parse_ofbiz_Relation ) )
            	    {
            	    // Ofbiz.g:1158:3: (a2= 'entityName' a3= ':' (a4= QUOTED_34_34 ) |a5= 'packageName' a6= ':' (a7= QUOTED_34_34 ) |a8= 'title' a9= ':' (a10= QUOTED_34_34 ) |a11= 'primKey' a12= ':' (a13= TEXT ) |a14= 'fields' a15= ':' (a16_0= parse_ofbiz_Field ) |a17= 'relations' a18= ':' (a19_0= parse_ofbiz_Relation ) )
            	    int alt5=6;
            	    switch ( input.LA(1) ) {
            	    case 56:
            	        {
            	        alt5=1;
            	        }
            	        break;
            	    case 88:
            	        {
            	        alt5=2;
            	        }
            	        break;
            	    case 105:
            	        {
            	        alt5=3;
            	        }
            	        break;
            	    case 90:
            	        {
            	        alt5=4;
            	        }
            	        break;
            	    case 58:
            	        {
            	        alt5=5;
            	        }
            	        break;
            	    case 92:
            	        {
            	        alt5=6;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return element;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 5, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt5) {
            	        case 1 :
            	            // Ofbiz.g:1159:4: a2= 'entityName' a3= ':' (a4= QUOTED_34_34 )
            	            {
            	            a2=(Token)match(input,56,FOLLOW_56_in_parse_ofbiz_Entity883); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_0_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[79]);
            	            			}

            	            a3=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Entity903); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_0_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[80]);
            	            			}

            	            // Ofbiz.g:1187:4: (a4= QUOTED_34_34 )
            	            // Ofbiz.g:1188:5: a4= QUOTED_34_34
            	            {
            	            a4=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_ofbiz_Entity929); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a4 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__ENTITY_NAME), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            	            						}
            	            						java.lang.String resolved = (java.lang.String) resolvedObject;
            	            						if (resolved != null) {
            	            							Object value = resolved;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__ENTITY_NAME), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_0_2, resolved, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[81]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[82]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[83]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[84]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[85]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[86]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[87]);
            	            			}

            	            }
            	            break;
            	        case 2 :
            	            // Ofbiz.g:1230:8: a5= 'packageName' a6= ':' (a7= QUOTED_34_34 )
            	            {
            	            a5=(Token)match(input,88,FOLLOW_88_in_parse_ofbiz_Entity970); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_1_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[88]);
            	            			}

            	            a6=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Entity990); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_1_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[89]);
            	            			}

            	            // Ofbiz.g:1258:4: (a7= QUOTED_34_34 )
            	            // Ofbiz.g:1259:5: a7= QUOTED_34_34
            	            {
            	            a7=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_ofbiz_Entity1016); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a7 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__PACKAGE_NAME), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
            	            						}
            	            						java.lang.String resolved = (java.lang.String) resolvedObject;
            	            						if (resolved != null) {
            	            							Object value = resolved;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__PACKAGE_NAME), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_1_2, resolved, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[90]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[91]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[92]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[93]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[94]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[95]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[96]);
            	            			}

            	            }
            	            break;
            	        case 3 :
            	            // Ofbiz.g:1301:8: a8= 'title' a9= ':' (a10= QUOTED_34_34 )
            	            {
            	            a8=(Token)match(input,105,FOLLOW_105_in_parse_ofbiz_Entity1057); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_2_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[97]);
            	            			}

            	            a9=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Entity1077); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_2_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[98]);
            	            			}

            	            // Ofbiz.g:1329:4: (a10= QUOTED_34_34 )
            	            // Ofbiz.g:1330:5: a10= QUOTED_34_34
            	            {
            	            a10=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_ofbiz_Entity1103); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a10 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a10.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__TITLE), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a10).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a10).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a10).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a10).getStopIndex());
            	            						}
            	            						java.lang.String resolved = (java.lang.String) resolvedObject;
            	            						if (resolved != null) {
            	            							Object value = resolved;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__TITLE), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_2_2, resolved, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a10, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[99]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[100]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[101]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[102]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[103]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[104]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[105]);
            	            			}

            	            }
            	            break;
            	        case 4 :
            	            // Ofbiz.g:1372:8: a11= 'primKey' a12= ':' (a13= TEXT )
            	            {
            	            a11=(Token)match(input,90,FOLLOW_90_in_parse_ofbiz_Entity1144); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_3_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[106]);
            	            			}

            	            a12=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Entity1164); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_3_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[107]);
            	            			}

            	            // Ofbiz.g:1400:4: (a13= TEXT )
            	            // Ofbiz.g:1401:5: a13= TEXT
            	            {
            	            a13=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_ofbiz_Entity1190); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a13 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a13.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__PRIM_KEY), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a13).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a13).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a13).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a13).getStopIndex());
            	            						}
            	            						String resolved = (String) resolvedObject;
            	            						ofbiz.Field proxy = ofbiz.OfbizFactory.eINSTANCE.createField();
            	            						collectHiddenTokens(element);
            	            						registerContextDependentProxy(new ofbiz.resource.ofbiz.mopp.OfbizContextDependentURIFragmentFactory<ofbiz.Entity, ofbiz.Field>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEntityPrimKeyReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__PRIM_KEY), resolved, proxy);
            	            						if (proxy != null) {
            	            							Object value = proxy;
            	            							addObjectToList(element, ofbiz.OfbizPackage.ENTITY__PRIM_KEY, value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_3_2, proxy, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a13, element);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a13, proxy);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[108]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[109]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[110]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[111]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[112]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[113]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[114]);
            	            			}

            	            }
            	            break;
            	        case 5 :
            	            // Ofbiz.g:1447:8: a14= 'fields' a15= ':' (a16_0= parse_ofbiz_Field )
            	            {
            	            a14=(Token)match(input,58,FOLLOW_58_in_parse_ofbiz_Entity1231); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_4_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a14, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[115]);
            	            			}

            	            a15=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Entity1251); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_4_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a15, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getEntity(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[116]);
            	            			}

            	            // Ofbiz.g:1475:4: (a16_0= parse_ofbiz_Field )
            	            // Ofbiz.g:1476:5: a16_0= parse_ofbiz_Field
            	            {
            	            pushFollow(FOLLOW_parse_ofbiz_Field_in_parse_ofbiz_Entity1277);
            	            a16_0=parse_ofbiz_Field();

            	            state._fsp--;
            	            if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a16_0 != null) {
            	            						if (a16_0 != null) {
            	            							Object value = a16_0;
            	            							addObjectToList(element, ofbiz.OfbizPackage.ENTITY__FIELDS, value);
            	            							completedElement(value, true);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_4_2, a16_0, true);
            	            						copyLocalizationInfos(a16_0, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[117]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[118]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[119]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[120]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[121]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[122]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[123]);
            	            			}

            	            }
            	            break;
            	        case 6 :
            	            // Ofbiz.g:1508:8: a17= 'relations' a18= ':' (a19_0= parse_ofbiz_Relation )
            	            {
            	            a17=(Token)match(input,92,FOLLOW_92_in_parse_ofbiz_Entity1313); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_5_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a17, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[124]);
            	            			}

            	            a18=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Entity1333); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_5_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a18, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getEntity(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[125]);
            	            			}

            	            // Ofbiz.g:1536:4: (a19_0= parse_ofbiz_Relation )
            	            // Ofbiz.g:1537:5: a19_0= parse_ofbiz_Relation
            	            {
            	            pushFollow(FOLLOW_parse_ofbiz_Relation_in_parse_ofbiz_Entity1359);
            	            a19_0=parse_ofbiz_Relation();

            	            state._fsp--;
            	            if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a19_0 != null) {
            	            						if (a19_0 != null) {
            	            							Object value = a19_0;
            	            							addObjectToList(element, ofbiz.OfbizPackage.ENTITY__RELATIONS, value);
            	            							completedElement(value, true);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_2_0_5_2, a19_0, true);
            	            						copyLocalizationInfos(a19_0, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[126]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[127]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[128]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[129]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[130]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[131]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[132]);
            	            			}

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[133]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[134]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[135]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[136]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[137]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[138]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[139]);
            	}

            a20=(Token)match(input,114,FOLLOW_114_in_parse_ofbiz_Entity1400); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_2_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a20, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[140]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[141]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[142]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[143]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[144]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 4, parse_ofbiz_Entity_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_ofbiz_Entity"



    // $ANTLR start "parse_ofbiz_Field"
    // Ofbiz.g:1601:1: parse_ofbiz_Field returns [ofbiz.Field element = null] : a0= 'Field' a1= '{' ( (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) |a5= 'type' a6= ':' ( (a7= 'object' |a8= 'blob' |a9= 'byte-array' |a10= 'date-time' |a11= 'date' |a12= 'time' |a13= 'currency-ammount' |a14= 'currency-precise' |a15= 'fixed-point' |a16= 'floating-point' |a17= 'numeric' |a18= 'id' |a19= 'id-long' |a20= 'id-vlong' |a21= 'indicator' |a22= 'very-short' |a23= 'short-varchar' |a24= 'long-varchar' |a25= 'very-long' |a26= 'comment' |a27= 'description' |a28= 'name' |a29= 'value' |a30= 'credit-card-number' |a31= 'credit-card-date' |a32= 'email' |a33= 'url' |a34= 'id-ne' |a35= 'id-long-ne' |a36= 'id-vlong-ne' |a37= 'tel-number' ) ) ) )* a40= '}' ;
    public final ofbiz.Field parse_ofbiz_Field() throws RecognitionException {
        ofbiz.Field element =  null;

        int parse_ofbiz_Field_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;
        Token a9=null;
        Token a10=null;
        Token a11=null;
        Token a12=null;
        Token a13=null;
        Token a14=null;
        Token a15=null;
        Token a16=null;
        Token a17=null;
        Token a18=null;
        Token a19=null;
        Token a20=null;
        Token a21=null;
        Token a22=null;
        Token a23=null;
        Token a24=null;
        Token a25=null;
        Token a26=null;
        Token a27=null;
        Token a28=null;
        Token a29=null;
        Token a30=null;
        Token a31=null;
        Token a32=null;
        Token a33=null;
        Token a34=null;
        Token a35=null;
        Token a36=null;
        Token a37=null;
        Token a40=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 5) ) { return element; }

            // Ofbiz.g:1604:2: (a0= 'Field' a1= '{' ( (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) |a5= 'type' a6= ':' ( (a7= 'object' |a8= 'blob' |a9= 'byte-array' |a10= 'date-time' |a11= 'date' |a12= 'time' |a13= 'currency-ammount' |a14= 'currency-precise' |a15= 'fixed-point' |a16= 'floating-point' |a17= 'numeric' |a18= 'id' |a19= 'id-long' |a20= 'id-vlong' |a21= 'indicator' |a22= 'very-short' |a23= 'short-varchar' |a24= 'long-varchar' |a25= 'very-long' |a26= 'comment' |a27= 'description' |a28= 'name' |a29= 'value' |a30= 'credit-card-number' |a31= 'credit-card-date' |a32= 'email' |a33= 'url' |a34= 'id-ne' |a35= 'id-long-ne' |a36= 'id-vlong-ne' |a37= 'tel-number' ) ) ) )* a40= '}' )
            // Ofbiz.g:1605:2: a0= 'Field' a1= '{' ( (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) |a5= 'type' a6= ':' ( (a7= 'object' |a8= 'blob' |a9= 'byte-array' |a10= 'date-time' |a11= 'date' |a12= 'time' |a13= 'currency-ammount' |a14= 'currency-precise' |a15= 'fixed-point' |a16= 'floating-point' |a17= 'numeric' |a18= 'id' |a19= 'id-long' |a20= 'id-vlong' |a21= 'indicator' |a22= 'very-short' |a23= 'short-varchar' |a24= 'long-varchar' |a25= 'very-long' |a26= 'comment' |a27= 'description' |a28= 'name' |a29= 'value' |a30= 'credit-card-number' |a31= 'credit-card-date' |a32= 'email' |a33= 'url' |a34= 'id-ne' |a35= 'id-long-ne' |a36= 'id-vlong-ne' |a37= 'tel-number' ) ) ) )* a40= '}'
            {
            a0=(Token)match(input,19,FOLLOW_19_in_parse_ofbiz_Field1429); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createField();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[145]);
            	}

            a1=(Token)match(input,113,FOLLOW_113_in_parse_ofbiz_Field1443); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createField();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[146]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[147]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[148]);
            	}

            // Ofbiz.g:1635:2: ( (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) |a5= 'type' a6= ':' ( (a7= 'object' |a8= 'blob' |a9= 'byte-array' |a10= 'date-time' |a11= 'date' |a12= 'time' |a13= 'currency-ammount' |a14= 'currency-precise' |a15= 'fixed-point' |a16= 'floating-point' |a17= 'numeric' |a18= 'id' |a19= 'id-long' |a20= 'id-vlong' |a21= 'indicator' |a22= 'very-short' |a23= 'short-varchar' |a24= 'long-varchar' |a25= 'very-long' |a26= 'comment' |a27= 'description' |a28= 'name' |a29= 'value' |a30= 'credit-card-number' |a31= 'credit-card-date' |a32= 'email' |a33= 'url' |a34= 'id-ne' |a35= 'id-long-ne' |a36= 'id-vlong-ne' |a37= 'tel-number' ) ) ) )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==83||LA9_0==106) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // Ofbiz.g:1636:3: (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) |a5= 'type' a6= ':' ( (a7= 'object' |a8= 'blob' |a9= 'byte-array' |a10= 'date-time' |a11= 'date' |a12= 'time' |a13= 'currency-ammount' |a14= 'currency-precise' |a15= 'fixed-point' |a16= 'floating-point' |a17= 'numeric' |a18= 'id' |a19= 'id-long' |a20= 'id-vlong' |a21= 'indicator' |a22= 'very-short' |a23= 'short-varchar' |a24= 'long-varchar' |a25= 'very-long' |a26= 'comment' |a27= 'description' |a28= 'name' |a29= 'value' |a30= 'credit-card-number' |a31= 'credit-card-date' |a32= 'email' |a33= 'url' |a34= 'id-ne' |a35= 'id-long-ne' |a36= 'id-vlong-ne' |a37= 'tel-number' ) ) )
            	    {
            	    // Ofbiz.g:1636:3: (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) |a5= 'type' a6= ':' ( (a7= 'object' |a8= 'blob' |a9= 'byte-array' |a10= 'date-time' |a11= 'date' |a12= 'time' |a13= 'currency-ammount' |a14= 'currency-precise' |a15= 'fixed-point' |a16= 'floating-point' |a17= 'numeric' |a18= 'id' |a19= 'id-long' |a20= 'id-vlong' |a21= 'indicator' |a22= 'very-short' |a23= 'short-varchar' |a24= 'long-varchar' |a25= 'very-long' |a26= 'comment' |a27= 'description' |a28= 'name' |a29= 'value' |a30= 'credit-card-number' |a31= 'credit-card-date' |a32= 'email' |a33= 'url' |a34= 'id-ne' |a35= 'id-long-ne' |a36= 'id-vlong-ne' |a37= 'tel-number' ) ) )
            	    int alt8=2;
            	    int LA8_0 = input.LA(1);

            	    if ( (LA8_0==83) ) {
            	        alt8=1;
            	    }
            	    else if ( (LA8_0==106) ) {
            	        alt8=2;
            	    }
            	    else {
            	        if (state.backtracking>0) {state.failed=true; return element;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 8, 0, input);

            	        throw nvae;

            	    }
            	    switch (alt8) {
            	        case 1 :
            	            // Ofbiz.g:1637:4: a2= 'name' a3= ':' (a4= QUOTED_34_34 )
            	            {
            	            a2=(Token)match(input,83,FOLLOW_83_in_parse_ofbiz_Field1466); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_0_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[149]);
            	            			}

            	            a3=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Field1486); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_0_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[150]);
            	            			}

            	            // Ofbiz.g:1665:4: (a4= QUOTED_34_34 )
            	            // Ofbiz.g:1666:5: a4= QUOTED_34_34
            	            {
            	            a4=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_ofbiz_Field1512); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a4 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__NAME), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            	            						}
            	            						java.lang.String resolved = (java.lang.String) resolvedObject;
            	            						if (resolved != null) {
            	            							Object value = resolved;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__NAME), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_0_2, resolved, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[151]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[152]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[153]);
            	            			}

            	            }
            	            break;
            	        case 2 :
            	            // Ofbiz.g:1704:8: a5= 'type' a6= ':' ( (a7= 'object' |a8= 'blob' |a9= 'byte-array' |a10= 'date-time' |a11= 'date' |a12= 'time' |a13= 'currency-ammount' |a14= 'currency-precise' |a15= 'fixed-point' |a16= 'floating-point' |a17= 'numeric' |a18= 'id' |a19= 'id-long' |a20= 'id-vlong' |a21= 'indicator' |a22= 'very-short' |a23= 'short-varchar' |a24= 'long-varchar' |a25= 'very-long' |a26= 'comment' |a27= 'description' |a28= 'name' |a29= 'value' |a30= 'credit-card-number' |a31= 'credit-card-date' |a32= 'email' |a33= 'url' |a34= 'id-ne' |a35= 'id-long-ne' |a36= 'id-vlong-ne' |a37= 'tel-number' ) )
            	            {
            	            a5=(Token)match(input,106,FOLLOW_106_in_parse_ofbiz_Field1553); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[154]);
            	            			}

            	            a6=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Field1573); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[155]);
            	            			}

            	            // Ofbiz.g:1732:4: ( (a7= 'object' |a8= 'blob' |a9= 'byte-array' |a10= 'date-time' |a11= 'date' |a12= 'time' |a13= 'currency-ammount' |a14= 'currency-precise' |a15= 'fixed-point' |a16= 'floating-point' |a17= 'numeric' |a18= 'id' |a19= 'id-long' |a20= 'id-vlong' |a21= 'indicator' |a22= 'very-short' |a23= 'short-varchar' |a24= 'long-varchar' |a25= 'very-long' |a26= 'comment' |a27= 'description' |a28= 'name' |a29= 'value' |a30= 'credit-card-number' |a31= 'credit-card-date' |a32= 'email' |a33= 'url' |a34= 'id-ne' |a35= 'id-long-ne' |a36= 'id-vlong-ne' |a37= 'tel-number' ) )
            	            // Ofbiz.g:1733:5: (a7= 'object' |a8= 'blob' |a9= 'byte-array' |a10= 'date-time' |a11= 'date' |a12= 'time' |a13= 'currency-ammount' |a14= 'currency-precise' |a15= 'fixed-point' |a16= 'floating-point' |a17= 'numeric' |a18= 'id' |a19= 'id-long' |a20= 'id-vlong' |a21= 'indicator' |a22= 'very-short' |a23= 'short-varchar' |a24= 'long-varchar' |a25= 'very-long' |a26= 'comment' |a27= 'description' |a28= 'name' |a29= 'value' |a30= 'credit-card-number' |a31= 'credit-card-date' |a32= 'email' |a33= 'url' |a34= 'id-ne' |a35= 'id-long-ne' |a36= 'id-vlong-ne' |a37= 'tel-number' )
            	            {
            	            // Ofbiz.g:1733:5: (a7= 'object' |a8= 'blob' |a9= 'byte-array' |a10= 'date-time' |a11= 'date' |a12= 'time' |a13= 'currency-ammount' |a14= 'currency-precise' |a15= 'fixed-point' |a16= 'floating-point' |a17= 'numeric' |a18= 'id' |a19= 'id-long' |a20= 'id-vlong' |a21= 'indicator' |a22= 'very-short' |a23= 'short-varchar' |a24= 'long-varchar' |a25= 'very-long' |a26= 'comment' |a27= 'description' |a28= 'name' |a29= 'value' |a30= 'credit-card-number' |a31= 'credit-card-date' |a32= 'email' |a33= 'url' |a34= 'id-ne' |a35= 'id-long-ne' |a36= 'id-vlong-ne' |a37= 'tel-number' )
            	            int alt7=31;
            	            switch ( input.LA(1) ) {
            	            case 85:
            	                {
            	                alt7=1;
            	                }
            	                break;
            	            case 41:
            	                {
            	                alt7=2;
            	                }
            	                break;
            	            case 42:
            	                {
            	                alt7=3;
            	                }
            	                break;
            	            case 49:
            	                {
            	                alt7=4;
            	                }
            	                break;
            	            case 48:
            	                {
            	                alt7=5;
            	                }
            	                break;
            	            case 104:
            	                {
            	                alt7=6;
            	                }
            	                break;
            	            case 46:
            	                {
            	                alt7=7;
            	                }
            	                break;
            	            case 47:
            	                {
            	                alt7=8;
            	                }
            	                break;
            	            case 59:
            	                {
            	                alt7=9;
            	                }
            	                break;
            	            case 61:
            	                {
            	                alt7=10;
            	                }
            	                break;
            	            case 84:
            	                {
            	                alt7=11;
            	                }
            	                break;
            	            case 65:
            	                {
            	                alt7=12;
            	                }
            	                break;
            	            case 66:
            	                {
            	                alt7=13;
            	                }
            	                break;
            	            case 69:
            	                {
            	                alt7=14;
            	                }
            	                break;
            	            case 71:
            	                {
            	                alt7=15;
            	                }
            	                break;
            	            case 112:
            	                {
            	                alt7=16;
            	                }
            	                break;
            	            case 100:
            	                {
            	                alt7=17;
            	                }
            	                break;
            	            case 81:
            	                {
            	                alt7=18;
            	                }
            	                break;
            	            case 111:
            	                {
            	                alt7=19;
            	                }
            	                break;
            	            case 43:
            	                {
            	                alt7=20;
            	                }
            	                break;
            	            case 50:
            	                {
            	                alt7=21;
            	                }
            	                break;
            	            case 83:
            	                {
            	                alt7=22;
            	                }
            	                break;
            	            case 109:
            	                {
            	                alt7=23;
            	                }
            	                break;
            	            case 45:
            	                {
            	                alt7=24;
            	                }
            	                break;
            	            case 44:
            	                {
            	                alt7=25;
            	                }
            	                break;
            	            case 51:
            	                {
            	                alt7=26;
            	                }
            	                break;
            	            case 107:
            	                {
            	                alt7=27;
            	                }
            	                break;
            	            case 68:
            	                {
            	                alt7=28;
            	                }
            	                break;
            	            case 67:
            	                {
            	                alt7=29;
            	                }
            	                break;
            	            case 70:
            	                {
            	                alt7=30;
            	                }
            	                break;
            	            case 103:
            	                {
            	                alt7=31;
            	                }
            	                break;
            	            default:
            	                if (state.backtracking>0) {state.failed=true; return element;}
            	                NoViableAltException nvae =
            	                    new NoViableAltException("", 7, 0, input);

            	                throw nvae;

            	            }

            	            switch (alt7) {
            	                case 1 :
            	                    // Ofbiz.g:1734:6: a7= 'object'
            	                    {
            	                    a7=(Token)match(input,85,FOLLOW_85_in_parse_ofbiz_Field1606); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.OBJECT_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 2 :
            	                    // Ofbiz.g:1747:12: a8= 'blob'
            	                    {
            	                    a8=(Token)match(input,41,FOLLOW_41_in_parse_ofbiz_Field1625); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.BLOB_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 3 :
            	                    // Ofbiz.g:1760:12: a9= 'byte-array'
            	                    {
            	                    a9=(Token)match(input,42,FOLLOW_42_in_parse_ofbiz_Field1644); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.BYTE_ARRAY_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 4 :
            	                    // Ofbiz.g:1773:12: a10= 'date-time'
            	                    {
            	                    a10=(Token)match(input,49,FOLLOW_49_in_parse_ofbiz_Field1663); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.DATE_TIME_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 5 :
            	                    // Ofbiz.g:1786:12: a11= 'date'
            	                    {
            	                    a11=(Token)match(input,48,FOLLOW_48_in_parse_ofbiz_Field1682); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.DATE_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 6 :
            	                    // Ofbiz.g:1799:12: a12= 'time'
            	                    {
            	                    a12=(Token)match(input,104,FOLLOW_104_in_parse_ofbiz_Field1701); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.TIME_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 7 :
            	                    // Ofbiz.g:1812:12: a13= 'currency-ammount'
            	                    {
            	                    a13=(Token)match(input,46,FOLLOW_46_in_parse_ofbiz_Field1720); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a13, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.CURRENCY_AMMOUNT_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 8 :
            	                    // Ofbiz.g:1825:12: a14= 'currency-precise'
            	                    {
            	                    a14=(Token)match(input,47,FOLLOW_47_in_parse_ofbiz_Field1739); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a14, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.CURRENCY_PRECISE_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 9 :
            	                    // Ofbiz.g:1838:12: a15= 'fixed-point'
            	                    {
            	                    a15=(Token)match(input,59,FOLLOW_59_in_parse_ofbiz_Field1758); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a15, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.FIXED_POINT_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 10 :
            	                    // Ofbiz.g:1851:12: a16= 'floating-point'
            	                    {
            	                    a16=(Token)match(input,61,FOLLOW_61_in_parse_ofbiz_Field1777); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a16, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.FLOATING_POINT_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 11 :
            	                    // Ofbiz.g:1864:12: a17= 'numeric'
            	                    {
            	                    a17=(Token)match(input,84,FOLLOW_84_in_parse_ofbiz_Field1796); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a17, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.NUMERIC_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 12 :
            	                    // Ofbiz.g:1877:12: a18= 'id'
            	                    {
            	                    a18=(Token)match(input,65,FOLLOW_65_in_parse_ofbiz_Field1815); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a18, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.ID_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 13 :
            	                    // Ofbiz.g:1890:12: a19= 'id-long'
            	                    {
            	                    a19=(Token)match(input,66,FOLLOW_66_in_parse_ofbiz_Field1834); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a19, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.ID_LONG_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 14 :
            	                    // Ofbiz.g:1903:12: a20= 'id-vlong'
            	                    {
            	                    a20=(Token)match(input,69,FOLLOW_69_in_parse_ofbiz_Field1853); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a20, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.ID_VLONG_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 15 :
            	                    // Ofbiz.g:1916:12: a21= 'indicator'
            	                    {
            	                    a21=(Token)match(input,71,FOLLOW_71_in_parse_ofbiz_Field1872); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a21, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.INDICATOR_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 16 :
            	                    // Ofbiz.g:1929:12: a22= 'very-short'
            	                    {
            	                    a22=(Token)match(input,112,FOLLOW_112_in_parse_ofbiz_Field1891); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a22, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.VERY_SHORT_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 17 :
            	                    // Ofbiz.g:1942:12: a23= 'short-varchar'
            	                    {
            	                    a23=(Token)match(input,100,FOLLOW_100_in_parse_ofbiz_Field1910); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a23, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.SHORT_VARCHAR_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 18 :
            	                    // Ofbiz.g:1955:12: a24= 'long-varchar'
            	                    {
            	                    a24=(Token)match(input,81,FOLLOW_81_in_parse_ofbiz_Field1929); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a24, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.LONG_VARCHAR_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 19 :
            	                    // Ofbiz.g:1968:12: a25= 'very-long'
            	                    {
            	                    a25=(Token)match(input,111,FOLLOW_111_in_parse_ofbiz_Field1948); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a25, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.VERY_LONG_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 20 :
            	                    // Ofbiz.g:1981:12: a26= 'comment'
            	                    {
            	                    a26=(Token)match(input,43,FOLLOW_43_in_parse_ofbiz_Field1967); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a26, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.COMMENT_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 21 :
            	                    // Ofbiz.g:1994:12: a27= 'description'
            	                    {
            	                    a27=(Token)match(input,50,FOLLOW_50_in_parse_ofbiz_Field1986); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a27, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.DESCRIPTION_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 22 :
            	                    // Ofbiz.g:2007:12: a28= 'name'
            	                    {
            	                    a28=(Token)match(input,83,FOLLOW_83_in_parse_ofbiz_Field2005); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a28, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.NAME_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 23 :
            	                    // Ofbiz.g:2020:12: a29= 'value'
            	                    {
            	                    a29=(Token)match(input,109,FOLLOW_109_in_parse_ofbiz_Field2024); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a29, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.VALUE_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 24 :
            	                    // Ofbiz.g:2033:12: a30= 'credit-card-number'
            	                    {
            	                    a30=(Token)match(input,45,FOLLOW_45_in_parse_ofbiz_Field2043); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a30, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.CREDIT_CARD_NUMBER_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 25 :
            	                    // Ofbiz.g:2046:12: a31= 'credit-card-date'
            	                    {
            	                    a31=(Token)match(input,44,FOLLOW_44_in_parse_ofbiz_Field2062); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a31, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.CREDIT_CARD_DATE_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 26 :
            	                    // Ofbiz.g:2059:12: a32= 'email'
            	                    {
            	                    a32=(Token)match(input,51,FOLLOW_51_in_parse_ofbiz_Field2081); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a32, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.EMAIL_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 27 :
            	                    // Ofbiz.g:2072:12: a33= 'url'
            	                    {
            	                    a33=(Token)match(input,107,FOLLOW_107_in_parse_ofbiz_Field2100); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a33, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.URL_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 28 :
            	                    // Ofbiz.g:2085:12: a34= 'id-ne'
            	                    {
            	                    a34=(Token)match(input,68,FOLLOW_68_in_parse_ofbiz_Field2119); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a34, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.ID_NE_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 29 :
            	                    // Ofbiz.g:2098:12: a35= 'id-long-ne'
            	                    {
            	                    a35=(Token)match(input,67,FOLLOW_67_in_parse_ofbiz_Field2138); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a35, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.ID_LONG_NE_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 30 :
            	                    // Ofbiz.g:2111:12: a36= 'id-vlong-ne'
            	                    {
            	                    a36=(Token)match(input,70,FOLLOW_70_in_parse_ofbiz_Field2157); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a36, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.ID_VLONG_NE_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 31 :
            	                    // Ofbiz.g:2124:12: a37= 'tel-number'
            	                    {
            	                    a37=(Token)match(input,103,FOLLOW_103_in_parse_ofbiz_Field2176); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createField();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a37, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getFieldType().getEEnumLiteral(ofbiz.FieldType.TEL_NUMBER_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;

            	            }


            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[156]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[157]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[158]);
            	            			}

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[159]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[160]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[161]);
            	}

            a40=(Token)match(input,114,FOLLOW_114_in_parse_ofbiz_Field2220); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createField();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_3_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a40, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[162]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[163]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[164]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[165]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[166]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[167]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[168]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 5, parse_ofbiz_Field_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_ofbiz_Field"



    // $ANTLR start "parse_ofbiz_Relation"
    // Ofbiz.g:2177:1: parse_ofbiz_Relation returns [ofbiz.Relation element = null] : a0= 'Relation' a1= '{' ( (a2= 'fkName' a3= ':' (a4= QUOTED_34_34 ) |a5= 'type' a6= ':' ( (a7= 'ONE' |a8= 'MANY' ) ) |a11= 'keyMap' a12= ':' (a13_0= parse_ofbiz_KeyMap ) |a14= 'related' a15= ':' (a16= TEXT ) ) )* a17= '}' ;
    public final ofbiz.Relation parse_ofbiz_Relation() throws RecognitionException {
        ofbiz.Relation element =  null;

        int parse_ofbiz_Relation_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;
        Token a11=null;
        Token a12=null;
        Token a14=null;
        Token a15=null;
        Token a16=null;
        Token a17=null;
        ofbiz.KeyMap a13_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 6) ) { return element; }

            // Ofbiz.g:2180:2: (a0= 'Relation' a1= '{' ( (a2= 'fkName' a3= ':' (a4= QUOTED_34_34 ) |a5= 'type' a6= ':' ( (a7= 'ONE' |a8= 'MANY' ) ) |a11= 'keyMap' a12= ':' (a13_0= parse_ofbiz_KeyMap ) |a14= 'related' a15= ':' (a16= TEXT ) ) )* a17= '}' )
            // Ofbiz.g:2181:2: a0= 'Relation' a1= '{' ( (a2= 'fkName' a3= ':' (a4= QUOTED_34_34 ) |a5= 'type' a6= ':' ( (a7= 'ONE' |a8= 'MANY' ) ) |a11= 'keyMap' a12= ':' (a13_0= parse_ofbiz_KeyMap ) |a14= 'related' a15= ':' (a16= TEXT ) ) )* a17= '}'
            {
            a0=(Token)match(input,31,FOLLOW_31_in_parse_ofbiz_Relation2249); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[169]);
            	}

            a1=(Token)match(input,113,FOLLOW_113_in_parse_ofbiz_Relation2263); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[170]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[171]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[172]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[173]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[174]);
            	}

            // Ofbiz.g:2213:2: ( (a2= 'fkName' a3= ':' (a4= QUOTED_34_34 ) |a5= 'type' a6= ':' ( (a7= 'ONE' |a8= 'MANY' ) ) |a11= 'keyMap' a12= ':' (a13_0= parse_ofbiz_KeyMap ) |a14= 'related' a15= ':' (a16= TEXT ) ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==60||LA12_0==79||LA12_0==91||LA12_0==106) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // Ofbiz.g:2214:3: (a2= 'fkName' a3= ':' (a4= QUOTED_34_34 ) |a5= 'type' a6= ':' ( (a7= 'ONE' |a8= 'MANY' ) ) |a11= 'keyMap' a12= ':' (a13_0= parse_ofbiz_KeyMap ) |a14= 'related' a15= ':' (a16= TEXT ) )
            	    {
            	    // Ofbiz.g:2214:3: (a2= 'fkName' a3= ':' (a4= QUOTED_34_34 ) |a5= 'type' a6= ':' ( (a7= 'ONE' |a8= 'MANY' ) ) |a11= 'keyMap' a12= ':' (a13_0= parse_ofbiz_KeyMap ) |a14= 'related' a15= ':' (a16= TEXT ) )
            	    int alt11=4;
            	    switch ( input.LA(1) ) {
            	    case 60:
            	        {
            	        alt11=1;
            	        }
            	        break;
            	    case 106:
            	        {
            	        alt11=2;
            	        }
            	        break;
            	    case 79:
            	        {
            	        alt11=3;
            	        }
            	        break;
            	    case 91:
            	        {
            	        alt11=4;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return element;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 11, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt11) {
            	        case 1 :
            	            // Ofbiz.g:2215:4: a2= 'fkName' a3= ':' (a4= QUOTED_34_34 )
            	            {
            	            a2=(Token)match(input,60,FOLLOW_60_in_parse_ofbiz_Relation2286); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_0_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[175]);
            	            			}

            	            a3=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Relation2306); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_0_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[176]);
            	            			}

            	            // Ofbiz.g:2243:4: (a4= QUOTED_34_34 )
            	            // Ofbiz.g:2244:5: a4= QUOTED_34_34
            	            {
            	            a4=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_ofbiz_Relation2332); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a4 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__FK_NAME), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            	            						}
            	            						java.lang.String resolved = (java.lang.String) resolvedObject;
            	            						if (resolved != null) {
            	            							Object value = resolved;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__FK_NAME), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_0_2, resolved, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[177]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[178]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[179]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[180]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[181]);
            	            			}

            	            }
            	            break;
            	        case 2 :
            	            // Ofbiz.g:2284:8: a5= 'type' a6= ':' ( (a7= 'ONE' |a8= 'MANY' ) )
            	            {
            	            a5=(Token)match(input,106,FOLLOW_106_in_parse_ofbiz_Relation2373); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_1_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[182]);
            	            			}

            	            a6=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Relation2393); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_1_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[183]);
            	            			}

            	            // Ofbiz.g:2312:4: ( (a7= 'ONE' |a8= 'MANY' ) )
            	            // Ofbiz.g:2313:5: (a7= 'ONE' |a8= 'MANY' )
            	            {
            	            // Ofbiz.g:2313:5: (a7= 'ONE' |a8= 'MANY' )
            	            int alt10=2;
            	            int LA10_0 = input.LA(1);

            	            if ( (LA10_0==26) ) {
            	                alt10=1;
            	            }
            	            else if ( (LA10_0==24) ) {
            	                alt10=2;
            	            }
            	            else {
            	                if (state.backtracking>0) {state.failed=true; return element;}
            	                NoViableAltException nvae =
            	                    new NoViableAltException("", 10, 0, input);

            	                throw nvae;

            	            }
            	            switch (alt10) {
            	                case 1 :
            	                    // Ofbiz.g:2314:6: a7= 'ONE'
            	                    {
            	                    a7=(Token)match(input,26,FOLLOW_26_in_parse_ofbiz_Relation2426); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getRelationType().getEEnumLiteral(ofbiz.RelationType.ONE_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 2 :
            	                    // Ofbiz.g:2327:12: a8= 'MANY'
            	                    {
            	                    a8=(Token)match(input,24,FOLLOW_24_in_parse_ofbiz_Relation2445); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getRelationType().getEEnumLiteral(ofbiz.RelationType.MANY_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;

            	            }


            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[184]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[185]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[186]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[187]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[188]);
            	            			}

            	            }
            	            break;
            	        case 3 :
            	            // Ofbiz.g:2352:8: a11= 'keyMap' a12= ':' (a13_0= parse_ofbiz_KeyMap )
            	            {
            	            a11=(Token)match(input,79,FOLLOW_79_in_parse_ofbiz_Relation2484); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_2_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[189]);
            	            			}

            	            a12=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Relation2504); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_2_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getRelation(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[190]);
            	            			}

            	            // Ofbiz.g:2380:4: (a13_0= parse_ofbiz_KeyMap )
            	            // Ofbiz.g:2381:5: a13_0= parse_ofbiz_KeyMap
            	            {
            	            pushFollow(FOLLOW_parse_ofbiz_KeyMap_in_parse_ofbiz_Relation2530);
            	            a13_0=parse_ofbiz_KeyMap();

            	            state._fsp--;
            	            if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a13_0 != null) {
            	            						if (a13_0 != null) {
            	            							Object value = a13_0;
            	            							addObjectToList(element, ofbiz.OfbizPackage.RELATION__KEY_MAP, value);
            	            							completedElement(value, true);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_2_2, a13_0, true);
            	            						copyLocalizationInfos(a13_0, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[191]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[192]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[193]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[194]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[195]);
            	            			}

            	            }
            	            break;
            	        case 4 :
            	            // Ofbiz.g:2411:8: a14= 'related' a15= ':' (a16= TEXT )
            	            {
            	            a14=(Token)match(input,91,FOLLOW_91_in_parse_ofbiz_Relation2566); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_3_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a14, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[196]);
            	            			}

            	            a15=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Relation2586); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_3_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a15, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[197]);
            	            			}

            	            // Ofbiz.g:2439:4: (a16= TEXT )
            	            // Ofbiz.g:2440:5: a16= TEXT
            	            {
            	            a16=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_ofbiz_Relation2612); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a16 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a16.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__RELATED), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a16).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a16).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a16).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a16).getStopIndex());
            	            						}
            	            						String resolved = (String) resolvedObject;
            	            						ofbiz.Entity proxy = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            	            						collectHiddenTokens(element);
            	            						registerContextDependentProxy(new ofbiz.resource.ofbiz.mopp.OfbizContextDependentURIFragmentFactory<ofbiz.Relation, ofbiz.Entity>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getRelationRelatedReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__RELATED), resolved, proxy);
            	            						if (proxy != null) {
            	            							Object value = proxy;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__RELATED), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_2_0_3_2, proxy, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a16, element);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a16, proxy);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[198]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[199]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[200]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[201]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[202]);
            	            			}

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[203]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[204]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[205]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[206]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[207]);
            	}

            a17=(Token)match(input,114,FOLLOW_114_in_parse_ofbiz_Relation2658); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createRelation();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_4_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a17, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[208]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[209]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[210]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[211]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[212]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[213]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[214]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 6, parse_ofbiz_Relation_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_ofbiz_Relation"



    // $ANTLR start "parse_ofbiz_KeyMap"
    // Ofbiz.g:2516:1: parse_ofbiz_KeyMap returns [ofbiz.KeyMap element = null] : a0= 'KeyMap' a1= '{' ( (a2= 'origin' a3= ':' (a4= TEXT ) |a5= 'related' a6= ':' (a7= TEXT ) ) )* a8= '}' ;
    public final ofbiz.KeyMap parse_ofbiz_KeyMap() throws RecognitionException {
        ofbiz.KeyMap element =  null;

        int parse_ofbiz_KeyMap_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 7) ) { return element; }

            // Ofbiz.g:2519:2: (a0= 'KeyMap' a1= '{' ( (a2= 'origin' a3= ':' (a4= TEXT ) |a5= 'related' a6= ':' (a7= TEXT ) ) )* a8= '}' )
            // Ofbiz.g:2520:2: a0= 'KeyMap' a1= '{' ( (a2= 'origin' a3= ':' (a4= TEXT ) |a5= 'related' a6= ':' (a7= TEXT ) ) )* a8= '}'
            {
            a0=(Token)match(input,22,FOLLOW_22_in_parse_ofbiz_KeyMap2687); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createKeyMap();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_5_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[215]);
            	}

            a1=(Token)match(input,113,FOLLOW_113_in_parse_ofbiz_KeyMap2701); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createKeyMap();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_5_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[216]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[217]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[218]);
            	}

            // Ofbiz.g:2550:2: ( (a2= 'origin' a3= ':' (a4= TEXT ) |a5= 'related' a6= ':' (a7= TEXT ) ) )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==87||LA14_0==91) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // Ofbiz.g:2551:3: (a2= 'origin' a3= ':' (a4= TEXT ) |a5= 'related' a6= ':' (a7= TEXT ) )
            	    {
            	    // Ofbiz.g:2551:3: (a2= 'origin' a3= ':' (a4= TEXT ) |a5= 'related' a6= ':' (a7= TEXT ) )
            	    int alt13=2;
            	    int LA13_0 = input.LA(1);

            	    if ( (LA13_0==87) ) {
            	        alt13=1;
            	    }
            	    else if ( (LA13_0==91) ) {
            	        alt13=2;
            	    }
            	    else {
            	        if (state.backtracking>0) {state.failed=true; return element;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 13, 0, input);

            	        throw nvae;

            	    }
            	    switch (alt13) {
            	        case 1 :
            	            // Ofbiz.g:2552:4: a2= 'origin' a3= ':' (a4= TEXT )
            	            {
            	            a2=(Token)match(input,87,FOLLOW_87_in_parse_ofbiz_KeyMap2724); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createKeyMap();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_5_0_0_2_0_0_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[219]);
            	            			}

            	            a3=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_KeyMap2744); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createKeyMap();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_5_0_0_2_0_0_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[220]);
            	            			}

            	            // Ofbiz.g:2580:4: (a4= TEXT )
            	            // Ofbiz.g:2581:5: a4= TEXT
            	            {
            	            a4=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_ofbiz_KeyMap2770); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createKeyMap();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a4 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__ORIGIN), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            	            						}
            	            						String resolved = (String) resolvedObject;
            	            						ofbiz.Field proxy = ofbiz.OfbizFactory.eINSTANCE.createField();
            	            						collectHiddenTokens(element);
            	            						registerContextDependentProxy(new ofbiz.resource.ofbiz.mopp.OfbizContextDependentURIFragmentFactory<ofbiz.KeyMap, ofbiz.Field>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getKeyMapOriginReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__ORIGIN), resolved, proxy);
            	            						if (proxy != null) {
            	            							Object value = proxy;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__ORIGIN), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_5_0_0_2_0_0_2, proxy, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, proxy);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[221]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[222]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[223]);
            	            			}

            	            }
            	            break;
            	        case 2 :
            	            // Ofbiz.g:2623:8: a5= 'related' a6= ':' (a7= TEXT )
            	            {
            	            a5=(Token)match(input,91,FOLLOW_91_in_parse_ofbiz_KeyMap2811); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createKeyMap();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_5_0_0_2_0_1_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[224]);
            	            			}

            	            a6=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_KeyMap2831); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createKeyMap();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_5_0_0_2_0_1_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[225]);
            	            			}

            	            // Ofbiz.g:2651:4: (a7= TEXT )
            	            // Ofbiz.g:2652:5: a7= TEXT
            	            {
            	            a7=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_ofbiz_KeyMap2857); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createKeyMap();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a7 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__RELATED), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
            	            						}
            	            						String resolved = (String) resolvedObject;
            	            						ofbiz.Field proxy = ofbiz.OfbizFactory.eINSTANCE.createField();
            	            						collectHiddenTokens(element);
            	            						registerContextDependentProxy(new ofbiz.resource.ofbiz.mopp.OfbizContextDependentURIFragmentFactory<ofbiz.KeyMap, ofbiz.Field>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getKeyMapRelatedReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__RELATED), resolved, proxy);
            	            						if (proxy != null) {
            	            							Object value = proxy;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__RELATED), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_5_0_0_2_0_1_2, proxy, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, proxy);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[226]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[227]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[228]);
            	            			}

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[229]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[230]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[231]);
            	}

            a8=(Token)match(input,114,FOLLOW_114_in_parse_ofbiz_KeyMap2903); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createKeyMap();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_5_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[232]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[233]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[234]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[235]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[236]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 7, parse_ofbiz_KeyMap_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_ofbiz_KeyMap"



    // $ANTLR start "parse_ofbiz_Services"
    // Ofbiz.g:2722:1: parse_ofbiz_Services returns [ofbiz.Services element = null] : a0= 'Services' a1= '{' ( (a2= 'description' a3= ':' (a4= QUOTED_34_34 ) |a5= 'version' a6= ':' (a7= QUOTED_34_34 ) |a8= 'services' a9= ':' (a10_0= parse_ofbiz_Service ) ) )* a11= '}' ;
    public final ofbiz.Services parse_ofbiz_Services() throws RecognitionException {
        ofbiz.Services element =  null;

        int parse_ofbiz_Services_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;
        Token a9=null;
        Token a11=null;
        ofbiz.Service a10_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 8) ) { return element; }

            // Ofbiz.g:2725:2: (a0= 'Services' a1= '{' ( (a2= 'description' a3= ':' (a4= QUOTED_34_34 ) |a5= 'version' a6= ':' (a7= QUOTED_34_34 ) |a8= 'services' a9= ':' (a10_0= parse_ofbiz_Service ) ) )* a11= '}' )
            // Ofbiz.g:2726:2: a0= 'Services' a1= '{' ( (a2= 'description' a3= ':' (a4= QUOTED_34_34 ) |a5= 'version' a6= ':' (a7= QUOTED_34_34 ) |a8= 'services' a9= ':' (a10_0= parse_ofbiz_Service ) ) )* a11= '}'
            {
            a0=(Token)match(input,36,FOLLOW_36_in_parse_ofbiz_Services2932); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createServices();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[237]);
            	}

            a1=(Token)match(input,113,FOLLOW_113_in_parse_ofbiz_Services2946); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createServices();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[238]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[239]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[240]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[241]);
            	}

            // Ofbiz.g:2757:2: ( (a2= 'description' a3= ':' (a4= QUOTED_34_34 ) |a5= 'version' a6= ':' (a7= QUOTED_34_34 ) |a8= 'services' a9= ':' (a10_0= parse_ofbiz_Service ) ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==50||LA16_0==99||LA16_0==110) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // Ofbiz.g:2758:3: (a2= 'description' a3= ':' (a4= QUOTED_34_34 ) |a5= 'version' a6= ':' (a7= QUOTED_34_34 ) |a8= 'services' a9= ':' (a10_0= parse_ofbiz_Service ) )
            	    {
            	    // Ofbiz.g:2758:3: (a2= 'description' a3= ':' (a4= QUOTED_34_34 ) |a5= 'version' a6= ':' (a7= QUOTED_34_34 ) |a8= 'services' a9= ':' (a10_0= parse_ofbiz_Service ) )
            	    int alt15=3;
            	    switch ( input.LA(1) ) {
            	    case 50:
            	        {
            	        alt15=1;
            	        }
            	        break;
            	    case 110:
            	        {
            	        alt15=2;
            	        }
            	        break;
            	    case 99:
            	        {
            	        alt15=3;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return element;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 15, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt15) {
            	        case 1 :
            	            // Ofbiz.g:2759:4: a2= 'description' a3= ':' (a4= QUOTED_34_34 )
            	            {
            	            a2=(Token)match(input,50,FOLLOW_50_in_parse_ofbiz_Services2969); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createServices();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_2_0_0_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[242]);
            	            			}

            	            a3=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Services2989); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createServices();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_2_0_0_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[243]);
            	            			}

            	            // Ofbiz.g:2787:4: (a4= QUOTED_34_34 )
            	            // Ofbiz.g:2788:5: a4= QUOTED_34_34
            	            {
            	            a4=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_ofbiz_Services3015); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createServices();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a4 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICES__DESCRIPTION), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            	            						}
            	            						java.lang.String resolved = (java.lang.String) resolvedObject;
            	            						if (resolved != null) {
            	            							Object value = resolved;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICES__DESCRIPTION), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_2_0_0_2, resolved, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[244]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[245]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[246]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[247]);
            	            			}

            	            }
            	            break;
            	        case 2 :
            	            // Ofbiz.g:2827:8: a5= 'version' a6= ':' (a7= QUOTED_34_34 )
            	            {
            	            a5=(Token)match(input,110,FOLLOW_110_in_parse_ofbiz_Services3056); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createServices();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_2_0_1_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[248]);
            	            			}

            	            a6=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Services3076); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createServices();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_2_0_1_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[249]);
            	            			}

            	            // Ofbiz.g:2855:4: (a7= QUOTED_34_34 )
            	            // Ofbiz.g:2856:5: a7= QUOTED_34_34
            	            {
            	            a7=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_ofbiz_Services3102); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createServices();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a7 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICES__VERSION), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
            	            						}
            	            						java.lang.String resolved = (java.lang.String) resolvedObject;
            	            						if (resolved != null) {
            	            							Object value = resolved;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICES__VERSION), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_2_0_1_2, resolved, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[250]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[251]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[252]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[253]);
            	            			}

            	            }
            	            break;
            	        case 3 :
            	            // Ofbiz.g:2895:8: a8= 'services' a9= ':' (a10_0= parse_ofbiz_Service )
            	            {
            	            a8=(Token)match(input,99,FOLLOW_99_in_parse_ofbiz_Services3143); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createServices();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_2_0_2_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[254]);
            	            			}

            	            a9=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Services3163); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createServices();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_2_0_2_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getServices(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[255]);
            	            			}

            	            // Ofbiz.g:2923:4: (a10_0= parse_ofbiz_Service )
            	            // Ofbiz.g:2924:5: a10_0= parse_ofbiz_Service
            	            {
            	            pushFollow(FOLLOW_parse_ofbiz_Service_in_parse_ofbiz_Services3189);
            	            a10_0=parse_ofbiz_Service();

            	            state._fsp--;
            	            if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createServices();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a10_0 != null) {
            	            						if (a10_0 != null) {
            	            							Object value = a10_0;
            	            							addObjectToList(element, ofbiz.OfbizPackage.SERVICES__SERVICES, value);
            	            							completedElement(value, true);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_2_0_2_2, a10_0, true);
            	            						copyLocalizationInfos(a10_0, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[256]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[257]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[258]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[259]);
            	            			}

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[260]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[261]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[262]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[263]);
            	}

            a11=(Token)match(input,114,FOLLOW_114_in_parse_ofbiz_Services3230); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createServices();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_6_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[264]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[265]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[266]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[267]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 8, parse_ofbiz_Services_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_ofbiz_Services"



    // $ANTLR start "parse_ofbiz_Service"
    // Ofbiz.g:2981:1: parse_ofbiz_Service returns [ofbiz.Service element = null] : a0= 'Service' a1= '{' ( (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) |a5= 'engine' a6= ':' ( (a7= 'entity-auto' |a8= 'group' |a9= 'interface' |a10= 'java' |a11= 'simple' |a12= 'script' |a13= 'groovy' |a14= 'jacl' |a15= 'javascript' |a16= 'jpython' |a17= 'route' |a18= 'http' |a19= 'jms' |a20= 'rmi' |a21= 'soap' ) ) |a24= 'location' a25= ':' (a26= QUOTED_34_34 ) |a27= 'invoke' a28= ':' (a29= QUOTED_34_34 ) |a30= 'description' a31= ':' (a32= QUOTED_34_34 ) |a33= 'attributes' a34= ':' (a35_0= parse_ofbiz_Attribute ) ) )* a36= '}' ;
    public final ofbiz.Service parse_ofbiz_Service() throws RecognitionException {
        ofbiz.Service element =  null;

        int parse_ofbiz_Service_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;
        Token a9=null;
        Token a10=null;
        Token a11=null;
        Token a12=null;
        Token a13=null;
        Token a14=null;
        Token a15=null;
        Token a16=null;
        Token a17=null;
        Token a18=null;
        Token a19=null;
        Token a20=null;
        Token a21=null;
        Token a24=null;
        Token a25=null;
        Token a26=null;
        Token a27=null;
        Token a28=null;
        Token a29=null;
        Token a30=null;
        Token a31=null;
        Token a32=null;
        Token a33=null;
        Token a34=null;
        Token a36=null;
        ofbiz.Attribute a35_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 9) ) { return element; }

            // Ofbiz.g:2984:2: (a0= 'Service' a1= '{' ( (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) |a5= 'engine' a6= ':' ( (a7= 'entity-auto' |a8= 'group' |a9= 'interface' |a10= 'java' |a11= 'simple' |a12= 'script' |a13= 'groovy' |a14= 'jacl' |a15= 'javascript' |a16= 'jpython' |a17= 'route' |a18= 'http' |a19= 'jms' |a20= 'rmi' |a21= 'soap' ) ) |a24= 'location' a25= ':' (a26= QUOTED_34_34 ) |a27= 'invoke' a28= ':' (a29= QUOTED_34_34 ) |a30= 'description' a31= ':' (a32= QUOTED_34_34 ) |a33= 'attributes' a34= ':' (a35_0= parse_ofbiz_Attribute ) ) )* a36= '}' )
            // Ofbiz.g:2985:2: a0= 'Service' a1= '{' ( (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) |a5= 'engine' a6= ':' ( (a7= 'entity-auto' |a8= 'group' |a9= 'interface' |a10= 'java' |a11= 'simple' |a12= 'script' |a13= 'groovy' |a14= 'jacl' |a15= 'javascript' |a16= 'jpython' |a17= 'route' |a18= 'http' |a19= 'jms' |a20= 'rmi' |a21= 'soap' ) ) |a24= 'location' a25= ':' (a26= QUOTED_34_34 ) |a27= 'invoke' a28= ':' (a29= QUOTED_34_34 ) |a30= 'description' a31= ':' (a32= QUOTED_34_34 ) |a33= 'attributes' a34= ':' (a35_0= parse_ofbiz_Attribute ) ) )* a36= '}'
            {
            a0=(Token)match(input,35,FOLLOW_35_in_parse_ofbiz_Service3259); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createService();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[268]);
            	}

            a1=(Token)match(input,113,FOLLOW_113_in_parse_ofbiz_Service3273); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createService();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[269]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[270]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[271]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[272]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[273]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[274]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[275]);
            	}

            // Ofbiz.g:3019:2: ( (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) |a5= 'engine' a6= ':' ( (a7= 'entity-auto' |a8= 'group' |a9= 'interface' |a10= 'java' |a11= 'simple' |a12= 'script' |a13= 'groovy' |a14= 'jacl' |a15= 'javascript' |a16= 'jpython' |a17= 'route' |a18= 'http' |a19= 'jms' |a20= 'rmi' |a21= 'soap' ) ) |a24= 'location' a25= ':' (a26= QUOTED_34_34 ) |a27= 'invoke' a28= ':' (a29= QUOTED_34_34 ) |a30= 'description' a31= ':' (a32= QUOTED_34_34 ) |a33= 'attributes' a34= ':' (a35_0= parse_ofbiz_Attribute ) ) )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==40||LA19_0==50||LA19_0==52||LA19_0==73||LA19_0==80||LA19_0==83) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // Ofbiz.g:3020:3: (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) |a5= 'engine' a6= ':' ( (a7= 'entity-auto' |a8= 'group' |a9= 'interface' |a10= 'java' |a11= 'simple' |a12= 'script' |a13= 'groovy' |a14= 'jacl' |a15= 'javascript' |a16= 'jpython' |a17= 'route' |a18= 'http' |a19= 'jms' |a20= 'rmi' |a21= 'soap' ) ) |a24= 'location' a25= ':' (a26= QUOTED_34_34 ) |a27= 'invoke' a28= ':' (a29= QUOTED_34_34 ) |a30= 'description' a31= ':' (a32= QUOTED_34_34 ) |a33= 'attributes' a34= ':' (a35_0= parse_ofbiz_Attribute ) )
            	    {
            	    // Ofbiz.g:3020:3: (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) |a5= 'engine' a6= ':' ( (a7= 'entity-auto' |a8= 'group' |a9= 'interface' |a10= 'java' |a11= 'simple' |a12= 'script' |a13= 'groovy' |a14= 'jacl' |a15= 'javascript' |a16= 'jpython' |a17= 'route' |a18= 'http' |a19= 'jms' |a20= 'rmi' |a21= 'soap' ) ) |a24= 'location' a25= ':' (a26= QUOTED_34_34 ) |a27= 'invoke' a28= ':' (a29= QUOTED_34_34 ) |a30= 'description' a31= ':' (a32= QUOTED_34_34 ) |a33= 'attributes' a34= ':' (a35_0= parse_ofbiz_Attribute ) )
            	    int alt18=6;
            	    switch ( input.LA(1) ) {
            	    case 83:
            	        {
            	        alt18=1;
            	        }
            	        break;
            	    case 52:
            	        {
            	        alt18=2;
            	        }
            	        break;
            	    case 80:
            	        {
            	        alt18=3;
            	        }
            	        break;
            	    case 73:
            	        {
            	        alt18=4;
            	        }
            	        break;
            	    case 50:
            	        {
            	        alt18=5;
            	        }
            	        break;
            	    case 40:
            	        {
            	        alt18=6;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return element;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 18, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt18) {
            	        case 1 :
            	            // Ofbiz.g:3021:4: a2= 'name' a3= ':' (a4= QUOTED_34_34 )
            	            {
            	            a2=(Token)match(input,83,FOLLOW_83_in_parse_ofbiz_Service3296); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_0_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[276]);
            	            			}

            	            a3=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Service3316); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_0_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[277]);
            	            			}

            	            // Ofbiz.g:3049:4: (a4= QUOTED_34_34 )
            	            // Ofbiz.g:3050:5: a4= QUOTED_34_34
            	            {
            	            a4=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_ofbiz_Service3342); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a4 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__NAME), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            	            						}
            	            						java.lang.String resolved = (java.lang.String) resolvedObject;
            	            						if (resolved != null) {
            	            							Object value = resolved;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__NAME), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_0_2, resolved, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[278]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[279]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[280]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[281]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[282]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[283]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[284]);
            	            			}

            	            }
            	            break;
            	        case 2 :
            	            // Ofbiz.g:3092:8: a5= 'engine' a6= ':' ( (a7= 'entity-auto' |a8= 'group' |a9= 'interface' |a10= 'java' |a11= 'simple' |a12= 'script' |a13= 'groovy' |a14= 'jacl' |a15= 'javascript' |a16= 'jpython' |a17= 'route' |a18= 'http' |a19= 'jms' |a20= 'rmi' |a21= 'soap' ) )
            	            {
            	            a5=(Token)match(input,52,FOLLOW_52_in_parse_ofbiz_Service3383); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[285]);
            	            			}

            	            a6=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Service3403); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[286]);
            	            			}

            	            // Ofbiz.g:3120:4: ( (a7= 'entity-auto' |a8= 'group' |a9= 'interface' |a10= 'java' |a11= 'simple' |a12= 'script' |a13= 'groovy' |a14= 'jacl' |a15= 'javascript' |a16= 'jpython' |a17= 'route' |a18= 'http' |a19= 'jms' |a20= 'rmi' |a21= 'soap' ) )
            	            // Ofbiz.g:3121:5: (a7= 'entity-auto' |a8= 'group' |a9= 'interface' |a10= 'java' |a11= 'simple' |a12= 'script' |a13= 'groovy' |a14= 'jacl' |a15= 'javascript' |a16= 'jpython' |a17= 'route' |a18= 'http' |a19= 'jms' |a20= 'rmi' |a21= 'soap' )
            	            {
            	            // Ofbiz.g:3121:5: (a7= 'entity-auto' |a8= 'group' |a9= 'interface' |a10= 'java' |a11= 'simple' |a12= 'script' |a13= 'groovy' |a14= 'jacl' |a15= 'javascript' |a16= 'jpython' |a17= 'route' |a18= 'http' |a19= 'jms' |a20= 'rmi' |a21= 'soap' )
            	            int alt17=15;
            	            switch ( input.LA(1) ) {
            	            case 55:
            	                {
            	                alt17=1;
            	                }
            	                break;
            	            case 63:
            	                {
            	                alt17=2;
            	                }
            	                break;
            	            case 72:
            	                {
            	                alt17=3;
            	                }
            	                break;
            	            case 75:
            	                {
            	                alt17=4;
            	                }
            	                break;
            	            case 101:
            	                {
            	                alt17=5;
            	                }
            	                break;
            	            case 98:
            	                {
            	                alt17=6;
            	                }
            	                break;
            	            case 62:
            	                {
            	                alt17=7;
            	                }
            	                break;
            	            case 74:
            	                {
            	                alt17=8;
            	                }
            	                break;
            	            case 76:
            	                {
            	                alt17=9;
            	                }
            	                break;
            	            case 78:
            	                {
            	                alt17=10;
            	                }
            	                break;
            	            case 97:
            	                {
            	                alt17=11;
            	                }
            	                break;
            	            case 64:
            	                {
            	                alt17=12;
            	                }
            	                break;
            	            case 77:
            	                {
            	                alt17=13;
            	                }
            	                break;
            	            case 96:
            	                {
            	                alt17=14;
            	                }
            	                break;
            	            case 102:
            	                {
            	                alt17=15;
            	                }
            	                break;
            	            default:
            	                if (state.backtracking>0) {state.failed=true; return element;}
            	                NoViableAltException nvae =
            	                    new NoViableAltException("", 17, 0, input);

            	                throw nvae;

            	            }

            	            switch (alt17) {
            	                case 1 :
            	                    // Ofbiz.g:3122:6: a7= 'entity-auto'
            	                    {
            	                    a7=(Token)match(input,55,FOLLOW_55_in_parse_ofbiz_Service3436); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.ENTITY_AUTO_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 2 :
            	                    // Ofbiz.g:3135:12: a8= 'group'
            	                    {
            	                    a8=(Token)match(input,63,FOLLOW_63_in_parse_ofbiz_Service3455); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.GROUP_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 3 :
            	                    // Ofbiz.g:3148:12: a9= 'interface'
            	                    {
            	                    a9=(Token)match(input,72,FOLLOW_72_in_parse_ofbiz_Service3474); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.INTERFACE_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 4 :
            	                    // Ofbiz.g:3161:12: a10= 'java'
            	                    {
            	                    a10=(Token)match(input,75,FOLLOW_75_in_parse_ofbiz_Service3493); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.JAVA_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 5 :
            	                    // Ofbiz.g:3174:12: a11= 'simple'
            	                    {
            	                    a11=(Token)match(input,101,FOLLOW_101_in_parse_ofbiz_Service3512); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.SIMPLE_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 6 :
            	                    // Ofbiz.g:3187:12: a12= 'script'
            	                    {
            	                    a12=(Token)match(input,98,FOLLOW_98_in_parse_ofbiz_Service3531); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.SCRIPT_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 7 :
            	                    // Ofbiz.g:3200:12: a13= 'groovy'
            	                    {
            	                    a13=(Token)match(input,62,FOLLOW_62_in_parse_ofbiz_Service3550); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a13, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.GROOVY_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 8 :
            	                    // Ofbiz.g:3213:12: a14= 'jacl'
            	                    {
            	                    a14=(Token)match(input,74,FOLLOW_74_in_parse_ofbiz_Service3569); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a14, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.JACL_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 9 :
            	                    // Ofbiz.g:3226:12: a15= 'javascript'
            	                    {
            	                    a15=(Token)match(input,76,FOLLOW_76_in_parse_ofbiz_Service3588); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a15, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.JAVASCRIPT_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 10 :
            	                    // Ofbiz.g:3239:12: a16= 'jpython'
            	                    {
            	                    a16=(Token)match(input,78,FOLLOW_78_in_parse_ofbiz_Service3607); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a16, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.JPYTHON_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 11 :
            	                    // Ofbiz.g:3252:12: a17= 'route'
            	                    {
            	                    a17=(Token)match(input,97,FOLLOW_97_in_parse_ofbiz_Service3626); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a17, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.ROUTE_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 12 :
            	                    // Ofbiz.g:3265:12: a18= 'http'
            	                    {
            	                    a18=(Token)match(input,64,FOLLOW_64_in_parse_ofbiz_Service3645); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a18, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.HTTP_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 13 :
            	                    // Ofbiz.g:3278:12: a19= 'jms'
            	                    {
            	                    a19=(Token)match(input,77,FOLLOW_77_in_parse_ofbiz_Service3664); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a19, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.JMS_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 14 :
            	                    // Ofbiz.g:3291:12: a20= 'rmi'
            	                    {
            	                    a20=(Token)match(input,96,FOLLOW_96_in_parse_ofbiz_Service3683); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a20, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.RMI_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 15 :
            	                    // Ofbiz.g:3304:12: a21= 'soap'
            	                    {
            	                    a21=(Token)match(input,102,FOLLOW_102_in_parse_ofbiz_Service3702); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a21, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceEngine().getEEnumLiteral(ofbiz.ServiceEngine.SOAP_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;

            	            }


            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[287]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[288]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[289]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[290]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[291]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[292]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[293]);
            	            			}

            	            }
            	            break;
            	        case 3 :
            	            // Ofbiz.g:3331:8: a24= 'location' a25= ':' (a26= QUOTED_34_34 )
            	            {
            	            a24=(Token)match(input,80,FOLLOW_80_in_parse_ofbiz_Service3741); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_2_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a24, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[294]);
            	            			}

            	            a25=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Service3761); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_2_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a25, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[295]);
            	            			}

            	            // Ofbiz.g:3359:4: (a26= QUOTED_34_34 )
            	            // Ofbiz.g:3360:5: a26= QUOTED_34_34
            	            {
            	            a26=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_ofbiz_Service3787); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a26 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a26.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__LOCATION), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a26).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a26).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a26).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a26).getStopIndex());
            	            						}
            	            						java.lang.String resolved = (java.lang.String) resolvedObject;
            	            						if (resolved != null) {
            	            							Object value = resolved;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__LOCATION), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_2_2, resolved, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a26, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[296]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[297]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[298]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[299]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[300]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[301]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[302]);
            	            			}

            	            }
            	            break;
            	        case 4 :
            	            // Ofbiz.g:3402:8: a27= 'invoke' a28= ':' (a29= QUOTED_34_34 )
            	            {
            	            a27=(Token)match(input,73,FOLLOW_73_in_parse_ofbiz_Service3828); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_3_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a27, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[303]);
            	            			}

            	            a28=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Service3848); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_3_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a28, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[304]);
            	            			}

            	            // Ofbiz.g:3430:4: (a29= QUOTED_34_34 )
            	            // Ofbiz.g:3431:5: a29= QUOTED_34_34
            	            {
            	            a29=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_ofbiz_Service3874); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a29 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a29.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__INVOKE), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a29).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a29).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a29).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a29).getStopIndex());
            	            						}
            	            						java.lang.String resolved = (java.lang.String) resolvedObject;
            	            						if (resolved != null) {
            	            							Object value = resolved;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__INVOKE), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_3_2, resolved, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a29, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[305]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[306]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[307]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[308]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[309]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[310]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[311]);
            	            			}

            	            }
            	            break;
            	        case 5 :
            	            // Ofbiz.g:3473:8: a30= 'description' a31= ':' (a32= QUOTED_34_34 )
            	            {
            	            a30=(Token)match(input,50,FOLLOW_50_in_parse_ofbiz_Service3915); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_4_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a30, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[312]);
            	            			}

            	            a31=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Service3935); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_4_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a31, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[313]);
            	            			}

            	            // Ofbiz.g:3501:4: (a32= QUOTED_34_34 )
            	            // Ofbiz.g:3502:5: a32= QUOTED_34_34
            	            {
            	            a32=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_ofbiz_Service3961); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a32 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a32.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__DESCRIPTION), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a32).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a32).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a32).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a32).getStopIndex());
            	            						}
            	            						java.lang.String resolved = (java.lang.String) resolvedObject;
            	            						if (resolved != null) {
            	            							Object value = resolved;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__DESCRIPTION), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_4_2, resolved, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a32, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[314]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[315]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[316]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[317]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[318]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[319]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[320]);
            	            			}

            	            }
            	            break;
            	        case 6 :
            	            // Ofbiz.g:3544:8: a33= 'attributes' a34= ':' (a35_0= parse_ofbiz_Attribute )
            	            {
            	            a33=(Token)match(input,40,FOLLOW_40_in_parse_ofbiz_Service4002); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_5_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a33, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[321]);
            	            			}

            	            a34=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Service4022); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_5_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a34, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getService(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[322]);
            	            				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getService(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[323]);
            	            			}

            	            // Ofbiz.g:3573:4: (a35_0= parse_ofbiz_Attribute )
            	            // Ofbiz.g:3574:5: a35_0= parse_ofbiz_Attribute
            	            {
            	            pushFollow(FOLLOW_parse_ofbiz_Attribute_in_parse_ofbiz_Service4048);
            	            a35_0=parse_ofbiz_Attribute();

            	            state._fsp--;
            	            if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createService();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a35_0 != null) {
            	            						if (a35_0 != null) {
            	            							Object value = a35_0;
            	            							addObjectToList(element, ofbiz.OfbizPackage.SERVICE__ATTRIBUTES, value);
            	            							completedElement(value, true);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_2_0_5_2, a35_0, true);
            	            						copyLocalizationInfos(a35_0, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[324]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[325]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[326]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[327]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[328]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[329]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[330]);
            	            			}

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[331]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[332]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[333]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[334]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[335]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[336]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[337]);
            	}

            a36=(Token)match(input,114,FOLLOW_114_in_parse_ofbiz_Service4089); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createService();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_7_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a36, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[338]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[339]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[340]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[341]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 9, parse_ofbiz_Service_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_ofbiz_Service"



    // $ANTLR start "parse_ofbiz_Attribute"
    // Ofbiz.g:3637:1: parse_ofbiz_Attribute returns [ofbiz.Attribute element = null] : ( (a0= 'optional' )? ) a3= 'Attribute' a4= '{' ( (a5= 'name' a6= ':' (a7= QUOTED_34_34 ) |a8= 'type' a9= ':' ( (a10= 'String' |a11= 'Boolean' |a12= 'BigDecimal' |a13= 'Timestamp' |a14= 'List' |a15= 'Map' ) ) |a18= 'mode' a19= ':' ( (a20= 'IN' |a21= 'OUT' ) ) ) )* a24= '}' ;
    public final ofbiz.Attribute parse_ofbiz_Attribute() throws RecognitionException {
        ofbiz.Attribute element =  null;

        int parse_ofbiz_Attribute_StartIndex = input.index();

        Token a0=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;
        Token a9=null;
        Token a10=null;
        Token a11=null;
        Token a12=null;
        Token a13=null;
        Token a14=null;
        Token a15=null;
        Token a18=null;
        Token a19=null;
        Token a20=null;
        Token a21=null;
        Token a24=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 10) ) { return element; }

            // Ofbiz.g:3640:2: ( ( (a0= 'optional' )? ) a3= 'Attribute' a4= '{' ( (a5= 'name' a6= ':' (a7= QUOTED_34_34 ) |a8= 'type' a9= ':' ( (a10= 'String' |a11= 'Boolean' |a12= 'BigDecimal' |a13= 'Timestamp' |a14= 'List' |a15= 'Map' ) ) |a18= 'mode' a19= ':' ( (a20= 'IN' |a21= 'OUT' ) ) ) )* a24= '}' )
            // Ofbiz.g:3641:2: ( (a0= 'optional' )? ) a3= 'Attribute' a4= '{' ( (a5= 'name' a6= ':' (a7= QUOTED_34_34 ) |a8= 'type' a9= ':' ( (a10= 'String' |a11= 'Boolean' |a12= 'BigDecimal' |a13= 'Timestamp' |a14= 'List' |a15= 'Map' ) ) |a18= 'mode' a19= ':' ( (a20= 'IN' |a21= 'OUT' ) ) ) )* a24= '}'
            {
            // Ofbiz.g:3641:2: ( (a0= 'optional' )? )
            // Ofbiz.g:3642:3: (a0= 'optional' )?
            {
            // Ofbiz.g:3642:3: (a0= 'optional' )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==86) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // Ofbiz.g:3643:4: a0= 'optional'
                    {
                    a0=(Token)match(input,86,FOLLOW_86_in_parse_ofbiz_Attribute4127); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
                    					startIncompleteElement(element);
                    					// initialize boolean attribute
                    					{
                    						Object value = false;
                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
                    					}
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_0, true, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    				// set value of boolean attribute
                    				Object value = true;
                    				element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
                    				completedElement(value, false);
                    			}

                    }
                    break;

            }


            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[342]);
            	}

            a3=(Token)match(input,12,FOLLOW_12_in_parse_ofbiz_Attribute4148); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
            			startIncompleteElement(element);
            			// initialize boolean attribute
            			{
            				Object value = false;
            				element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
            			}
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[343]);
            	}

            a4=(Token)match(input,113,FOLLOW_113_in_parse_ofbiz_Attribute4162); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
            			startIncompleteElement(element);
            			// initialize boolean attribute
            			{
            				Object value = false;
            				element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
            			}
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[344]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[345]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[346]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[347]);
            	}

            // Ofbiz.g:3708:2: ( (a5= 'name' a6= ':' (a7= QUOTED_34_34 ) |a8= 'type' a9= ':' ( (a10= 'String' |a11= 'Boolean' |a12= 'BigDecimal' |a13= 'Timestamp' |a14= 'List' |a15= 'Map' ) ) |a18= 'mode' a19= ':' ( (a20= 'IN' |a21= 'OUT' ) ) ) )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( ((LA24_0 >= 82 && LA24_0 <= 83)||LA24_0==106) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // Ofbiz.g:3709:3: (a5= 'name' a6= ':' (a7= QUOTED_34_34 ) |a8= 'type' a9= ':' ( (a10= 'String' |a11= 'Boolean' |a12= 'BigDecimal' |a13= 'Timestamp' |a14= 'List' |a15= 'Map' ) ) |a18= 'mode' a19= ':' ( (a20= 'IN' |a21= 'OUT' ) ) )
            	    {
            	    // Ofbiz.g:3709:3: (a5= 'name' a6= ':' (a7= QUOTED_34_34 ) |a8= 'type' a9= ':' ( (a10= 'String' |a11= 'Boolean' |a12= 'BigDecimal' |a13= 'Timestamp' |a14= 'List' |a15= 'Map' ) ) |a18= 'mode' a19= ':' ( (a20= 'IN' |a21= 'OUT' ) ) )
            	    int alt23=3;
            	    switch ( input.LA(1) ) {
            	    case 83:
            	        {
            	        alt23=1;
            	        }
            	        break;
            	    case 106:
            	        {
            	        alt23=2;
            	        }
            	        break;
            	    case 82:
            	        {
            	        alt23=3;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return element;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 23, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt23) {
            	        case 1 :
            	            // Ofbiz.g:3710:4: a5= 'name' a6= ':' (a7= QUOTED_34_34 )
            	            {
            	            a5=(Token)match(input,83,FOLLOW_83_in_parse_ofbiz_Attribute4185); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
            	            					startIncompleteElement(element);
            	            					// initialize boolean attribute
            	            					{
            	            						Object value = false;
            	            						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
            	            					}
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_0_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[348]);
            	            			}

            	            a6=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Attribute4205); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
            	            					startIncompleteElement(element);
            	            					// initialize boolean attribute
            	            					{
            	            						Object value = false;
            	            						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
            	            					}
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_0_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[349]);
            	            			}

            	            // Ofbiz.g:3748:4: (a7= QUOTED_34_34 )
            	            // Ofbiz.g:3749:5: a7= QUOTED_34_34
            	            {
            	            a7=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_ofbiz_Attribute4231); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
            	            						startIncompleteElement(element);
            	            						// initialize boolean attribute
            	            						{
            	            							Object value = false;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
            	            						}
            	            					}
            	            					if (a7 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__NAME), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
            	            						}
            	            						java.lang.String resolved = (java.lang.String) resolvedObject;
            	            						if (resolved != null) {
            	            							Object value = resolved;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__NAME), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_0_2, resolved, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[350]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[351]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[352]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[353]);
            	            			}

            	            }
            	            break;
            	        case 2 :
            	            // Ofbiz.g:3793:8: a8= 'type' a9= ':' ( (a10= 'String' |a11= 'Boolean' |a12= 'BigDecimal' |a13= 'Timestamp' |a14= 'List' |a15= 'Map' ) )
            	            {
            	            a8=(Token)match(input,106,FOLLOW_106_in_parse_ofbiz_Attribute4272); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
            	            					startIncompleteElement(element);
            	            					// initialize boolean attribute
            	            					{
            	            						Object value = false;
            	            						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
            	            					}
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_1_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[354]);
            	            			}

            	            a9=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Attribute4292); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
            	            					startIncompleteElement(element);
            	            					// initialize boolean attribute
            	            					{
            	            						Object value = false;
            	            						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
            	            					}
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_1_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[355]);
            	            			}

            	            // Ofbiz.g:3831:4: ( (a10= 'String' |a11= 'Boolean' |a12= 'BigDecimal' |a13= 'Timestamp' |a14= 'List' |a15= 'Map' ) )
            	            // Ofbiz.g:3832:5: (a10= 'String' |a11= 'Boolean' |a12= 'BigDecimal' |a13= 'Timestamp' |a14= 'List' |a15= 'Map' )
            	            {
            	            // Ofbiz.g:3832:5: (a10= 'String' |a11= 'Boolean' |a12= 'BigDecimal' |a13= 'Timestamp' |a14= 'List' |a15= 'Map' )
            	            int alt21=6;
            	            switch ( input.LA(1) ) {
            	            case 37:
            	                {
            	                alt21=1;
            	                }
            	                break;
            	            case 14:
            	                {
            	                alt21=2;
            	                }
            	                break;
            	            case 13:
            	                {
            	                alt21=3;
            	                }
            	                break;
            	            case 38:
            	                {
            	                alt21=4;
            	                }
            	                break;
            	            case 23:
            	                {
            	                alt21=5;
            	                }
            	                break;
            	            case 25:
            	                {
            	                alt21=6;
            	                }
            	                break;
            	            default:
            	                if (state.backtracking>0) {state.failed=true; return element;}
            	                NoViableAltException nvae =
            	                    new NoViableAltException("", 21, 0, input);

            	                throw nvae;

            	            }

            	            switch (alt21) {
            	                case 1 :
            	                    // Ofbiz.g:3833:6: a10= 'String'
            	                    {
            	                    a10=(Token)match(input,37,FOLLOW_37_in_parse_ofbiz_Attribute4325); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
            	                    							startIncompleteElement(element);
            	                    							// initialize boolean attribute
            	                    							{
            	                    								Object value = false;
            	                    								element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
            	                    							}
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceDatatype().getEEnumLiteral(ofbiz.ServiceDatatype.STRING_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 2 :
            	                    // Ofbiz.g:3851:12: a11= 'Boolean'
            	                    {
            	                    a11=(Token)match(input,14,FOLLOW_14_in_parse_ofbiz_Attribute4344); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
            	                    							startIncompleteElement(element);
            	                    							// initialize boolean attribute
            	                    							{
            	                    								Object value = false;
            	                    								element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
            	                    							}
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceDatatype().getEEnumLiteral(ofbiz.ServiceDatatype.BOOLEAN_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 3 :
            	                    // Ofbiz.g:3869:12: a12= 'BigDecimal'
            	                    {
            	                    a12=(Token)match(input,13,FOLLOW_13_in_parse_ofbiz_Attribute4363); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
            	                    							startIncompleteElement(element);
            	                    							// initialize boolean attribute
            	                    							{
            	                    								Object value = false;
            	                    								element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
            	                    							}
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceDatatype().getEEnumLiteral(ofbiz.ServiceDatatype.BIG_DECIMAL_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 4 :
            	                    // Ofbiz.g:3887:12: a13= 'Timestamp'
            	                    {
            	                    a13=(Token)match(input,38,FOLLOW_38_in_parse_ofbiz_Attribute4382); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
            	                    							startIncompleteElement(element);
            	                    							// initialize boolean attribute
            	                    							{
            	                    								Object value = false;
            	                    								element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
            	                    							}
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a13, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceDatatype().getEEnumLiteral(ofbiz.ServiceDatatype.TIMESTAMP_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 5 :
            	                    // Ofbiz.g:3905:12: a14= 'List'
            	                    {
            	                    a14=(Token)match(input,23,FOLLOW_23_in_parse_ofbiz_Attribute4401); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
            	                    							startIncompleteElement(element);
            	                    							// initialize boolean attribute
            	                    							{
            	                    								Object value = false;
            	                    								element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
            	                    							}
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a14, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceDatatype().getEEnumLiteral(ofbiz.ServiceDatatype.LIST_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 6 :
            	                    // Ofbiz.g:3923:12: a15= 'Map'
            	                    {
            	                    a15=(Token)match(input,25,FOLLOW_25_in_parse_ofbiz_Attribute4420); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
            	                    							startIncompleteElement(element);
            	                    							// initialize boolean attribute
            	                    							{
            	                    								Object value = false;
            	                    								element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
            	                    							}
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_1_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a15, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getServiceDatatype().getEEnumLiteral(ofbiz.ServiceDatatype.MAP_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__TYPE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;

            	            }


            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[356]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[357]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[358]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[359]);
            	            			}

            	            }
            	            break;
            	        case 3 :
            	            // Ofbiz.g:3952:8: a18= 'mode' a19= ':' ( (a20= 'IN' |a21= 'OUT' ) )
            	            {
            	            a18=(Token)match(input,82,FOLLOW_82_in_parse_ofbiz_Attribute4459); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
            	            					startIncompleteElement(element);
            	            					// initialize boolean attribute
            	            					{
            	            						Object value = false;
            	            						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
            	            					}
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_2_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a18, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[360]);
            	            			}

            	            a19=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_Attribute4479); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
            	            					startIncompleteElement(element);
            	            					// initialize boolean attribute
            	            					{
            	            						Object value = false;
            	            						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
            	            					}
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_2_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a19, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[361]);
            	            			}

            	            // Ofbiz.g:3990:4: ( (a20= 'IN' |a21= 'OUT' ) )
            	            // Ofbiz.g:3991:5: (a20= 'IN' |a21= 'OUT' )
            	            {
            	            // Ofbiz.g:3991:5: (a20= 'IN' |a21= 'OUT' )
            	            int alt22=2;
            	            int LA22_0 = input.LA(1);

            	            if ( (LA22_0==21) ) {
            	                alt22=1;
            	            }
            	            else if ( (LA22_0==27) ) {
            	                alt22=2;
            	            }
            	            else {
            	                if (state.backtracking>0) {state.failed=true; return element;}
            	                NoViableAltException nvae =
            	                    new NoViableAltException("", 22, 0, input);

            	                throw nvae;

            	            }
            	            switch (alt22) {
            	                case 1 :
            	                    // Ofbiz.g:3992:6: a20= 'IN'
            	                    {
            	                    a20=(Token)match(input,21,FOLLOW_21_in_parse_ofbiz_Attribute4512); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
            	                    							startIncompleteElement(element);
            	                    							// initialize boolean attribute
            	                    							{
            	                    								Object value = false;
            	                    								element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
            	                    							}
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_2_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a20, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getAttributeMode().getEEnumLiteral(ofbiz.AttributeMode.IN_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__MODE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 2 :
            	                    // Ofbiz.g:4010:12: a21= 'OUT'
            	                    {
            	                    a21=(Token)match(input,27,FOLLOW_27_in_parse_ofbiz_Attribute4531); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
            	                    							startIncompleteElement(element);
            	                    							// initialize boolean attribute
            	                    							{
            	                    								Object value = false;
            	                    								element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
            	                    							}
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_3_0_2_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a21, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getAttributeMode().getEEnumLiteral(ofbiz.AttributeMode.OUT_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__MODE), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;

            	            }


            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[362]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[363]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[364]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[365]);
            	            			}

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[366]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[367]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[368]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[369]);
            	}

            a24=(Token)match(input,114,FOLLOW_114_in_parse_ofbiz_Attribute4575); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createAttribute();
            			startIncompleteElement(element);
            			// initialize boolean attribute
            			{
            				Object value = false;
            				element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), value);
            			}
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_8_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a24, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[370]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[371]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[372]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[373]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[374]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[375]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[376]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 10, parse_ofbiz_Attribute_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_ofbiz_Attribute"



    // $ANTLR start "parse_ofbiz_RestService"
    // Ofbiz.g:4075:1: parse_ofbiz_RestService returns [ofbiz.RestService element = null] : a0= 'RestService' a1= '{' ( (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) |a5= 'version' a6= ':' (a7= QUOTED_34_34 ) |a8= 'resources' a9= ':' (a10_0= parse_ofbiz_EntityResource ) ) )* a11= '}' ;
    public final ofbiz.RestService parse_ofbiz_RestService() throws RecognitionException {
        ofbiz.RestService element =  null;

        int parse_ofbiz_RestService_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;
        Token a9=null;
        Token a11=null;
        ofbiz.EntityResource a10_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 11) ) { return element; }

            // Ofbiz.g:4078:2: (a0= 'RestService' a1= '{' ( (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) |a5= 'version' a6= ':' (a7= QUOTED_34_34 ) |a8= 'resources' a9= ':' (a10_0= parse_ofbiz_EntityResource ) ) )* a11= '}' )
            // Ofbiz.g:4079:2: a0= 'RestService' a1= '{' ( (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) |a5= 'version' a6= ':' (a7= QUOTED_34_34 ) |a8= 'resources' a9= ':' (a10_0= parse_ofbiz_EntityResource ) ) )* a11= '}'
            {
            a0=(Token)match(input,34,FOLLOW_34_in_parse_ofbiz_RestService4604); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[377]);
            	}

            a1=(Token)match(input,113,FOLLOW_113_in_parse_ofbiz_RestService4618); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[378]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[379]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[380]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[381]);
            	}

            // Ofbiz.g:4110:2: ( (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) |a5= 'version' a6= ':' (a7= QUOTED_34_34 ) |a8= 'resources' a9= ':' (a10_0= parse_ofbiz_EntityResource ) ) )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==83||LA26_0==94||LA26_0==110) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // Ofbiz.g:4111:3: (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) |a5= 'version' a6= ':' (a7= QUOTED_34_34 ) |a8= 'resources' a9= ':' (a10_0= parse_ofbiz_EntityResource ) )
            	    {
            	    // Ofbiz.g:4111:3: (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) |a5= 'version' a6= ':' (a7= QUOTED_34_34 ) |a8= 'resources' a9= ':' (a10_0= parse_ofbiz_EntityResource ) )
            	    int alt25=3;
            	    switch ( input.LA(1) ) {
            	    case 83:
            	        {
            	        alt25=1;
            	        }
            	        break;
            	    case 110:
            	        {
            	        alt25=2;
            	        }
            	        break;
            	    case 94:
            	        {
            	        alt25=3;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return element;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 25, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt25) {
            	        case 1 :
            	            // Ofbiz.g:4112:4: a2= 'name' a3= ':' (a4= QUOTED_34_34 )
            	            {
            	            a2=(Token)match(input,83,FOLLOW_83_in_parse_ofbiz_RestService4641); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_2_0_0_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[382]);
            	            			}

            	            a3=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_RestService4661); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_2_0_0_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[383]);
            	            			}

            	            // Ofbiz.g:4140:4: (a4= QUOTED_34_34 )
            	            // Ofbiz.g:4141:5: a4= QUOTED_34_34
            	            {
            	            a4=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_ofbiz_RestService4687); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a4 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_SERVICE__NAME), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            	            						}
            	            						java.lang.String resolved = (java.lang.String) resolvedObject;
            	            						if (resolved != null) {
            	            							Object value = resolved;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_SERVICE__NAME), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_2_0_0_2, resolved, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[384]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[385]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[386]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[387]);
            	            			}

            	            }
            	            break;
            	        case 2 :
            	            // Ofbiz.g:4180:8: a5= 'version' a6= ':' (a7= QUOTED_34_34 )
            	            {
            	            a5=(Token)match(input,110,FOLLOW_110_in_parse_ofbiz_RestService4728); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_2_0_1_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[388]);
            	            			}

            	            a6=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_RestService4748); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_2_0_1_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[389]);
            	            			}

            	            // Ofbiz.g:4208:4: (a7= QUOTED_34_34 )
            	            // Ofbiz.g:4209:5: a7= QUOTED_34_34
            	            {
            	            a7=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_ofbiz_RestService4774); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a7 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_SERVICE__VERSION), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
            	            						}
            	            						java.lang.String resolved = (java.lang.String) resolvedObject;
            	            						if (resolved != null) {
            	            							Object value = resolved;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_SERVICE__VERSION), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_2_0_1_2, resolved, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[390]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[391]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[392]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[393]);
            	            			}

            	            }
            	            break;
            	        case 3 :
            	            // Ofbiz.g:4248:8: a8= 'resources' a9= ':' (a10_0= parse_ofbiz_EntityResource )
            	            {
            	            a8=(Token)match(input,94,FOLLOW_94_in_parse_ofbiz_RestService4815); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_2_0_2_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[394]);
            	            			}

            	            a9=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_RestService4835); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_2_0_2_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getRestService(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[395]);
            	            			}

            	            // Ofbiz.g:4276:4: (a10_0= parse_ofbiz_EntityResource )
            	            // Ofbiz.g:4277:5: a10_0= parse_ofbiz_EntityResource
            	            {
            	            pushFollow(FOLLOW_parse_ofbiz_EntityResource_in_parse_ofbiz_RestService4861);
            	            a10_0=parse_ofbiz_EntityResource();

            	            state._fsp--;
            	            if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a10_0 != null) {
            	            						if (a10_0 != null) {
            	            							Object value = a10_0;
            	            							addObjectToList(element, ofbiz.OfbizPackage.REST_SERVICE__RESOURCES, value);
            	            							completedElement(value, true);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_2_0_2_2, a10_0, true);
            	            						copyLocalizationInfos(a10_0, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[396]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[397]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[398]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[399]);
            	            			}

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[400]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[401]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[402]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[403]);
            	}

            a11=(Token)match(input,114,FOLLOW_114_in_parse_ofbiz_RestService4902); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createRestService();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_9_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[404]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[405]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[406]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[407]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 11, parse_ofbiz_RestService_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_ofbiz_RestService"



    // $ANTLR start "parse_ofbiz_RestRequest"
    // Ofbiz.g:4334:1: parse_ofbiz_RestRequest returns [ofbiz.RestRequest element = null] : a0= 'RestRequest' a1= '{' ( (a2= 'action' a3= ':' ( (a4= 'GET' |a5= 'PUT' |a6= 'POST' |a7= 'DELETE' ) ) |a10= 'parameters' a11= ':' (a12_0= parse_ofbiz_RestParameter ) ) )* a13= '}' ;
    public final ofbiz.RestRequest parse_ofbiz_RestRequest() throws RecognitionException {
        ofbiz.RestRequest element =  null;

        int parse_ofbiz_RestRequest_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a10=null;
        Token a11=null;
        Token a13=null;
        ofbiz.RestParameter a12_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 12) ) { return element; }

            // Ofbiz.g:4337:2: (a0= 'RestRequest' a1= '{' ( (a2= 'action' a3= ':' ( (a4= 'GET' |a5= 'PUT' |a6= 'POST' |a7= 'DELETE' ) ) |a10= 'parameters' a11= ':' (a12_0= parse_ofbiz_RestParameter ) ) )* a13= '}' )
            // Ofbiz.g:4338:2: a0= 'RestRequest' a1= '{' ( (a2= 'action' a3= ':' ( (a4= 'GET' |a5= 'PUT' |a6= 'POST' |a7= 'DELETE' ) ) |a10= 'parameters' a11= ':' (a12_0= parse_ofbiz_RestParameter ) ) )* a13= '}'
            {
            a0=(Token)match(input,33,FOLLOW_33_in_parse_ofbiz_RestRequest4931); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[408]);
            	}

            a1=(Token)match(input,113,FOLLOW_113_in_parse_ofbiz_RestRequest4945); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[409]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[410]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[411]);
            	}

            // Ofbiz.g:4368:2: ( (a2= 'action' a3= ':' ( (a4= 'GET' |a5= 'PUT' |a6= 'POST' |a7= 'DELETE' ) ) |a10= 'parameters' a11= ':' (a12_0= parse_ofbiz_RestParameter ) ) )*
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==39||LA29_0==89) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // Ofbiz.g:4369:3: (a2= 'action' a3= ':' ( (a4= 'GET' |a5= 'PUT' |a6= 'POST' |a7= 'DELETE' ) ) |a10= 'parameters' a11= ':' (a12_0= parse_ofbiz_RestParameter ) )
            	    {
            	    // Ofbiz.g:4369:3: (a2= 'action' a3= ':' ( (a4= 'GET' |a5= 'PUT' |a6= 'POST' |a7= 'DELETE' ) ) |a10= 'parameters' a11= ':' (a12_0= parse_ofbiz_RestParameter ) )
            	    int alt28=2;
            	    int LA28_0 = input.LA(1);

            	    if ( (LA28_0==39) ) {
            	        alt28=1;
            	    }
            	    else if ( (LA28_0==89) ) {
            	        alt28=2;
            	    }
            	    else {
            	        if (state.backtracking>0) {state.failed=true; return element;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 28, 0, input);

            	        throw nvae;

            	    }
            	    switch (alt28) {
            	        case 1 :
            	            // Ofbiz.g:4370:4: a2= 'action' a3= ':' ( (a4= 'GET' |a5= 'PUT' |a6= 'POST' |a7= 'DELETE' ) )
            	            {
            	            a2=(Token)match(input,39,FOLLOW_39_in_parse_ofbiz_RestRequest4968); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_2_0_0_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[412]);
            	            			}

            	            a3=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_RestRequest4988); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_2_0_0_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[413]);
            	            			}

            	            // Ofbiz.g:4398:4: ( (a4= 'GET' |a5= 'PUT' |a6= 'POST' |a7= 'DELETE' ) )
            	            // Ofbiz.g:4399:5: (a4= 'GET' |a5= 'PUT' |a6= 'POST' |a7= 'DELETE' )
            	            {
            	            // Ofbiz.g:4399:5: (a4= 'GET' |a5= 'PUT' |a6= 'POST' |a7= 'DELETE' )
            	            int alt27=4;
            	            switch ( input.LA(1) ) {
            	            case 20:
            	                {
            	                alt27=1;
            	                }
            	                break;
            	            case 30:
            	                {
            	                alt27=2;
            	                }
            	                break;
            	            case 29:
            	                {
            	                alt27=3;
            	                }
            	                break;
            	            case 15:
            	                {
            	                alt27=4;
            	                }
            	                break;
            	            default:
            	                if (state.backtracking>0) {state.failed=true; return element;}
            	                NoViableAltException nvae =
            	                    new NoViableAltException("", 27, 0, input);

            	                throw nvae;

            	            }

            	            switch (alt27) {
            	                case 1 :
            	                    // Ofbiz.g:4400:6: a4= 'GET'
            	                    {
            	                    a4=(Token)match(input,20,FOLLOW_20_in_parse_ofbiz_RestRequest5021); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_2_0_0_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getRestMethod().getEEnumLiteral(ofbiz.RestMethod.GET_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_REQUEST__ACTION), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 2 :
            	                    // Ofbiz.g:4413:12: a5= 'PUT'
            	                    {
            	                    a5=(Token)match(input,30,FOLLOW_30_in_parse_ofbiz_RestRequest5040); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_2_0_0_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getRestMethod().getEEnumLiteral(ofbiz.RestMethod.PUT_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_REQUEST__ACTION), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 3 :
            	                    // Ofbiz.g:4426:12: a6= 'POST'
            	                    {
            	                    a6=(Token)match(input,29,FOLLOW_29_in_parse_ofbiz_RestRequest5059); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_2_0_0_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getRestMethod().getEEnumLiteral(ofbiz.RestMethod.POST_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_REQUEST__ACTION), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;
            	                case 4 :
            	                    // Ofbiz.g:4439:12: a7= 'DELETE'
            	                    {
            	                    a7=(Token)match(input,15,FOLLOW_15_in_parse_ofbiz_RestRequest5078); if (state.failed) return element;

            	                    if ( state.backtracking==0 ) {
            	                    						if (element == null) {
            	                    							element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
            	                    							startIncompleteElement(element);
            	                    						}
            	                    						collectHiddenTokens(element);
            	                    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_2_0_0_2, null, true);
            	                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
            	                    						// set value of enumeration attribute
            	                    						Object value = ofbiz.OfbizPackage.eINSTANCE.getRestMethod().getEEnumLiteral(ofbiz.RestMethod.DELETE_VALUE).getInstance();
            	                    						element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_REQUEST__ACTION), value);
            	                    						completedElement(value, false);
            	                    					}

            	                    }
            	                    break;

            	            }


            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[414]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[415]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[416]);
            	            			}

            	            }
            	            break;
            	        case 2 :
            	            // Ofbiz.g:4462:8: a10= 'parameters' a11= ':' (a12_0= parse_ofbiz_RestParameter )
            	            {
            	            a10=(Token)match(input,89,FOLLOW_89_in_parse_ofbiz_RestRequest5117); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_2_0_1_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[417]);
            	            			}

            	            a11=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_RestRequest5137); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_2_0_1_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getRestRequest(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[418]);
            	            			}

            	            // Ofbiz.g:4490:4: (a12_0= parse_ofbiz_RestParameter )
            	            // Ofbiz.g:4491:5: a12_0= parse_ofbiz_RestParameter
            	            {
            	            pushFollow(FOLLOW_parse_ofbiz_RestParameter_in_parse_ofbiz_RestRequest5163);
            	            a12_0=parse_ofbiz_RestParameter();

            	            state._fsp--;
            	            if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a12_0 != null) {
            	            						if (a12_0 != null) {
            	            							Object value = a12_0;
            	            							addObjectToList(element, ofbiz.OfbizPackage.REST_REQUEST__PARAMETERS, value);
            	            							completedElement(value, true);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_2_0_1_2, a12_0, true);
            	            						copyLocalizationInfos(a12_0, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[419]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[420]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[421]);
            	            			}

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[422]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[423]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[424]);
            	}

            a13=(Token)match(input,114,FOLLOW_114_in_parse_ofbiz_RestRequest5204); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createRestRequest();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_10_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a13, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[425]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[426]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[427]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[428]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 12, parse_ofbiz_RestRequest_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_ofbiz_RestRequest"



    // $ANTLR start "parse_ofbiz_RestParameter"
    // Ofbiz.g:4546:1: parse_ofbiz_RestParameter returns [ofbiz.RestParameter element = null] : a0= 'RestParameter' a1= '{' ( (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) ) )* a5= '}' ;
    public final ofbiz.RestParameter parse_ofbiz_RestParameter() throws RecognitionException {
        ofbiz.RestParameter element =  null;

        int parse_ofbiz_RestParameter_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 13) ) { return element; }

            // Ofbiz.g:4549:2: (a0= 'RestParameter' a1= '{' ( (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) ) )* a5= '}' )
            // Ofbiz.g:4550:2: a0= 'RestParameter' a1= '{' ( (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) ) )* a5= '}'
            {
            a0=(Token)match(input,32,FOLLOW_32_in_parse_ofbiz_RestParameter5233); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createRestParameter();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_11_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[429]);
            	}

            a1=(Token)match(input,113,FOLLOW_113_in_parse_ofbiz_RestParameter5247); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createRestParameter();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_11_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[430]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[431]);
            	}

            // Ofbiz.g:4579:2: ( (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) ) )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==83) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // Ofbiz.g:4580:3: (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) )
            	    {
            	    // Ofbiz.g:4580:3: (a2= 'name' a3= ':' (a4= QUOTED_34_34 ) )
            	    // Ofbiz.g:4581:4: a2= 'name' a3= ':' (a4= QUOTED_34_34 )
            	    {
            	    a2=(Token)match(input,83,FOLLOW_83_in_parse_ofbiz_RestParameter5270); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (element == null) {
            	    					element = ofbiz.OfbizFactory.eINSTANCE.createRestParameter();
            	    					startIncompleteElement(element);
            	    				}
            	    				collectHiddenTokens(element);
            	    				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_11_0_0_2_0_0_0, null, true);
            	    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	    			}

            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[432]);
            	    			}

            	    a3=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_RestParameter5290); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (element == null) {
            	    					element = ofbiz.OfbizFactory.eINSTANCE.createRestParameter();
            	    					startIncompleteElement(element);
            	    				}
            	    				collectHiddenTokens(element);
            	    				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_11_0_0_2_0_0_1, null, true);
            	    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	    			}

            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[433]);
            	    			}

            	    // Ofbiz.g:4609:4: (a4= QUOTED_34_34 )
            	    // Ofbiz.g:4610:5: a4= QUOTED_34_34
            	    {
            	    a4=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_ofbiz_RestParameter5316); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = ofbiz.OfbizFactory.eINSTANCE.createRestParameter();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a4 != null) {
            	    						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            	    						tokenResolver.setOptions(getOptions());
            	    						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	    						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_PARAMETER__NAME), result);
            	    						Object resolvedObject = result.getResolvedToken();
            	    						if (resolvedObject == null) {
            	    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            	    						}
            	    						java.lang.String resolved = (java.lang.String) resolvedObject;
            	    						if (resolved != null) {
            	    							Object value = resolved;
            	    							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.REST_PARAMETER__NAME), value);
            	    							completedElement(value, false);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_11_0_0_2_0_0_2, resolved, true);
            	    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[434]);
            	    				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[435]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[436]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[437]);
            	}

            a5=(Token)match(input,114,FOLLOW_114_in_parse_ofbiz_RestParameter5362); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createRestParameter();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_11_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[438]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[439]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[440]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 13, parse_ofbiz_RestParameter_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_ofbiz_RestParameter"



    // $ANTLR start "parse_ofbiz_EntityResource"
    // Ofbiz.g:4672:1: parse_ofbiz_EntityResource returns [ofbiz.EntityResource element = null] : a0= 'EntityResource' a1= '{' ( (a2= 'entity' a3= ':' (a4= TEXT ) |a5= 'requests' a6= ':' (a7_0= parse_ofbiz_RestRequest ) |a8= 'urlPath' a9= ':' (a10= QUOTED_34_34 ) ) )* a11= '}' ;
    public final ofbiz.EntityResource parse_ofbiz_EntityResource() throws RecognitionException {
        ofbiz.EntityResource element =  null;

        int parse_ofbiz_EntityResource_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a8=null;
        Token a9=null;
        Token a10=null;
        Token a11=null;
        ofbiz.RestRequest a7_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 14) ) { return element; }

            // Ofbiz.g:4675:2: (a0= 'EntityResource' a1= '{' ( (a2= 'entity' a3= ':' (a4= TEXT ) |a5= 'requests' a6= ':' (a7_0= parse_ofbiz_RestRequest ) |a8= 'urlPath' a9= ':' (a10= QUOTED_34_34 ) ) )* a11= '}' )
            // Ofbiz.g:4676:2: a0= 'EntityResource' a1= '{' ( (a2= 'entity' a3= ':' (a4= TEXT ) |a5= 'requests' a6= ':' (a7_0= parse_ofbiz_RestRequest ) |a8= 'urlPath' a9= ':' (a10= QUOTED_34_34 ) ) )* a11= '}'
            {
            a0=(Token)match(input,18,FOLLOW_18_in_parse_ofbiz_EntityResource5391); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[441]);
            	}

            a1=(Token)match(input,113,FOLLOW_113_in_parse_ofbiz_EntityResource5405); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[442]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[443]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[444]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[445]);
            	}

            // Ofbiz.g:4707:2: ( (a2= 'entity' a3= ':' (a4= TEXT ) |a5= 'requests' a6= ':' (a7_0= parse_ofbiz_RestRequest ) |a8= 'urlPath' a9= ':' (a10= QUOTED_34_34 ) ) )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( (LA32_0==54||LA32_0==93||LA32_0==108) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // Ofbiz.g:4708:3: (a2= 'entity' a3= ':' (a4= TEXT ) |a5= 'requests' a6= ':' (a7_0= parse_ofbiz_RestRequest ) |a8= 'urlPath' a9= ':' (a10= QUOTED_34_34 ) )
            	    {
            	    // Ofbiz.g:4708:3: (a2= 'entity' a3= ':' (a4= TEXT ) |a5= 'requests' a6= ':' (a7_0= parse_ofbiz_RestRequest ) |a8= 'urlPath' a9= ':' (a10= QUOTED_34_34 ) )
            	    int alt31=3;
            	    switch ( input.LA(1) ) {
            	    case 54:
            	        {
            	        alt31=1;
            	        }
            	        break;
            	    case 93:
            	        {
            	        alt31=2;
            	        }
            	        break;
            	    case 108:
            	        {
            	        alt31=3;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return element;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 31, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt31) {
            	        case 1 :
            	            // Ofbiz.g:4709:4: a2= 'entity' a3= ':' (a4= TEXT )
            	            {
            	            a2=(Token)match(input,54,FOLLOW_54_in_parse_ofbiz_EntityResource5428); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_2_0_0_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[446]);
            	            			}

            	            a3=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_EntityResource5448); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_2_0_0_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[447]);
            	            			}

            	            // Ofbiz.g:4737:4: (a4= TEXT )
            	            // Ofbiz.g:4738:5: a4= TEXT
            	            {
            	            a4=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_ofbiz_EntityResource5474); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a4 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__ENTITY), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            	            						}
            	            						String resolved = (String) resolvedObject;
            	            						ofbiz.Entity proxy = ofbiz.OfbizFactory.eINSTANCE.createEntity();
            	            						collectHiddenTokens(element);
            	            						registerContextDependentProxy(new ofbiz.resource.ofbiz.mopp.OfbizContextDependentURIFragmentFactory<ofbiz.EntityResource, ofbiz.Entity>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEntityResourceEntityReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__ENTITY), resolved, proxy);
            	            						if (proxy != null) {
            	            							Object value = proxy;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__ENTITY), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_2_0_0_2, proxy, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, proxy);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[448]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[449]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[450]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[451]);
            	            			}

            	            }
            	            break;
            	        case 2 :
            	            // Ofbiz.g:4781:8: a5= 'requests' a6= ':' (a7_0= parse_ofbiz_RestRequest )
            	            {
            	            a5=(Token)match(input,93,FOLLOW_93_in_parse_ofbiz_EntityResource5515); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_2_0_1_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[452]);
            	            			}

            	            a6=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_EntityResource5535); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_2_0_1_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(ofbiz.OfbizPackage.eINSTANCE.getEntityResource(), ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[453]);
            	            			}

            	            // Ofbiz.g:4809:4: (a7_0= parse_ofbiz_RestRequest )
            	            // Ofbiz.g:4810:5: a7_0= parse_ofbiz_RestRequest
            	            {
            	            pushFollow(FOLLOW_parse_ofbiz_RestRequest_in_parse_ofbiz_EntityResource5561);
            	            a7_0=parse_ofbiz_RestRequest();

            	            state._fsp--;
            	            if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a7_0 != null) {
            	            						if (a7_0 != null) {
            	            							Object value = a7_0;
            	            							addObjectToList(element, ofbiz.OfbizPackage.ENTITY_RESOURCE__REQUESTS, value);
            	            							completedElement(value, true);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_2_0_1_2, a7_0, true);
            	            						copyLocalizationInfos(a7_0, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[454]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[455]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[456]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[457]);
            	            			}

            	            }
            	            break;
            	        case 3 :
            	            // Ofbiz.g:4839:8: a8= 'urlPath' a9= ':' (a10= QUOTED_34_34 )
            	            {
            	            a8=(Token)match(input,108,FOLLOW_108_in_parse_ofbiz_EntityResource5597); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_2_0_2_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[458]);
            	            			}

            	            a9=(Token)match(input,11,FOLLOW_11_in_parse_ofbiz_EntityResource5617); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_2_0_2_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[459]);
            	            			}

            	            // Ofbiz.g:4867:4: (a10= QUOTED_34_34 )
            	            // Ofbiz.g:4868:5: a10= QUOTED_34_34
            	            {
            	            a10=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_ofbiz_EntityResource5643); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new ofbiz.resource.ofbiz.mopp.OfbizTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a10 != null) {
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            	            						tokenResolver.setOptions(getOptions());
            	            						ofbiz.resource.ofbiz.IOfbizTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a10.getText(), element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__URL_PATH), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a10).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a10).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a10).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a10).getStopIndex());
            	            						}
            	            						java.lang.String resolved = (java.lang.String) resolvedObject;
            	            						if (resolved != null) {
            	            							Object value = resolved;
            	            							element.eSet(element.eClass().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__URL_PATH), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_2_0_2_2, resolved, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a10, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[460]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[461]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[462]);
            	            				addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[463]);
            	            			}

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[464]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[465]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[466]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[467]);
            	}

            a11=(Token)match(input,114,FOLLOW_114_in_parse_ofbiz_EntityResource5689); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = ofbiz.OfbizFactory.eINSTANCE.createEntityResource();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.OFBIZ_12_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[468]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[469]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[470]);
            		addExpectedElement(null, ofbiz.resource.ofbiz.mopp.OfbizExpectationConstants.EXPECTATIONS[471]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 14, parse_ofbiz_EntityResource_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_ofbiz_EntityResource"

    // Delegated rules


 

    public static final BitSet FOLLOW_parse_ofbiz_OfbizComponent_in_start82 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_start89 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_parse_ofbiz_OfbizComponent115 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L});
    public static final BitSet FOLLOW_113_in_parse_ofbiz_OfbizComponent129 = new BitSet(new long[]{0x0200000000000000L,0x0004000880000000L});
    public static final BitSet FOLLOW_57_in_parse_ofbiz_OfbizComponent152 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_OfbizComponent172 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_parse_ofbiz_EntityModel_in_parse_ofbiz_OfbizComponent198 = new BitSet(new long[]{0x0200000000000000L,0x0004000880000000L});
    public static final BitSet FOLLOW_99_in_parse_ofbiz_OfbizComponent234 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_OfbizComponent254 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_parse_ofbiz_Services_in_parse_ofbiz_OfbizComponent280 = new BitSet(new long[]{0x0200000000000000L,0x0004000880000000L});
    public static final BitSet FOLLOW_95_in_parse_ofbiz_OfbizComponent316 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_OfbizComponent336 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_parse_ofbiz_RestService_in_parse_ofbiz_OfbizComponent362 = new BitSet(new long[]{0x0200000000000000L,0x0004000880000000L});
    public static final BitSet FOLLOW_114_in_parse_ofbiz_OfbizComponent403 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_parse_ofbiz_EntityModel432 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L});
    public static final BitSet FOLLOW_113_in_parse_ofbiz_EntityModel446 = new BitSet(new long[]{0x0024000000000000L,0x0004420000000000L});
    public static final BitSet FOLLOW_105_in_parse_ofbiz_EntityModel469 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_EntityModel489 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_ofbiz_EntityModel515 = new BitSet(new long[]{0x0024000000000000L,0x0004420000000000L});
    public static final BitSet FOLLOW_50_in_parse_ofbiz_EntityModel556 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_EntityModel576 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_ofbiz_EntityModel602 = new BitSet(new long[]{0x0024000000000000L,0x0004420000000000L});
    public static final BitSet FOLLOW_110_in_parse_ofbiz_EntityModel643 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_EntityModel663 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_ofbiz_EntityModel689 = new BitSet(new long[]{0x0024000000000000L,0x0004420000000000L});
    public static final BitSet FOLLOW_53_in_parse_ofbiz_EntityModel730 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_EntityModel750 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_parse_ofbiz_Entity_in_parse_ofbiz_EntityModel776 = new BitSet(new long[]{0x0024000000000000L,0x0004420000000000L});
    public static final BitSet FOLLOW_114_in_parse_ofbiz_EntityModel817 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_parse_ofbiz_Entity846 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L});
    public static final BitSet FOLLOW_113_in_parse_ofbiz_Entity860 = new BitSet(new long[]{0x0500000000000000L,0x0004020015000000L});
    public static final BitSet FOLLOW_56_in_parse_ofbiz_Entity883 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Entity903 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_ofbiz_Entity929 = new BitSet(new long[]{0x0500000000000000L,0x0004020015000000L});
    public static final BitSet FOLLOW_88_in_parse_ofbiz_Entity970 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Entity990 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_ofbiz_Entity1016 = new BitSet(new long[]{0x0500000000000000L,0x0004020015000000L});
    public static final BitSet FOLLOW_105_in_parse_ofbiz_Entity1057 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Entity1077 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_ofbiz_Entity1103 = new BitSet(new long[]{0x0500000000000000L,0x0004020015000000L});
    public static final BitSet FOLLOW_90_in_parse_ofbiz_Entity1144 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Entity1164 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_TEXT_in_parse_ofbiz_Entity1190 = new BitSet(new long[]{0x0500000000000000L,0x0004020015000000L});
    public static final BitSet FOLLOW_58_in_parse_ofbiz_Entity1231 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Entity1251 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_parse_ofbiz_Field_in_parse_ofbiz_Entity1277 = new BitSet(new long[]{0x0500000000000000L,0x0004020015000000L});
    public static final BitSet FOLLOW_92_in_parse_ofbiz_Entity1313 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Entity1333 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_parse_ofbiz_Relation_in_parse_ofbiz_Entity1359 = new BitSet(new long[]{0x0500000000000000L,0x0004020015000000L});
    public static final BitSet FOLLOW_114_in_parse_ofbiz_Entity1400 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_parse_ofbiz_Field1429 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L});
    public static final BitSet FOLLOW_113_in_parse_ofbiz_Field1443 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_83_in_parse_ofbiz_Field1466 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Field1486 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_ofbiz_Field1512 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_106_in_parse_ofbiz_Field1553 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Field1573 = new BitSet(new long[]{0x280FFE0000000000L,0x0001A990003A00FEL});
    public static final BitSet FOLLOW_85_in_parse_ofbiz_Field1606 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_41_in_parse_ofbiz_Field1625 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_42_in_parse_ofbiz_Field1644 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_49_in_parse_ofbiz_Field1663 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_48_in_parse_ofbiz_Field1682 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_104_in_parse_ofbiz_Field1701 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_46_in_parse_ofbiz_Field1720 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_47_in_parse_ofbiz_Field1739 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_59_in_parse_ofbiz_Field1758 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_61_in_parse_ofbiz_Field1777 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_84_in_parse_ofbiz_Field1796 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_65_in_parse_ofbiz_Field1815 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_66_in_parse_ofbiz_Field1834 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_69_in_parse_ofbiz_Field1853 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_71_in_parse_ofbiz_Field1872 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_112_in_parse_ofbiz_Field1891 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_100_in_parse_ofbiz_Field1910 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_81_in_parse_ofbiz_Field1929 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_111_in_parse_ofbiz_Field1948 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_43_in_parse_ofbiz_Field1967 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_50_in_parse_ofbiz_Field1986 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_83_in_parse_ofbiz_Field2005 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_109_in_parse_ofbiz_Field2024 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_45_in_parse_ofbiz_Field2043 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_44_in_parse_ofbiz_Field2062 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_51_in_parse_ofbiz_Field2081 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_107_in_parse_ofbiz_Field2100 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_68_in_parse_ofbiz_Field2119 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_67_in_parse_ofbiz_Field2138 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_70_in_parse_ofbiz_Field2157 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_103_in_parse_ofbiz_Field2176 = new BitSet(new long[]{0x0000000000000000L,0x0004040000080000L});
    public static final BitSet FOLLOW_114_in_parse_ofbiz_Field2220 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_parse_ofbiz_Relation2249 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L});
    public static final BitSet FOLLOW_113_in_parse_ofbiz_Relation2263 = new BitSet(new long[]{0x1000000000000000L,0x0004040008008000L});
    public static final BitSet FOLLOW_60_in_parse_ofbiz_Relation2286 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Relation2306 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_ofbiz_Relation2332 = new BitSet(new long[]{0x1000000000000000L,0x0004040008008000L});
    public static final BitSet FOLLOW_106_in_parse_ofbiz_Relation2373 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Relation2393 = new BitSet(new long[]{0x0000000005000000L});
    public static final BitSet FOLLOW_26_in_parse_ofbiz_Relation2426 = new BitSet(new long[]{0x1000000000000000L,0x0004040008008000L});
    public static final BitSet FOLLOW_24_in_parse_ofbiz_Relation2445 = new BitSet(new long[]{0x1000000000000000L,0x0004040008008000L});
    public static final BitSet FOLLOW_79_in_parse_ofbiz_Relation2484 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Relation2504 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_parse_ofbiz_KeyMap_in_parse_ofbiz_Relation2530 = new BitSet(new long[]{0x1000000000000000L,0x0004040008008000L});
    public static final BitSet FOLLOW_91_in_parse_ofbiz_Relation2566 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Relation2586 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_TEXT_in_parse_ofbiz_Relation2612 = new BitSet(new long[]{0x1000000000000000L,0x0004040008008000L});
    public static final BitSet FOLLOW_114_in_parse_ofbiz_Relation2658 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_parse_ofbiz_KeyMap2687 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L});
    public static final BitSet FOLLOW_113_in_parse_ofbiz_KeyMap2701 = new BitSet(new long[]{0x0000000000000000L,0x0004000008800000L});
    public static final BitSet FOLLOW_87_in_parse_ofbiz_KeyMap2724 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_KeyMap2744 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_TEXT_in_parse_ofbiz_KeyMap2770 = new BitSet(new long[]{0x0000000000000000L,0x0004000008800000L});
    public static final BitSet FOLLOW_91_in_parse_ofbiz_KeyMap2811 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_KeyMap2831 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_TEXT_in_parse_ofbiz_KeyMap2857 = new BitSet(new long[]{0x0000000000000000L,0x0004000008800000L});
    public static final BitSet FOLLOW_114_in_parse_ofbiz_KeyMap2903 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_parse_ofbiz_Services2932 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L});
    public static final BitSet FOLLOW_113_in_parse_ofbiz_Services2946 = new BitSet(new long[]{0x0004000000000000L,0x0004400800000000L});
    public static final BitSet FOLLOW_50_in_parse_ofbiz_Services2969 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Services2989 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_ofbiz_Services3015 = new BitSet(new long[]{0x0004000000000000L,0x0004400800000000L});
    public static final BitSet FOLLOW_110_in_parse_ofbiz_Services3056 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Services3076 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_ofbiz_Services3102 = new BitSet(new long[]{0x0004000000000000L,0x0004400800000000L});
    public static final BitSet FOLLOW_99_in_parse_ofbiz_Services3143 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Services3163 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_parse_ofbiz_Service_in_parse_ofbiz_Services3189 = new BitSet(new long[]{0x0004000000000000L,0x0004400800000000L});
    public static final BitSet FOLLOW_114_in_parse_ofbiz_Services3230 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_parse_ofbiz_Service3259 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L});
    public static final BitSet FOLLOW_113_in_parse_ofbiz_Service3273 = new BitSet(new long[]{0x0014010000000000L,0x0004000000090200L});
    public static final BitSet FOLLOW_83_in_parse_ofbiz_Service3296 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Service3316 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_ofbiz_Service3342 = new BitSet(new long[]{0x0014010000000000L,0x0004000000090200L});
    public static final BitSet FOLLOW_52_in_parse_ofbiz_Service3383 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Service3403 = new BitSet(new long[]{0xC080000000000000L,0x0000006700007D01L});
    public static final BitSet FOLLOW_55_in_parse_ofbiz_Service3436 = new BitSet(new long[]{0x0014010000000000L,0x0004000000090200L});
    public static final BitSet FOLLOW_63_in_parse_ofbiz_Service3455 = new BitSet(new long[]{0x0014010000000000L,0x0004000000090200L});
    public static final BitSet FOLLOW_72_in_parse_ofbiz_Service3474 = new BitSet(new long[]{0x0014010000000000L,0x0004000000090200L});
    public static final BitSet FOLLOW_75_in_parse_ofbiz_Service3493 = new BitSet(new long[]{0x0014010000000000L,0x0004000000090200L});
    public static final BitSet FOLLOW_101_in_parse_ofbiz_Service3512 = new BitSet(new long[]{0x0014010000000000L,0x0004000000090200L});
    public static final BitSet FOLLOW_98_in_parse_ofbiz_Service3531 = new BitSet(new long[]{0x0014010000000000L,0x0004000000090200L});
    public static final BitSet FOLLOW_62_in_parse_ofbiz_Service3550 = new BitSet(new long[]{0x0014010000000000L,0x0004000000090200L});
    public static final BitSet FOLLOW_74_in_parse_ofbiz_Service3569 = new BitSet(new long[]{0x0014010000000000L,0x0004000000090200L});
    public static final BitSet FOLLOW_76_in_parse_ofbiz_Service3588 = new BitSet(new long[]{0x0014010000000000L,0x0004000000090200L});
    public static final BitSet FOLLOW_78_in_parse_ofbiz_Service3607 = new BitSet(new long[]{0x0014010000000000L,0x0004000000090200L});
    public static final BitSet FOLLOW_97_in_parse_ofbiz_Service3626 = new BitSet(new long[]{0x0014010000000000L,0x0004000000090200L});
    public static final BitSet FOLLOW_64_in_parse_ofbiz_Service3645 = new BitSet(new long[]{0x0014010000000000L,0x0004000000090200L});
    public static final BitSet FOLLOW_77_in_parse_ofbiz_Service3664 = new BitSet(new long[]{0x0014010000000000L,0x0004000000090200L});
    public static final BitSet FOLLOW_96_in_parse_ofbiz_Service3683 = new BitSet(new long[]{0x0014010000000000L,0x0004000000090200L});
    public static final BitSet FOLLOW_102_in_parse_ofbiz_Service3702 = new BitSet(new long[]{0x0014010000000000L,0x0004000000090200L});
    public static final BitSet FOLLOW_80_in_parse_ofbiz_Service3741 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Service3761 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_ofbiz_Service3787 = new BitSet(new long[]{0x0014010000000000L,0x0004000000090200L});
    public static final BitSet FOLLOW_73_in_parse_ofbiz_Service3828 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Service3848 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_ofbiz_Service3874 = new BitSet(new long[]{0x0014010000000000L,0x0004000000090200L});
    public static final BitSet FOLLOW_50_in_parse_ofbiz_Service3915 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Service3935 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_ofbiz_Service3961 = new BitSet(new long[]{0x0014010000000000L,0x0004000000090200L});
    public static final BitSet FOLLOW_40_in_parse_ofbiz_Service4002 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Service4022 = new BitSet(new long[]{0x0000000000001000L,0x0000000000400000L});
    public static final BitSet FOLLOW_parse_ofbiz_Attribute_in_parse_ofbiz_Service4048 = new BitSet(new long[]{0x0014010000000000L,0x0004000000090200L});
    public static final BitSet FOLLOW_114_in_parse_ofbiz_Service4089 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_86_in_parse_ofbiz_Attribute4127 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_parse_ofbiz_Attribute4148 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L});
    public static final BitSet FOLLOW_113_in_parse_ofbiz_Attribute4162 = new BitSet(new long[]{0x0000000000000000L,0x00040400000C0000L});
    public static final BitSet FOLLOW_83_in_parse_ofbiz_Attribute4185 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Attribute4205 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_ofbiz_Attribute4231 = new BitSet(new long[]{0x0000000000000000L,0x00040400000C0000L});
    public static final BitSet FOLLOW_106_in_parse_ofbiz_Attribute4272 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Attribute4292 = new BitSet(new long[]{0x0000006002806000L});
    public static final BitSet FOLLOW_37_in_parse_ofbiz_Attribute4325 = new BitSet(new long[]{0x0000000000000000L,0x00040400000C0000L});
    public static final BitSet FOLLOW_14_in_parse_ofbiz_Attribute4344 = new BitSet(new long[]{0x0000000000000000L,0x00040400000C0000L});
    public static final BitSet FOLLOW_13_in_parse_ofbiz_Attribute4363 = new BitSet(new long[]{0x0000000000000000L,0x00040400000C0000L});
    public static final BitSet FOLLOW_38_in_parse_ofbiz_Attribute4382 = new BitSet(new long[]{0x0000000000000000L,0x00040400000C0000L});
    public static final BitSet FOLLOW_23_in_parse_ofbiz_Attribute4401 = new BitSet(new long[]{0x0000000000000000L,0x00040400000C0000L});
    public static final BitSet FOLLOW_25_in_parse_ofbiz_Attribute4420 = new BitSet(new long[]{0x0000000000000000L,0x00040400000C0000L});
    public static final BitSet FOLLOW_82_in_parse_ofbiz_Attribute4459 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_Attribute4479 = new BitSet(new long[]{0x0000000008200000L});
    public static final BitSet FOLLOW_21_in_parse_ofbiz_Attribute4512 = new BitSet(new long[]{0x0000000000000000L,0x00040400000C0000L});
    public static final BitSet FOLLOW_27_in_parse_ofbiz_Attribute4531 = new BitSet(new long[]{0x0000000000000000L,0x00040400000C0000L});
    public static final BitSet FOLLOW_114_in_parse_ofbiz_Attribute4575 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_parse_ofbiz_RestService4604 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L});
    public static final BitSet FOLLOW_113_in_parse_ofbiz_RestService4618 = new BitSet(new long[]{0x0000000000000000L,0x0004400040080000L});
    public static final BitSet FOLLOW_83_in_parse_ofbiz_RestService4641 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_RestService4661 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_ofbiz_RestService4687 = new BitSet(new long[]{0x0000000000000000L,0x0004400040080000L});
    public static final BitSet FOLLOW_110_in_parse_ofbiz_RestService4728 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_RestService4748 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_ofbiz_RestService4774 = new BitSet(new long[]{0x0000000000000000L,0x0004400040080000L});
    public static final BitSet FOLLOW_94_in_parse_ofbiz_RestService4815 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_RestService4835 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_parse_ofbiz_EntityResource_in_parse_ofbiz_RestService4861 = new BitSet(new long[]{0x0000000000000000L,0x0004400040080000L});
    public static final BitSet FOLLOW_114_in_parse_ofbiz_RestService4902 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_parse_ofbiz_RestRequest4931 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L});
    public static final BitSet FOLLOW_113_in_parse_ofbiz_RestRequest4945 = new BitSet(new long[]{0x0000008000000000L,0x0004000002000000L});
    public static final BitSet FOLLOW_39_in_parse_ofbiz_RestRequest4968 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_RestRequest4988 = new BitSet(new long[]{0x0000000060108000L});
    public static final BitSet FOLLOW_20_in_parse_ofbiz_RestRequest5021 = new BitSet(new long[]{0x0000008000000000L,0x0004000002000000L});
    public static final BitSet FOLLOW_30_in_parse_ofbiz_RestRequest5040 = new BitSet(new long[]{0x0000008000000000L,0x0004000002000000L});
    public static final BitSet FOLLOW_29_in_parse_ofbiz_RestRequest5059 = new BitSet(new long[]{0x0000008000000000L,0x0004000002000000L});
    public static final BitSet FOLLOW_15_in_parse_ofbiz_RestRequest5078 = new BitSet(new long[]{0x0000008000000000L,0x0004000002000000L});
    public static final BitSet FOLLOW_89_in_parse_ofbiz_RestRequest5117 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_RestRequest5137 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_parse_ofbiz_RestParameter_in_parse_ofbiz_RestRequest5163 = new BitSet(new long[]{0x0000008000000000L,0x0004000002000000L});
    public static final BitSet FOLLOW_114_in_parse_ofbiz_RestRequest5204 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_parse_ofbiz_RestParameter5233 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L});
    public static final BitSet FOLLOW_113_in_parse_ofbiz_RestParameter5247 = new BitSet(new long[]{0x0000000000000000L,0x0004000000080000L});
    public static final BitSet FOLLOW_83_in_parse_ofbiz_RestParameter5270 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_RestParameter5290 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_ofbiz_RestParameter5316 = new BitSet(new long[]{0x0000000000000000L,0x0004000000080000L});
    public static final BitSet FOLLOW_114_in_parse_ofbiz_RestParameter5362 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_parse_ofbiz_EntityResource5391 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L});
    public static final BitSet FOLLOW_113_in_parse_ofbiz_EntityResource5405 = new BitSet(new long[]{0x0040000000000000L,0x0004100020000000L});
    public static final BitSet FOLLOW_54_in_parse_ofbiz_EntityResource5428 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_EntityResource5448 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_TEXT_in_parse_ofbiz_EntityResource5474 = new BitSet(new long[]{0x0040000000000000L,0x0004100020000000L});
    public static final BitSet FOLLOW_93_in_parse_ofbiz_EntityResource5515 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_EntityResource5535 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_parse_ofbiz_RestRequest_in_parse_ofbiz_EntityResource5561 = new BitSet(new long[]{0x0040000000000000L,0x0004100020000000L});
    public static final BitSet FOLLOW_108_in_parse_ofbiz_EntityResource5597 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_ofbiz_EntityResource5617 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_ofbiz_EntityResource5643 = new BitSet(new long[]{0x0040000000000000L,0x0004100020000000L});
    public static final BitSet FOLLOW_114_in_parse_ofbiz_EntityResource5689 = new BitSet(new long[]{0x0000000000000002L});

}