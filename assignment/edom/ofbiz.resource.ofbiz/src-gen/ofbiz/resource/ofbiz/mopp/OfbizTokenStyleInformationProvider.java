/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.mopp;

public class OfbizTokenStyleInformationProvider {
	
	public static String TASK_ITEM_TOKEN_NAME = "TASK_ITEM";
	
	public ofbiz.resource.ofbiz.IOfbizTokenStyle getDefaultTokenStyle(String tokenName) {
		if ("OfbizComponent".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("entitymodel".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("services".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("rest".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("EntityModel".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("title".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("description".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("version".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("entities".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("Entity".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("entityName".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("packageName".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("primKey".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("fields".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("relations".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("Field".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("name".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("type".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("Relation".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("fkName".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("keyMap".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("related".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("KeyMap".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("origin".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("Services".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("Service".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("engine".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("location".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("invoke".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("attributes".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("Attribute".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("mode".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("RestService".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("resources".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("RestRequest".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("action".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("parameters".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("RestParameter".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("EntityResource".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("entity".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("requests".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("urlPath".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("object".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("blob".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("byte-array".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("date-time".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("date".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("time".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("currency-ammount".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("currency-precise".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("fixed-point".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("floating-point".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("numeric".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("id".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("id-long".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("id-vlong".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("indicator".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("very-short".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("short-varchar".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("long-varchar".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("very-long".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("comment".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("value".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("credit-card-number".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("credit-card-date".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("email".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("url".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("id-ne".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("id-long-ne".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("id-vlong-ne".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("tel-number".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("ONE".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("MANY".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("entity-auto".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("group".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("interface".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("java".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("simple".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("script".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("groovy".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("jacl".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("javascript".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("jpython".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("route".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("http".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("jms".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("rmi".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("soap".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("optional".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("String".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("Boolean".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("BigDecimal".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("Timestamp".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("List".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("Map".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("IN".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("OUT".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("GET".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("PUT".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("POST".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("DELETE".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("QUOTED_34_34".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x2A, 0x00, 0xFF}, null, false, false, false, false);
		}
		if ("TASK_ITEM".equals(tokenName)) {
			return new ofbiz.resource.ofbiz.mopp.OfbizTokenStyle(new int[] {0x7F, 0x9F, 0xBF}, null, true, false, false, false);
		}
		return null;
	}
	
}
