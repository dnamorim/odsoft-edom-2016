/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.mopp;

public class OfbizAntlrScanner implements ofbiz.resource.ofbiz.IOfbizTextScanner {
	
	private org.antlr.runtime3_4_0.Lexer antlrLexer;
	
	public OfbizAntlrScanner(org.antlr.runtime3_4_0.Lexer antlrLexer) {
		this.antlrLexer = antlrLexer;
	}
	
	public ofbiz.resource.ofbiz.IOfbizTextToken getNextToken() {
		if (antlrLexer.getCharStream() == null) {
			return null;
		}
		final org.antlr.runtime3_4_0.Token current = antlrLexer.nextToken();
		if (current == null || current.getType() < 0) {
			return null;
		}
		ofbiz.resource.ofbiz.IOfbizTextToken result = new ofbiz.resource.ofbiz.mopp.OfbizANTLRTextToken(current);
		return result;
	}
	
	public void setText(String text) {
		antlrLexer.setCharStream(new org.antlr.runtime3_4_0.ANTLRStringStream(text));
	}
	
}
