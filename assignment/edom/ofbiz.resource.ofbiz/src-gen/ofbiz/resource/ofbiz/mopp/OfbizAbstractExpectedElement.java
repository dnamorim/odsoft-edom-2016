/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.mopp;

/**
 * Abstract super class for all expected elements. Provides methods to add
 * followers.
 */
public abstract class OfbizAbstractExpectedElement implements ofbiz.resource.ofbiz.IOfbizExpectedElement {
	
	private org.eclipse.emf.ecore.EClass ruleMetaclass;
	
	private java.util.Set<ofbiz.resource.ofbiz.util.OfbizPair<ofbiz.resource.ofbiz.IOfbizExpectedElement, ofbiz.resource.ofbiz.mopp.OfbizContainedFeature[]>> followers = new java.util.LinkedHashSet<ofbiz.resource.ofbiz.util.OfbizPair<ofbiz.resource.ofbiz.IOfbizExpectedElement, ofbiz.resource.ofbiz.mopp.OfbizContainedFeature[]>>();
	
	public OfbizAbstractExpectedElement(org.eclipse.emf.ecore.EClass ruleMetaclass) {
		super();
		this.ruleMetaclass = ruleMetaclass;
	}
	
	public org.eclipse.emf.ecore.EClass getRuleMetaclass() {
		return ruleMetaclass;
	}
	
	public void addFollower(ofbiz.resource.ofbiz.IOfbizExpectedElement follower, ofbiz.resource.ofbiz.mopp.OfbizContainedFeature[] path) {
		followers.add(new ofbiz.resource.ofbiz.util.OfbizPair<ofbiz.resource.ofbiz.IOfbizExpectedElement, ofbiz.resource.ofbiz.mopp.OfbizContainedFeature[]>(follower, path));
	}
	
	public java.util.Collection<ofbiz.resource.ofbiz.util.OfbizPair<ofbiz.resource.ofbiz.IOfbizExpectedElement, ofbiz.resource.ofbiz.mopp.OfbizContainedFeature[]>> getFollowers() {
		return followers;
	}
	
}
