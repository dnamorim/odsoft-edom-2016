/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz;

/**
 * An element that is expected at a given position in a resource stream.
 */
public interface IOfbizExpectedElement {
	
	/**
	 * Returns the names of all tokens that are expected at the given position.
	 */
	public java.util.Set<String> getTokenNames();
	
	/**
	 * Returns the metaclass of the rule that contains the expected element.
	 */
	public org.eclipse.emf.ecore.EClass getRuleMetaclass();
	
	/**
	 * Returns the syntax element that is expected.
	 */
	public ofbiz.resource.ofbiz.grammar.OfbizSyntaxElement getSymtaxElement();
	
	/**
	 * Adds an element that is a valid follower for this element.
	 */
	public void addFollower(ofbiz.resource.ofbiz.IOfbizExpectedElement follower, ofbiz.resource.ofbiz.mopp.OfbizContainedFeature[] path);
	
	/**
	 * Returns all valid followers for this element. Each follower is represented by a
	 * pair of an expected elements and the containment trace that leads from the
	 * current element to the follower.
	 */
	public java.util.Collection<ofbiz.resource.ofbiz.util.OfbizPair<ofbiz.resource.ofbiz.IOfbizExpectedElement, ofbiz.resource.ofbiz.mopp.OfbizContainedFeature[]>> getFollowers();
	
}
