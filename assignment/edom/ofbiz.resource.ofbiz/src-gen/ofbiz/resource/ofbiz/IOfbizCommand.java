/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz;

/**
 * A simple interface for commands that can be executed and that return
 * information about the success of their execution.
 */
public interface IOfbizCommand<ContextType> {
	
	public boolean execute(ContextType context);
}
