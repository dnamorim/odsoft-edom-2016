/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.grammar;

public class OfbizWhiteSpace extends ofbiz.resource.ofbiz.grammar.OfbizFormattingElement {
	
	private final int amount;
	
	public OfbizWhiteSpace(int amount, ofbiz.resource.ofbiz.grammar.OfbizCardinality cardinality) {
		super(cardinality);
		this.amount = amount;
	}
	
	public int getAmount() {
		return amount;
	}
	
	public String toString() {
		return "#" + getAmount();
	}
	
}
