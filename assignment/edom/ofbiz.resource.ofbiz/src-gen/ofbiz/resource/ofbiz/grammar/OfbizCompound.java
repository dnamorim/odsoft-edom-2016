/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.grammar;

public class OfbizCompound extends ofbiz.resource.ofbiz.grammar.OfbizSyntaxElement {
	
	public OfbizCompound(ofbiz.resource.ofbiz.grammar.OfbizChoice choice, ofbiz.resource.ofbiz.grammar.OfbizCardinality cardinality) {
		super(cardinality, new ofbiz.resource.ofbiz.grammar.OfbizSyntaxElement[] {choice});
	}
	
	public String toString() {
		return "(" + getChildren()[0] + ")";
	}
	
}
