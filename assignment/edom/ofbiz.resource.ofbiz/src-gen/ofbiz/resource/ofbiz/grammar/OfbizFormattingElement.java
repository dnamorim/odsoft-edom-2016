/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.grammar;

public abstract class OfbizFormattingElement extends ofbiz.resource.ofbiz.grammar.OfbizSyntaxElement {
	
	public OfbizFormattingElement(ofbiz.resource.ofbiz.grammar.OfbizCardinality cardinality) {
		super(cardinality, null);
	}
	
}
