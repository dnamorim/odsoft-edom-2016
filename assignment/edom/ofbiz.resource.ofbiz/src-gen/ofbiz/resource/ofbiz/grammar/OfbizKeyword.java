/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.grammar;

/**
 * A class to represent a keyword in the grammar.
 */
public class OfbizKeyword extends ofbiz.resource.ofbiz.grammar.OfbizSyntaxElement {
	
	private final String value;
	
	public OfbizKeyword(String value, ofbiz.resource.ofbiz.grammar.OfbizCardinality cardinality) {
		super(cardinality, null);
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	public String toString() {
		return value;
	}
	
}
