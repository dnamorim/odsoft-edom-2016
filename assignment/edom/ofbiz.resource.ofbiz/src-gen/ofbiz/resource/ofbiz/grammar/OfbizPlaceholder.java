/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.grammar;

/**
 * A class to represent placeholders in a grammar.
 */
public class OfbizPlaceholder extends ofbiz.resource.ofbiz.grammar.OfbizTerminal {
	
	private final String tokenName;
	
	public OfbizPlaceholder(org.eclipse.emf.ecore.EStructuralFeature feature, String tokenName, ofbiz.resource.ofbiz.grammar.OfbizCardinality cardinality, int mandatoryOccurencesAfter) {
		super(feature, cardinality, mandatoryOccurencesAfter);
		this.tokenName = tokenName;
	}
	
	public String getTokenName() {
		return tokenName;
	}
	
}
