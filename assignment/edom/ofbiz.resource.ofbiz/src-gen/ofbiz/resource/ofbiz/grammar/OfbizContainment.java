/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.grammar;

public class OfbizContainment extends ofbiz.resource.ofbiz.grammar.OfbizTerminal {
	
	private final org.eclipse.emf.ecore.EClass[] allowedTypes;
	
	public OfbizContainment(org.eclipse.emf.ecore.EStructuralFeature feature, ofbiz.resource.ofbiz.grammar.OfbizCardinality cardinality, org.eclipse.emf.ecore.EClass[] allowedTypes, int mandatoryOccurencesAfter) {
		super(feature, cardinality, mandatoryOccurencesAfter);
		this.allowedTypes = allowedTypes;
	}
	
	public org.eclipse.emf.ecore.EClass[] getAllowedTypes() {
		return allowedTypes;
	}
	
	public String toString() {
		String typeRestrictions = null;
		if (allowedTypes != null && allowedTypes.length > 0) {
			typeRestrictions = ofbiz.resource.ofbiz.util.OfbizStringUtil.explode(allowedTypes, ", ", new ofbiz.resource.ofbiz.IOfbizFunction1<String, org.eclipse.emf.ecore.EClass>() {
				public String execute(org.eclipse.emf.ecore.EClass eClass) {
					return eClass.getName();
				}
			});
		}
		return getFeature().getName() + (typeRestrictions == null ? "" : "[" + typeRestrictions + "]");
	}
	
}
