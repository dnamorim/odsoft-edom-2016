/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.grammar;

public class OfbizGrammarInformationProvider {
	
	public final static org.eclipse.emf.ecore.EStructuralFeature ANONYMOUS_FEATURE = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
	static {
		ANONYMOUS_FEATURE.setName("_");
	}
	
	public final static OfbizGrammarInformationProvider INSTANCE = new OfbizGrammarInformationProvider();
	
	private java.util.Set<String> keywords;
	
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_0_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("OfbizComponent", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_0_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("{", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_0_0_0_2_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("entitymodel", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_0_0_0_2_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizContainment OFBIZ_0_0_0_2_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizContainment(ofbiz.OfbizPackage.eINSTANCE.getOfbizComponent().getEStructuralFeature(ofbiz.OfbizPackage.OFBIZ_COMPONENT__ENTITYMODEL), ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, new org.eclipse.emf.ecore.EClass[] {ofbiz.OfbizPackage.eINSTANCE.getEntityModel(), }, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_0_0_0_2_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_0_0_0_2_0_0_0, OFBIZ_0_0_0_2_0_0_1, OFBIZ_0_0_0_2_0_0_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_0_0_0_2_0_1_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("services", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_0_0_0_2_0_1_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizContainment OFBIZ_0_0_0_2_0_1_2 = new ofbiz.resource.ofbiz.grammar.OfbizContainment(ofbiz.OfbizPackage.eINSTANCE.getOfbizComponent().getEStructuralFeature(ofbiz.OfbizPackage.OFBIZ_COMPONENT__SERVICES), ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, new org.eclipse.emf.ecore.EClass[] {ofbiz.OfbizPackage.eINSTANCE.getServices(), }, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_0_0_0_2_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_0_0_0_2_0_1_0, OFBIZ_0_0_0_2_0_1_1, OFBIZ_0_0_0_2_0_1_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_0_0_0_2_0_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("rest", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_0_0_0_2_0_2_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizContainment OFBIZ_0_0_0_2_0_2_2 = new ofbiz.resource.ofbiz.grammar.OfbizContainment(ofbiz.OfbizPackage.eINSTANCE.getOfbizComponent().getEStructuralFeature(ofbiz.OfbizPackage.OFBIZ_COMPONENT__REST), ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, new org.eclipse.emf.ecore.EClass[] {ofbiz.OfbizPackage.eINSTANCE.getRestService(), }, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_0_0_0_2_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_0_0_0_2_0_2_0, OFBIZ_0_0_0_2_0_2_1, OFBIZ_0_0_0_2_0_2_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_0_0_0_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_0_0_0_2_0_0, OFBIZ_0_0_0_2_0_1, OFBIZ_0_0_0_2_0_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizCompound OFBIZ_0_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizCompound(OFBIZ_0_0_0_2_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.STAR);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_0_0_0_3 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("}", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_0_0_0_0, OFBIZ_0_0_0_1, OFBIZ_0_0_0_2, OFBIZ_0_0_0_3);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_0_0_0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizRule OFBIZ_0 = new ofbiz.resource.ofbiz.grammar.OfbizRule(ofbiz.OfbizPackage.eINSTANCE.getOfbizComponent(), OFBIZ_0_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_1_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("EntityModel", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_1_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("{", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_1_0_0_2_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("title", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_1_0_0_2_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_1_0_0_2_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getEntityModel().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__TITLE), "QUOTED_34_34", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_1_0_0_2_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_1_0_0_2_0_0_0, OFBIZ_1_0_0_2_0_0_1, OFBIZ_1_0_0_2_0_0_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_1_0_0_2_0_1_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("description", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_1_0_0_2_0_1_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_1_0_0_2_0_1_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getEntityModel().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__DESCRIPTION), "QUOTED_34_34", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_1_0_0_2_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_1_0_0_2_0_1_0, OFBIZ_1_0_0_2_0_1_1, OFBIZ_1_0_0_2_0_1_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_1_0_0_2_0_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("version", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_1_0_0_2_0_2_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_1_0_0_2_0_2_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getEntityModel().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__VERSION), "QUOTED_34_34", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_1_0_0_2_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_1_0_0_2_0_2_0, OFBIZ_1_0_0_2_0_2_1, OFBIZ_1_0_0_2_0_2_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_1_0_0_2_0_3_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("entities", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_1_0_0_2_0_3_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizContainment OFBIZ_1_0_0_2_0_3_2 = new ofbiz.resource.ofbiz.grammar.OfbizContainment(ofbiz.OfbizPackage.eINSTANCE.getEntityModel().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_MODEL__ENTITIES), ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, new org.eclipse.emf.ecore.EClass[] {ofbiz.OfbizPackage.eINSTANCE.getEntity(), }, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_1_0_0_2_0_3 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_1_0_0_2_0_3_0, OFBIZ_1_0_0_2_0_3_1, OFBIZ_1_0_0_2_0_3_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_1_0_0_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_1_0_0_2_0_0, OFBIZ_1_0_0_2_0_1, OFBIZ_1_0_0_2_0_2, OFBIZ_1_0_0_2_0_3);
	public final static ofbiz.resource.ofbiz.grammar.OfbizCompound OFBIZ_1_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizCompound(OFBIZ_1_0_0_2_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.STAR);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_1_0_0_3 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("}", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_1_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_1_0_0_0, OFBIZ_1_0_0_1, OFBIZ_1_0_0_2, OFBIZ_1_0_0_3);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_1_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_1_0_0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizRule OFBIZ_1 = new ofbiz.resource.ofbiz.grammar.OfbizRule(ofbiz.OfbizPackage.eINSTANCE.getEntityModel(), OFBIZ_1_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_2_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("Entity", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_2_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("{", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_2_0_0_2_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("entityName", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_2_0_0_2_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_2_0_0_2_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getEntity().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__ENTITY_NAME), "QUOTED_34_34", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_2_0_0_2_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_2_0_0_2_0_0_0, OFBIZ_2_0_0_2_0_0_1, OFBIZ_2_0_0_2_0_0_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_2_0_0_2_0_1_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("packageName", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_2_0_0_2_0_1_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_2_0_0_2_0_1_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getEntity().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__PACKAGE_NAME), "QUOTED_34_34", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_2_0_0_2_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_2_0_0_2_0_1_0, OFBIZ_2_0_0_2_0_1_1, OFBIZ_2_0_0_2_0_1_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_2_0_0_2_0_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("title", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_2_0_0_2_0_2_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_2_0_0_2_0_2_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getEntity().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__TITLE), "QUOTED_34_34", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_2_0_0_2_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_2_0_0_2_0_2_0, OFBIZ_2_0_0_2_0_2_1, OFBIZ_2_0_0_2_0_2_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_2_0_0_2_0_3_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("primKey", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_2_0_0_2_0_3_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_2_0_0_2_0_3_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getEntity().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__PRIM_KEY), "TEXT", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_2_0_0_2_0_3 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_2_0_0_2_0_3_0, OFBIZ_2_0_0_2_0_3_1, OFBIZ_2_0_0_2_0_3_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_2_0_0_2_0_4_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("fields", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_2_0_0_2_0_4_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizContainment OFBIZ_2_0_0_2_0_4_2 = new ofbiz.resource.ofbiz.grammar.OfbizContainment(ofbiz.OfbizPackage.eINSTANCE.getEntity().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__FIELDS), ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, new org.eclipse.emf.ecore.EClass[] {ofbiz.OfbizPackage.eINSTANCE.getField(), }, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_2_0_0_2_0_4 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_2_0_0_2_0_4_0, OFBIZ_2_0_0_2_0_4_1, OFBIZ_2_0_0_2_0_4_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_2_0_0_2_0_5_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("relations", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_2_0_0_2_0_5_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizContainment OFBIZ_2_0_0_2_0_5_2 = new ofbiz.resource.ofbiz.grammar.OfbizContainment(ofbiz.OfbizPackage.eINSTANCE.getEntity().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY__RELATIONS), ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, new org.eclipse.emf.ecore.EClass[] {ofbiz.OfbizPackage.eINSTANCE.getRelation(), }, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_2_0_0_2_0_5 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_2_0_0_2_0_5_0, OFBIZ_2_0_0_2_0_5_1, OFBIZ_2_0_0_2_0_5_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_2_0_0_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_2_0_0_2_0_0, OFBIZ_2_0_0_2_0_1, OFBIZ_2_0_0_2_0_2, OFBIZ_2_0_0_2_0_3, OFBIZ_2_0_0_2_0_4, OFBIZ_2_0_0_2_0_5);
	public final static ofbiz.resource.ofbiz.grammar.OfbizCompound OFBIZ_2_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizCompound(OFBIZ_2_0_0_2_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.STAR);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_2_0_0_3 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("}", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_2_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_2_0_0_0, OFBIZ_2_0_0_1, OFBIZ_2_0_0_2, OFBIZ_2_0_0_3);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_2_0_0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizRule OFBIZ_2 = new ofbiz.resource.ofbiz.grammar.OfbizRule(ofbiz.OfbizPackage.eINSTANCE.getEntity(), OFBIZ_2_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_3_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("Field", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_3_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("{", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_3_0_0_2_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("name", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_3_0_0_2_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_3_0_0_2_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getField().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__NAME), "QUOTED_34_34", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_3_0_0_2_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_3_0_0_2_0_0_0, OFBIZ_3_0_0_2_0_0_1, OFBIZ_3_0_0_2_0_0_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_3_0_0_2_0_1_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("type", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_3_0_0_2_0_1_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizEnumerationTerminal OFBIZ_3_0_0_2_0_1_2 = new ofbiz.resource.ofbiz.grammar.OfbizEnumerationTerminal(ofbiz.OfbizPackage.eINSTANCE.getField().getEStructuralFeature(ofbiz.OfbizPackage.FIELD__TYPE), new String[] {"object", "object", "blob", "blob", "byte_array", "byte-array", "date_time", "date-time", "date", "date", "time", "time", "currency_ammount", "currency-ammount", "currency_precise", "currency-precise", "fixed_point", "fixed-point", "floating_point", "floating-point", "numeric", "numeric", "id", "id", "id_long", "id-long", "id_vlong", "id-vlong", "indicator", "indicator", "very_short", "very-short", "short_varchar", "short-varchar", "long_varchar", "long-varchar", "very_long", "very-long", "comment", "comment", "description", "description", "name", "name", "value", "value", "credit_card_number", "credit-card-number", "credit_card_date", "credit-card-date", "email", "email", "url", "url", "id_ne", "id-ne", "id_long_ne", "id-long-ne", "id_vlong_ne", "id-vlong-ne", "tel_number", "tel-number", }, ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_3_0_0_2_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_3_0_0_2_0_1_0, OFBIZ_3_0_0_2_0_1_1, OFBIZ_3_0_0_2_0_1_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_3_0_0_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_3_0_0_2_0_0, OFBIZ_3_0_0_2_0_1);
	public final static ofbiz.resource.ofbiz.grammar.OfbizCompound OFBIZ_3_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizCompound(OFBIZ_3_0_0_2_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.STAR);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_3_0_0_3 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("}", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_3_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_3_0_0_0, OFBIZ_3_0_0_1, OFBIZ_3_0_0_2, OFBIZ_3_0_0_3);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_3_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_3_0_0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizRule OFBIZ_3 = new ofbiz.resource.ofbiz.grammar.OfbizRule(ofbiz.OfbizPackage.eINSTANCE.getField(), OFBIZ_3_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_4_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("Relation", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_4_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("{", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_4_0_0_2_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("fkName", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_4_0_0_2_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_4_0_0_2_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getRelation().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__FK_NAME), "QUOTED_34_34", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_4_0_0_2_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_4_0_0_2_0_0_0, OFBIZ_4_0_0_2_0_0_1, OFBIZ_4_0_0_2_0_0_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_4_0_0_2_0_1_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("type", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_4_0_0_2_0_1_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizEnumerationTerminal OFBIZ_4_0_0_2_0_1_2 = new ofbiz.resource.ofbiz.grammar.OfbizEnumerationTerminal(ofbiz.OfbizPackage.eINSTANCE.getRelation().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__TYPE), new String[] {"ONE", "ONE", "MANY", "MANY", }, ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_4_0_0_2_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_4_0_0_2_0_1_0, OFBIZ_4_0_0_2_0_1_1, OFBIZ_4_0_0_2_0_1_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_4_0_0_2_0_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("keyMap", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_4_0_0_2_0_2_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizContainment OFBIZ_4_0_0_2_0_2_2 = new ofbiz.resource.ofbiz.grammar.OfbizContainment(ofbiz.OfbizPackage.eINSTANCE.getRelation().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__KEY_MAP), ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, new org.eclipse.emf.ecore.EClass[] {ofbiz.OfbizPackage.eINSTANCE.getKeyMap(), }, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_4_0_0_2_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_4_0_0_2_0_2_0, OFBIZ_4_0_0_2_0_2_1, OFBIZ_4_0_0_2_0_2_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_4_0_0_2_0_3_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("related", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_4_0_0_2_0_3_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_4_0_0_2_0_3_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getRelation().getEStructuralFeature(ofbiz.OfbizPackage.RELATION__RELATED), "TEXT", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_4_0_0_2_0_3 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_4_0_0_2_0_3_0, OFBIZ_4_0_0_2_0_3_1, OFBIZ_4_0_0_2_0_3_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_4_0_0_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_4_0_0_2_0_0, OFBIZ_4_0_0_2_0_1, OFBIZ_4_0_0_2_0_2, OFBIZ_4_0_0_2_0_3);
	public final static ofbiz.resource.ofbiz.grammar.OfbizCompound OFBIZ_4_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizCompound(OFBIZ_4_0_0_2_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.STAR);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_4_0_0_3 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("}", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_4_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_4_0_0_0, OFBIZ_4_0_0_1, OFBIZ_4_0_0_2, OFBIZ_4_0_0_3);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_4_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_4_0_0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizRule OFBIZ_4 = new ofbiz.resource.ofbiz.grammar.OfbizRule(ofbiz.OfbizPackage.eINSTANCE.getRelation(), OFBIZ_4_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_5_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("KeyMap", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_5_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("{", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_5_0_0_2_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("origin", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_5_0_0_2_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_5_0_0_2_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getKeyMap().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__ORIGIN), "TEXT", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_5_0_0_2_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_5_0_0_2_0_0_0, OFBIZ_5_0_0_2_0_0_1, OFBIZ_5_0_0_2_0_0_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_5_0_0_2_0_1_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("related", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_5_0_0_2_0_1_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_5_0_0_2_0_1_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getKeyMap().getEStructuralFeature(ofbiz.OfbizPackage.KEY_MAP__RELATED), "TEXT", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_5_0_0_2_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_5_0_0_2_0_1_0, OFBIZ_5_0_0_2_0_1_1, OFBIZ_5_0_0_2_0_1_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_5_0_0_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_5_0_0_2_0_0, OFBIZ_5_0_0_2_0_1);
	public final static ofbiz.resource.ofbiz.grammar.OfbizCompound OFBIZ_5_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizCompound(OFBIZ_5_0_0_2_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.STAR);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_5_0_0_3 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("}", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_5_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_5_0_0_0, OFBIZ_5_0_0_1, OFBIZ_5_0_0_2, OFBIZ_5_0_0_3);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_5_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_5_0_0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizRule OFBIZ_5 = new ofbiz.resource.ofbiz.grammar.OfbizRule(ofbiz.OfbizPackage.eINSTANCE.getKeyMap(), OFBIZ_5_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_6_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("Services", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_6_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("{", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_6_0_0_2_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("description", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_6_0_0_2_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_6_0_0_2_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getServices().getEStructuralFeature(ofbiz.OfbizPackage.SERVICES__DESCRIPTION), "QUOTED_34_34", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_6_0_0_2_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_6_0_0_2_0_0_0, OFBIZ_6_0_0_2_0_0_1, OFBIZ_6_0_0_2_0_0_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_6_0_0_2_0_1_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("version", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_6_0_0_2_0_1_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_6_0_0_2_0_1_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getServices().getEStructuralFeature(ofbiz.OfbizPackage.SERVICES__VERSION), "QUOTED_34_34", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_6_0_0_2_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_6_0_0_2_0_1_0, OFBIZ_6_0_0_2_0_1_1, OFBIZ_6_0_0_2_0_1_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_6_0_0_2_0_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("services", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_6_0_0_2_0_2_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizContainment OFBIZ_6_0_0_2_0_2_2 = new ofbiz.resource.ofbiz.grammar.OfbizContainment(ofbiz.OfbizPackage.eINSTANCE.getServices().getEStructuralFeature(ofbiz.OfbizPackage.SERVICES__SERVICES), ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, new org.eclipse.emf.ecore.EClass[] {ofbiz.OfbizPackage.eINSTANCE.getService(), }, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_6_0_0_2_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_6_0_0_2_0_2_0, OFBIZ_6_0_0_2_0_2_1, OFBIZ_6_0_0_2_0_2_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_6_0_0_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_6_0_0_2_0_0, OFBIZ_6_0_0_2_0_1, OFBIZ_6_0_0_2_0_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizCompound OFBIZ_6_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizCompound(OFBIZ_6_0_0_2_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.STAR);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_6_0_0_3 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("}", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_6_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_6_0_0_0, OFBIZ_6_0_0_1, OFBIZ_6_0_0_2, OFBIZ_6_0_0_3);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_6_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_6_0_0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizRule OFBIZ_6 = new ofbiz.resource.ofbiz.grammar.OfbizRule(ofbiz.OfbizPackage.eINSTANCE.getServices(), OFBIZ_6_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_7_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("Service", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_7_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("{", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_7_0_0_2_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("name", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_7_0_0_2_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_7_0_0_2_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getService().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__NAME), "QUOTED_34_34", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_7_0_0_2_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_7_0_0_2_0_0_0, OFBIZ_7_0_0_2_0_0_1, OFBIZ_7_0_0_2_0_0_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_7_0_0_2_0_1_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("engine", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_7_0_0_2_0_1_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizEnumerationTerminal OFBIZ_7_0_0_2_0_1_2 = new ofbiz.resource.ofbiz.grammar.OfbizEnumerationTerminal(ofbiz.OfbizPackage.eINSTANCE.getService().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ENGINE), new String[] {"entity_auto", "entity-auto", "group", "group", "interface", "interface", "java", "java", "simple", "simple", "script", "script", "groovy", "groovy", "jacl", "jacl", "javascript", "javascript", "jpython", "jpython", "route", "route", "http", "http", "jms", "jms", "rmi", "rmi", "soap", "soap", }, ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_7_0_0_2_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_7_0_0_2_0_1_0, OFBIZ_7_0_0_2_0_1_1, OFBIZ_7_0_0_2_0_1_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_7_0_0_2_0_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("location", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_7_0_0_2_0_2_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_7_0_0_2_0_2_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getService().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__LOCATION), "QUOTED_34_34", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_7_0_0_2_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_7_0_0_2_0_2_0, OFBIZ_7_0_0_2_0_2_1, OFBIZ_7_0_0_2_0_2_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_7_0_0_2_0_3_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("invoke", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_7_0_0_2_0_3_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_7_0_0_2_0_3_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getService().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__INVOKE), "QUOTED_34_34", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_7_0_0_2_0_3 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_7_0_0_2_0_3_0, OFBIZ_7_0_0_2_0_3_1, OFBIZ_7_0_0_2_0_3_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_7_0_0_2_0_4_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("description", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_7_0_0_2_0_4_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_7_0_0_2_0_4_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getService().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__DESCRIPTION), "QUOTED_34_34", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_7_0_0_2_0_4 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_7_0_0_2_0_4_0, OFBIZ_7_0_0_2_0_4_1, OFBIZ_7_0_0_2_0_4_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_7_0_0_2_0_5_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("attributes", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_7_0_0_2_0_5_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizContainment OFBIZ_7_0_0_2_0_5_2 = new ofbiz.resource.ofbiz.grammar.OfbizContainment(ofbiz.OfbizPackage.eINSTANCE.getService().getEStructuralFeature(ofbiz.OfbizPackage.SERVICE__ATTRIBUTES), ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, new org.eclipse.emf.ecore.EClass[] {ofbiz.OfbizPackage.eINSTANCE.getAttribute(), }, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_7_0_0_2_0_5 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_7_0_0_2_0_5_0, OFBIZ_7_0_0_2_0_5_1, OFBIZ_7_0_0_2_0_5_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_7_0_0_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_7_0_0_2_0_0, OFBIZ_7_0_0_2_0_1, OFBIZ_7_0_0_2_0_2, OFBIZ_7_0_0_2_0_3, OFBIZ_7_0_0_2_0_4, OFBIZ_7_0_0_2_0_5);
	public final static ofbiz.resource.ofbiz.grammar.OfbizCompound OFBIZ_7_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizCompound(OFBIZ_7_0_0_2_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.STAR);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_7_0_0_3 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("}", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_7_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_7_0_0_0, OFBIZ_7_0_0_1, OFBIZ_7_0_0_2, OFBIZ_7_0_0_3);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_7_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_7_0_0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizRule OFBIZ_7 = new ofbiz.resource.ofbiz.grammar.OfbizRule(ofbiz.OfbizPackage.eINSTANCE.getService(), OFBIZ_7_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizBooleanTerminal OFBIZ_8_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizBooleanTerminal(ofbiz.OfbizPackage.eINSTANCE.getAttribute().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__OPTIONAL), "optional", "", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_8_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("Attribute", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_8_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("{", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_8_0_0_3_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("name", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_8_0_0_3_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_8_0_0_3_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getAttribute().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__NAME), "QUOTED_34_34", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_8_0_0_3_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_8_0_0_3_0_0_0, OFBIZ_8_0_0_3_0_0_1, OFBIZ_8_0_0_3_0_0_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_8_0_0_3_0_1_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("type", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_8_0_0_3_0_1_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizEnumerationTerminal OFBIZ_8_0_0_3_0_1_2 = new ofbiz.resource.ofbiz.grammar.OfbizEnumerationTerminal(ofbiz.OfbizPackage.eINSTANCE.getAttribute().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__TYPE), new String[] {"String", "String", "Boolean", "Boolean", "BigDecimal", "BigDecimal", "Timestamp", "Timestamp", "List", "List", "Map", "Map", }, ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_8_0_0_3_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_8_0_0_3_0_1_0, OFBIZ_8_0_0_3_0_1_1, OFBIZ_8_0_0_3_0_1_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_8_0_0_3_0_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("mode", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_8_0_0_3_0_2_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizEnumerationTerminal OFBIZ_8_0_0_3_0_2_2 = new ofbiz.resource.ofbiz.grammar.OfbizEnumerationTerminal(ofbiz.OfbizPackage.eINSTANCE.getAttribute().getEStructuralFeature(ofbiz.OfbizPackage.ATTRIBUTE__MODE), new String[] {"IN", "IN", "OUT", "OUT", }, ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_8_0_0_3_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_8_0_0_3_0_2_0, OFBIZ_8_0_0_3_0_2_1, OFBIZ_8_0_0_3_0_2_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_8_0_0_3_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_8_0_0_3_0_0, OFBIZ_8_0_0_3_0_1, OFBIZ_8_0_0_3_0_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizCompound OFBIZ_8_0_0_3 = new ofbiz.resource.ofbiz.grammar.OfbizCompound(OFBIZ_8_0_0_3_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.STAR);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_8_0_0_4 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("}", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_8_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_8_0_0_0, OFBIZ_8_0_0_1, OFBIZ_8_0_0_2, OFBIZ_8_0_0_3, OFBIZ_8_0_0_4);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_8_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_8_0_0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizRule OFBIZ_8 = new ofbiz.resource.ofbiz.grammar.OfbizRule(ofbiz.OfbizPackage.eINSTANCE.getAttribute(), OFBIZ_8_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_9_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("RestService", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_9_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("{", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_9_0_0_2_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("name", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_9_0_0_2_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_9_0_0_2_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getRestService().getEStructuralFeature(ofbiz.OfbizPackage.REST_SERVICE__NAME), "QUOTED_34_34", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_9_0_0_2_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_9_0_0_2_0_0_0, OFBIZ_9_0_0_2_0_0_1, OFBIZ_9_0_0_2_0_0_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_9_0_0_2_0_1_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("version", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_9_0_0_2_0_1_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_9_0_0_2_0_1_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getRestService().getEStructuralFeature(ofbiz.OfbizPackage.REST_SERVICE__VERSION), "QUOTED_34_34", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_9_0_0_2_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_9_0_0_2_0_1_0, OFBIZ_9_0_0_2_0_1_1, OFBIZ_9_0_0_2_0_1_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_9_0_0_2_0_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("resources", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_9_0_0_2_0_2_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizContainment OFBIZ_9_0_0_2_0_2_2 = new ofbiz.resource.ofbiz.grammar.OfbizContainment(ofbiz.OfbizPackage.eINSTANCE.getRestService().getEStructuralFeature(ofbiz.OfbizPackage.REST_SERVICE__RESOURCES), ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, new org.eclipse.emf.ecore.EClass[] {ofbiz.OfbizPackage.eINSTANCE.getEntityResource(), }, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_9_0_0_2_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_9_0_0_2_0_2_0, OFBIZ_9_0_0_2_0_2_1, OFBIZ_9_0_0_2_0_2_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_9_0_0_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_9_0_0_2_0_0, OFBIZ_9_0_0_2_0_1, OFBIZ_9_0_0_2_0_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizCompound OFBIZ_9_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizCompound(OFBIZ_9_0_0_2_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.STAR);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_9_0_0_3 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("}", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_9_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_9_0_0_0, OFBIZ_9_0_0_1, OFBIZ_9_0_0_2, OFBIZ_9_0_0_3);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_9_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_9_0_0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizRule OFBIZ_9 = new ofbiz.resource.ofbiz.grammar.OfbizRule(ofbiz.OfbizPackage.eINSTANCE.getRestService(), OFBIZ_9_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_10_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("RestRequest", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_10_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("{", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_10_0_0_2_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("action", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_10_0_0_2_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizEnumerationTerminal OFBIZ_10_0_0_2_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizEnumerationTerminal(ofbiz.OfbizPackage.eINSTANCE.getRestRequest().getEStructuralFeature(ofbiz.OfbizPackage.REST_REQUEST__ACTION), new String[] {"GET", "GET", "PUT", "PUT", "POST", "POST", "DELETE", "DELETE", }, ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_10_0_0_2_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_10_0_0_2_0_0_0, OFBIZ_10_0_0_2_0_0_1, OFBIZ_10_0_0_2_0_0_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_10_0_0_2_0_1_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("parameters", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_10_0_0_2_0_1_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizContainment OFBIZ_10_0_0_2_0_1_2 = new ofbiz.resource.ofbiz.grammar.OfbizContainment(ofbiz.OfbizPackage.eINSTANCE.getRestRequest().getEStructuralFeature(ofbiz.OfbizPackage.REST_REQUEST__PARAMETERS), ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, new org.eclipse.emf.ecore.EClass[] {ofbiz.OfbizPackage.eINSTANCE.getRestParameter(), }, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_10_0_0_2_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_10_0_0_2_0_1_0, OFBIZ_10_0_0_2_0_1_1, OFBIZ_10_0_0_2_0_1_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_10_0_0_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_10_0_0_2_0_0, OFBIZ_10_0_0_2_0_1);
	public final static ofbiz.resource.ofbiz.grammar.OfbizCompound OFBIZ_10_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizCompound(OFBIZ_10_0_0_2_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.STAR);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_10_0_0_3 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("}", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_10_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_10_0_0_0, OFBIZ_10_0_0_1, OFBIZ_10_0_0_2, OFBIZ_10_0_0_3);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_10_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_10_0_0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizRule OFBIZ_10 = new ofbiz.resource.ofbiz.grammar.OfbizRule(ofbiz.OfbizPackage.eINSTANCE.getRestRequest(), OFBIZ_10_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_11_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("RestParameter", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_11_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("{", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_11_0_0_2_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("name", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_11_0_0_2_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_11_0_0_2_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getRestParameter().getEStructuralFeature(ofbiz.OfbizPackage.REST_PARAMETER__NAME), "QUOTED_34_34", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_11_0_0_2_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_11_0_0_2_0_0_0, OFBIZ_11_0_0_2_0_0_1, OFBIZ_11_0_0_2_0_0_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_11_0_0_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_11_0_0_2_0_0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizCompound OFBIZ_11_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizCompound(OFBIZ_11_0_0_2_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.STAR);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_11_0_0_3 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("}", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_11_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_11_0_0_0, OFBIZ_11_0_0_1, OFBIZ_11_0_0_2, OFBIZ_11_0_0_3);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_11_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_11_0_0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizRule OFBIZ_11 = new ofbiz.resource.ofbiz.grammar.OfbizRule(ofbiz.OfbizPackage.eINSTANCE.getRestParameter(), OFBIZ_11_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_12_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("EntityResource", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_12_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("{", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_12_0_0_2_0_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("entity", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_12_0_0_2_0_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_12_0_0_2_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getEntityResource().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__ENTITY), "TEXT", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_12_0_0_2_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_12_0_0_2_0_0_0, OFBIZ_12_0_0_2_0_0_1, OFBIZ_12_0_0_2_0_0_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_12_0_0_2_0_1_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("requests", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_12_0_0_2_0_1_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizContainment OFBIZ_12_0_0_2_0_1_2 = new ofbiz.resource.ofbiz.grammar.OfbizContainment(ofbiz.OfbizPackage.eINSTANCE.getEntityResource().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__REQUESTS), ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, new org.eclipse.emf.ecore.EClass[] {ofbiz.OfbizPackage.eINSTANCE.getRestRequest(), }, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_12_0_0_2_0_1 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_12_0_0_2_0_1_0, OFBIZ_12_0_0_2_0_1_1, OFBIZ_12_0_0_2_0_1_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_12_0_0_2_0_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("urlPath", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_12_0_0_2_0_2_1 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword(":", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizPlaceholder OFBIZ_12_0_0_2_0_2_2 = new ofbiz.resource.ofbiz.grammar.OfbizPlaceholder(ofbiz.OfbizPackage.eINSTANCE.getEntityResource().getEStructuralFeature(ofbiz.OfbizPackage.ENTITY_RESOURCE__URL_PATH), "QUOTED_34_34", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, 0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_12_0_0_2_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_12_0_0_2_0_2_0, OFBIZ_12_0_0_2_0_2_1, OFBIZ_12_0_0_2_0_2_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_12_0_0_2_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_12_0_0_2_0_0, OFBIZ_12_0_0_2_0_1, OFBIZ_12_0_0_2_0_2);
	public final static ofbiz.resource.ofbiz.grammar.OfbizCompound OFBIZ_12_0_0_2 = new ofbiz.resource.ofbiz.grammar.OfbizCompound(OFBIZ_12_0_0_2_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.STAR);
	public final static ofbiz.resource.ofbiz.grammar.OfbizKeyword OFBIZ_12_0_0_3 = new ofbiz.resource.ofbiz.grammar.OfbizKeyword("}", ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	public final static ofbiz.resource.ofbiz.grammar.OfbizSequence OFBIZ_12_0_0 = new ofbiz.resource.ofbiz.grammar.OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_12_0_0_0, OFBIZ_12_0_0_1, OFBIZ_12_0_0_2, OFBIZ_12_0_0_3);
	public final static ofbiz.resource.ofbiz.grammar.OfbizChoice OFBIZ_12_0 = new ofbiz.resource.ofbiz.grammar.OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE, OFBIZ_12_0_0);
	public final static ofbiz.resource.ofbiz.grammar.OfbizRule OFBIZ_12 = new ofbiz.resource.ofbiz.grammar.OfbizRule(ofbiz.OfbizPackage.eINSTANCE.getEntityResource(), OFBIZ_12_0, ofbiz.resource.ofbiz.grammar.OfbizCardinality.ONE);
	
	public static String getSyntaxElementID(ofbiz.resource.ofbiz.grammar.OfbizSyntaxElement syntaxElement) {
		if (syntaxElement == null) {
			// null indicates EOF
			return "<EOF>";
		}
		for (java.lang.reflect.Field field : ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.class.getFields()) {
			Object fieldValue;
			try {
				fieldValue = field.get(null);
				if (fieldValue == syntaxElement) {
					String id = field.getName();
					return id;
				}
			} catch (Exception e) { }
		}
		return null;
	}
	
	public static ofbiz.resource.ofbiz.grammar.OfbizSyntaxElement getSyntaxElementByID(String syntaxElementID) {
		try {
			return (ofbiz.resource.ofbiz.grammar.OfbizSyntaxElement) ofbiz.resource.ofbiz.grammar.OfbizGrammarInformationProvider.class.getField(syntaxElementID).get(null);
		} catch (Exception e) {
			return null;
		}
	}
	
	public final static ofbiz.resource.ofbiz.grammar.OfbizRule[] RULES = new ofbiz.resource.ofbiz.grammar.OfbizRule[] {
		OFBIZ_0,
		OFBIZ_1,
		OFBIZ_2,
		OFBIZ_3,
		OFBIZ_4,
		OFBIZ_5,
		OFBIZ_6,
		OFBIZ_7,
		OFBIZ_8,
		OFBIZ_9,
		OFBIZ_10,
		OFBIZ_11,
		OFBIZ_12,
	};
	
	/**
	 * Returns all keywords of the grammar. This includes all literals for boolean and
	 * enumeration terminals.
	 */
	public java.util.Set<String> getKeywords() {
		if (this.keywords == null) {
			this.keywords = new java.util.LinkedHashSet<String>();
			for (ofbiz.resource.ofbiz.grammar.OfbizRule rule : RULES) {
				findKeywords(rule, this.keywords);
			}
		}
		return keywords;
	}
	
	/**
	 * Finds all keywords in the given element and its children and adds them to the
	 * set. This includes all literals for boolean and enumeration terminals.
	 */
	private void findKeywords(ofbiz.resource.ofbiz.grammar.OfbizSyntaxElement element, java.util.Set<String> keywords) {
		if (element instanceof ofbiz.resource.ofbiz.grammar.OfbizKeyword) {
			keywords.add(((ofbiz.resource.ofbiz.grammar.OfbizKeyword) element).getValue());
		} else if (element instanceof ofbiz.resource.ofbiz.grammar.OfbizBooleanTerminal) {
			keywords.add(((ofbiz.resource.ofbiz.grammar.OfbizBooleanTerminal) element).getTrueLiteral());
			keywords.add(((ofbiz.resource.ofbiz.grammar.OfbizBooleanTerminal) element).getFalseLiteral());
		} else if (element instanceof ofbiz.resource.ofbiz.grammar.OfbizEnumerationTerminal) {
			ofbiz.resource.ofbiz.grammar.OfbizEnumerationTerminal terminal = (ofbiz.resource.ofbiz.grammar.OfbizEnumerationTerminal) element;
			for (String key : terminal.getLiteralMapping().keySet()) {
				keywords.add(key);
			}
		}
		for (ofbiz.resource.ofbiz.grammar.OfbizSyntaxElement child : element.getChildren()) {
			findKeywords(child, this.keywords);
		}
	}
	
}
