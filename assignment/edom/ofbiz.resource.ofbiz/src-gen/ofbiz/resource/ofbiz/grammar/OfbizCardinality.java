/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.grammar;

public enum OfbizCardinality {
	
	ONE, PLUS, QUESTIONMARK, STAR;
	
}
