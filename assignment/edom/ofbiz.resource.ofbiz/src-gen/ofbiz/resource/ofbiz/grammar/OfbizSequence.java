/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.grammar;

public class OfbizSequence extends ofbiz.resource.ofbiz.grammar.OfbizSyntaxElement {
	
	public OfbizSequence(ofbiz.resource.ofbiz.grammar.OfbizCardinality cardinality, ofbiz.resource.ofbiz.grammar.OfbizSyntaxElement... elements) {
		super(cardinality, elements);
	}
	
	public String toString() {
		return ofbiz.resource.ofbiz.util.OfbizStringUtil.explode(getChildren(), " ");
	}
	
}
