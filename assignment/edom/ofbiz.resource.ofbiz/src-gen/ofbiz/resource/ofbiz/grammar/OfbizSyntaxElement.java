/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.grammar;

/**
 * The abstract super class for all elements of a grammar. This class provides
 * methods to traverse the grammar rules.
 */
public abstract class OfbizSyntaxElement {
	
	private OfbizSyntaxElement[] children;
	private OfbizSyntaxElement parent;
	private ofbiz.resource.ofbiz.grammar.OfbizCardinality cardinality;
	
	public OfbizSyntaxElement(ofbiz.resource.ofbiz.grammar.OfbizCardinality cardinality, OfbizSyntaxElement[] children) {
		this.cardinality = cardinality;
		this.children = children;
		if (this.children != null) {
			for (OfbizSyntaxElement child : this.children) {
				child.setParent(this);
			}
		}
	}
	
	/**
	 * Sets the parent of this syntax element. This method must be invoked at most
	 * once.
	 */
	public void setParent(OfbizSyntaxElement parent) {
		assert this.parent == null;
		this.parent = parent;
	}
	
	/**
	 * Returns the parent of this syntax element. This parent is determined by the
	 * containment hierarchy in the CS model.
	 */
	public OfbizSyntaxElement getParent() {
		return parent;
	}
	
	public OfbizSyntaxElement[] getChildren() {
		if (children == null) {
			return new OfbizSyntaxElement[0];
		}
		return children;
	}
	
	public org.eclipse.emf.ecore.EClass getMetaclass() {
		return parent.getMetaclass();
	}
	
	public ofbiz.resource.ofbiz.grammar.OfbizCardinality getCardinality() {
		return cardinality;
	}
	
}
