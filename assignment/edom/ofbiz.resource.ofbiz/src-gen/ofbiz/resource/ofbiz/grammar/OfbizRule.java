/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.grammar;

/**
 * A class to represent a rules in the grammar.
 */
public class OfbizRule extends ofbiz.resource.ofbiz.grammar.OfbizSyntaxElement {
	
	private final org.eclipse.emf.ecore.EClass metaclass;
	
	public OfbizRule(org.eclipse.emf.ecore.EClass metaclass, ofbiz.resource.ofbiz.grammar.OfbizChoice choice, ofbiz.resource.ofbiz.grammar.OfbizCardinality cardinality) {
		super(cardinality, new ofbiz.resource.ofbiz.grammar.OfbizSyntaxElement[] {choice});
		this.metaclass = metaclass;
	}
	
	public org.eclipse.emf.ecore.EClass getMetaclass() {
		return metaclass;
	}
	
	public ofbiz.resource.ofbiz.grammar.OfbizChoice getDefinition() {
		return (ofbiz.resource.ofbiz.grammar.OfbizChoice) getChildren()[0];
	}
	
}

