/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.grammar;

public class OfbizChoice extends ofbiz.resource.ofbiz.grammar.OfbizSyntaxElement {
	
	public OfbizChoice(ofbiz.resource.ofbiz.grammar.OfbizCardinality cardinality, ofbiz.resource.ofbiz.grammar.OfbizSyntaxElement... choices) {
		super(cardinality, choices);
	}
	
	public String toString() {
		return ofbiz.resource.ofbiz.util.OfbizStringUtil.explode(getChildren(), "|");
	}
	
}
