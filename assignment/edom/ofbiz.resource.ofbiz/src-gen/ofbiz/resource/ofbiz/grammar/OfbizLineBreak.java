/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.grammar;

public class OfbizLineBreak extends ofbiz.resource.ofbiz.grammar.OfbizFormattingElement {
	
	private final int tabs;
	
	public OfbizLineBreak(ofbiz.resource.ofbiz.grammar.OfbizCardinality cardinality, int tabs) {
		super(cardinality);
		this.tabs = tabs;
	}
	
	public int getTabs() {
		return tabs;
	}
	
	public String toString() {
		return "!" + getTabs();
	}
	
}
