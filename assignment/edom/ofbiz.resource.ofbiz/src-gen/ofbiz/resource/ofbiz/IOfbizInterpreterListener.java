/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz;

public interface IOfbizInterpreterListener {
	
	public void handleInterpreteObject(org.eclipse.emf.ecore.EObject element);
}
