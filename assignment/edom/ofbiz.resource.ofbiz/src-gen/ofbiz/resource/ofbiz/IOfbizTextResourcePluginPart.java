/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz;

/**
 * This interface is extended by some other generated classes. It provides access
 * to the plug-in meta information.
 */
public interface IOfbizTextResourcePluginPart {
	
	/**
	 * Returns a meta information object for the language plug-in that contains this
	 * part.
	 */
	public ofbiz.resource.ofbiz.IOfbizMetaInformation getMetaInformation();
	
}
