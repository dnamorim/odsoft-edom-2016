/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz;

/**
 * Implementors of this interface provide an EMF resource.
 */
public interface IOfbizResourceProvider {
	
	/**
	 * Returns the resource.
	 */
	public ofbiz.resource.ofbiz.IOfbizTextResource getResource();
	
}
