/**
 */
package ofbiz.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import ofbiz.OfbizFactory;
import ofbiz.RestParameter;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Rest Parameter</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class RestParameterTest extends TestCase {

	/**
	 * The fixture for this Rest Parameter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RestParameter fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(RestParameterTest.class);
	}

	/**
	 * Constructs a new Rest Parameter test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RestParameterTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Rest Parameter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(RestParameter fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Rest Parameter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RestParameter getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(OfbizFactory.eINSTANCE.createRestParameter());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //RestParameterTest
