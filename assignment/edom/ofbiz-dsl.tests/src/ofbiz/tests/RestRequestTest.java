/**
 */
package ofbiz.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import ofbiz.OfbizFactory;
import ofbiz.RestRequest;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Rest Request</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class RestRequestTest extends TestCase {

	/**
	 * The fixture for this Rest Request test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RestRequest fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(RestRequestTest.class);
	}

	/**
	 * Constructs a new Rest Request test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RestRequestTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Rest Request test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(RestRequest fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Rest Request test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RestRequest getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(OfbizFactory.eINSTANCE.createRestRequest());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //RestRequestTest
