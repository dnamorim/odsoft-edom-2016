/**
 */
package ofbiz.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import ofbiz.EntityResource;
import ofbiz.OfbizFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Entity Resource</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class EntityResourceTest extends TestCase {

	/**
	 * The fixture for this Entity Resource test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EntityResource fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(EntityResourceTest.class);
	}

	/**
	 * Constructs a new Entity Resource test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntityResourceTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Entity Resource test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(EntityResource fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Entity Resource test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EntityResource getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(OfbizFactory.eINSTANCE.createEntityResource());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //EntityResourceTest
