/**
 */
package ofbiz.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import ofbiz.KeyMap;
import ofbiz.OfbizFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Key Map</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class KeyMapTest extends TestCase {

	/**
	 * The fixture for this Key Map test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KeyMap fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(KeyMapTest.class);
	}

	/**
	 * Constructs a new Key Map test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public KeyMapTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Key Map test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(KeyMap fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Key Map test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KeyMap getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(OfbizFactory.eINSTANCE.createKeyMap());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //KeyMapTest
