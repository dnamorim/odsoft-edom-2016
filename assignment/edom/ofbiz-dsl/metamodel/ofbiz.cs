SYNTAXDEF ofbiz
FOR <http://www.emftext.org/language/ofbiz>
START OfbizComponent


TOKENS {
	DEFINE COMMENT $'//'(~('\n'|'\r'|'\uffff'))*$;
	DEFINE INTEGER $('-')?('1'..'9')('0'..'9')*|'0'$;
	DEFINE FLOAT $('-')?(('1'..'9') ('0'..'9')* | '0') '.' ('0'..'9')+ $;
}


TOKENSTYLES {
	"OfbizComponent" COLOR #7F0055, BOLD;
	"entitymodel" COLOR #7F0055, BOLD;
	"services" COLOR #7F0055, BOLD;
	"rest" COLOR #7F0055, BOLD;
	"EntityModel" COLOR #7F0055, BOLD;
	"title" COLOR #7F0055, BOLD;
	"description" COLOR #7F0055, BOLD;
	"version" COLOR #7F0055, BOLD;
	"entities" COLOR #7F0055, BOLD;
	"Entity" COLOR #7F0055, BOLD;
	"entityName" COLOR #7F0055, BOLD;
	"packageName" COLOR #7F0055, BOLD;
	"primKey" COLOR #7F0055, BOLD;
	"fields" COLOR #7F0055, BOLD;
	"relations" COLOR #7F0055, BOLD;
	"Field" COLOR #7F0055, BOLD;
	"name" COLOR #7F0055, BOLD;
	"type" COLOR #7F0055, BOLD;
	"Relation" COLOR #7F0055, BOLD;
	"fkName" COLOR #7F0055, BOLD;
	"keyMap" COLOR #7F0055, BOLD;
	"related" COLOR #7F0055, BOLD;
	"KeyMap" COLOR #7F0055, BOLD;
	"origin" COLOR #7F0055, BOLD;
	"Services" COLOR #7F0055, BOLD;
	"Service" COLOR #7F0055, BOLD;
	"engine" COLOR #7F0055, BOLD;
	"location" COLOR #7F0055, BOLD;
	"invoke" COLOR #7F0055, BOLD;
	"attributes" COLOR #7F0055, BOLD;
	"Attribute" COLOR #7F0055, BOLD;
	"mode" COLOR #7F0055, BOLD;
	"RestService" COLOR #7F0055, BOLD;
	"resources" COLOR #7F0055, BOLD;
	"RestRequest" COLOR #7F0055, BOLD;
	"action" COLOR #7F0055, BOLD;
	"parameters" COLOR #7F0055, BOLD;
	"RestParameter" COLOR #7F0055, BOLD;
	"EntityResource" COLOR #7F0055, BOLD;
	"entity" COLOR #7F0055, BOLD;
	"requests" COLOR #7F0055, BOLD;
	"urlPath" COLOR #7F0055, BOLD;
}


RULES {
	OfbizComponent ::= "OfbizComponent" "{" ("entitymodel" ":" entitymodel | "services" ":" services | "rest" ":" rest)* "}";
	EntityModel ::= "EntityModel" "{" ("title" ":" title['"','"'] | "description" ":" description['"','"'] | "version" ":" version['"','"'] | "entities" ":" entities)* "}";
	Entity ::= "Entity" "{" ("entityName" ":" entityName['"','"'] | "packageName" ":" packageName['"','"'] | "title" ":" title['"','"'] | "primKey" ":" primKey[] | "fields" ":" fields | "relations" ":" relations)* "}";
	Field ::= "Field" "{" ("name" ":" name['"','"'] | "type" ":" type[object:"object", blob:"blob", byte_array:"byte-array", date_time:"date-time", date:"date", time:"time", currency_ammount:"currency-ammount", curreidncy_precise:"currency-precise", fixed_point:"fixed-point", floating_point:"floating-point", numeric:"numeric", id:"id", id_long:"id-long", id_vlong:"id-vlong", indicator:"indicator", very_short:"very-short", short_varchar:"short-varchar", long_varchar:"long-varchar", very_long:"very-long", comment:"comment", description:"description", name:"name", value:"value", credit_card_number:"credit-card-number", credit_card_date:"credit-card-date", email:"email", url:"url", id_ne:"id-ne", id_long_ne:"id-long-ne", id_vlong_ne:"id-vlong-ne", tel_number:"tel-number"])* "}";
	Relation ::= "Relation" "{" ("fkName" ":" fkName['"','"'] | "type" ":" type[ONE:"ONE", MANY:"MANY"] | "keyMap" ":" keyMap | "related" ":" related[])* "}";
	KeyMap ::= "KeyMap" "{" ("origin" ":" origin[] | "related" ":" related[])* "}";
	Services ::= "Services" "{" ("description" ":" description['"','"'] | "version" ":" version['"','"'] | "services" ":" services)* "}";
	Service ::= "Service" "{" ("name" ":" name['"','"'] | "engine" ":" engine[entity_auto:"entity-auto", group:"group", interface:"interface", java:"java", simple:"simple", script:"script", groovy:"groovy", jacl:"jacl", javascript:"javascript", jpython:"jpython", route:"route", http:"http", jms:"jms", rmi:"rmi", soap:"soap"] | "location" ":" location['"','"'] | "invoke" ":" invoke['"','"'] | "description" ":" description['"','"'] | "attributes" ":" attributes)* "}";
	Attribute ::= optional["optional" : ""] "Attribute" "{" ("name" ":" name['"','"'] | "type" ":" type[String:"String", Boolean:"Boolean", BigDecimal:"BigDecimal", Timestamp:"Timestamp", List:"List", Map:"Map"] | "mode" ":" mode[_IN:"IN", OUT:"OUT"])* "}";
	RestService ::= "RestService" "{" ("name" ":" name['"','"'] | "version" ":" version['"','"'] | "resources" ":" resources)* "}";
	RestRequest ::= "RestRequest" "{" ("action" ":" action[GET:"GET", PUT:"PUT", POST:"POST", DELETE:"DELETE"] | "parameters" ":" parameters)* "}";
	RestParameter ::= "RestParameter" "{" ("name" ":" name['"','"'])* "}";
	EntityResource ::= "EntityResource" "{" ("entity" ":" entity[] | "requests" ":" requests | "urlPath" ":" urlPath['"','"'])* "}";
}