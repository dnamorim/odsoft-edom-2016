/**
 */
package ofbiz;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entity Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ofbiz.EntityResource#getEntity <em>Entity</em>}</li>
 *   <li>{@link ofbiz.EntityResource#getRequests <em>Requests</em>}</li>
 *   <li>{@link ofbiz.EntityResource#getUrlPath <em>Url Path</em>}</li>
 * </ul>
 *
 * @see ofbiz.OfbizPackage#getEntityResource()
 * @model
 * @generated
 */
public interface EntityResource extends EObject {
	/**
	 * Returns the value of the '<em><b>Entity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entity</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entity</em>' reference.
	 * @see #setEntity(Entity)
	 * @see ofbiz.OfbizPackage#getEntityResource_Entity()
	 * @model required="true"
	 * @generated
	 */
	Entity getEntity();

	/**
	 * Sets the value of the '{@link ofbiz.EntityResource#getEntity <em>Entity</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entity</em>' reference.
	 * @see #getEntity()
	 * @generated
	 */
	void setEntity(Entity value);

	/**
	 * Returns the value of the '<em><b>Requests</b></em>' containment reference list.
	 * The list contents are of type {@link ofbiz.RestRequest}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requests</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requests</em>' containment reference list.
	 * @see ofbiz.OfbizPackage#getEntityResource_Requests()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<RestRequest> getRequests();

	/**
	 * Returns the value of the '<em><b>Url Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Url Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Url Path</em>' attribute.
	 * @see #setUrlPath(String)
	 * @see ofbiz.OfbizPackage#getEntityResource_UrlPath()
	 * @model
	 * @generated
	 */
	String getUrlPath();

	/**
	 * Sets the value of the '{@link ofbiz.EntityResource#getUrlPath <em>Url Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Url Path</em>' attribute.
	 * @see #getUrlPath()
	 * @generated
	 */
	void setUrlPath(String value);

} // EntityResource
