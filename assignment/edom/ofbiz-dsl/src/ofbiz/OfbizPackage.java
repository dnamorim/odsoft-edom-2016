/**
 */
package ofbiz;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ofbiz.OfbizFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/OCL/Import ecore='http://www.eclipse.org/emf/2002/Ecore'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface OfbizPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ofbiz";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.emftext.org/language/ofbiz";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ofbiz";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OfbizPackage eINSTANCE = ofbiz.impl.OfbizPackageImpl.init();

	/**
	 * The meta object id for the '{@link ofbiz.impl.OfbizComponentImpl <em>Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ofbiz.impl.OfbizComponentImpl
	 * @see ofbiz.impl.OfbizPackageImpl#getOfbizComponent()
	 * @generated
	 */
	int OFBIZ_COMPONENT = 0;

	/**
	 * The feature id for the '<em><b>Entitymodel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OFBIZ_COMPONENT__ENTITYMODEL = 0;

	/**
	 * The feature id for the '<em><b>Services</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OFBIZ_COMPONENT__SERVICES = 1;

	/**
	 * The feature id for the '<em><b>Rest</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OFBIZ_COMPONENT__REST = 2;

	/**
	 * The number of structural features of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OFBIZ_COMPONENT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OFBIZ_COMPONENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ofbiz.impl.EntityModelImpl <em>Entity Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ofbiz.impl.EntityModelImpl
	 * @see ofbiz.impl.OfbizPackageImpl#getEntityModel()
	 * @generated
	 */
	int ENTITY_MODEL = 1;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_MODEL__TITLE = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_MODEL__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_MODEL__VERSION = 2;

	/**
	 * The feature id for the '<em><b>Entities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_MODEL__ENTITIES = 3;

	/**
	 * The number of structural features of the '<em>Entity Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_MODEL_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Entity Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ofbiz.impl.EntityImpl <em>Entity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ofbiz.impl.EntityImpl
	 * @see ofbiz.impl.OfbizPackageImpl#getEntity()
	 * @generated
	 */
	int ENTITY = 2;

	/**
	 * The feature id for the '<em><b>Entity Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__ENTITY_NAME = 0;

	/**
	 * The feature id for the '<em><b>Package Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__PACKAGE_NAME = 1;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__TITLE = 2;

	/**
	 * The feature id for the '<em><b>Prim Key</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__PRIM_KEY = 3;

	/**
	 * The feature id for the '<em><b>Fields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__FIELDS = 4;

	/**
	 * The feature id for the '<em><b>Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__RELATIONS = 5;

	/**
	 * The number of structural features of the '<em>Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ofbiz.impl.FieldImpl <em>Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ofbiz.impl.FieldImpl
	 * @see ofbiz.impl.OfbizPackageImpl#getField()
	 * @generated
	 */
	int FIELD = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__NAME = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__TYPE = 1;

	/**
	 * The number of structural features of the '<em>Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ofbiz.impl.RelationImpl <em>Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ofbiz.impl.RelationImpl
	 * @see ofbiz.impl.OfbizPackageImpl#getRelation()
	 * @generated
	 */
	int RELATION = 4;

	/**
	 * The feature id for the '<em><b>Fk Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__FK_NAME = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__TYPE = 1;

	/**
	 * The feature id for the '<em><b>Key Map</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__KEY_MAP = 2;

	/**
	 * The feature id for the '<em><b>Related</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__RELATED = 3;

	/**
	 * The number of structural features of the '<em>Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ofbiz.impl.KeyMapImpl <em>Key Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ofbiz.impl.KeyMapImpl
	 * @see ofbiz.impl.OfbizPackageImpl#getKeyMap()
	 * @generated
	 */
	int KEY_MAP = 5;

	/**
	 * The feature id for the '<em><b>Origin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_MAP__ORIGIN = 0;

	/**
	 * The feature id for the '<em><b>Related</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_MAP__RELATED = 1;

	/**
	 * The number of structural features of the '<em>Key Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_MAP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Key Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_MAP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ofbiz.impl.ServicesImpl <em>Services</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ofbiz.impl.ServicesImpl
	 * @see ofbiz.impl.OfbizPackageImpl#getServices()
	 * @generated
	 */
	int SERVICES = 6;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICES__DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICES__VERSION = 1;

	/**
	 * The feature id for the '<em><b>Services</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICES__SERVICES = 2;

	/**
	 * The number of structural features of the '<em>Services</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICES_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Services</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ofbiz.impl.ServiceImpl <em>Service</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ofbiz.impl.ServiceImpl
	 * @see ofbiz.impl.OfbizPackageImpl#getService()
	 * @generated
	 */
	int SERVICE = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Engine</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__ENGINE = 1;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__LOCATION = 2;

	/**
	 * The feature id for the '<em><b>Invoke</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__INVOKE = 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__DESCRIPTION = 4;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__ATTRIBUTES = 5;

	/**
	 * The number of structural features of the '<em>Service</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Service</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ofbiz.impl.AttributeImpl <em>Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ofbiz.impl.AttributeImpl
	 * @see ofbiz.impl.OfbizPackageImpl#getAttribute()
	 * @generated
	 */
	int ATTRIBUTE = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__TYPE = 1;

	/**
	 * The feature id for the '<em><b>Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__MODE = 2;

	/**
	 * The feature id for the '<em><b>Optional</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__OPTIONAL = 3;

	/**
	 * The number of structural features of the '<em>Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ofbiz.impl.RestServiceImpl <em>Rest Service</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ofbiz.impl.RestServiceImpl
	 * @see ofbiz.impl.OfbizPackageImpl#getRestService()
	 * @generated
	 */
	int REST_SERVICE = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REST_SERVICE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REST_SERVICE__VERSION = 1;

	/**
	 * The feature id for the '<em><b>Resources</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REST_SERVICE__RESOURCES = 2;

	/**
	 * The number of structural features of the '<em>Rest Service</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REST_SERVICE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Rest Service</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REST_SERVICE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ofbiz.impl.RestRequestImpl <em>Rest Request</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ofbiz.impl.RestRequestImpl
	 * @see ofbiz.impl.OfbizPackageImpl#getRestRequest()
	 * @generated
	 */
	int REST_REQUEST = 10;

	/**
	 * The feature id for the '<em><b>Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REST_REQUEST__ACTION = 0;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REST_REQUEST__PARAMETERS = 1;

	/**
	 * The number of structural features of the '<em>Rest Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REST_REQUEST_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Rest Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REST_REQUEST_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ofbiz.impl.RestParameterImpl <em>Rest Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ofbiz.impl.RestParameterImpl
	 * @see ofbiz.impl.OfbizPackageImpl#getRestParameter()
	 * @generated
	 */
	int REST_PARAMETER = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REST_PARAMETER__NAME = 0;

	/**
	 * The number of structural features of the '<em>Rest Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REST_PARAMETER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Rest Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REST_PARAMETER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ofbiz.impl.EntityResourceImpl <em>Entity Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ofbiz.impl.EntityResourceImpl
	 * @see ofbiz.impl.OfbizPackageImpl#getEntityResource()
	 * @generated
	 */
	int ENTITY_RESOURCE = 12;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_RESOURCE__ENTITY = 0;

	/**
	 * The feature id for the '<em><b>Requests</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_RESOURCE__REQUESTS = 1;

	/**
	 * The feature id for the '<em><b>Url Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_RESOURCE__URL_PATH = 2;

	/**
	 * The number of structural features of the '<em>Entity Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_RESOURCE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Entity Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_RESOURCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ofbiz.ServiceEngine <em>Service Engine</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ofbiz.ServiceEngine
	 * @see ofbiz.impl.OfbizPackageImpl#getServiceEngine()
	 * @generated
	 */
	int SERVICE_ENGINE = 13;

	/**
	 * The meta object id for the '{@link ofbiz.ServiceDatatype <em>Service Datatype</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ofbiz.ServiceDatatype
	 * @see ofbiz.impl.OfbizPackageImpl#getServiceDatatype()
	 * @generated
	 */
	int SERVICE_DATATYPE = 14;

	/**
	 * The meta object id for the '{@link ofbiz.AttributeMode <em>Attribute Mode</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ofbiz.AttributeMode
	 * @see ofbiz.impl.OfbizPackageImpl#getAttributeMode()
	 * @generated
	 */
	int ATTRIBUTE_MODE = 15;

	/**
	 * The meta object id for the '{@link ofbiz.FieldType <em>Field Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ofbiz.FieldType
	 * @see ofbiz.impl.OfbizPackageImpl#getFieldType()
	 * @generated
	 */
	int FIELD_TYPE = 16;

	/**
	 * The meta object id for the '{@link ofbiz.RelationType <em>Relation Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ofbiz.RelationType
	 * @see ofbiz.impl.OfbizPackageImpl#getRelationType()
	 * @generated
	 */
	int RELATION_TYPE = 17;

	/**
	 * The meta object id for the '{@link ofbiz.RestMethod <em>Rest Method</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ofbiz.RestMethod
	 * @see ofbiz.impl.OfbizPackageImpl#getRestMethod()
	 * @generated
	 */
	int REST_METHOD = 18;


	/**
	 * Returns the meta object for class '{@link ofbiz.OfbizComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component</em>'.
	 * @see ofbiz.OfbizComponent
	 * @generated
	 */
	EClass getOfbizComponent();

	/**
	 * Returns the meta object for the containment reference '{@link ofbiz.OfbizComponent#getEntitymodel <em>Entitymodel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Entitymodel</em>'.
	 * @see ofbiz.OfbizComponent#getEntitymodel()
	 * @see #getOfbizComponent()
	 * @generated
	 */
	EReference getOfbizComponent_Entitymodel();

	/**
	 * Returns the meta object for the containment reference '{@link ofbiz.OfbizComponent#getServices <em>Services</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Services</em>'.
	 * @see ofbiz.OfbizComponent#getServices()
	 * @see #getOfbizComponent()
	 * @generated
	 */
	EReference getOfbizComponent_Services();

	/**
	 * Returns the meta object for the containment reference '{@link ofbiz.OfbizComponent#getRest <em>Rest</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Rest</em>'.
	 * @see ofbiz.OfbizComponent#getRest()
	 * @see #getOfbizComponent()
	 * @generated
	 */
	EReference getOfbizComponent_Rest();

	/**
	 * Returns the meta object for class '{@link ofbiz.EntityModel <em>Entity Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entity Model</em>'.
	 * @see ofbiz.EntityModel
	 * @generated
	 */
	EClass getEntityModel();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.EntityModel#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Title</em>'.
	 * @see ofbiz.EntityModel#getTitle()
	 * @see #getEntityModel()
	 * @generated
	 */
	EAttribute getEntityModel_Title();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.EntityModel#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see ofbiz.EntityModel#getDescription()
	 * @see #getEntityModel()
	 * @generated
	 */
	EAttribute getEntityModel_Description();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.EntityModel#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see ofbiz.EntityModel#getVersion()
	 * @see #getEntityModel()
	 * @generated
	 */
	EAttribute getEntityModel_Version();

	/**
	 * Returns the meta object for the containment reference list '{@link ofbiz.EntityModel#getEntities <em>Entities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Entities</em>'.
	 * @see ofbiz.EntityModel#getEntities()
	 * @see #getEntityModel()
	 * @generated
	 */
	EReference getEntityModel_Entities();

	/**
	 * Returns the meta object for class '{@link ofbiz.Entity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entity</em>'.
	 * @see ofbiz.Entity
	 * @generated
	 */
	EClass getEntity();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.Entity#getEntityName <em>Entity Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Entity Name</em>'.
	 * @see ofbiz.Entity#getEntityName()
	 * @see #getEntity()
	 * @generated
	 */
	EAttribute getEntity_EntityName();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.Entity#getPackageName <em>Package Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Package Name</em>'.
	 * @see ofbiz.Entity#getPackageName()
	 * @see #getEntity()
	 * @generated
	 */
	EAttribute getEntity_PackageName();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.Entity#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Title</em>'.
	 * @see ofbiz.Entity#getTitle()
	 * @see #getEntity()
	 * @generated
	 */
	EAttribute getEntity_Title();

	/**
	 * Returns the meta object for the reference list '{@link ofbiz.Entity#getPrimKey <em>Prim Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Prim Key</em>'.
	 * @see ofbiz.Entity#getPrimKey()
	 * @see #getEntity()
	 * @generated
	 */
	EReference getEntity_PrimKey();

	/**
	 * Returns the meta object for the containment reference list '{@link ofbiz.Entity#getFields <em>Fields</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fields</em>'.
	 * @see ofbiz.Entity#getFields()
	 * @see #getEntity()
	 * @generated
	 */
	EReference getEntity_Fields();

	/**
	 * Returns the meta object for the containment reference list '{@link ofbiz.Entity#getRelations <em>Relations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Relations</em>'.
	 * @see ofbiz.Entity#getRelations()
	 * @see #getEntity()
	 * @generated
	 */
	EReference getEntity_Relations();

	/**
	 * Returns the meta object for class '{@link ofbiz.Field <em>Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Field</em>'.
	 * @see ofbiz.Field
	 * @generated
	 */
	EClass getField();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.Field#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see ofbiz.Field#getName()
	 * @see #getField()
	 * @generated
	 */
	EAttribute getField_Name();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.Field#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see ofbiz.Field#getType()
	 * @see #getField()
	 * @generated
	 */
	EAttribute getField_Type();

	/**
	 * Returns the meta object for class '{@link ofbiz.Relation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relation</em>'.
	 * @see ofbiz.Relation
	 * @generated
	 */
	EClass getRelation();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.Relation#getFkName <em>Fk Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fk Name</em>'.
	 * @see ofbiz.Relation#getFkName()
	 * @see #getRelation()
	 * @generated
	 */
	EAttribute getRelation_FkName();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.Relation#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see ofbiz.Relation#getType()
	 * @see #getRelation()
	 * @generated
	 */
	EAttribute getRelation_Type();

	/**
	 * Returns the meta object for the containment reference list '{@link ofbiz.Relation#getKeyMap <em>Key Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Key Map</em>'.
	 * @see ofbiz.Relation#getKeyMap()
	 * @see #getRelation()
	 * @generated
	 */
	EReference getRelation_KeyMap();

	/**
	 * Returns the meta object for the reference '{@link ofbiz.Relation#getRelated <em>Related</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Related</em>'.
	 * @see ofbiz.Relation#getRelated()
	 * @see #getRelation()
	 * @generated
	 */
	EReference getRelation_Related();

	/**
	 * Returns the meta object for class '{@link ofbiz.KeyMap <em>Key Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Key Map</em>'.
	 * @see ofbiz.KeyMap
	 * @generated
	 */
	EClass getKeyMap();

	/**
	 * Returns the meta object for the reference '{@link ofbiz.KeyMap#getOrigin <em>Origin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Origin</em>'.
	 * @see ofbiz.KeyMap#getOrigin()
	 * @see #getKeyMap()
	 * @generated
	 */
	EReference getKeyMap_Origin();

	/**
	 * Returns the meta object for the reference '{@link ofbiz.KeyMap#getRelated <em>Related</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Related</em>'.
	 * @see ofbiz.KeyMap#getRelated()
	 * @see #getKeyMap()
	 * @generated
	 */
	EReference getKeyMap_Related();

	/**
	 * Returns the meta object for class '{@link ofbiz.Services <em>Services</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Services</em>'.
	 * @see ofbiz.Services
	 * @generated
	 */
	EClass getServices();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.Services#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see ofbiz.Services#getDescription()
	 * @see #getServices()
	 * @generated
	 */
	EAttribute getServices_Description();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.Services#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see ofbiz.Services#getVersion()
	 * @see #getServices()
	 * @generated
	 */
	EAttribute getServices_Version();

	/**
	 * Returns the meta object for the containment reference list '{@link ofbiz.Services#getServices <em>Services</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Services</em>'.
	 * @see ofbiz.Services#getServices()
	 * @see #getServices()
	 * @generated
	 */
	EReference getServices_Services();

	/**
	 * Returns the meta object for class '{@link ofbiz.Service <em>Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service</em>'.
	 * @see ofbiz.Service
	 * @generated
	 */
	EClass getService();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.Service#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see ofbiz.Service#getName()
	 * @see #getService()
	 * @generated
	 */
	EAttribute getService_Name();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.Service#getEngine <em>Engine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Engine</em>'.
	 * @see ofbiz.Service#getEngine()
	 * @see #getService()
	 * @generated
	 */
	EAttribute getService_Engine();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.Service#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Location</em>'.
	 * @see ofbiz.Service#getLocation()
	 * @see #getService()
	 * @generated
	 */
	EAttribute getService_Location();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.Service#getInvoke <em>Invoke</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Invoke</em>'.
	 * @see ofbiz.Service#getInvoke()
	 * @see #getService()
	 * @generated
	 */
	EAttribute getService_Invoke();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.Service#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see ofbiz.Service#getDescription()
	 * @see #getService()
	 * @generated
	 */
	EAttribute getService_Description();

	/**
	 * Returns the meta object for the containment reference list '{@link ofbiz.Service#getAttributes <em>Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attributes</em>'.
	 * @see ofbiz.Service#getAttributes()
	 * @see #getService()
	 * @generated
	 */
	EReference getService_Attributes();

	/**
	 * Returns the meta object for class '{@link ofbiz.Attribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute</em>'.
	 * @see ofbiz.Attribute
	 * @generated
	 */
	EClass getAttribute();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.Attribute#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see ofbiz.Attribute#getName()
	 * @see #getAttribute()
	 * @generated
	 */
	EAttribute getAttribute_Name();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.Attribute#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see ofbiz.Attribute#getType()
	 * @see #getAttribute()
	 * @generated
	 */
	EAttribute getAttribute_Type();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.Attribute#getMode <em>Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mode</em>'.
	 * @see ofbiz.Attribute#getMode()
	 * @see #getAttribute()
	 * @generated
	 */
	EAttribute getAttribute_Mode();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.Attribute#isOptional <em>Optional</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Optional</em>'.
	 * @see ofbiz.Attribute#isOptional()
	 * @see #getAttribute()
	 * @generated
	 */
	EAttribute getAttribute_Optional();

	/**
	 * Returns the meta object for class '{@link ofbiz.RestService <em>Rest Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rest Service</em>'.
	 * @see ofbiz.RestService
	 * @generated
	 */
	EClass getRestService();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.RestService#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see ofbiz.RestService#getName()
	 * @see #getRestService()
	 * @generated
	 */
	EAttribute getRestService_Name();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.RestService#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see ofbiz.RestService#getVersion()
	 * @see #getRestService()
	 * @generated
	 */
	EAttribute getRestService_Version();

	/**
	 * Returns the meta object for the containment reference list '{@link ofbiz.RestService#getResources <em>Resources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Resources</em>'.
	 * @see ofbiz.RestService#getResources()
	 * @see #getRestService()
	 * @generated
	 */
	EReference getRestService_Resources();

	/**
	 * Returns the meta object for class '{@link ofbiz.RestRequest <em>Rest Request</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rest Request</em>'.
	 * @see ofbiz.RestRequest
	 * @generated
	 */
	EClass getRestRequest();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.RestRequest#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Action</em>'.
	 * @see ofbiz.RestRequest#getAction()
	 * @see #getRestRequest()
	 * @generated
	 */
	EAttribute getRestRequest_Action();

	/**
	 * Returns the meta object for the containment reference list '{@link ofbiz.RestRequest#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see ofbiz.RestRequest#getParameters()
	 * @see #getRestRequest()
	 * @generated
	 */
	EReference getRestRequest_Parameters();

	/**
	 * Returns the meta object for class '{@link ofbiz.RestParameter <em>Rest Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rest Parameter</em>'.
	 * @see ofbiz.RestParameter
	 * @generated
	 */
	EClass getRestParameter();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.RestParameter#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see ofbiz.RestParameter#getName()
	 * @see #getRestParameter()
	 * @generated
	 */
	EAttribute getRestParameter_Name();

	/**
	 * Returns the meta object for class '{@link ofbiz.EntityResource <em>Entity Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entity Resource</em>'.
	 * @see ofbiz.EntityResource
	 * @generated
	 */
	EClass getEntityResource();

	/**
	 * Returns the meta object for the reference '{@link ofbiz.EntityResource#getEntity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Entity</em>'.
	 * @see ofbiz.EntityResource#getEntity()
	 * @see #getEntityResource()
	 * @generated
	 */
	EReference getEntityResource_Entity();

	/**
	 * Returns the meta object for the containment reference list '{@link ofbiz.EntityResource#getRequests <em>Requests</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Requests</em>'.
	 * @see ofbiz.EntityResource#getRequests()
	 * @see #getEntityResource()
	 * @generated
	 */
	EReference getEntityResource_Requests();

	/**
	 * Returns the meta object for the attribute '{@link ofbiz.EntityResource#getUrlPath <em>Url Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Url Path</em>'.
	 * @see ofbiz.EntityResource#getUrlPath()
	 * @see #getEntityResource()
	 * @generated
	 */
	EAttribute getEntityResource_UrlPath();

	/**
	 * Returns the meta object for enum '{@link ofbiz.ServiceEngine <em>Service Engine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Service Engine</em>'.
	 * @see ofbiz.ServiceEngine
	 * @generated
	 */
	EEnum getServiceEngine();

	/**
	 * Returns the meta object for enum '{@link ofbiz.ServiceDatatype <em>Service Datatype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Service Datatype</em>'.
	 * @see ofbiz.ServiceDatatype
	 * @generated
	 */
	EEnum getServiceDatatype();

	/**
	 * Returns the meta object for enum '{@link ofbiz.AttributeMode <em>Attribute Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Attribute Mode</em>'.
	 * @see ofbiz.AttributeMode
	 * @generated
	 */
	EEnum getAttributeMode();

	/**
	 * Returns the meta object for enum '{@link ofbiz.FieldType <em>Field Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Field Type</em>'.
	 * @see ofbiz.FieldType
	 * @generated
	 */
	EEnum getFieldType();

	/**
	 * Returns the meta object for enum '{@link ofbiz.RelationType <em>Relation Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Relation Type</em>'.
	 * @see ofbiz.RelationType
	 * @generated
	 */
	EEnum getRelationType();

	/**
	 * Returns the meta object for enum '{@link ofbiz.RestMethod <em>Rest Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Rest Method</em>'.
	 * @see ofbiz.RestMethod
	 * @generated
	 */
	EEnum getRestMethod();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OfbizFactory getOfbizFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ofbiz.impl.OfbizComponentImpl <em>Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ofbiz.impl.OfbizComponentImpl
		 * @see ofbiz.impl.OfbizPackageImpl#getOfbizComponent()
		 * @generated
		 */
		EClass OFBIZ_COMPONENT = eINSTANCE.getOfbizComponent();

		/**
		 * The meta object literal for the '<em><b>Entitymodel</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OFBIZ_COMPONENT__ENTITYMODEL = eINSTANCE.getOfbizComponent_Entitymodel();

		/**
		 * The meta object literal for the '<em><b>Services</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OFBIZ_COMPONENT__SERVICES = eINSTANCE.getOfbizComponent_Services();

		/**
		 * The meta object literal for the '<em><b>Rest</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OFBIZ_COMPONENT__REST = eINSTANCE.getOfbizComponent_Rest();

		/**
		 * The meta object literal for the '{@link ofbiz.impl.EntityModelImpl <em>Entity Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ofbiz.impl.EntityModelImpl
		 * @see ofbiz.impl.OfbizPackageImpl#getEntityModel()
		 * @generated
		 */
		EClass ENTITY_MODEL = eINSTANCE.getEntityModel();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_MODEL__TITLE = eINSTANCE.getEntityModel_Title();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_MODEL__DESCRIPTION = eINSTANCE.getEntityModel_Description();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_MODEL__VERSION = eINSTANCE.getEntityModel_Version();

		/**
		 * The meta object literal for the '<em><b>Entities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY_MODEL__ENTITIES = eINSTANCE.getEntityModel_Entities();

		/**
		 * The meta object literal for the '{@link ofbiz.impl.EntityImpl <em>Entity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ofbiz.impl.EntityImpl
		 * @see ofbiz.impl.OfbizPackageImpl#getEntity()
		 * @generated
		 */
		EClass ENTITY = eINSTANCE.getEntity();

		/**
		 * The meta object literal for the '<em><b>Entity Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY__ENTITY_NAME = eINSTANCE.getEntity_EntityName();

		/**
		 * The meta object literal for the '<em><b>Package Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY__PACKAGE_NAME = eINSTANCE.getEntity_PackageName();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY__TITLE = eINSTANCE.getEntity_Title();

		/**
		 * The meta object literal for the '<em><b>Prim Key</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY__PRIM_KEY = eINSTANCE.getEntity_PrimKey();

		/**
		 * The meta object literal for the '<em><b>Fields</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY__FIELDS = eINSTANCE.getEntity_Fields();

		/**
		 * The meta object literal for the '<em><b>Relations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY__RELATIONS = eINSTANCE.getEntity_Relations();

		/**
		 * The meta object literal for the '{@link ofbiz.impl.FieldImpl <em>Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ofbiz.impl.FieldImpl
		 * @see ofbiz.impl.OfbizPackageImpl#getField()
		 * @generated
		 */
		EClass FIELD = eINSTANCE.getField();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD__NAME = eINSTANCE.getField_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD__TYPE = eINSTANCE.getField_Type();

		/**
		 * The meta object literal for the '{@link ofbiz.impl.RelationImpl <em>Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ofbiz.impl.RelationImpl
		 * @see ofbiz.impl.OfbizPackageImpl#getRelation()
		 * @generated
		 */
		EClass RELATION = eINSTANCE.getRelation();

		/**
		 * The meta object literal for the '<em><b>Fk Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATION__FK_NAME = eINSTANCE.getRelation_FkName();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATION__TYPE = eINSTANCE.getRelation_Type();

		/**
		 * The meta object literal for the '<em><b>Key Map</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATION__KEY_MAP = eINSTANCE.getRelation_KeyMap();

		/**
		 * The meta object literal for the '<em><b>Related</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATION__RELATED = eINSTANCE.getRelation_Related();

		/**
		 * The meta object literal for the '{@link ofbiz.impl.KeyMapImpl <em>Key Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ofbiz.impl.KeyMapImpl
		 * @see ofbiz.impl.OfbizPackageImpl#getKeyMap()
		 * @generated
		 */
		EClass KEY_MAP = eINSTANCE.getKeyMap();

		/**
		 * The meta object literal for the '<em><b>Origin</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference KEY_MAP__ORIGIN = eINSTANCE.getKeyMap_Origin();

		/**
		 * The meta object literal for the '<em><b>Related</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference KEY_MAP__RELATED = eINSTANCE.getKeyMap_Related();

		/**
		 * The meta object literal for the '{@link ofbiz.impl.ServicesImpl <em>Services</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ofbiz.impl.ServicesImpl
		 * @see ofbiz.impl.OfbizPackageImpl#getServices()
		 * @generated
		 */
		EClass SERVICES = eINSTANCE.getServices();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVICES__DESCRIPTION = eINSTANCE.getServices_Description();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVICES__VERSION = eINSTANCE.getServices_Version();

		/**
		 * The meta object literal for the '<em><b>Services</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICES__SERVICES = eINSTANCE.getServices_Services();

		/**
		 * The meta object literal for the '{@link ofbiz.impl.ServiceImpl <em>Service</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ofbiz.impl.ServiceImpl
		 * @see ofbiz.impl.OfbizPackageImpl#getService()
		 * @generated
		 */
		EClass SERVICE = eINSTANCE.getService();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVICE__NAME = eINSTANCE.getService_Name();

		/**
		 * The meta object literal for the '<em><b>Engine</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVICE__ENGINE = eINSTANCE.getService_Engine();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVICE__LOCATION = eINSTANCE.getService_Location();

		/**
		 * The meta object literal for the '<em><b>Invoke</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVICE__INVOKE = eINSTANCE.getService_Invoke();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVICE__DESCRIPTION = eINSTANCE.getService_Description();

		/**
		 * The meta object literal for the '<em><b>Attributes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE__ATTRIBUTES = eINSTANCE.getService_Attributes();

		/**
		 * The meta object literal for the '{@link ofbiz.impl.AttributeImpl <em>Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ofbiz.impl.AttributeImpl
		 * @see ofbiz.impl.OfbizPackageImpl#getAttribute()
		 * @generated
		 */
		EClass ATTRIBUTE = eINSTANCE.getAttribute();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE__NAME = eINSTANCE.getAttribute_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE__TYPE = eINSTANCE.getAttribute_Type();

		/**
		 * The meta object literal for the '<em><b>Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE__MODE = eINSTANCE.getAttribute_Mode();

		/**
		 * The meta object literal for the '<em><b>Optional</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE__OPTIONAL = eINSTANCE.getAttribute_Optional();

		/**
		 * The meta object literal for the '{@link ofbiz.impl.RestServiceImpl <em>Rest Service</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ofbiz.impl.RestServiceImpl
		 * @see ofbiz.impl.OfbizPackageImpl#getRestService()
		 * @generated
		 */
		EClass REST_SERVICE = eINSTANCE.getRestService();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REST_SERVICE__NAME = eINSTANCE.getRestService_Name();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REST_SERVICE__VERSION = eINSTANCE.getRestService_Version();

		/**
		 * The meta object literal for the '<em><b>Resources</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REST_SERVICE__RESOURCES = eINSTANCE.getRestService_Resources();

		/**
		 * The meta object literal for the '{@link ofbiz.impl.RestRequestImpl <em>Rest Request</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ofbiz.impl.RestRequestImpl
		 * @see ofbiz.impl.OfbizPackageImpl#getRestRequest()
		 * @generated
		 */
		EClass REST_REQUEST = eINSTANCE.getRestRequest();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REST_REQUEST__ACTION = eINSTANCE.getRestRequest_Action();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REST_REQUEST__PARAMETERS = eINSTANCE.getRestRequest_Parameters();

		/**
		 * The meta object literal for the '{@link ofbiz.impl.RestParameterImpl <em>Rest Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ofbiz.impl.RestParameterImpl
		 * @see ofbiz.impl.OfbizPackageImpl#getRestParameter()
		 * @generated
		 */
		EClass REST_PARAMETER = eINSTANCE.getRestParameter();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REST_PARAMETER__NAME = eINSTANCE.getRestParameter_Name();

		/**
		 * The meta object literal for the '{@link ofbiz.impl.EntityResourceImpl <em>Entity Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ofbiz.impl.EntityResourceImpl
		 * @see ofbiz.impl.OfbizPackageImpl#getEntityResource()
		 * @generated
		 */
		EClass ENTITY_RESOURCE = eINSTANCE.getEntityResource();

		/**
		 * The meta object literal for the '<em><b>Entity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY_RESOURCE__ENTITY = eINSTANCE.getEntityResource_Entity();

		/**
		 * The meta object literal for the '<em><b>Requests</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY_RESOURCE__REQUESTS = eINSTANCE.getEntityResource_Requests();

		/**
		 * The meta object literal for the '<em><b>Url Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_RESOURCE__URL_PATH = eINSTANCE.getEntityResource_UrlPath();

		/**
		 * The meta object literal for the '{@link ofbiz.ServiceEngine <em>Service Engine</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ofbiz.ServiceEngine
		 * @see ofbiz.impl.OfbizPackageImpl#getServiceEngine()
		 * @generated
		 */
		EEnum SERVICE_ENGINE = eINSTANCE.getServiceEngine();

		/**
		 * The meta object literal for the '{@link ofbiz.ServiceDatatype <em>Service Datatype</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ofbiz.ServiceDatatype
		 * @see ofbiz.impl.OfbizPackageImpl#getServiceDatatype()
		 * @generated
		 */
		EEnum SERVICE_DATATYPE = eINSTANCE.getServiceDatatype();

		/**
		 * The meta object literal for the '{@link ofbiz.AttributeMode <em>Attribute Mode</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ofbiz.AttributeMode
		 * @see ofbiz.impl.OfbizPackageImpl#getAttributeMode()
		 * @generated
		 */
		EEnum ATTRIBUTE_MODE = eINSTANCE.getAttributeMode();

		/**
		 * The meta object literal for the '{@link ofbiz.FieldType <em>Field Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ofbiz.FieldType
		 * @see ofbiz.impl.OfbizPackageImpl#getFieldType()
		 * @generated
		 */
		EEnum FIELD_TYPE = eINSTANCE.getFieldType();

		/**
		 * The meta object literal for the '{@link ofbiz.RelationType <em>Relation Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ofbiz.RelationType
		 * @see ofbiz.impl.OfbizPackageImpl#getRelationType()
		 * @generated
		 */
		EEnum RELATION_TYPE = eINSTANCE.getRelationType();

		/**
		 * The meta object literal for the '{@link ofbiz.RestMethod <em>Rest Method</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ofbiz.RestMethod
		 * @see ofbiz.impl.OfbizPackageImpl#getRestMethod()
		 * @generated
		 */
		EEnum REST_METHOD = eINSTANCE.getRestMethod();

	}

} //OfbizPackage
