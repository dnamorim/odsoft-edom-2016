/**
 */
package ofbiz.util;

import java.util.Map;

import ofbiz.*;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see ofbiz.OfbizPackage
 * @generated
 */
public class OfbizValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final OfbizValidator INSTANCE = new OfbizValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "ofbiz";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OfbizValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return OfbizPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case OfbizPackage.OFBIZ_COMPONENT:
				return validateOfbizComponent((OfbizComponent)value, diagnostics, context);
			case OfbizPackage.ENTITY_MODEL:
				return validateEntityModel((EntityModel)value, diagnostics, context);
			case OfbizPackage.ENTITY:
				return validateEntity((Entity)value, diagnostics, context);
			case OfbizPackage.FIELD:
				return validateField((Field)value, diagnostics, context);
			case OfbizPackage.RELATION:
				return validateRelation((Relation)value, diagnostics, context);
			case OfbizPackage.KEY_MAP:
				return validateKeyMap((KeyMap)value, diagnostics, context);
			case OfbizPackage.SERVICES:
				return validateServices((Services)value, diagnostics, context);
			case OfbizPackage.SERVICE:
				return validateService((Service)value, diagnostics, context);
			case OfbizPackage.ATTRIBUTE:
				return validateAttribute((Attribute)value, diagnostics, context);
			case OfbizPackage.REST_SERVICE:
				return validateRestService((RestService)value, diagnostics, context);
			case OfbizPackage.REST_REQUEST:
				return validateRestRequest((RestRequest)value, diagnostics, context);
			case OfbizPackage.REST_PARAMETER:
				return validateRestParameter((RestParameter)value, diagnostics, context);
			case OfbizPackage.ENTITY_RESOURCE:
				return validateEntityResource((EntityResource)value, diagnostics, context);
			case OfbizPackage.SERVICE_ENGINE:
				return validateServiceEngine((ServiceEngine)value, diagnostics, context);
			case OfbizPackage.SERVICE_DATATYPE:
				return validateServiceDatatype((ServiceDatatype)value, diagnostics, context);
			case OfbizPackage.ATTRIBUTE_MODE:
				return validateAttributeMode((AttributeMode)value, diagnostics, context);
			case OfbizPackage.FIELD_TYPE:
				return validateFieldType((FieldType)value, diagnostics, context);
			case OfbizPackage.RELATION_TYPE:
				return validateRelationType((RelationType)value, diagnostics, context);
			case OfbizPackage.REST_METHOD:
				return validateRestMethod((RestMethod)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOfbizComponent(OfbizComponent ofbizComponent, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(ofbizComponent, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEntityModel(EntityModel entityModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(entityModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEntity(Entity entity, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(entity, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(entity, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(entity, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(entity, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(entity, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(entity, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(entity, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(entity, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(entity, diagnostics, context);
		if (result || diagnostics != null) result &= validateEntity_primKeyAssociatedWithValidField(entity, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the primKeyAssociatedWithValidField constraint of '<em>Entity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String ENTITY__PRIM_KEY_ASSOCIATED_WITH_VALID_FIELD__EEXPRESSION = "self.fields->select(f|f = self.primKey)->size()=1";

	/**
	 * Validates the primKeyAssociatedWithValidField constraint of '<em>Entity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEntity_primKeyAssociatedWithValidField(Entity entity, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(OfbizPackage.Literals.ENTITY,
				 entity,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "primKeyAssociatedWithValidField",
				 ENTITY__PRIM_KEY_ASSOCIATED_WITH_VALID_FIELD__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateField(Field field, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(field, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRelation(Relation relation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(relation, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateKeyMap(KeyMap keyMap, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(keyMap, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(keyMap, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(keyMap, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(keyMap, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(keyMap, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(keyMap, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(keyMap, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(keyMap, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(keyMap, diagnostics, context);
		if (result || diagnostics != null) result &= validateKeyMap_mustBeRelated(keyMap, diagnostics, context);
		if (result || diagnostics != null) result &= validateKeyMap_validField(keyMap, diagnostics, context);
		if (result || diagnostics != null) result &= validateKeyMap_validRelatedField(keyMap, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the mustBeRelated constraint of '<em>Key Map</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String KEY_MAP__MUST_BE_RELATED__EEXPRESSION = "Relation.allInstances()->select(r|r.keyMap = self)->size()=1";

	/**
	 * Validates the mustBeRelated constraint of '<em>Key Map</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateKeyMap_mustBeRelated(KeyMap keyMap, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(OfbizPackage.Literals.KEY_MAP,
				 keyMap,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "mustBeRelated",
				 KEY_MAP__MUST_BE_RELATED__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the validField constraint of '<em>Key Map</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String KEY_MAP__VALID_FIELD__EEXPRESSION = "Entity.allInstances()->select(e|e.relations->select(r|r.keyMap = self)->size()=1).fields->select(f|f=self.origin)->size()=1";

	/**
	 * Validates the validField constraint of '<em>Key Map</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateKeyMap_validField(KeyMap keyMap, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(OfbizPackage.Literals.KEY_MAP,
				 keyMap,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "validField",
				 KEY_MAP__VALID_FIELD__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the validRelatedField constraint of '<em>Key Map</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String KEY_MAP__VALID_RELATED_FIELD__EEXPRESSION = "         (Relation.allInstances()->select(r|r.keymap = self).related.fields->select(f|f = self.related)->size() = 1) or self.related.oclIsUndefined()";

	/**
	 * Validates the validRelatedField constraint of '<em>Key Map</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateKeyMap_validRelatedField(KeyMap keyMap, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(OfbizPackage.Literals.KEY_MAP,
				 keyMap,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "validRelatedField",
				 KEY_MAP__VALID_RELATED_FIELD__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateServices(Services services, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(services, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateService(Service service, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(service, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(service, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(service, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(service, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(service, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(service, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(service, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(service, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(service, diagnostics, context);
		if (result || diagnostics != null) result &= validateService_OneOutAttributeAllowed(service, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the OneOutAttributeAllowed constraint of '<em>Service</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SERVICE__ONE_OUT_ATTRIBUTE_ALLOWED__EEXPRESSION = "self.attributes->select(a|a.mode = 2)->size() < 2";

	/**
	 * Validates the OneOutAttributeAllowed constraint of '<em>Service</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateService_OneOutAttributeAllowed(Service service, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(OfbizPackage.Literals.SERVICE,
				 service,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "OneOutAttributeAllowed",
				 SERVICE__ONE_OUT_ATTRIBUTE_ALLOWED__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAttribute(Attribute attribute, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(attribute, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRestService(RestService restService, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(restService, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRestRequest(RestRequest restRequest, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(restRequest, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRestParameter(RestParameter restParameter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(restParameter, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEntityResource(EntityResource entityResource, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(entityResource, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateServiceEngine(ServiceEngine serviceEngine, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateServiceDatatype(ServiceDatatype serviceDatatype, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAttributeMode(AttributeMode attributeMode, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFieldType(FieldType fieldType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRelationType(RelationType relationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRestMethod(RestMethod restMethod, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //OfbizValidator
