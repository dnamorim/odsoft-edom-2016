/**
 */
package ofbiz;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ofbiz.OfbizComponent#getEntitymodel <em>Entitymodel</em>}</li>
 *   <li>{@link ofbiz.OfbizComponent#getServices <em>Services</em>}</li>
 *   <li>{@link ofbiz.OfbizComponent#getRest <em>Rest</em>}</li>
 * </ul>
 *
 * @see ofbiz.OfbizPackage#getOfbizComponent()
 * @model
 * @generated
 */
public interface OfbizComponent extends EObject {
	/**
	 * Returns the value of the '<em><b>Entitymodel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entitymodel</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entitymodel</em>' containment reference.
	 * @see #setEntitymodel(EntityModel)
	 * @see ofbiz.OfbizPackage#getOfbizComponent_Entitymodel()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EntityModel getEntitymodel();

	/**
	 * Sets the value of the '{@link ofbiz.OfbizComponent#getEntitymodel <em>Entitymodel</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entitymodel</em>' containment reference.
	 * @see #getEntitymodel()
	 * @generated
	 */
	void setEntitymodel(EntityModel value);

	/**
	 * Returns the value of the '<em><b>Services</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Services</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Services</em>' containment reference.
	 * @see #setServices(Services)
	 * @see ofbiz.OfbizPackage#getOfbizComponent_Services()
	 * @model containment="true"
	 * @generated
	 */
	Services getServices();

	/**
	 * Sets the value of the '{@link ofbiz.OfbizComponent#getServices <em>Services</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Services</em>' containment reference.
	 * @see #getServices()
	 * @generated
	 */
	void setServices(Services value);

	/**
	 * Returns the value of the '<em><b>Rest</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rest</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rest</em>' containment reference.
	 * @see #setRest(RestService)
	 * @see ofbiz.OfbizPackage#getOfbizComponent_Rest()
	 * @model containment="true"
	 * @generated
	 */
	RestService getRest();

	/**
	 * Sets the value of the '{@link ofbiz.OfbizComponent#getRest <em>Rest</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rest</em>' containment reference.
	 * @see #getRest()
	 * @generated
	 */
	void setRest(RestService value);

} // OfbizComponent
