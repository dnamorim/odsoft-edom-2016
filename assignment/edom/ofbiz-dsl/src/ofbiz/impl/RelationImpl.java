/**
 */
package ofbiz.impl;

import java.util.Collection;

import ofbiz.Entity;
import ofbiz.KeyMap;
import ofbiz.OfbizPackage;
import ofbiz.Relation;
import ofbiz.RelationType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Relation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ofbiz.impl.RelationImpl#getFkName <em>Fk Name</em>}</li>
 *   <li>{@link ofbiz.impl.RelationImpl#getType <em>Type</em>}</li>
 *   <li>{@link ofbiz.impl.RelationImpl#getKeyMap <em>Key Map</em>}</li>
 *   <li>{@link ofbiz.impl.RelationImpl#getRelated <em>Related</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RelationImpl extends MinimalEObjectImpl.Container implements Relation {
	/**
	 * The default value of the '{@link #getFkName() <em>Fk Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFkName()
	 * @generated
	 * @ordered
	 */
	protected static final String FK_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFkName() <em>Fk Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFkName()
	 * @generated
	 * @ordered
	 */
	protected String fkName = FK_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final RelationType TYPE_EDEFAULT = RelationType.ONE;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected RelationType type = TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getKeyMap() <em>Key Map</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKeyMap()
	 * @generated
	 * @ordered
	 */
	protected EList<KeyMap> keyMap;

	/**
	 * The cached value of the '{@link #getRelated() <em>Related</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelated()
	 * @generated
	 * @ordered
	 */
	protected Entity related;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RelationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OfbizPackage.Literals.RELATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFkName() {
		return fkName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFkName(String newFkName) {
		String oldFkName = fkName;
		fkName = newFkName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OfbizPackage.RELATION__FK_NAME, oldFkName, fkName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(RelationType newType) {
		RelationType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OfbizPackage.RELATION__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<KeyMap> getKeyMap() {
		if (keyMap == null) {
			keyMap = new EObjectContainmentEList<KeyMap>(KeyMap.class, this, OfbizPackage.RELATION__KEY_MAP);
		}
		return keyMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Entity getRelated() {
		if (related != null && related.eIsProxy()) {
			InternalEObject oldRelated = (InternalEObject)related;
			related = (Entity)eResolveProxy(oldRelated);
			if (related != oldRelated) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OfbizPackage.RELATION__RELATED, oldRelated, related));
			}
		}
		return related;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Entity basicGetRelated() {
		return related;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRelated(Entity newRelated) {
		Entity oldRelated = related;
		related = newRelated;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OfbizPackage.RELATION__RELATED, oldRelated, related));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OfbizPackage.RELATION__KEY_MAP:
				return ((InternalEList<?>)getKeyMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OfbizPackage.RELATION__FK_NAME:
				return getFkName();
			case OfbizPackage.RELATION__TYPE:
				return getType();
			case OfbizPackage.RELATION__KEY_MAP:
				return getKeyMap();
			case OfbizPackage.RELATION__RELATED:
				if (resolve) return getRelated();
				return basicGetRelated();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OfbizPackage.RELATION__FK_NAME:
				setFkName((String)newValue);
				return;
			case OfbizPackage.RELATION__TYPE:
				setType((RelationType)newValue);
				return;
			case OfbizPackage.RELATION__KEY_MAP:
				getKeyMap().clear();
				getKeyMap().addAll((Collection<? extends KeyMap>)newValue);
				return;
			case OfbizPackage.RELATION__RELATED:
				setRelated((Entity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OfbizPackage.RELATION__FK_NAME:
				setFkName(FK_NAME_EDEFAULT);
				return;
			case OfbizPackage.RELATION__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case OfbizPackage.RELATION__KEY_MAP:
				getKeyMap().clear();
				return;
			case OfbizPackage.RELATION__RELATED:
				setRelated((Entity)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OfbizPackage.RELATION__FK_NAME:
				return FK_NAME_EDEFAULT == null ? fkName != null : !FK_NAME_EDEFAULT.equals(fkName);
			case OfbizPackage.RELATION__TYPE:
				return type != TYPE_EDEFAULT;
			case OfbizPackage.RELATION__KEY_MAP:
				return keyMap != null && !keyMap.isEmpty();
			case OfbizPackage.RELATION__RELATED:
				return related != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (fkName: ");
		result.append(fkName);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //RelationImpl
