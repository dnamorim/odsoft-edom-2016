/**
 */
package ofbiz.impl;

import ofbiz.EntityModel;
import ofbiz.OfbizComponent;
import ofbiz.OfbizPackage;
import ofbiz.RestService;
import ofbiz.Services;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ofbiz.impl.OfbizComponentImpl#getEntitymodel <em>Entitymodel</em>}</li>
 *   <li>{@link ofbiz.impl.OfbizComponentImpl#getServices <em>Services</em>}</li>
 *   <li>{@link ofbiz.impl.OfbizComponentImpl#getRest <em>Rest</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OfbizComponentImpl extends MinimalEObjectImpl.Container implements OfbizComponent {
	/**
	 * The cached value of the '{@link #getEntitymodel() <em>Entitymodel</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntitymodel()
	 * @generated
	 * @ordered
	 */
	protected EntityModel entitymodel;

	/**
	 * The cached value of the '{@link #getServices() <em>Services</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServices()
	 * @generated
	 * @ordered
	 */
	protected Services services;

	/**
	 * The cached value of the '{@link #getRest() <em>Rest</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRest()
	 * @generated
	 * @ordered
	 */
	protected RestService rest;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OfbizComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OfbizPackage.Literals.OFBIZ_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntityModel getEntitymodel() {
		return entitymodel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEntitymodel(EntityModel newEntitymodel, NotificationChain msgs) {
		EntityModel oldEntitymodel = entitymodel;
		entitymodel = newEntitymodel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OfbizPackage.OFBIZ_COMPONENT__ENTITYMODEL, oldEntitymodel, newEntitymodel);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntitymodel(EntityModel newEntitymodel) {
		if (newEntitymodel != entitymodel) {
			NotificationChain msgs = null;
			if (entitymodel != null)
				msgs = ((InternalEObject)entitymodel).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OfbizPackage.OFBIZ_COMPONENT__ENTITYMODEL, null, msgs);
			if (newEntitymodel != null)
				msgs = ((InternalEObject)newEntitymodel).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OfbizPackage.OFBIZ_COMPONENT__ENTITYMODEL, null, msgs);
			msgs = basicSetEntitymodel(newEntitymodel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OfbizPackage.OFBIZ_COMPONENT__ENTITYMODEL, newEntitymodel, newEntitymodel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Services getServices() {
		return services;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetServices(Services newServices, NotificationChain msgs) {
		Services oldServices = services;
		services = newServices;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OfbizPackage.OFBIZ_COMPONENT__SERVICES, oldServices, newServices);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setServices(Services newServices) {
		if (newServices != services) {
			NotificationChain msgs = null;
			if (services != null)
				msgs = ((InternalEObject)services).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OfbizPackage.OFBIZ_COMPONENT__SERVICES, null, msgs);
			if (newServices != null)
				msgs = ((InternalEObject)newServices).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OfbizPackage.OFBIZ_COMPONENT__SERVICES, null, msgs);
			msgs = basicSetServices(newServices, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OfbizPackage.OFBIZ_COMPONENT__SERVICES, newServices, newServices));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RestService getRest() {
		return rest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRest(RestService newRest, NotificationChain msgs) {
		RestService oldRest = rest;
		rest = newRest;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OfbizPackage.OFBIZ_COMPONENT__REST, oldRest, newRest);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRest(RestService newRest) {
		if (newRest != rest) {
			NotificationChain msgs = null;
			if (rest != null)
				msgs = ((InternalEObject)rest).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OfbizPackage.OFBIZ_COMPONENT__REST, null, msgs);
			if (newRest != null)
				msgs = ((InternalEObject)newRest).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OfbizPackage.OFBIZ_COMPONENT__REST, null, msgs);
			msgs = basicSetRest(newRest, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OfbizPackage.OFBIZ_COMPONENT__REST, newRest, newRest));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OfbizPackage.OFBIZ_COMPONENT__ENTITYMODEL:
				return basicSetEntitymodel(null, msgs);
			case OfbizPackage.OFBIZ_COMPONENT__SERVICES:
				return basicSetServices(null, msgs);
			case OfbizPackage.OFBIZ_COMPONENT__REST:
				return basicSetRest(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OfbizPackage.OFBIZ_COMPONENT__ENTITYMODEL:
				return getEntitymodel();
			case OfbizPackage.OFBIZ_COMPONENT__SERVICES:
				return getServices();
			case OfbizPackage.OFBIZ_COMPONENT__REST:
				return getRest();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OfbizPackage.OFBIZ_COMPONENT__ENTITYMODEL:
				setEntitymodel((EntityModel)newValue);
				return;
			case OfbizPackage.OFBIZ_COMPONENT__SERVICES:
				setServices((Services)newValue);
				return;
			case OfbizPackage.OFBIZ_COMPONENT__REST:
				setRest((RestService)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OfbizPackage.OFBIZ_COMPONENT__ENTITYMODEL:
				setEntitymodel((EntityModel)null);
				return;
			case OfbizPackage.OFBIZ_COMPONENT__SERVICES:
				setServices((Services)null);
				return;
			case OfbizPackage.OFBIZ_COMPONENT__REST:
				setRest((RestService)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OfbizPackage.OFBIZ_COMPONENT__ENTITYMODEL:
				return entitymodel != null;
			case OfbizPackage.OFBIZ_COMPONENT__SERVICES:
				return services != null;
			case OfbizPackage.OFBIZ_COMPONENT__REST:
				return rest != null;
		}
		return super.eIsSet(featureID);
	}

} //OfbizComponentImpl
