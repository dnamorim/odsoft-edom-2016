/**
 */
package ofbiz;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Field Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see ofbiz.OfbizPackage#getFieldType()
 * @model
 * @generated
 */
public enum FieldType implements Enumerator {
	/**
	 * The '<em><b>Object</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OBJECT_VALUE
	 * @generated
	 * @ordered
	 */
	OBJECT(0, "object", "object"),

	/**
	 * The '<em><b>Blob</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BLOB_VALUE
	 * @generated
	 * @ordered
	 */
	BLOB(1, "blob", "blob"),

	/**
	 * The '<em><b>Byte array</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BYTE_ARRAY_VALUE
	 * @generated
	 * @ordered
	 */
	BYTE_ARRAY(2, "byte_array", "byte-array"),

	/**
	 * The '<em><b>Date time</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DATE_TIME_VALUE
	 * @generated
	 * @ordered
	 */
	DATE_TIME(3, "date_time", "date-time"),

	/**
	 * The '<em><b>Date</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DATE_VALUE
	 * @generated
	 * @ordered
	 */
	DATE(4, "date", "date"),

	/**
	 * The '<em><b>Time</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TIME_VALUE
	 * @generated
	 * @ordered
	 */
	TIME(5, "time", "time"),

	/**
	 * The '<em><b>Currency ammount</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CURRENCY_AMMOUNT_VALUE
	 * @generated
	 * @ordered
	 */
	CURRENCY_AMMOUNT(5, "currency_ammount", "currency-ammount"),

	/**
	 * The '<em><b>Currency precise</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CURRENCY_PRECISE_VALUE
	 * @generated
	 * @ordered
	 */
	CURRENCY_PRECISE(6, "currency_precise", "currency-precise"),

	/**
	 * The '<em><b>Fixed point</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FIXED_POINT_VALUE
	 * @generated
	 * @ordered
	 */
	FIXED_POINT(7, "fixed_point", "fixed-point"),

	/**
	 * The '<em><b>Floating point</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FLOATING_POINT_VALUE
	 * @generated
	 * @ordered
	 */
	FLOATING_POINT(8, "floating_point", "floating-point"),

	/**
	 * The '<em><b>Numeric</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NUMERIC_VALUE
	 * @generated
	 * @ordered
	 */
	NUMERIC(9, "numeric", "numeric"),

	/**
	 * The '<em><b>Id</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ID_VALUE
	 * @generated
	 * @ordered
	 */
	ID(10, "id", "id"),

	/**
	 * The '<em><b>Id long</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ID_LONG_VALUE
	 * @generated
	 * @ordered
	 */
	ID_LONG(11, "id_long", "id-long"),

	/**
	 * The '<em><b>Id vlong</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ID_VLONG_VALUE
	 * @generated
	 * @ordered
	 */
	ID_VLONG(12, "id_vlong", "id-vlong"),

	/**
	 * The '<em><b>Indicator</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INDICATOR_VALUE
	 * @generated
	 * @ordered
	 */
	INDICATOR(13, "indicator", "indicator"),

	/**
	 * The '<em><b>Very short</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VERY_SHORT_VALUE
	 * @generated
	 * @ordered
	 */
	VERY_SHORT(14, "very_short", "very-short"),

	/**
	 * The '<em><b>Short varchar</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SHORT_VARCHAR_VALUE
	 * @generated
	 * @ordered
	 */
	SHORT_VARCHAR(15, "short_varchar", "short-varchar"),

	/**
	 * The '<em><b>Long varchar</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LONG_VARCHAR_VALUE
	 * @generated
	 * @ordered
	 */
	LONG_VARCHAR(16, "long_varchar", "long-varchar"),

	/**
	 * The '<em><b>Very long</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VERY_LONG_VALUE
	 * @generated
	 * @ordered
	 */
	VERY_LONG(17, "very_long", "very-long"),

	/**
	 * The '<em><b>Comment</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COMMENT_VALUE
	 * @generated
	 * @ordered
	 */
	COMMENT(18, "comment", "comment"),

	/**
	 * The '<em><b>Description</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DESCRIPTION_VALUE
	 * @generated
	 * @ordered
	 */
	DESCRIPTION(19, "description", "description"),

	/**
	 * The '<em><b>Name</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NAME_VALUE
	 * @generated
	 * @ordered
	 */
	NAME(20, "name", "name"),

	/**
	 * The '<em><b>Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	VALUE(21, "value", "value"),

	/**
	 * The '<em><b>Credit card number</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CREDIT_CARD_NUMBER_VALUE
	 * @generated
	 * @ordered
	 */
	CREDIT_CARD_NUMBER(22, "credit_card_number", "credit-card-number"),

	/**
	 * The '<em><b>Credit card date</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CREDIT_CARD_DATE_VALUE
	 * @generated
	 * @ordered
	 */
	CREDIT_CARD_DATE(23, "credit_card_date", "credit-card-date"),

	/**
	 * The '<em><b>Email</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EMAIL_VALUE
	 * @generated
	 * @ordered
	 */
	EMAIL(24, "email", "email"),

	/**
	 * The '<em><b>Url</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #URL_VALUE
	 * @generated
	 * @ordered
	 */
	URL(25, "url", "url"),

	/**
	 * The '<em><b>Id ne</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ID_NE_VALUE
	 * @generated
	 * @ordered
	 */
	ID_NE(26, "id_ne", "id-ne"),

	/**
	 * The '<em><b>Id long ne</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ID_LONG_NE_VALUE
	 * @generated
	 * @ordered
	 */
	ID_LONG_NE(27, "id_long_ne", "id-long-ne"),

	/**
	 * The '<em><b>Id vlong ne</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ID_VLONG_NE_VALUE
	 * @generated
	 * @ordered
	 */
	ID_VLONG_NE(28, "id_vlong_ne", "id-vlong-ne"),

	/**
	 * The '<em><b>Tel number</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEL_NUMBER_VALUE
	 * @generated
	 * @ordered
	 */
	TEL_NUMBER(29, "tel_number", "tel-number");

	/**
	 * The '<em><b>Object</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Object</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OBJECT
	 * @model name="object"
	 * @generated
	 * @ordered
	 */
	public static final int OBJECT_VALUE = 0;

	/**
	 * The '<em><b>Blob</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Blob</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BLOB
	 * @model name="blob"
	 * @generated
	 * @ordered
	 */
	public static final int BLOB_VALUE = 1;

	/**
	 * The '<em><b>Byte array</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Byte array</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BYTE_ARRAY
	 * @model name="byte_array" literal="byte-array"
	 * @generated
	 * @ordered
	 */
	public static final int BYTE_ARRAY_VALUE = 2;

	/**
	 * The '<em><b>Date time</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Date time</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DATE_TIME
	 * @model name="date_time" literal="date-time"
	 * @generated
	 * @ordered
	 */
	public static final int DATE_TIME_VALUE = 3;

	/**
	 * The '<em><b>Date</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Date</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DATE
	 * @model name="date"
	 * @generated
	 * @ordered
	 */
	public static final int DATE_VALUE = 4;

	/**
	 * The '<em><b>Time</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Time</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TIME
	 * @model name="time"
	 * @generated
	 * @ordered
	 */
	public static final int TIME_VALUE = 5;

	/**
	 * The '<em><b>Currency ammount</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Currency ammount</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CURRENCY_AMMOUNT
	 * @model name="currency_ammount" literal="currency-ammount"
	 * @generated
	 * @ordered
	 */
	public static final int CURRENCY_AMMOUNT_VALUE = 5;

	/**
	 * The '<em><b>Currency precise</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Currency precise</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CURRENCY_PRECISE
	 * @model name="currency_precise" literal="currency-precise"
	 * @generated
	 * @ordered
	 */
	public static final int CURRENCY_PRECISE_VALUE = 6;

	/**
	 * The '<em><b>Fixed point</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Fixed point</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FIXED_POINT
	 * @model name="fixed_point" literal="fixed-point"
	 * @generated
	 * @ordered
	 */
	public static final int FIXED_POINT_VALUE = 7;

	/**
	 * The '<em><b>Floating point</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Floating point</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FLOATING_POINT
	 * @model name="floating_point" literal="floating-point"
	 * @generated
	 * @ordered
	 */
	public static final int FLOATING_POINT_VALUE = 8;

	/**
	 * The '<em><b>Numeric</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Numeric</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NUMERIC
	 * @model name="numeric"
	 * @generated
	 * @ordered
	 */
	public static final int NUMERIC_VALUE = 9;

	/**
	 * The '<em><b>Id</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Id</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ID
	 * @model name="id"
	 * @generated
	 * @ordered
	 */
	public static final int ID_VALUE = 10;

	/**
	 * The '<em><b>Id long</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Id long</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ID_LONG
	 * @model name="id_long" literal="id-long"
	 * @generated
	 * @ordered
	 */
	public static final int ID_LONG_VALUE = 11;

	/**
	 * The '<em><b>Id vlong</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Id vlong</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ID_VLONG
	 * @model name="id_vlong" literal="id-vlong"
	 * @generated
	 * @ordered
	 */
	public static final int ID_VLONG_VALUE = 12;

	/**
	 * The '<em><b>Indicator</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Indicator</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INDICATOR
	 * @model name="indicator"
	 * @generated
	 * @ordered
	 */
	public static final int INDICATOR_VALUE = 13;

	/**
	 * The '<em><b>Very short</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Very short</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #VERY_SHORT
	 * @model name="very_short" literal="very-short"
	 * @generated
	 * @ordered
	 */
	public static final int VERY_SHORT_VALUE = 14;

	/**
	 * The '<em><b>Short varchar</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Short varchar</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SHORT_VARCHAR
	 * @model name="short_varchar" literal="short-varchar"
	 * @generated
	 * @ordered
	 */
	public static final int SHORT_VARCHAR_VALUE = 15;

	/**
	 * The '<em><b>Long varchar</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Long varchar</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LONG_VARCHAR
	 * @model name="long_varchar" literal="long-varchar"
	 * @generated
	 * @ordered
	 */
	public static final int LONG_VARCHAR_VALUE = 16;

	/**
	 * The '<em><b>Very long</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Very long</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #VERY_LONG
	 * @model name="very_long" literal="very-long"
	 * @generated
	 * @ordered
	 */
	public static final int VERY_LONG_VALUE = 17;

	/**
	 * The '<em><b>Comment</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Comment</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #COMMENT
	 * @model name="comment"
	 * @generated
	 * @ordered
	 */
	public static final int COMMENT_VALUE = 18;

	/**
	 * The '<em><b>Description</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Description</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DESCRIPTION
	 * @model name="description"
	 * @generated
	 * @ordered
	 */
	public static final int DESCRIPTION_VALUE = 19;

	/**
	 * The '<em><b>Name</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Name</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NAME
	 * @model name="name"
	 * @generated
	 * @ordered
	 */
	public static final int NAME_VALUE = 20;

	/**
	 * The '<em><b>Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #VALUE
	 * @model name="value"
	 * @generated
	 * @ordered
	 */
	public static final int VALUE_VALUE = 21;

	/**
	 * The '<em><b>Credit card number</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Credit card number</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CREDIT_CARD_NUMBER
	 * @model name="credit_card_number" literal="credit-card-number"
	 * @generated
	 * @ordered
	 */
	public static final int CREDIT_CARD_NUMBER_VALUE = 22;

	/**
	 * The '<em><b>Credit card date</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Credit card date</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CREDIT_CARD_DATE
	 * @model name="credit_card_date" literal="credit-card-date"
	 * @generated
	 * @ordered
	 */
	public static final int CREDIT_CARD_DATE_VALUE = 23;

	/**
	 * The '<em><b>Email</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Email</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EMAIL
	 * @model name="email"
	 * @generated
	 * @ordered
	 */
	public static final int EMAIL_VALUE = 24;

	/**
	 * The '<em><b>Url</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Url</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #URL
	 * @model name="url"
	 * @generated
	 * @ordered
	 */
	public static final int URL_VALUE = 25;

	/**
	 * The '<em><b>Id ne</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Id ne</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ID_NE
	 * @model name="id_ne" literal="id-ne"
	 * @generated
	 * @ordered
	 */
	public static final int ID_NE_VALUE = 26;

	/**
	 * The '<em><b>Id long ne</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Id long ne</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ID_LONG_NE
	 * @model name="id_long_ne" literal="id-long-ne"
	 * @generated
	 * @ordered
	 */
	public static final int ID_LONG_NE_VALUE = 27;

	/**
	 * The '<em><b>Id vlong ne</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Id vlong ne</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ID_VLONG_NE
	 * @model name="id_vlong_ne" literal="id-vlong-ne"
	 * @generated
	 * @ordered
	 */
	public static final int ID_VLONG_NE_VALUE = 28;

	/**
	 * The '<em><b>Tel number</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Tel number</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TEL_NUMBER
	 * @model name="tel_number" literal="tel-number"
	 * @generated
	 * @ordered
	 */
	public static final int TEL_NUMBER_VALUE = 29;

	/**
	 * An array of all the '<em><b>Field Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final FieldType[] VALUES_ARRAY =
		new FieldType[] {
			OBJECT,
			BLOB,
			BYTE_ARRAY,
			DATE_TIME,
			DATE,
			TIME,
			CURRENCY_AMMOUNT,
			CURRENCY_PRECISE,
			FIXED_POINT,
			FLOATING_POINT,
			NUMERIC,
			ID,
			ID_LONG,
			ID_VLONG,
			INDICATOR,
			VERY_SHORT,
			SHORT_VARCHAR,
			LONG_VARCHAR,
			VERY_LONG,
			COMMENT,
			DESCRIPTION,
			NAME,
			VALUE,
			CREDIT_CARD_NUMBER,
			CREDIT_CARD_DATE,
			EMAIL,
			URL,
			ID_NE,
			ID_LONG_NE,
			ID_VLONG_NE,
			TEL_NUMBER,
		};

	/**
	 * A public read-only list of all the '<em><b>Field Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<FieldType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Field Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FieldType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FieldType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Field Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FieldType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FieldType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Field Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FieldType get(int value) {
		switch (value) {
			case OBJECT_VALUE: return OBJECT;
			case BLOB_VALUE: return BLOB;
			case BYTE_ARRAY_VALUE: return BYTE_ARRAY;
			case DATE_TIME_VALUE: return DATE_TIME;
			case DATE_VALUE: return DATE;
			case TIME_VALUE: return TIME;
			case CURRENCY_PRECISE_VALUE: return CURRENCY_PRECISE;
			case FIXED_POINT_VALUE: return FIXED_POINT;
			case FLOATING_POINT_VALUE: return FLOATING_POINT;
			case NUMERIC_VALUE: return NUMERIC;
			case ID_VALUE: return ID;
			case ID_LONG_VALUE: return ID_LONG;
			case ID_VLONG_VALUE: return ID_VLONG;
			case INDICATOR_VALUE: return INDICATOR;
			case VERY_SHORT_VALUE: return VERY_SHORT;
			case SHORT_VARCHAR_VALUE: return SHORT_VARCHAR;
			case LONG_VARCHAR_VALUE: return LONG_VARCHAR;
			case VERY_LONG_VALUE: return VERY_LONG;
			case COMMENT_VALUE: return COMMENT;
			case DESCRIPTION_VALUE: return DESCRIPTION;
			case NAME_VALUE: return NAME;
			case VALUE_VALUE: return VALUE;
			case CREDIT_CARD_NUMBER_VALUE: return CREDIT_CARD_NUMBER;
			case CREDIT_CARD_DATE_VALUE: return CREDIT_CARD_DATE;
			case EMAIL_VALUE: return EMAIL;
			case URL_VALUE: return URL;
			case ID_NE_VALUE: return ID_NE;
			case ID_LONG_NE_VALUE: return ID_LONG_NE;
			case ID_VLONG_NE_VALUE: return ID_VLONG_NE;
			case TEL_NUMBER_VALUE: return TEL_NUMBER;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private FieldType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //FieldType
