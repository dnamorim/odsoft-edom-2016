/**
 */
package ofbiz;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Key Map</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ofbiz.KeyMap#getOrigin <em>Origin</em>}</li>
 *   <li>{@link ofbiz.KeyMap#getRelated <em>Related</em>}</li>
 * </ul>
 *
 * @see ofbiz.OfbizPackage#getKeyMap()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='mustBeRelated validField validRelatedField'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot mustBeRelated='Relation.allInstances()->select(r|r.keyMap = self)->size()=1' validField='Entity.allInstances()->select(e|e.relations->select(r|r.keyMap = self)->size()=1).fields->select(f|f=self.origin)->size()=1' validRelatedField='         (Relation.allInstances()->select(r|r.keymap = self).related.fields->select(f|f = self.related)->size() = 1) or self.related.oclIsUndefined()'"
 * @generated
 */
public interface KeyMap extends EObject {
	/**
	 * Returns the value of the '<em><b>Origin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Origin</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Origin</em>' reference.
	 * @see #setOrigin(Field)
	 * @see ofbiz.OfbizPackage#getKeyMap_Origin()
	 * @model required="true"
	 * @generated
	 */
	Field getOrigin();

	/**
	 * Sets the value of the '{@link ofbiz.KeyMap#getOrigin <em>Origin</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Origin</em>' reference.
	 * @see #getOrigin()
	 * @generated
	 */
	void setOrigin(Field value);

	/**
	 * Returns the value of the '<em><b>Related</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Related</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Related</em>' reference.
	 * @see #setRelated(Field)
	 * @see ofbiz.OfbizPackage#getKeyMap_Related()
	 * @model required="true"
	 * @generated
	 */
	Field getRelated();

	/**
	 * Sets the value of the '{@link ofbiz.KeyMap#getRelated <em>Related</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Related</em>' reference.
	 * @see #getRelated()
	 * @generated
	 */
	void setRelated(Field value);

} // KeyMap
