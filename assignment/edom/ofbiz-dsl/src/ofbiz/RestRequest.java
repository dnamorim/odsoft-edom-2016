/**
 */
package ofbiz;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rest Request</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ofbiz.RestRequest#getAction <em>Action</em>}</li>
 *   <li>{@link ofbiz.RestRequest#getParameters <em>Parameters</em>}</li>
 * </ul>
 *
 * @see ofbiz.OfbizPackage#getRestRequest()
 * @model
 * @generated
 */
public interface RestRequest extends EObject {
	/**
	 * Returns the value of the '<em><b>Action</b></em>' attribute.
	 * The literals are from the enumeration {@link ofbiz.RestMethod}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' attribute.
	 * @see ofbiz.RestMethod
	 * @see #setAction(RestMethod)
	 * @see ofbiz.OfbizPackage#getRestRequest_Action()
	 * @model
	 * @generated
	 */
	RestMethod getAction();

	/**
	 * Sets the value of the '{@link ofbiz.RestRequest#getAction <em>Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action</em>' attribute.
	 * @see ofbiz.RestMethod
	 * @see #getAction()
	 * @generated
	 */
	void setAction(RestMethod value);

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link ofbiz.RestParameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see ofbiz.OfbizPackage#getRestRequest_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<RestParameter> getParameters();

} // RestRequest
