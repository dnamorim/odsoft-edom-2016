/**
 */
package ofbiz;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ofbiz.Relation#getFkName <em>Fk Name</em>}</li>
 *   <li>{@link ofbiz.Relation#getType <em>Type</em>}</li>
 *   <li>{@link ofbiz.Relation#getKeyMap <em>Key Map</em>}</li>
 *   <li>{@link ofbiz.Relation#getRelated <em>Related</em>}</li>
 * </ul>
 *
 * @see ofbiz.OfbizPackage#getRelation()
 * @model
 * @generated
 */
public interface Relation extends EObject {
	/**
	 * Returns the value of the '<em><b>Fk Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fk Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fk Name</em>' attribute.
	 * @see #setFkName(String)
	 * @see ofbiz.OfbizPackage#getRelation_FkName()
	 * @model
	 * @generated
	 */
	String getFkName();

	/**
	 * Sets the value of the '{@link ofbiz.Relation#getFkName <em>Fk Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fk Name</em>' attribute.
	 * @see #getFkName()
	 * @generated
	 */
	void setFkName(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link ofbiz.RelationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see ofbiz.RelationType
	 * @see #setType(RelationType)
	 * @see ofbiz.OfbizPackage#getRelation_Type()
	 * @model
	 * @generated
	 */
	RelationType getType();

	/**
	 * Sets the value of the '{@link ofbiz.Relation#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see ofbiz.RelationType
	 * @see #getType()
	 * @generated
	 */
	void setType(RelationType value);

	/**
	 * Returns the value of the '<em><b>Key Map</b></em>' containment reference list.
	 * The list contents are of type {@link ofbiz.KeyMap}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key Map</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key Map</em>' containment reference list.
	 * @see ofbiz.OfbizPackage#getRelation_KeyMap()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<KeyMap> getKeyMap();

	/**
	 * Returns the value of the '<em><b>Related</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Related</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Related</em>' reference.
	 * @see #setRelated(Entity)
	 * @see ofbiz.OfbizPackage#getRelation_Related()
	 * @model required="true"
	 * @generated
	 */
	Entity getRelated();

	/**
	 * Sets the value of the '{@link ofbiz.Relation#getRelated <em>Related</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Related</em>' reference.
	 * @see #getRelated()
	 * @generated
	 */
	void setRelated(Entity value);

} // Relation
