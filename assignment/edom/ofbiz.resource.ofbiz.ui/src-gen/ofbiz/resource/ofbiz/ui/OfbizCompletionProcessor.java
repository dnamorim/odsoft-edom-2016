/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.ui;

public class OfbizCompletionProcessor implements org.eclipse.jface.text.contentassist.IContentAssistProcessor {
	
	private ofbiz.resource.ofbiz.IOfbizResourceProvider resourceProvider;
	private ofbiz.resource.ofbiz.ui.IOfbizBracketHandlerProvider bracketHandlerProvider;
	
	public OfbizCompletionProcessor(ofbiz.resource.ofbiz.IOfbizResourceProvider resourceProvider, ofbiz.resource.ofbiz.ui.IOfbizBracketHandlerProvider bracketHandlerProvider) {
		this.resourceProvider = resourceProvider;
		this.bracketHandlerProvider = bracketHandlerProvider;
	}
	
	public org.eclipse.jface.text.contentassist.ICompletionProposal[] computeCompletionProposals(org.eclipse.jface.text.ITextViewer viewer, int offset) {
		ofbiz.resource.ofbiz.IOfbizTextResource textResource = resourceProvider.getResource();
		if (textResource == null) {
			return new org.eclipse.jface.text.contentassist.ICompletionProposal[0];
		}
		String content = viewer.getDocument().get();
		ofbiz.resource.ofbiz.ui.OfbizCodeCompletionHelper helper = new ofbiz.resource.ofbiz.ui.OfbizCodeCompletionHelper();
		ofbiz.resource.ofbiz.ui.OfbizCompletionProposal[] computedProposals = helper.computeCompletionProposals(textResource, content, offset);
		
		// call completion proposal post processor to allow for customizing the proposals
		ofbiz.resource.ofbiz.ui.OfbizProposalPostProcessor proposalPostProcessor = new ofbiz.resource.ofbiz.ui.OfbizProposalPostProcessor();
		java.util.List<ofbiz.resource.ofbiz.ui.OfbizCompletionProposal> computedProposalList = java.util.Arrays.asList(computedProposals);
		java.util.List<ofbiz.resource.ofbiz.ui.OfbizCompletionProposal> extendedProposalList = proposalPostProcessor.process(computedProposalList);
		if (extendedProposalList == null) {
			extendedProposalList = java.util.Collections.emptyList();
		}
		java.util.List<ofbiz.resource.ofbiz.ui.OfbizCompletionProposal> finalProposalList = new java.util.ArrayList<ofbiz.resource.ofbiz.ui.OfbizCompletionProposal>();
		for (ofbiz.resource.ofbiz.ui.OfbizCompletionProposal proposal : extendedProposalList) {
			if (proposal.getMatchesPrefix()) {
				finalProposalList.add(proposal);
			}
		}
		org.eclipse.jface.text.contentassist.ICompletionProposal[] result = new org.eclipse.jface.text.contentassist.ICompletionProposal[finalProposalList.size()];
		int i = 0;
		for (ofbiz.resource.ofbiz.ui.OfbizCompletionProposal proposal : finalProposalList) {
			String proposalString = proposal.getInsertString();
			String displayString = proposal.getDisplayString();
			String prefix = proposal.getPrefix();
			org.eclipse.swt.graphics.Image image = proposal.getImage();
			org.eclipse.jface.text.contentassist.IContextInformation info;
			info = new org.eclipse.jface.text.contentassist.ContextInformation(image, proposalString, proposalString);
			int begin = offset - prefix.length();
			int replacementLength = prefix.length();
			// if a closing bracket was automatically inserted right before, we enlarge the
			// replacement length in order to overwrite the bracket.
			ofbiz.resource.ofbiz.ui.IOfbizBracketHandler bracketHandler = bracketHandlerProvider.getBracketHandler();
			String closingBracket = bracketHandler.getClosingBracket();
			if (bracketHandler.addedClosingBracket() && proposalString.endsWith(closingBracket)) {
				replacementLength += closingBracket.length();
			}
			result[i++] = new org.eclipse.jface.text.contentassist.CompletionProposal(proposalString, begin, replacementLength, proposalString.length(), image, displayString, info, proposalString);
		}
		return result;
	}
	
	public org.eclipse.jface.text.contentassist.IContextInformation[] computeContextInformation(org.eclipse.jface.text.ITextViewer viewer, int offset) {
		return null;
	}
	
	public char[] getCompletionProposalAutoActivationCharacters() {
		return null;
	}
	
	public char[] getContextInformationAutoActivationCharacters() {
		return null;
	}
	
	public org.eclipse.jface.text.contentassist.IContextInformationValidator getContextInformationValidator() {
		return null;
	}
	
	public String getErrorMessage() {
		return null;
	}
}
