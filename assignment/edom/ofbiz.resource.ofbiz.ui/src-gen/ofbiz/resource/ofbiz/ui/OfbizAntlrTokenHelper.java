/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.ui;

/**
 * This class is only generated for backwards compatiblity. The original contents
 * of this class have been moved to class
 * ofbiz.resource.ofbiz.mopp.OfbizAntlrTokenHelper.
 */
public class OfbizAntlrTokenHelper {
	// This class is intentionally left empty.
}
