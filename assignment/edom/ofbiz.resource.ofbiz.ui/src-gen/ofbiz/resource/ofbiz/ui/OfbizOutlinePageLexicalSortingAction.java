/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.ui;

public class OfbizOutlinePageLexicalSortingAction extends ofbiz.resource.ofbiz.ui.AbstractOfbizOutlinePageAction {
	
	public OfbizOutlinePageLexicalSortingAction(ofbiz.resource.ofbiz.ui.OfbizOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Sort alphabetically", org.eclipse.jface.action.IAction.AS_CHECK_BOX);
		initialize("icons/sort_lexically_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewerComparator().setSortLexically(on);
		getTreeViewer().refresh();
	}
	
}
