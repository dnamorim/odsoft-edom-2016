/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.ui;

public class OfbizUIMetaInformation extends ofbiz.resource.ofbiz.mopp.OfbizMetaInformation {
	
	public ofbiz.resource.ofbiz.IOfbizHoverTextProvider getHoverTextProvider() {
		return new ofbiz.resource.ofbiz.ui.OfbizHoverTextProvider();
	}
	
	public ofbiz.resource.ofbiz.ui.OfbizImageProvider getImageProvider() {
		return ofbiz.resource.ofbiz.ui.OfbizImageProvider.INSTANCE;
	}
	
	public ofbiz.resource.ofbiz.ui.OfbizColorManager createColorManager() {
		return new ofbiz.resource.ofbiz.ui.OfbizColorManager();
	}
	
	/**
	 * @deprecated this method is only provided to preserve API compatibility. Use
	 * createTokenScanner(ofbiz.resource.ofbiz.IOfbizTextResource,
	 * ofbiz.resource.ofbiz.ui.OfbizColorManager) instead.
	 */
	public ofbiz.resource.ofbiz.ui.OfbizTokenScanner createTokenScanner(ofbiz.resource.ofbiz.ui.OfbizColorManager colorManager) {
		return createTokenScanner(null, colorManager);
	}
	
	public ofbiz.resource.ofbiz.ui.OfbizTokenScanner createTokenScanner(ofbiz.resource.ofbiz.IOfbizTextResource resource, ofbiz.resource.ofbiz.ui.OfbizColorManager colorManager) {
		return new ofbiz.resource.ofbiz.ui.OfbizTokenScanner(resource, colorManager);
	}
	
	public ofbiz.resource.ofbiz.ui.OfbizCodeCompletionHelper createCodeCompletionHelper() {
		return new ofbiz.resource.ofbiz.ui.OfbizCodeCompletionHelper();
	}
	
}
