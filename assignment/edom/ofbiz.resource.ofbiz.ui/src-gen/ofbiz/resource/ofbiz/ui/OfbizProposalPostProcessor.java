/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.ui;

/**
 * A class which can be overridden to customize code completion proposals.
 */
public class OfbizProposalPostProcessor {
	
	public java.util.List<ofbiz.resource.ofbiz.ui.OfbizCompletionProposal> process(java.util.List<ofbiz.resource.ofbiz.ui.OfbizCompletionProposal> proposals) {
		// the default implementation does returns the proposals as they are
		return proposals;
	}
	
}
