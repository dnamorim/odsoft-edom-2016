/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.ui;

public class OfbizOutlinePageCollapseAllAction extends ofbiz.resource.ofbiz.ui.AbstractOfbizOutlinePageAction {
	
	public OfbizOutlinePageCollapseAllAction(ofbiz.resource.ofbiz.ui.OfbizOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Collapse all", org.eclipse.jface.action.IAction.AS_PUSH_BUTTON);
		initialize("icons/collapse_all_icon.gif");
	}
	
	public void runInternal(boolean on) {
		if (on) {
			getTreeViewer().collapseAll();
		}
	}
	
	public boolean keepState() {
		return false;
	}
	
}
