/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.ui;

/**
 * An enumeration of all position categories.
 */
public enum OfbizPositionCategory {
	BRACKET, DEFINTION, PROXY;
}
