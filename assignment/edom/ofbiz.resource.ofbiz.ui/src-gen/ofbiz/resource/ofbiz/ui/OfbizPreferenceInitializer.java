/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.ui;

/**
 * A class used to initialize default preference values.
 */
public class OfbizPreferenceInitializer extends org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer {
	
	public void initializeDefaultPreferences() {
		
		initializeDefaultSyntaxHighlighting();
		initializeDefaultBrackets();
		
		org.eclipse.jface.preference.IPreferenceStore store = ofbiz.resource.ofbiz.ui.OfbizUIPlugin.getDefault().getPreferenceStore();
		// Set default value for matching brackets
		store.setDefault(ofbiz.resource.ofbiz.ui.OfbizPreferenceConstants.EDITOR_MATCHING_BRACKETS_COLOR, "192,192,192");
		store.setDefault(ofbiz.resource.ofbiz.ui.OfbizPreferenceConstants.EDITOR_MATCHING_BRACKETS_CHECKBOX, true);
		
	}
	
	private void initializeDefaultBrackets() {
		org.eclipse.jface.preference.IPreferenceStore store = ofbiz.resource.ofbiz.ui.OfbizUIPlugin.getDefault().getPreferenceStore();
		initializeDefaultBrackets(store, new ofbiz.resource.ofbiz.mopp.OfbizMetaInformation());
	}
	
	public void initializeDefaultSyntaxHighlighting() {
		org.eclipse.jface.preference.IPreferenceStore store = ofbiz.resource.ofbiz.ui.OfbizUIPlugin.getDefault().getPreferenceStore();
		initializeDefaultSyntaxHighlighting(store, new ofbiz.resource.ofbiz.mopp.OfbizMetaInformation());
	}
	
	private void initializeDefaultBrackets(org.eclipse.jface.preference.IPreferenceStore store, ofbiz.resource.ofbiz.IOfbizMetaInformation metaInformation) {
		String languageId = metaInformation.getSyntaxName();
		// set default brackets for ITextResource bracket set
		ofbiz.resource.ofbiz.ui.OfbizBracketSet bracketSet = new ofbiz.resource.ofbiz.ui.OfbizBracketSet(null, null);
		final java.util.Collection<ofbiz.resource.ofbiz.IOfbizBracketPair> bracketPairs = metaInformation.getBracketPairs();
		if (bracketPairs != null) {
			for (ofbiz.resource.ofbiz.IOfbizBracketPair bracketPair : bracketPairs) {
				bracketSet.addBracketPair(bracketPair.getOpeningBracket(), bracketPair.getClosingBracket(), bracketPair.isClosingEnabledInside());
			}
		}
		store.setDefault(languageId + ofbiz.resource.ofbiz.ui.OfbizPreferenceConstants.EDITOR_BRACKETS_SUFFIX, bracketSet.getBracketString());
	}
	
	private void initializeDefaultSyntaxHighlighting(org.eclipse.jface.preference.IPreferenceStore store, ofbiz.resource.ofbiz.mopp.OfbizMetaInformation metaInformation) {
		String languageId = metaInformation.getSyntaxName();
		String[] tokenNames = metaInformation.getSyntaxHighlightableTokenNames();
		if (tokenNames == null) {
			return;
		}
		for (int i = 0; i < tokenNames.length; i++) {
			String tokenName = tokenNames[i];
			ofbiz.resource.ofbiz.IOfbizTokenStyle style = metaInformation.getDefaultTokenStyle(tokenName);
			if (style != null) {
				String color = getColorString(style.getColorAsRGB());
				setProperties(store, languageId, tokenName, color, style.isBold(), true, style.isItalic(), style.isStrikethrough(), style.isUnderline());
			} else {
				setProperties(store, languageId, tokenName, "0,0,0", false, false, false, false, false);
			}
		}
	}
	
	private void setProperties(org.eclipse.jface.preference.IPreferenceStore store, String languageID, String tokenName, String color, boolean bold, boolean enable, boolean italic, boolean strikethrough, boolean underline) {
		store.setDefault(ofbiz.resource.ofbiz.ui.OfbizSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, ofbiz.resource.ofbiz.ui.OfbizSyntaxColoringHelper.StyleProperty.BOLD), bold);
		store.setDefault(ofbiz.resource.ofbiz.ui.OfbizSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, ofbiz.resource.ofbiz.ui.OfbizSyntaxColoringHelper.StyleProperty.COLOR), color);
		store.setDefault(ofbiz.resource.ofbiz.ui.OfbizSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, ofbiz.resource.ofbiz.ui.OfbizSyntaxColoringHelper.StyleProperty.ENABLE), enable);
		store.setDefault(ofbiz.resource.ofbiz.ui.OfbizSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, ofbiz.resource.ofbiz.ui.OfbizSyntaxColoringHelper.StyleProperty.ITALIC), italic);
		store.setDefault(ofbiz.resource.ofbiz.ui.OfbizSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, ofbiz.resource.ofbiz.ui.OfbizSyntaxColoringHelper.StyleProperty.STRIKETHROUGH), strikethrough);
		store.setDefault(ofbiz.resource.ofbiz.ui.OfbizSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, ofbiz.resource.ofbiz.ui.OfbizSyntaxColoringHelper.StyleProperty.UNDERLINE), underline);
	}
	
	private String getColorString(int[] colorAsRGB) {
		if (colorAsRGB == null) {
			return "0,0,0";
		}
		if (colorAsRGB.length != 3) {
			return "0,0,0";
		}
		return colorAsRGB[0] + "," +colorAsRGB[1] + ","+ colorAsRGB[2];
	}
}
