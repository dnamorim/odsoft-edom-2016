/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.ui;

public class OfbizOutlinePageExpandAllAction extends ofbiz.resource.ofbiz.ui.AbstractOfbizOutlinePageAction {
	
	public OfbizOutlinePageExpandAllAction(ofbiz.resource.ofbiz.ui.OfbizOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Expand all", org.eclipse.jface.action.IAction.AS_PUSH_BUTTON);
		initialize("icons/expand_all_icon.gif");
	}
	
	public void runInternal(boolean on) {
		if (on) {
			getTreeViewer().expandAll();
		}
	}
	
	public boolean keepState() {
		return false;
	}
	
}
