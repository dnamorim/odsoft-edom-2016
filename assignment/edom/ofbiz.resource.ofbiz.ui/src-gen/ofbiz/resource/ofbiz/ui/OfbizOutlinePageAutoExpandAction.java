/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.ui;

public class OfbizOutlinePageAutoExpandAction extends ofbiz.resource.ofbiz.ui.AbstractOfbizOutlinePageAction {
	
	public OfbizOutlinePageAutoExpandAction(ofbiz.resource.ofbiz.ui.OfbizOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Auto expand", org.eclipse.jface.action.IAction.AS_CHECK_BOX);
		initialize("icons/auto_expand_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewer().setAutoExpand(on);
		getTreeViewer().refresh();
	}
	
}
