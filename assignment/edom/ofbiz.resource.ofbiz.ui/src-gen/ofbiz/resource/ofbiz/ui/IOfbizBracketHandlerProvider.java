/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.ui;

/**
 * A provider for BracketHandler objects.
 */
public interface IOfbizBracketHandlerProvider {
	
	/**
	 * Returns the bracket handler.
	 */
	public ofbiz.resource.ofbiz.ui.IOfbizBracketHandler getBracketHandler();
	
}
