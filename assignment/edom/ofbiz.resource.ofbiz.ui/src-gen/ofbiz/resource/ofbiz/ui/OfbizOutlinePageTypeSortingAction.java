/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.ui;

public class OfbizOutlinePageTypeSortingAction extends ofbiz.resource.ofbiz.ui.AbstractOfbizOutlinePageAction {
	
	public OfbizOutlinePageTypeSortingAction(ofbiz.resource.ofbiz.ui.OfbizOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Group types", org.eclipse.jface.action.IAction.AS_CHECK_BOX);
		initialize("icons/group_types_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewerComparator().setGroupTypes(on);
		getTreeViewer().refresh();
	}
	
}
