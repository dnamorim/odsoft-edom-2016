/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.ui;

public abstract class AbstractOfbizOutlinePageAction extends org.eclipse.jface.action.Action {
	
	private String preferenceKey = this.getClass().getSimpleName() + ".isChecked";
	
	private ofbiz.resource.ofbiz.ui.OfbizOutlinePageTreeViewer treeViewer;
	
	public AbstractOfbizOutlinePageAction(ofbiz.resource.ofbiz.ui.OfbizOutlinePageTreeViewer treeViewer, String text, int style) {
		super(text, style);
		this.treeViewer = treeViewer;
	}
	
	public void initialize(String imagePath) {
		org.eclipse.jface.resource.ImageDescriptor descriptor = ofbiz.resource.ofbiz.ui.OfbizImageProvider.INSTANCE.getImageDescriptor(imagePath);
		setDisabledImageDescriptor(descriptor);
		setImageDescriptor(descriptor);
		setHoverImageDescriptor(descriptor);
		boolean checked = ofbiz.resource.ofbiz.ui.OfbizUIPlugin.getDefault().getPreferenceStore().getBoolean(preferenceKey);
		valueChanged(checked, false);
	}
	
	@Override	
	public void run() {
		if (keepState()) {
			valueChanged(isChecked(), true);
		} else {
			runBusy(true);
		}
	}
	
	public void runBusy(final boolean on) {
		org.eclipse.swt.custom.BusyIndicator.showWhile(org.eclipse.swt.widgets.Display.getCurrent(), new Runnable() {
			public void run() {
				runInternal(on);
			}
		});
	}
	
	public abstract void runInternal(boolean on);
	
	private void valueChanged(boolean on, boolean store) {
		setChecked(on);
		runBusy(on);
		if (store) {
			ofbiz.resource.ofbiz.ui.OfbizUIPlugin.getDefault().getPreferenceStore().setValue(preferenceKey, on);
		}
	}
	
	public boolean keepState() {
		return true;
	}
	
	public ofbiz.resource.ofbiz.ui.OfbizOutlinePageTreeViewer getTreeViewer() {
		return treeViewer;
	}
	
	public ofbiz.resource.ofbiz.ui.OfbizOutlinePageTreeViewerComparator getTreeViewerComparator() {
		return (ofbiz.resource.ofbiz.ui.OfbizOutlinePageTreeViewerComparator) treeViewer.getComparator();
	}
	
}
