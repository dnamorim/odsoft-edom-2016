/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package ofbiz.resource.ofbiz.ui;

public class OfbizOutlinePageActionProvider {
	
	public java.util.List<org.eclipse.jface.action.IAction> getActions(ofbiz.resource.ofbiz.ui.OfbizOutlinePageTreeViewer treeViewer) {
		// To add custom actions to the outline view, set the
		// 'overrideOutlinePageActionProvider' option to <code>false</code> and modify
		// this method.
		java.util.List<org.eclipse.jface.action.IAction> defaultActions = new java.util.ArrayList<org.eclipse.jface.action.IAction>();
		defaultActions.add(new ofbiz.resource.ofbiz.ui.OfbizOutlinePageLinkWithEditorAction(treeViewer));
		defaultActions.add(new ofbiz.resource.ofbiz.ui.OfbizOutlinePageCollapseAllAction(treeViewer));
		defaultActions.add(new ofbiz.resource.ofbiz.ui.OfbizOutlinePageExpandAllAction(treeViewer));
		defaultActions.add(new ofbiz.resource.ofbiz.ui.OfbizOutlinePageAutoExpandAction(treeViewer));
		defaultActions.add(new ofbiz.resource.ofbiz.ui.OfbizOutlinePageLexicalSortingAction(treeViewer));
		defaultActions.add(new ofbiz.resource.ofbiz.ui.OfbizOutlinePageTypeSortingAction(treeViewer));
		return defaultActions;
	}
	
}
