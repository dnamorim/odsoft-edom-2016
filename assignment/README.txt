This is the main folder for the EDOM and ODSOFT assignment.

In here, it is possible to find the following folders:

- EDOM: The assignment related to the EDOM subject containing the code, documentation and diagrams.
- ODSOFT: The assignment related to the ODSOFT subject containing the code, documentation and diagrams.
- Global Documentation: Contains any necessary documentation that is related to both projects.

