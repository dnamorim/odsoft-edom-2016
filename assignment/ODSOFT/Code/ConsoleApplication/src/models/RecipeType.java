package models;

public class RecipeType {
	//FISH,
	//BREAD,
	//MEAT,
	//SEA_FOOD
		
	private String recipeTypeId;
	
	private String description;
	
	public RecipeType(){
		
	}

	public String getRecipeTypeId() {
		return recipeTypeId;
	}

	public void setRecipeTypeId(String recipeTypeId) {
		this.recipeTypeId = recipeTypeId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
