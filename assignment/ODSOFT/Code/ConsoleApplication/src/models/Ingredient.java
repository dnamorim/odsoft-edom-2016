package models;

/**
 * Defines the recipe ingredient.
 */
public class Ingredient {

	private String ingredientId;
	
	private String name;
	
	private float quantity;
	
	private String uomId;
	
	public Ingredient(){
		
	}
	
	public Ingredient(String name, float quantity, String uomId){
		this.name = name;
		this.quantity = quantity;
		this.uomId = uomId;
	}
	
	public String getIngredientId() {
		return ingredientId;
	}

	public void setIngredientId(String ingredientId) {
		this.ingredientId = ingredientId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getQuantity() {
		return quantity;
	}

	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}

	public String getUomId() {
		return uomId;
	}

	public void setMeasureType(String uomId) {
		this.uomId = uomId;
	}
	
	
		
}
