package models;

/**
 * Defines a visibility for a recipe.
 */
public class Visibility {
	//PUBLIC,
	//PRIVATE
	
	private String visibilityId;
	
	private String description;
	
	public Visibility(){
		
	}

	public String getVisibilityId() {
		return visibilityId;
	}

	public void setVisibilityId(String visibilityId) {
		this.visibilityId = visibilityId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
