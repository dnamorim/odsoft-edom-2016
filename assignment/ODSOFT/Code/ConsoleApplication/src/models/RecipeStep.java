package models;

import java.util.List;

public class RecipeStep {
	
	private String recipeStepId;
	private int stepNumber;
	private String description;
	private String imageUrl;
	
	
	public RecipeStep(){
		
	}
	
	public RecipeStep (String description){
		this.description = description;		
	}

	public String getRecipeStepId() {
		return recipeStepId;
	}

	public void setRecipeStepId(String recipeStepId) {
		this.recipeStepId = recipeStepId;
	}

	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public int getStepNumber() {
		return stepNumber;
	}


	public void setStepNumber(int stepNumber) {
		this.stepNumber = stepNumber;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}	
	
}
