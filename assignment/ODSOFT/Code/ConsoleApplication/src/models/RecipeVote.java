package models;

/**
 * Defines a recipe vote. 
 */
public class RecipeVote {
	
	// The recipe vote ID.
	private String recipeVoteId;
	
	// The vote level.
	private int level;
	
	// The vote comment.
	private String comment;
	
	// The user that voted.
	private String user; 
	
	// The recipe that this vote belongs to.
	private String recipeId;
	
	public RecipeVote(){
		
	}
	
	public RecipeVote(int level, String userLoginId){
		this.level = level;
		this.user = userLoginId;
	}
		
	public String getRecipeVoteId() {
		return recipeVoteId;
	}

	public void setRecipeVoteId(String recipeVoteId) {
		this.recipeVoteId = recipeVoteId;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public int getLevel() {
		return level;
	}


	public void setLevel(int level) {
		this.level = level;
	}


	public String getComment() {
		return comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}


	public String getUsername() {
		return user;
	}


	public void setUsername(String username) {
		this.user = username;
	}

	public String getRecipeId() {
		return recipeId;
	}

	public void setRecipeId(String recipeId) {
		this.recipeId = recipeId;
	}
	
}
