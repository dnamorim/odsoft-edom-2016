package models;

import java.util.List;

/**
 * This class defines a recipe.
 */
public class Recipe {
	
	// The recipe ID.
	private String recipeId;
	
	// The recipe name.
	private String name;
	
	// The recipe type.
	private RecipeType recipeType;
	
	// The recipe notes.
	private String notes;
	
	// The recipe image.
	private String imageUrl;
	
	// The recipe visibility.
	private Visibility visibility;
	
	// The recipe ingredients.
	private List<Ingredient> ingredients;
	
	// The recipe steps.
	private List<RecipeStep> steps;
			
	/**
	 * Base Constructor.
	 */
	public Recipe(){
		
	}
	
	/**
	 * Creates a recipe based on the mandatory attributes.
	 * @param name - Recipe name.
	 * @param ingredients - List of recipe ingredients.
	 * @param steps - List of recipe steps.
	 * @param visibility - Recipe visibility.
	 */
	public Recipe(String name, RecipeType recipeType, List<Ingredient> ingredients, List<RecipeStep> steps, Visibility visibility)
	{
		this.name = name;
		this.recipeType = recipeType;
		this.ingredients = ingredients;
		this.steps = steps;
		this.visibility = visibility;
	}

	public String getRecipeId() {
		return recipeId;
	}

	public void setRecipeId(String recipeId) {
		this.recipeId = recipeId;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
	
	public RecipeType getRecipeType() {
		return recipeType;
	}


	public void setRecipeType(RecipeType recipeType) {
		this.recipeType = recipeType;
	}


	public String getNotes() {
		return notes;
	}


	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Visibility getVisibility() {
		return visibility;
	}

	public void setVisibility(Visibility visibility) {
		this.visibility = visibility;
	}


	public List<Ingredient> getIngredients() {
		return ingredients;
	}


	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}


	public List<RecipeStep> getSteps() {
		return steps;
	}


	public void setSteps(List<RecipeStep> steps) {
		this.steps = steps;
	}
	
}