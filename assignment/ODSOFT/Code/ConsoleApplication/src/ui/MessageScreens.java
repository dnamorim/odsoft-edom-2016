package ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import core.UserState;
import models.*;
import rest.recipe.RecipeClient;

/**
 * Contains all the message screens functions
 * that need to be shown to the user.
 */
public class MessageScreens {
	
	private UserState userState;
	
	public MessageScreens(){
		this.userState = new UserState();
	}
	
	public int showWelcomeScreen(){
				
		Scanner in = new Scanner(System.in);
		int selectedOption = 0;
		
		do{
			System.out.println("*******************************************");
			System.out.println("* Welcome to MyRecipe Console Application *");
			System.out.println("*******************************************");
			System.out.println("[1] - Login");
			System.out.println("[2] - Register User");
			System.out.println("[3] - Exit");
			
			System.out.print("\nYour choice: ");
			selectedOption = in.nextInt();
		} while(selectedOption <= 0 || selectedOption > 3);
				
		// Close application right away case necessary.
		if(selectedOption == 3){
			System.exit(0);
		}	
		
		return selectedOption;
		
	}
		
	public void showLoginScreen(){
		System.out.println("**************");
		System.out.println("* User Login *");
		System.out.println("**************");
	}
	
	public void showRegisterUserScreen(){
		System.out.println("*********************");
		System.out.println("* Register New User *");
		System.out.println("*********************");
	}
	
	public void showMainMenu(){
		
		Scanner in = new Scanner(System.in);
		int selectedOption = 0;
		
		do{
			System.out.println("\n");
			System.out.println("***********************");
			System.out.println("* My Recipe Main Menu *");
			System.out.println("***********************");
			System.out.println("[1] - Register Recipe");
			System.out.println("[2] - View Recipes");
			System.out.println("[3] - Exit");
			
			System.out.print("\nYour choice: ");
			selectedOption = in.nextInt();
			
		} while(selectedOption <= 0 || selectedOption > 3);
		
		
		switch(selectedOption){
			case 1:
				showRegisterRecipeMenu();
				break;
			case 2:
				showAllRecipes();
				break;
			case 3:
				System.exit(0);
				break;
		}
		
	}
	
	public void showRegisterRecipeMenu(){
		
		Scanner in = new Scanner(System.in);
		
		String recipeName;
		
		String recipeNotes;
		
		String recipeTypeId;
				
		List<Ingredient> recipeIngredients;
		
		List<RecipeStep> recipeSteps;
		
		Visibility recipeVisibility;
		
		String recipeImageUrl;
		
		int ingredientNumber = 0;
		int stepNumber = 0;
		
		
		System.out.println("\n");
		System.out.println("*******************");
		System.out.println("* Register Recipe *");
		System.out.println("*******************");
		
		System.out.print("\nRecipe Name:");
		recipeName = in.nextLine();
		
		System.out.println("\nRecipe Notes: ");
		recipeNotes = in.nextLine();
		
		System.out.println("\nRecipe Type[ fish, meat, vegetarian]: ");
		recipeTypeId = in.nextLine();
		
		System.out.print("\nRecipe Image URL (Optional): ");
		recipeImageUrl = in.nextLine();
		
		if(recipeImageUrl == null){
			recipeImageUrl = "";
		}
		
		//System.out.print("\nNumber of ingredients: ");
		//ingredientNumber = in.nextInt();
		
		//System.out.print("\nNumber of steps: ");
		//stepNumber = in.nextInt();
		
		//System.out.println("*****");
		
		//recipeIngredients = addIngredients(ingredientNumber);
		
		//System.out.println("*****");
		
		//recipeSteps = addSteps(stepNumber);
		
		System.out.println("***********************");
		System.out.println("* Creating Recipe ... *");
		System.out.println("***********************");
		
		RecipeType type = new RecipeType();
		type.setRecipeTypeId(recipeTypeId);
		
		Recipe recipe = new Recipe();
		recipe.setName(recipeName);
		recipe.setImageUrl(recipeImageUrl);
		recipe.setRecipeType(type);
		recipe.setNotes(recipeNotes);
		
		// Not possible to perform at this time...
		//recipe.setIngredients(recipeIngredients);
		//recipe.setSteps(recipeSteps);
	
		createRecipe(recipe);		
		showMainMenu();
		
	}
	
	public void showAllRecipes(){
		
		List<String> recipeList = RecipeClient.GetRecipeList(userState.getUsername(), userState.getPassword());
		
		System.out.println("Recipes:");
			
		for (String recipe : recipeList){
			System.out.println("- " + recipe);
		}
		
		Scanner in = new Scanner(System.in);

		System.out.println("\nPress any key to go back...");
		String option = in.nextLine();
		System.out.println("\n");
		showMainMenu();
	}
	
	private List<Ingredient> addIngredients(int totalIngredients){
		
		Scanner in = new Scanner(System.in);
				
		List<String> uomList = RecipeClient
				.GetUomList(userState.getUsername(), userState.getPassword());

		List<Ingredient> recipeIngredients
			= new ArrayList<Ingredient>();
		
		for(int i = 0; i < totalIngredients; i++){
			
			String name;
			float quantity;
			int measureTypeInt;
			
			System.out.println("- Ingredient " + (i+1) + "\n");
			
			System.out.println("Name: ");
			name = in.nextLine();
			
			System.out.println("Quantity: ");
			quantity = in.nextFloat();
			
			System.out.println("Avaliable Measure Types -");
			
			for(int j = 0; j < uomList.size(); j++){
				System.out.println("["+ j +"] - " + uomList.get(j));
			}
			
			System.out.println("Your Choice: ");
			measureTypeInt = in.nextInt();
						
			Ingredient ingredient = new Ingredient(name, quantity, uomList.get(measureTypeInt));
			recipeIngredients.add(ingredient);
		}
				
		return recipeIngredients;
		
	}
	
	private List<RecipeStep> addSteps(int totalSteps){
		
		List<RecipeStep> recipeSteps
			= new ArrayList<RecipeStep>();
		
		Scanner in = new Scanner(System.in);
		
		for(int i = 0; i < totalSteps; i++){
			
			System.out.println("Step Number: ");
			int stepNumber = in.nextInt();
			
			System.out.println("Description: ");
			String description = in.nextLine();
			
			System.out.println("Image:");
			String imageUrl = in.nextLine();
			
			RecipeStep step = new RecipeStep(description);
			
			if(imageUrl != null || imageUrl != ""){
				step.setImageUrl(imageUrl);
			}
			
			step.setStepNumber(stepNumber);
			
			
			recipeSteps.add(step);
		}
			
		return recipeSteps;
		
	}
	
	private void createRecipe(Recipe recipe){
		boolean recipeStatus = RecipeClient.CreateRecipe(userState.getUsername(), userState.getPassword(), recipe);
		
		if(recipeStatus){
			System.out.println("\n** Recipe Created Successfully **");
		}else{
			System.out.println("\n** Could Not Create Recipe **");
		}
		
	}
	
	public String requestUsername(){
		
		String username = "";
		
	    Scanner in = new Scanner(System.in);
		System.out.print("Username:");
		
		username = in.nextLine();
		
		this.userState.setUsername(username);
		
		return username;    
	}
	
	public String requestPassword(){
		
		String password = "";
		
	    Scanner in = new Scanner(System.in);  
		System.out.print("Password:");
		
		password = in.nextLine();
		
		this.userState.setPassword(password);
		
		return password;
		
	}
	
}
