package rest.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;


/**
 * Contains all the functions for the REST service
 * that are related to the user account (login/logout).
 */
public class AccountClient {

	public static boolean LoginUser(String username, String password){
		
		try{
		
			Client client = ClientBuilder.newClient();
			
			MultivaluedMap<String, String> values = new MultivaluedHashMap<String, String>();
			values.add("username", username);
			values.add("password", password);
	
			Form form = new Form(values);
					
			Response response = client
					.target("http://localhost:8080/restcomponent/account/login")
					.request("application/json")
					 //.header("Content-Type", "application/x-www-form-urlencoded")
					.post(Entity.form(form));
	
			if (response.getStatus() == 200) {
				System.out.println("\n** Logged In Successfully **");
				return true;
			} else {
				System.out.println("\n** Cannot Log In **");
				return false;
			}
		}catch(Exception Ex){
			System.out.println("\n** Cannot Log In - Server Seems to be Down **");
			return false;
		}	
	}
	
}
