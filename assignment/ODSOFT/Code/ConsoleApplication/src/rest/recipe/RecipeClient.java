package rest.recipe;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import models.Recipe;

public class RecipeClient {

	public static List<String> GetUomList(String username, String password){
		
		ArrayList<String> uomList = new ArrayList<String>();
		
		try{
		
			Client client = ClientBuilder.newClient();
			
			MultivaluedMap<String, Object> headerMap = new MultivaluedHashMap<String, Object>();
				headerMap.put("login.username", Arrays.asList(new Object[] { username }));
				headerMap.put("login.password", Arrays.asList(new Object[] { password }));
			
			MultivaluedMap<String, String> values = new MultivaluedHashMap<String, String>();
			values.add("username", username);
			values.add("password", password);
						
			Response response = client
					.target("http://localhost:8080/restcomponent/recipe/uom")
					.request("application/json")
					.headers(headerMap)
					.get();
				
			String stringObj = response.readEntity(String.class);
			JsonReader jsonReader = Json.createReader(new StringReader(stringObj));
			JsonArray jsonArray = jsonReader.readArray();
			
			for(int i = 0; i < jsonArray.size(); i++){
				JsonObject jsonObj = jsonArray.getJsonObject(i);
				uomList.add(jsonObj.getString("uomId"));
			}
			
			return uomList;
		
		}catch(Exception Ex){
			System.out.println("\n** Cannot Log In - Server Seems to be Down **");
			return uomList;
		}	
	}
	
	public static boolean CreateRecipe(String username, String password, Recipe recipe){
		
		try{
		
			Client client = ClientBuilder.newClient();
						
			MultivaluedMap<String, Object> headerValue = new MultivaluedHashMap<String, Object>();
			headerValue.add("login.username", username);
			headerValue.add("login.password", password);
			
			MultivaluedMap<String, String> createRecipeValues = new MultivaluedHashMap<String, String>();
			createRecipeValues.add("recipeName", recipe.getName());
			createRecipeValues.add("imageUrl", recipe.getImageUrl());
			createRecipeValues.add("notes", recipe.getNotes());
			createRecipeValues.add("recipeTypeId", recipe.getRecipeType().getRecipeTypeId());

	
			Form createRecipeForm = new Form(createRecipeValues);
					
			Response createRecipeResponse = client
					.target("http://localhost:8080/restcomponent/recipe")
					.request("application/json")
					.headers(headerValue)
					.post(Entity.form(createRecipeValues));
			
			if(createRecipeResponse.getStatus() == 200){
				return true;
			}else{
				return false;
			}
			
		}catch(Exception Ex){
			System.out.println("\n** Cannot Log In - Server Seems to be Down **");
			return false;
		}	
	}
	
	public static List<String> GetRecipeList(String username, String password){
		
		ArrayList<String> recipeList = new ArrayList<String>();
		
		try{
		
			Client client = ClientBuilder.newClient();
			
			MultivaluedMap<String, Object> headerMap = new MultivaluedHashMap<String, Object>();
				headerMap.put("login.username", Arrays.asList(new Object[] { username }));
				headerMap.put("login.password", Arrays.asList(new Object[] { password }));
			
			MultivaluedMap<String, String> values = new MultivaluedHashMap<String, String>();
			values.add("username", username);
			values.add("password", password);
						
			Response response = client
					.target("http://localhost:8080/restcomponent/recipe")
					.request("application/json")
					.headers(headerMap)
					.get();
				
			String stringObj = response.readEntity(String.class);
			JsonReader jsonReader = Json.createReader(new StringReader(stringObj));
			JsonArray jsonArray = jsonReader.readArray();
			
			for(int i = 0; i < jsonArray.size(); i++){
				JsonObject jsonObj = jsonArray.getJsonObject(i);
				recipeList.add("[" + jsonObj.getString("recipeId") + "] - " 
				+ jsonObj.getString("name") + " : " + jsonObj.getString("notes"));
			}
			
			return recipeList;
		
		}catch(Exception Ex){
			System.out.println("\n** Cannot Log In - Server Seems to be Down **");
			return recipeList;
		}	
	}
	
	
}
