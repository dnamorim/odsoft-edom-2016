package core;

public class UserState {

	
	private String _username;
	private String _password;
	
	public UserState(){
		
	}
	
	public String getUsername(){
		return this._username;
	}
	
	public String getPassword(){
		return this._password;
	}
	
	public void setUsername(String username){
		this._username = username;
	}
	
	public void setPassword(String password){
		this._password = password;
	}
	
}
