package core;

import ui.MessageScreens;
import rest.client.AccountClient;
import rest.recipe.RecipeClient;

/**
 * Defines the main class that will start the 
 * console application.
 *
 */
public class Program {
	
	private static UserState _userState;
	private static MessageScreens _messageScreens;
	
	/**
	 * Main method.
	 * @param args
	 */
	public static void main(String [] args)
	{
				
		loadObjects();
			
		boolean isLoggedIn = false;
			
			do{
				_messageScreens.showLoginScreen();
				_userState.setUsername(_messageScreens.requestUsername());
				_userState.setPassword(_messageScreens.requestPassword());
				
				isLoggedIn = AccountClient.LoginUser(_userState.getUsername(), 
						_userState.getPassword());
				
			} while(!isLoggedIn);
		
			_messageScreens.showMainMenu();
			
	}
	
	
	private static void loadObjects(){
		_userState = new UserState();
		_messageScreens = new MessageScreens();
	}

}
