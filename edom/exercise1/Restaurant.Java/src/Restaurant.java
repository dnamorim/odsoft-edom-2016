import java.util.ArrayList;
import java.util.Random;

/**
 * Class that defines a restaurant.
 * @author Ricardo Mota.
 *
 */
public class Restaurant {

	
	private int Id;
	private String Name;
	private String AddressLine1;
	private String AddressLine2;
	private String PostCode;
	private String Country;
	private int PhoneNumber;
	
	
	private ArrayList<Service> ServiceList;
	private ArrayList<Client> ClientList;
	private ArrayList<Worker> WorkerList;
	
	
	/**
	 * Restaurant Contructor.
	 * The ID is randomly defined for this purpose.
	 * All the restaurant collections are initialized.
	 */
	public Restaurant(){
		
		this.Id = new Random().nextInt(100);
		
		this.ServiceList = new ArrayList<Service>();
		this.ClientList = new ArrayList<Client>();
		this.WorkerList = new ArrayList<Worker>();
		
	}
	
	
	/** Setters **/
	public void setName(String sName){
		this.Name = sName;
	}
	
	public void setAddressLine1(String sAddressLine1){
		this.AddressLine1 = sAddressLine1;
	}
	
	public void setAddressLine2(String sAddressLine2){
		this.AddressLine2 = sAddressLine2;
	}
	
	public void setPostCode(String sPostCode){
		this.PostCode = sPostCode;
	}
	
	public void setCountry(String sCountry){
		this.Country = sCountry;
	}
	
	public void setPhoneNumber(int nPhoneNumber){
		this.PhoneNumber = nPhoneNumber;
	}
	
	public void setServices(ArrayList<Service> lServiceList){
		this.ServiceList = lServiceList;
	}
	
	public void setClients(ArrayList<Client> lClientList){
		this.ClientList = lClientList;
	}
	
	public void setWorkers(ArrayList<Worker> lWorkerList){
		this.WorkerList = lWorkerList;
	}
	
	
	/** Getters **/
	public int getId(){
		return this.Id;
	}
	
	public String getName(){
		return this.Name;
	}
	
	public String getAddressLine1(){
		return this.AddressLine1;
	}
	
	public String getAddressLine2(){
		return this.AddressLine2;
	}
	
	public String getPostCode(){
		return this.PostCode;
	}
	
	public String getCountry(){
		return this.Country;
	}
	
	public int getPhoneNumber(){
		return this.PhoneNumber;
	}
	
	public ArrayList<Service> getServices(){
		return this.ServiceList;
	}
	
	public ArrayList<Client> getClients(){
		return this.ClientList;
	}
	
	public ArrayList<Worker> getWorkers(){
		return this.WorkerList;
	}
	
}
