import java.util.Random;

/**
 * Client Card class.
 * @author Ricardo Mota.
 *
 */
public class ClientCard {

	
	private int Id;
	
	
	public ClientCard(){
		this.Id = new Random().nextInt(100);
	}
	
	/** Getters **/
	public int getId(){
		return this.Id;
	}
	
}
