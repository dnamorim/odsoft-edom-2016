import java.util.ArrayList;

/**
 * Simple menu just for this case.
 * Each menu contains a category.
 * @author Ricardo Mota.
 *
 */
public class Menu {

	
	private ArrayList<Meal> MealList;
	
	public Menu(){
		this.MealList = new ArrayList<Meal>();
	}
	
	/** Setters **/
	public void setMeals(ArrayList<Meal> oMealList){
		this.MealList = oMealList;
	}
	
	/** Getters **/
	public ArrayList<Meal> getMealList(){
		return this.MealList;
	}
	
}
