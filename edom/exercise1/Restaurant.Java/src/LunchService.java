import java.util.ArrayList;


/***
 * Basic LunchService class just for the exercise purpose.
 * @author Ricardo Mota.
 *
 */
public class LunchService extends Service {

	
	private Menu Menu;
	private ArrayList<Order> OrdersList;
	
	
	public LunchService(Menu oMenu){
		this.Menu = oMenu;
		this.OrdersList = new ArrayList<Order>();
	}
	
	
	/** Getters **/
	public Menu getMenu(){
		return this.Menu;
	}
	
	public ArrayList<Order> getOrdersList(){
		return this.OrdersList;
	}
	
	
	/** Setters **/
	public void setMenu(Menu oMenu){
		this.Menu = oMenu;
	}
	
	public void setOrdersList(ArrayList<Order> lOrdersList){
		this.OrdersList = lOrdersList;
	}
	
	
	/** Behavior **/
	public boolean PerformOrder(Order oOrder){
		
		// This is just a basic proof of concept class.
		
		if(oOrder.getPayment().isPaid()){
			return true;
		}
		
		return false;
		
	}
	
}
