/**
 * Main class for this java project.
 * @author Ricardo Mota
 *
 */
public class Main {
	
	
	public static void main(String [] args)
	{
		RunDemo();
	}
	
	
	/**
	 * Proof of concept demo in order to perform the "Order Meal"
	 * in our restaurant case.
	 */
	private static void RunDemo(){
		
		
		// First of all create a new restaurant.
		Restaurant oRestaurant = new Restaurant();
		
		
		// Create a restaurant worker.
		Worker oWorker = new Worker();
		
		// Create a new menu with meals and the lunch service.
		Menu oMenu = new Menu();
		Meal oMeal = new Meal();
		
		oMenu.getMealList().add(oMeal);
		LunchService oLunchService = new LunchService(oMenu);
		
		
		// Create a client card and give it to a new registered client.
		ClientCard oClientCard = new ClientCard();
		Client oClient = new RegisteredClient(oClientCard);
		
		// Insert the worker, service and client into the restaurant.
		oRestaurant.getWorkers().add(oWorker);
		oRestaurant.getServices().add(oLunchService);
		oRestaurant.getClients().add(oClient);
		
		// Create a new order with a meal.
		Order oOrder = new Order();
		oOrder.getMeals().add(oMeal);
				
		// Add the order to the lunch service.	
		oLunchService.getOrdersList().add(oOrder);
		
		// Create the order payment and set it to the order.
		Payment oPayment = new Payment();
		oOrder.setPayment(oPayment);
		
		// Set it to be paid.
		oPayment.setPaymentStatus(true);
		
		//Process the lunch service order.
		boolean isOrdered = oLunchService.PerformOrder(oOrder);
		
		System.out.println("Order paid. Is order complete? " + isOrdered);
			
		// Just to test, lets state the the payment was not completed.
		oPayment.setPaymentStatus(false);
		isOrdered = oLunchService.PerformOrder(oOrder);
		System.out.println("Order cancelled. Is order complete? " + isOrdered);
	
		
	}
	
	
	

}
