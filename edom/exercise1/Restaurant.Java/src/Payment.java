
/**
 * Simple order payment proof of concept class.
 * @author Ricardo Mota.
 *
 */
public class Payment {


	private boolean IsPaid;
	
	
	public Payment(){
		
	}
	
	/** Getters **/
	public boolean isPaid(){
		return this.IsPaid;
	}
	
	/** Setters **/
	public void setPaymentStatus(boolean bValue){
		this.IsPaid = bValue;
	}
	
	
}
