
/**
 * Inherits from the client class.
 * @author Ricardo Mota.
 *
 */
public class RegisteredClient extends Client {

	
	private String Name;
	private String EmailAddress;
	private int PhoneNumber;
	private int TIN;
	
	private ClientCard ClientCard;
	
	
	/**
	 * Constructor.
	 */
	public RegisteredClient(ClientCard oCard) {
		super();
		this.ClientCard = oCard;
	}
	
	
	/**
	 * Constructor with attributes.
	 */
	public RegisteredClient(ClientCard oCard, String sName, String sEmailAddress, int nPhoneNumber, int nTIN) {
		super();
		this.ClientCard = oCard;
		
		this.Name = sName;
		this.EmailAddress = sEmailAddress;
		this.PhoneNumber = nPhoneNumber;
		this.TIN = nTIN;
		
	}
	
	
	/** Getters **/
	public String getName(){
		return this.Name;
	}
	
	public String getEmailAddress(){
		return this.EmailAddress;
	}
	
	public int getPhoneNumber(){
		return this.PhoneNumber;
	}
	
	public int getTIN(){
		return this.TIN;
	}
	
	public ClientCard getClientCard(){
		return this.ClientCard;
	}
	
	
	/** Setters **/
	public void setName(String sName){
		this.Name = sName;
	}
	
	public void setEmailAddress(String sEmailAddress){
		this.EmailAddress = sEmailAddress;
	}
	
	public void setPhoneNumber(int nPhoneNumber){
		this.PhoneNumber = nPhoneNumber;
	}
	
	public void setTIN(int nTIN){
		this.TIN = nTIN;
	}
	
}
