import java.util.Random;

/**
 * Just keeps a simple client class without any thing set for this purpose.
 * @author Ricardo Mota.
 *
 */
public class Client {

	
	private int Id;
	
	
	public Client(){
		this.Id = new Random().nextInt(100);
	}
	
	
	public int getId(){
		return this.Id;
	}
	
}
