import java.util.ArrayList;

/**
 * Simple order class just for this purpose.
 * @author Ricardo Mota.
 *
 */
public class Order {
	
	
	private Payment Payment;
	private ArrayList<Meal> Meals;

	/**
	 * Constructor.
	 */
	public Order(){
		this.Payment = new Payment();
		this.Meals = new ArrayList<Meal>();
	}
	
	
	/** Getters **/
	public Payment getPayment(){
		return this.Payment;
	}
	
	public ArrayList<Meal> getMeals(){
		return this.Meals;
	}
	
	
	/** Setters **/
	public void setPayment(Payment oPayment){
		this.Payment = oPayment;
	}
	
}
