/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.grammar;

public class EmftextmindmapContainment extends mindmap.resource.emftextmindmap.grammar.EmftextmindmapTerminal {
	
	private final org.eclipse.emf.ecore.EClass[] allowedTypes;
	
	public EmftextmindmapContainment(org.eclipse.emf.ecore.EStructuralFeature feature, mindmap.resource.emftextmindmap.grammar.EmftextmindmapCardinality cardinality, org.eclipse.emf.ecore.EClass[] allowedTypes, int mandatoryOccurencesAfter) {
		super(feature, cardinality, mandatoryOccurencesAfter);
		this.allowedTypes = allowedTypes;
	}
	
	public org.eclipse.emf.ecore.EClass[] getAllowedTypes() {
		return allowedTypes;
	}
	
	public String toString() {
		String typeRestrictions = null;
		if (allowedTypes != null && allowedTypes.length > 0) {
			typeRestrictions = mindmap.resource.emftextmindmap.util.EmftextmindmapStringUtil.explode(allowedTypes, ", ", new mindmap.resource.emftextmindmap.IEmftextmindmapFunction1<String, org.eclipse.emf.ecore.EClass>() {
				public String execute(org.eclipse.emf.ecore.EClass eClass) {
					return eClass.getName();
				}
			});
		}
		return getFeature().getName() + (typeRestrictions == null ? "" : "[" + typeRestrictions + "]");
	}
	
}
