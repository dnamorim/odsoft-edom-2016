/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.grammar;

public class EmftextmindmapSequence extends mindmap.resource.emftextmindmap.grammar.EmftextmindmapSyntaxElement {
	
	public EmftextmindmapSequence(mindmap.resource.emftextmindmap.grammar.EmftextmindmapCardinality cardinality, mindmap.resource.emftextmindmap.grammar.EmftextmindmapSyntaxElement... elements) {
		super(cardinality, elements);
	}
	
	public String toString() {
		return mindmap.resource.emftextmindmap.util.EmftextmindmapStringUtil.explode(getChildren(), " ");
	}
	
}
