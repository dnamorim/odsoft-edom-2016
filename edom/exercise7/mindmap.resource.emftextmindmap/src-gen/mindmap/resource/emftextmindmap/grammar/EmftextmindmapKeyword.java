/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.grammar;

/**
 * A class to represent a keyword in the grammar.
 */
public class EmftextmindmapKeyword extends mindmap.resource.emftextmindmap.grammar.EmftextmindmapSyntaxElement {
	
	private final String value;
	
	public EmftextmindmapKeyword(String value, mindmap.resource.emftextmindmap.grammar.EmftextmindmapCardinality cardinality) {
		super(cardinality, null);
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	public String toString() {
		return value;
	}
	
}
