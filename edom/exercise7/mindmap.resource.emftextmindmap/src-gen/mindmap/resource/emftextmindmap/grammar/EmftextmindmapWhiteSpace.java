/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.grammar;

public class EmftextmindmapWhiteSpace extends mindmap.resource.emftextmindmap.grammar.EmftextmindmapFormattingElement {
	
	private final int amount;
	
	public EmftextmindmapWhiteSpace(int amount, mindmap.resource.emftextmindmap.grammar.EmftextmindmapCardinality cardinality) {
		super(cardinality);
		this.amount = amount;
	}
	
	public int getAmount() {
		return amount;
	}
	
	public String toString() {
		return "#" + getAmount();
	}
	
}
