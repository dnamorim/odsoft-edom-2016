/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.grammar;

public class EmftextmindmapCompound extends mindmap.resource.emftextmindmap.grammar.EmftextmindmapSyntaxElement {
	
	public EmftextmindmapCompound(mindmap.resource.emftextmindmap.grammar.EmftextmindmapChoice choice, mindmap.resource.emftextmindmap.grammar.EmftextmindmapCardinality cardinality) {
		super(cardinality, new mindmap.resource.emftextmindmap.grammar.EmftextmindmapSyntaxElement[] {choice});
	}
	
	public String toString() {
		return "(" + getChildren()[0] + ")";
	}
	
}
