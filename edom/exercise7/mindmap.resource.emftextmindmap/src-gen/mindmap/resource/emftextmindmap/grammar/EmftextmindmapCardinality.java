/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.grammar;

public enum EmftextmindmapCardinality {
	
	ONE, PLUS, QUESTIONMARK, STAR;
	
}
