/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.grammar;

/**
 * The abstract super class for all elements of a grammar. This class provides
 * methods to traverse the grammar rules.
 */
public abstract class EmftextmindmapSyntaxElement {
	
	private EmftextmindmapSyntaxElement[] children;
	private EmftextmindmapSyntaxElement parent;
	private mindmap.resource.emftextmindmap.grammar.EmftextmindmapCardinality cardinality;
	
	public EmftextmindmapSyntaxElement(mindmap.resource.emftextmindmap.grammar.EmftextmindmapCardinality cardinality, EmftextmindmapSyntaxElement[] children) {
		this.cardinality = cardinality;
		this.children = children;
		if (this.children != null) {
			for (EmftextmindmapSyntaxElement child : this.children) {
				child.setParent(this);
			}
		}
	}
	
	/**
	 * Sets the parent of this syntax element. This method must be invoked at most
	 * once.
	 */
	public void setParent(EmftextmindmapSyntaxElement parent) {
		assert this.parent == null;
		this.parent = parent;
	}
	
	/**
	 * Returns the parent of this syntax element. This parent is determined by the
	 * containment hierarchy in the CS model.
	 */
	public EmftextmindmapSyntaxElement getParent() {
		return parent;
	}
	
	public EmftextmindmapSyntaxElement[] getChildren() {
		if (children == null) {
			return new EmftextmindmapSyntaxElement[0];
		}
		return children;
	}
	
	public org.eclipse.emf.ecore.EClass getMetaclass() {
		return parent.getMetaclass();
	}
	
	public mindmap.resource.emftextmindmap.grammar.EmftextmindmapCardinality getCardinality() {
		return cardinality;
	}
	
}
