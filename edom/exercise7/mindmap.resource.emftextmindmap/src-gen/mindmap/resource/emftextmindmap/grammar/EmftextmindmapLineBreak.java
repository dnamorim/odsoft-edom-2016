/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.grammar;

public class EmftextmindmapLineBreak extends mindmap.resource.emftextmindmap.grammar.EmftextmindmapFormattingElement {
	
	private final int tabs;
	
	public EmftextmindmapLineBreak(mindmap.resource.emftextmindmap.grammar.EmftextmindmapCardinality cardinality, int tabs) {
		super(cardinality);
		this.tabs = tabs;
	}
	
	public int getTabs() {
		return tabs;
	}
	
	public String toString() {
		return "!" + getTabs();
	}
	
}
