/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.grammar;

public class EmftextmindmapChoice extends mindmap.resource.emftextmindmap.grammar.EmftextmindmapSyntaxElement {
	
	public EmftextmindmapChoice(mindmap.resource.emftextmindmap.grammar.EmftextmindmapCardinality cardinality, mindmap.resource.emftextmindmap.grammar.EmftextmindmapSyntaxElement... choices) {
		super(cardinality, choices);
	}
	
	public String toString() {
		return mindmap.resource.emftextmindmap.util.EmftextmindmapStringUtil.explode(getChildren(), "|");
	}
	
}
