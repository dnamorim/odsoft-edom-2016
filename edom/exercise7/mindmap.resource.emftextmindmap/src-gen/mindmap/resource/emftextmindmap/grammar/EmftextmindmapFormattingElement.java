/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.grammar;

public abstract class EmftextmindmapFormattingElement extends mindmap.resource.emftextmindmap.grammar.EmftextmindmapSyntaxElement {
	
	public EmftextmindmapFormattingElement(mindmap.resource.emftextmindmap.grammar.EmftextmindmapCardinality cardinality) {
		super(cardinality, null);
	}
	
}
