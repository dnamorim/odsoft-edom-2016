/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.grammar;

/**
 * A class to represent placeholders in a grammar.
 */
public class EmftextmindmapPlaceholder extends mindmap.resource.emftextmindmap.grammar.EmftextmindmapTerminal {
	
	private final String tokenName;
	
	public EmftextmindmapPlaceholder(org.eclipse.emf.ecore.EStructuralFeature feature, String tokenName, mindmap.resource.emftextmindmap.grammar.EmftextmindmapCardinality cardinality, int mandatoryOccurencesAfter) {
		super(feature, cardinality, mandatoryOccurencesAfter);
		this.tokenName = tokenName;
	}
	
	public String getTokenName() {
		return tokenName;
	}
	
}
