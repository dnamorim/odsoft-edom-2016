/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.grammar;

/**
 * A class to represent a rules in the grammar.
 */
public class EmftextmindmapRule extends mindmap.resource.emftextmindmap.grammar.EmftextmindmapSyntaxElement {
	
	private final org.eclipse.emf.ecore.EClass metaclass;
	
	public EmftextmindmapRule(org.eclipse.emf.ecore.EClass metaclass, mindmap.resource.emftextmindmap.grammar.EmftextmindmapChoice choice, mindmap.resource.emftextmindmap.grammar.EmftextmindmapCardinality cardinality) {
		super(cardinality, new mindmap.resource.emftextmindmap.grammar.EmftextmindmapSyntaxElement[] {choice});
		this.metaclass = metaclass;
	}
	
	public org.eclipse.emf.ecore.EClass getMetaclass() {
		return metaclass;
	}
	
	public mindmap.resource.emftextmindmap.grammar.EmftextmindmapChoice getDefinition() {
		return (mindmap.resource.emftextmindmap.grammar.EmftextmindmapChoice) getChildren()[0];
	}
	
}

