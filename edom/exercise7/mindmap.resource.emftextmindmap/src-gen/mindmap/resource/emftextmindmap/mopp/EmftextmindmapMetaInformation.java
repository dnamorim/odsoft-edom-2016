/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.mopp;

public class EmftextmindmapMetaInformation implements mindmap.resource.emftextmindmap.IEmftextmindmapMetaInformation {
	
	public String getSyntaxName() {
		return "emftextmindmap";
	}
	
	public String getURI() {
		return "http://org/eclipse/dsl/mindmap";
	}
	
	public mindmap.resource.emftextmindmap.IEmftextmindmapTextScanner createLexer() {
		return new mindmap.resource.emftextmindmap.mopp.EmftextmindmapAntlrScanner(new mindmap.resource.emftextmindmap.mopp.EmftextmindmapLexer());
	}
	
	public mindmap.resource.emftextmindmap.IEmftextmindmapTextParser createParser(java.io.InputStream inputStream, String encoding) {
		return new mindmap.resource.emftextmindmap.mopp.EmftextmindmapParser().createInstance(inputStream, encoding);
	}
	
	public mindmap.resource.emftextmindmap.IEmftextmindmapTextPrinter createPrinter(java.io.OutputStream outputStream, mindmap.resource.emftextmindmap.IEmftextmindmapTextResource resource) {
		return new mindmap.resource.emftextmindmap.mopp.EmftextmindmapPrinter2(outputStream, resource);
	}
	
	public org.eclipse.emf.ecore.EClass[] getClassesWithSyntax() {
		return new mindmap.resource.emftextmindmap.mopp.EmftextmindmapSyntaxCoverageInformationProvider().getClassesWithSyntax();
	}
	
	public org.eclipse.emf.ecore.EClass[] getStartSymbols() {
		return new mindmap.resource.emftextmindmap.mopp.EmftextmindmapSyntaxCoverageInformationProvider().getStartSymbols();
	}
	
	public mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolverSwitch getReferenceResolverSwitch() {
		return new mindmap.resource.emftextmindmap.mopp.EmftextmindmapReferenceResolverSwitch();
	}
	
	public mindmap.resource.emftextmindmap.IEmftextmindmapTokenResolverFactory getTokenResolverFactory() {
		return new mindmap.resource.emftextmindmap.mopp.EmftextmindmapTokenResolverFactory();
	}
	
	public String getPathToCSDefinition() {
		return "org.emftext.language.mindmap/metamodel/mindmap.cs";
	}
	
	public String[] getTokenNames() {
		return mindmap.resource.emftextmindmap.mopp.EmftextmindmapParser.tokenNames;
	}
	
	public mindmap.resource.emftextmindmap.IEmftextmindmapTokenStyle getDefaultTokenStyle(String tokenName) {
		return new mindmap.resource.emftextmindmap.mopp.EmftextmindmapTokenStyleInformationProvider().getDefaultTokenStyle(tokenName);
	}
	
	public java.util.Collection<mindmap.resource.emftextmindmap.IEmftextmindmapBracketPair> getBracketPairs() {
		return new mindmap.resource.emftextmindmap.mopp.EmftextmindmapBracketInformationProvider().getBracketPairs();
	}
	
	public org.eclipse.emf.ecore.EClass[] getFoldableClasses() {
		return new mindmap.resource.emftextmindmap.mopp.EmftextmindmapFoldingInformationProvider().getFoldableClasses();
	}
	
	public org.eclipse.emf.ecore.resource.Resource.Factory createResourceFactory() {
		return new mindmap.resource.emftextmindmap.mopp.EmftextmindmapResourceFactory();
	}
	
	public mindmap.resource.emftextmindmap.mopp.EmftextmindmapNewFileContentProvider getNewFileContentProvider() {
		return new mindmap.resource.emftextmindmap.mopp.EmftextmindmapNewFileContentProvider();
	}
	
	public void registerResourceFactory() {
		org.eclipse.emf.ecore.resource.Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(getSyntaxName(), new mindmap.resource.emftextmindmap.mopp.EmftextmindmapResourceFactory());
	}
	
	/**
	 * Returns the key of the option that can be used to register a preprocessor that
	 * is used as a pipe when loading resources. This key is language-specific. To
	 * register one preprocessor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getInputStreamPreprocessorProviderOptionKey() {
		return getSyntaxName() + "_" + "INPUT_STREAM_PREPROCESSOR_PROVIDER";
	}
	
	/**
	 * Returns the key of the option that can be used to register a post-processors
	 * that are invoked after loading resources. This key is language-specific. To
	 * register one post-processor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getResourcePostProcessorProviderOptionKey() {
		return getSyntaxName() + "_" + "RESOURCE_POSTPROCESSOR_PROVIDER";
	}
	
	public String getLaunchConfigurationType() {
		return "mindmap.resource.emftextmindmap.ui.launchConfigurationType";
	}
	
	public mindmap.resource.emftextmindmap.IEmftextmindmapNameProvider createNameProvider() {
		return new mindmap.resource.emftextmindmap.analysis.EmftextmindmapDefaultNameProvider();
	}
	
	public String[] getSyntaxHighlightableTokenNames() {
		mindmap.resource.emftextmindmap.mopp.EmftextmindmapAntlrTokenHelper tokenHelper = new mindmap.resource.emftextmindmap.mopp.EmftextmindmapAntlrTokenHelper();
		java.util.List<String> highlightableTokens = new java.util.ArrayList<String>();
		String[] parserTokenNames = getTokenNames();
		for (int i = 0; i < parserTokenNames.length; i++) {
			// If ANTLR is used we need to normalize the token names
			if (!tokenHelper.canBeUsedForSyntaxHighlighting(i)) {
				continue;
			}
			String tokenName = tokenHelper.getTokenName(parserTokenNames, i);
			if (tokenName == null) {
				continue;
			}
			highlightableTokens.add(tokenName);
		}
		highlightableTokens.add(mindmap.resource.emftextmindmap.mopp.EmftextmindmapTokenStyleInformationProvider.TASK_ITEM_TOKEN_NAME);
		return highlightableTokens.toArray(new String[highlightableTokens.size()]);
	}
	
}
