/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.mopp;

public class EmftextmindmapResourceFactory implements org.eclipse.emf.ecore.resource.Resource.Factory {
	
	public EmftextmindmapResourceFactory() {
		super();
	}
	
	public org.eclipse.emf.ecore.resource.Resource createResource(org.eclipse.emf.common.util.URI uri) {
		return new mindmap.resource.emftextmindmap.mopp.EmftextmindmapResource(uri);
	}
	
}
