/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.mopp;

/**
 * An Excpetion to represent invalid content types for parser instances.
 * 
 * @see
 * mindmap.resource.emftextmindmap.IEmftextmindmapOptions.RESOURCE_CONTENT_TYPE
 */
public class EmftextmindmapUnexpectedContentTypeException extends org.antlr.runtime3_4_0.RecognitionException {
	
	private static final long serialVersionUID = 4791359811519433999L;
	
	private Object contentType = null;
	
	public  EmftextmindmapUnexpectedContentTypeException(Object contentType) {
		this.contentType = contentType;
	}
	
	public Object getContentType() {
		return contentType;
	}
	
}
