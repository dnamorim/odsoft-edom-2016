/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.mopp;

public class EmftextmindmapProblem implements mindmap.resource.emftextmindmap.IEmftextmindmapProblem {
	
	private String message;
	private mindmap.resource.emftextmindmap.EmftextmindmapEProblemType type;
	private mindmap.resource.emftextmindmap.EmftextmindmapEProblemSeverity severity;
	private java.util.Collection<mindmap.resource.emftextmindmap.IEmftextmindmapQuickFix> quickFixes;
	
	public EmftextmindmapProblem(String message, mindmap.resource.emftextmindmap.EmftextmindmapEProblemType type, mindmap.resource.emftextmindmap.EmftextmindmapEProblemSeverity severity) {
		this(message, type, severity, java.util.Collections.<mindmap.resource.emftextmindmap.IEmftextmindmapQuickFix>emptySet());
	}
	
	public EmftextmindmapProblem(String message, mindmap.resource.emftextmindmap.EmftextmindmapEProblemType type, mindmap.resource.emftextmindmap.EmftextmindmapEProblemSeverity severity, mindmap.resource.emftextmindmap.IEmftextmindmapQuickFix quickFix) {
		this(message, type, severity, java.util.Collections.singleton(quickFix));
	}
	
	public EmftextmindmapProblem(String message, mindmap.resource.emftextmindmap.EmftextmindmapEProblemType type, mindmap.resource.emftextmindmap.EmftextmindmapEProblemSeverity severity, java.util.Collection<mindmap.resource.emftextmindmap.IEmftextmindmapQuickFix> quickFixes) {
		super();
		this.message = message;
		this.type = type;
		this.severity = severity;
		this.quickFixes = new java.util.LinkedHashSet<mindmap.resource.emftextmindmap.IEmftextmindmapQuickFix>();
		this.quickFixes.addAll(quickFixes);
	}
	
	public mindmap.resource.emftextmindmap.EmftextmindmapEProblemType getType() {
		return type;
	}
	
	public mindmap.resource.emftextmindmap.EmftextmindmapEProblemSeverity getSeverity() {
		return severity;
	}
	
	public String getMessage() {
		return message;
	}
	
	public java.util.Collection<mindmap.resource.emftextmindmap.IEmftextmindmapQuickFix> getQuickFixes() {
		return quickFixes;
	}
	
}
