/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.mopp;

/**
 * Abstract super class for all expected elements. Provides methods to add
 * followers.
 */
public abstract class EmftextmindmapAbstractExpectedElement implements mindmap.resource.emftextmindmap.IEmftextmindmapExpectedElement {
	
	private org.eclipse.emf.ecore.EClass ruleMetaclass;
	
	private java.util.Set<mindmap.resource.emftextmindmap.util.EmftextmindmapPair<mindmap.resource.emftextmindmap.IEmftextmindmapExpectedElement, mindmap.resource.emftextmindmap.mopp.EmftextmindmapContainedFeature[]>> followers = new java.util.LinkedHashSet<mindmap.resource.emftextmindmap.util.EmftextmindmapPair<mindmap.resource.emftextmindmap.IEmftextmindmapExpectedElement, mindmap.resource.emftextmindmap.mopp.EmftextmindmapContainedFeature[]>>();
	
	public EmftextmindmapAbstractExpectedElement(org.eclipse.emf.ecore.EClass ruleMetaclass) {
		super();
		this.ruleMetaclass = ruleMetaclass;
	}
	
	public org.eclipse.emf.ecore.EClass getRuleMetaclass() {
		return ruleMetaclass;
	}
	
	public void addFollower(mindmap.resource.emftextmindmap.IEmftextmindmapExpectedElement follower, mindmap.resource.emftextmindmap.mopp.EmftextmindmapContainedFeature[] path) {
		followers.add(new mindmap.resource.emftextmindmap.util.EmftextmindmapPair<mindmap.resource.emftextmindmap.IEmftextmindmapExpectedElement, mindmap.resource.emftextmindmap.mopp.EmftextmindmapContainedFeature[]>(follower, path));
	}
	
	public java.util.Collection<mindmap.resource.emftextmindmap.util.EmftextmindmapPair<mindmap.resource.emftextmindmap.IEmftextmindmapExpectedElement, mindmap.resource.emftextmindmap.mopp.EmftextmindmapContainedFeature[]>> getFollowers() {
		return followers;
	}
	
}
