/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.mopp;

/**
 * A representation for a range in a document where a boolean attribute is
 * expected.
 */
public class EmftextmindmapExpectedBooleanTerminal extends mindmap.resource.emftextmindmap.mopp.EmftextmindmapAbstractExpectedElement {
	
	private mindmap.resource.emftextmindmap.grammar.EmftextmindmapBooleanTerminal booleanTerminal;
	
	public EmftextmindmapExpectedBooleanTerminal(mindmap.resource.emftextmindmap.grammar.EmftextmindmapBooleanTerminal booleanTerminal) {
		super(booleanTerminal.getMetaclass());
		this.booleanTerminal = booleanTerminal;
	}
	
	public mindmap.resource.emftextmindmap.grammar.EmftextmindmapBooleanTerminal getBooleanTerminal() {
		return booleanTerminal;
	}
	
	/**
	 * Returns the expected boolean terminal.
	 */
	public mindmap.resource.emftextmindmap.grammar.EmftextmindmapSyntaxElement getSymtaxElement() {
		return booleanTerminal;
	}
	
	private org.eclipse.emf.ecore.EStructuralFeature getFeature() {
		return booleanTerminal.getFeature();
	}
	
	public String toString() {
		return "EFeature " + getFeature().getEContainingClass().getName() + "." + getFeature().getName();
	}
	
	public boolean equals(Object o) {
		if (o instanceof EmftextmindmapExpectedBooleanTerminal) {
			return getFeature().equals(((EmftextmindmapExpectedBooleanTerminal) o).getFeature());
		}
		return false;
	}
	
	@Override	
	public int hashCode() {
		return getFeature().hashCode();
	}
	
	public java.util.Set<String> getTokenNames() {
		// BooleanTerminals are associated with two or one token(s)
		java.util.Set<String> tokenNames = new java.util.LinkedHashSet<String>(2);
		String trueLiteral = booleanTerminal.getTrueLiteral();
		if (!"".equals(trueLiteral)) {
			tokenNames.add("'" + trueLiteral + "'");
		}
		String falseLiteral = booleanTerminal.getFalseLiteral();
		if (!"".equals(falseLiteral)) {
			tokenNames.add("'" + falseLiteral + "'");
		}
		return tokenNames;
	}
	
}
