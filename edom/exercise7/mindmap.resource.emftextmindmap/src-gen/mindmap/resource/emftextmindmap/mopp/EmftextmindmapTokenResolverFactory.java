/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.mopp;

/**
 * The EmftextmindmapTokenResolverFactory class provides access to all generated
 * token resolvers. By giving the name of a defined token, the corresponding
 * resolve can be obtained. Despite the fact that this class is called
 * TokenResolverFactory is does NOT create new token resolvers whenever a client
 * calls methods to obtain a resolver. Rather, this class maintains a map of all
 * resolvers and creates each resolver at most once.
 */
public class EmftextmindmapTokenResolverFactory implements mindmap.resource.emftextmindmap.IEmftextmindmapTokenResolverFactory {
	
	private java.util.Map<String, mindmap.resource.emftextmindmap.IEmftextmindmapTokenResolver> tokenName2TokenResolver;
	private java.util.Map<String, mindmap.resource.emftextmindmap.IEmftextmindmapTokenResolver> featureName2CollectInTokenResolver;
	private static mindmap.resource.emftextmindmap.IEmftextmindmapTokenResolver defaultResolver = new mindmap.resource.emftextmindmap.analysis.EmftextmindmapDefaultTokenResolver();
	
	public EmftextmindmapTokenResolverFactory() {
		tokenName2TokenResolver = new java.util.LinkedHashMap<String, mindmap.resource.emftextmindmap.IEmftextmindmapTokenResolver>();
		featureName2CollectInTokenResolver = new java.util.LinkedHashMap<String, mindmap.resource.emftextmindmap.IEmftextmindmapTokenResolver>();
		registerTokenResolver("TEXT", new mindmap.resource.emftextmindmap.analysis.EmftextmindmapTEXTTokenResolver());
		registerTokenResolver("QUOTED_34_34", new mindmap.resource.emftextmindmap.analysis.EmftextmindmapQUOTED_34_34TokenResolver());
	}
	
	public mindmap.resource.emftextmindmap.IEmftextmindmapTokenResolver createTokenResolver(String tokenName) {
		return internalCreateResolver(tokenName2TokenResolver, tokenName);
	}
	
	public mindmap.resource.emftextmindmap.IEmftextmindmapTokenResolver createCollectInTokenResolver(String featureName) {
		return internalCreateResolver(featureName2CollectInTokenResolver, featureName);
	}
	
	protected boolean registerTokenResolver(String tokenName, mindmap.resource.emftextmindmap.IEmftextmindmapTokenResolver resolver){
		return internalRegisterTokenResolver(tokenName2TokenResolver, tokenName, resolver);
	}
	
	protected boolean registerCollectInTokenResolver(String featureName, mindmap.resource.emftextmindmap.IEmftextmindmapTokenResolver resolver){
		return internalRegisterTokenResolver(featureName2CollectInTokenResolver, featureName, resolver);
	}
	
	protected mindmap.resource.emftextmindmap.IEmftextmindmapTokenResolver deRegisterTokenResolver(String tokenName){
		return tokenName2TokenResolver.remove(tokenName);
	}
	
	private mindmap.resource.emftextmindmap.IEmftextmindmapTokenResolver internalCreateResolver(java.util.Map<String, mindmap.resource.emftextmindmap.IEmftextmindmapTokenResolver> resolverMap, String key) {
		if (resolverMap.containsKey(key)){
			return resolverMap.get(key);
		} else {
			return defaultResolver;
		}
	}
	
	private boolean internalRegisterTokenResolver(java.util.Map<String, mindmap.resource.emftextmindmap.IEmftextmindmapTokenResolver> resolverMap, String key, mindmap.resource.emftextmindmap.IEmftextmindmapTokenResolver resolver) {
		if (!resolverMap.containsKey(key)) {
			resolverMap.put(key,resolver);
			return true;
		}
		return false;
	}
	
}
