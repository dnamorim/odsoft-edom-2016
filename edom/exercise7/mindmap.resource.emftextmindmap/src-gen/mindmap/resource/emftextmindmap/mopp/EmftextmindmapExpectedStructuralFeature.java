/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.mopp;

/**
 * A representation for a range in a document where a structural feature (e.g., a
 * reference) is expected.
 */
public class EmftextmindmapExpectedStructuralFeature extends mindmap.resource.emftextmindmap.mopp.EmftextmindmapAbstractExpectedElement {
	
	private mindmap.resource.emftextmindmap.grammar.EmftextmindmapPlaceholder placeholder;
	
	public EmftextmindmapExpectedStructuralFeature(mindmap.resource.emftextmindmap.grammar.EmftextmindmapPlaceholder placeholder) {
		super(placeholder.getMetaclass());
		this.placeholder = placeholder;
	}
	
	public org.eclipse.emf.ecore.EStructuralFeature getFeature() {
		return placeholder.getFeature();
	}
	
	/**
	 * Returns the expected placeholder.
	 */
	public mindmap.resource.emftextmindmap.grammar.EmftextmindmapSyntaxElement getSymtaxElement() {
		return placeholder;
	}
	
	public String getTokenName() {
		return placeholder.getTokenName();
	}
	
	public java.util.Set<String> getTokenNames() {
		return java.util.Collections.singleton(getTokenName());
	}
	
	public String toString() {
		return "EFeature " + getFeature().getEContainingClass().getName() + "." + getFeature().getName();
	}
	
	public boolean equals(Object o) {
		if (o instanceof EmftextmindmapExpectedStructuralFeature) {
			return getFeature().equals(((EmftextmindmapExpectedStructuralFeature) o).getFeature());
		}
		return false;
	}
	@Override	
	public int hashCode() {
		return getFeature().hashCode();
	}
	
}
