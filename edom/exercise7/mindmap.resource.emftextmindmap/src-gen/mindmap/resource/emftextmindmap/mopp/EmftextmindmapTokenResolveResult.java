/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.mopp;

/**
 * A basic implementation of the ITokenResolveResult interface.
 */
public class EmftextmindmapTokenResolveResult implements mindmap.resource.emftextmindmap.IEmftextmindmapTokenResolveResult {
	
	private String errorMessage;
	private Object resolvedToken;
	
	public EmftextmindmapTokenResolveResult() {
		super();
		clear();
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public Object getResolvedToken() {
		return resolvedToken;
	}
	
	public void setErrorMessage(String message) {
		errorMessage = message;
	}
	
	public void setResolvedToken(Object resolvedToken) {
		this.resolvedToken = resolvedToken;
	}
	
	public void clear() {
		errorMessage = "Can't resolve token.";
		resolvedToken = null;
	}
	
}
