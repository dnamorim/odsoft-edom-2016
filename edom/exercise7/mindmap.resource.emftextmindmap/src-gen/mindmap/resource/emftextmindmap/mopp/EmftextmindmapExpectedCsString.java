/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.mopp;

/**
 * A representation for a range in a document where a keyword (i.e., a static
 * string) is expected.
 */
public class EmftextmindmapExpectedCsString extends mindmap.resource.emftextmindmap.mopp.EmftextmindmapAbstractExpectedElement {
	
	private mindmap.resource.emftextmindmap.grammar.EmftextmindmapKeyword keyword;
	
	public EmftextmindmapExpectedCsString(mindmap.resource.emftextmindmap.grammar.EmftextmindmapKeyword keyword) {
		super(keyword.getMetaclass());
		this.keyword = keyword;
	}
	
	public String getValue() {
		return keyword.getValue();
	}
	
	/**
	 * Returns the expected keyword.
	 */
	public mindmap.resource.emftextmindmap.grammar.EmftextmindmapSyntaxElement getSymtaxElement() {
		return keyword;
	}
	
	public java.util.Set<String> getTokenNames() {
		return java.util.Collections.singleton("'" + getValue() + "'");
	}
	
	public String toString() {
		return "CsString \"" + getValue() + "\"";
	}
	
	public boolean equals(Object o) {
		if (o instanceof EmftextmindmapExpectedCsString) {
			return getValue().equals(((EmftextmindmapExpectedCsString) o).getValue());
		}
		return false;
	}
	
	@Override	
	public int hashCode() {
		return getValue().hashCode();
	}
	
}
