/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.mopp;

public class EmftextmindmapAntlrScanner implements mindmap.resource.emftextmindmap.IEmftextmindmapTextScanner {
	
	private org.antlr.runtime3_4_0.Lexer antlrLexer;
	
	public EmftextmindmapAntlrScanner(org.antlr.runtime3_4_0.Lexer antlrLexer) {
		this.antlrLexer = antlrLexer;
	}
	
	public mindmap.resource.emftextmindmap.IEmftextmindmapTextToken getNextToken() {
		if (antlrLexer.getCharStream() == null) {
			return null;
		}
		final org.antlr.runtime3_4_0.Token current = antlrLexer.nextToken();
		if (current == null || current.getType() < 0) {
			return null;
		}
		mindmap.resource.emftextmindmap.IEmftextmindmapTextToken result = new mindmap.resource.emftextmindmap.mopp.EmftextmindmapANTLRTextToken(current);
		return result;
	}
	
	public void setText(String text) {
		antlrLexer.setCharStream(new org.antlr.runtime3_4_0.ANTLRStringStream(text));
	}
	
}
