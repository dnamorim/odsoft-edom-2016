/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.mopp;

/**
 * A basic implementation of the
 * mindmap.resource.emftextmindmap.IEmftextmindmapElementMapping interface.
 * 
 * @param <ReferenceType> the type of the reference that can be mapped to
 */
public class EmftextmindmapElementMapping<ReferenceType> implements mindmap.resource.emftextmindmap.IEmftextmindmapElementMapping<ReferenceType> {
	
	private final ReferenceType target;
	private String identifier;
	private String warning;
	
	public EmftextmindmapElementMapping(String identifier, ReferenceType target, String warning) {
		super();
		this.target = target;
		this.identifier = identifier;
		this.warning = warning;
	}
	
	public ReferenceType getTargetElement() {
		return target;
	}
	
	public String getIdentifier() {
		return identifier;
	}
	
	public String getWarning() {
		return warning;
	}
	
}
