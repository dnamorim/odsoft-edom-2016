/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.mopp;

public class EmftextmindmapSyntaxElementDecorator {
	
	/**
	 * the syntax element to be decorated
	 */
	private mindmap.resource.emftextmindmap.grammar.EmftextmindmapSyntaxElement decoratedElement;
	
	/**
	 * an array of child decorators (one decorator per child of the decorated syntax
	 * element
	 */
	private EmftextmindmapSyntaxElementDecorator[] childDecorators;
	
	/**
	 * a list of the indices that must be printed
	 */
	private java.util.List<Integer> indicesToPrint = new java.util.ArrayList<Integer>();
	
	public EmftextmindmapSyntaxElementDecorator(mindmap.resource.emftextmindmap.grammar.EmftextmindmapSyntaxElement decoratedElement, EmftextmindmapSyntaxElementDecorator[] childDecorators) {
		super();
		this.decoratedElement = decoratedElement;
		this.childDecorators = childDecorators;
	}
	
	public void addIndexToPrint(Integer index) {
		indicesToPrint.add(index);
	}
	
	public mindmap.resource.emftextmindmap.grammar.EmftextmindmapSyntaxElement getDecoratedElement() {
		return decoratedElement;
	}
	
	public EmftextmindmapSyntaxElementDecorator[] getChildDecorators() {
		return childDecorators;
	}
	
	public Integer getNextIndexToPrint() {
		if (indicesToPrint.size() == 0) {
			return null;
		}
		return indicesToPrint.remove(0);
	}
	
	public String toString() {
		return "" + getDecoratedElement();
	}
	
}
