/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.mopp;

public class EmftextmindmapParseResult implements mindmap.resource.emftextmindmap.IEmftextmindmapParseResult {
	
	private org.eclipse.emf.ecore.EObject root;
	private java.util.Collection<mindmap.resource.emftextmindmap.IEmftextmindmapCommand<mindmap.resource.emftextmindmap.IEmftextmindmapTextResource>> commands = new java.util.ArrayList<mindmap.resource.emftextmindmap.IEmftextmindmapCommand<mindmap.resource.emftextmindmap.IEmftextmindmapTextResource>>();
	
	public EmftextmindmapParseResult() {
		super();
	}
	
	public void setRoot(org.eclipse.emf.ecore.EObject root) {
		this.root = root;
	}
	
	public org.eclipse.emf.ecore.EObject getRoot() {
		return root;
	}
	
	public java.util.Collection<mindmap.resource.emftextmindmap.IEmftextmindmapCommand<mindmap.resource.emftextmindmap.IEmftextmindmapTextResource>> getPostParseCommands() {
		return commands;
	}
	
}
