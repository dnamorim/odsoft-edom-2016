/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.mopp;

/**
 * A factory for ContextDependentURIFragments. Given a feasible reference
 * resolver, this factory returns a matching fragment that used the resolver to
 * resolver proxy objects.
 * 
 * @param <ContainerType> the type of the class containing the reference to be
 * resolved
 * @param <ReferenceType> the type of the reference to be resolved
 */
public class EmftextmindmapContextDependentURIFragmentFactory<ContainerType extends org.eclipse.emf.ecore.EObject, ReferenceType extends org.eclipse.emf.ecore.EObject>  implements mindmap.resource.emftextmindmap.IEmftextmindmapContextDependentURIFragmentFactory<ContainerType, ReferenceType> {
	
	private final mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolver<ContainerType, ReferenceType> resolver;
	
	public EmftextmindmapContextDependentURIFragmentFactory(mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolver<ContainerType, ReferenceType> resolver) {
		this.resolver = resolver;
	}
	
	public mindmap.resource.emftextmindmap.IEmftextmindmapContextDependentURIFragment<?> create(String identifier, ContainerType container, org.eclipse.emf.ecore.EReference reference, int positionInReference, org.eclipse.emf.ecore.EObject proxy) {
		
		return new mindmap.resource.emftextmindmap.mopp.EmftextmindmapContextDependentURIFragment<ContainerType, ReferenceType>(identifier, container, reference, positionInReference, proxy) {
			public mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolver<ContainerType, ReferenceType> getResolver() {
				return resolver;
			}
		};
	}
}
