/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.mopp;

public class EmftextmindmapReferenceResolverSwitch implements mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolverSwitch {
	
	/**
	 * This map stores a copy of the options the were set for loading the resource.
	 */
	private java.util.Map<Object, Object> options;
	
	protected mindmap.resource.emftextmindmap.analysis.TopicSubtopicsReferenceResolver topicSubtopicsReferenceResolver = new mindmap.resource.emftextmindmap.analysis.TopicSubtopicsReferenceResolver();
	protected mindmap.resource.emftextmindmap.analysis.TopicParentReferenceResolver topicParentReferenceResolver = new mindmap.resource.emftextmindmap.analysis.TopicParentReferenceResolver();
	protected mindmap.resource.emftextmindmap.analysis.RelationshipSourceReferenceResolver relationshipSourceReferenceResolver = new mindmap.resource.emftextmindmap.analysis.RelationshipSourceReferenceResolver();
	protected mindmap.resource.emftextmindmap.analysis.RelationshipTargetReferenceResolver relationshipTargetReferenceResolver = new mindmap.resource.emftextmindmap.analysis.RelationshipTargetReferenceResolver();
	
	public mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolver<mindmap.Topic, mindmap.Topic> getTopicSubtopicsReferenceResolver() {
		return getResolverChain(mindmap.MindmapPackage.eINSTANCE.getTopic_Subtopics(), topicSubtopicsReferenceResolver);
	}
	
	public mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolver<mindmap.Topic, mindmap.Topic> getTopicParentReferenceResolver() {
		return getResolverChain(mindmap.MindmapPackage.eINSTANCE.getTopic_Parent(), topicParentReferenceResolver);
	}
	
	public mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolver<mindmap.Relationship, mindmap.Topic> getRelationshipSourceReferenceResolver() {
		return getResolverChain(mindmap.MindmapPackage.eINSTANCE.getRelationship_Source(), relationshipSourceReferenceResolver);
	}
	
	public mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolver<mindmap.Relationship, mindmap.Topic> getRelationshipTargetReferenceResolver() {
		return getResolverChain(mindmap.MindmapPackage.eINSTANCE.getRelationship_Target(), relationshipTargetReferenceResolver);
	}
	
	public void setOptions(java.util.Map<?, ?> options) {
		if (options != null) {
			this.options = new java.util.LinkedHashMap<Object, Object>();
			this.options.putAll(options);
		}
		topicSubtopicsReferenceResolver.setOptions(options);
		topicParentReferenceResolver.setOptions(options);
		relationshipSourceReferenceResolver.setOptions(options);
		relationshipTargetReferenceResolver.setOptions(options);
	}
	
	public void resolveFuzzy(String identifier, org.eclipse.emf.ecore.EObject container, org.eclipse.emf.ecore.EReference reference, int position, mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolveResult<org.eclipse.emf.ecore.EObject> result) {
		if (container == null) {
			return;
		}
		if (mindmap.MindmapPackage.eINSTANCE.getTopic().isInstance(container)) {
			EmftextmindmapFuzzyResolveResult<mindmap.Topic> frr = new EmftextmindmapFuzzyResolveResult<mindmap.Topic>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("subtopics")) {
				topicSubtopicsReferenceResolver.resolve(identifier, (mindmap.Topic) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (mindmap.MindmapPackage.eINSTANCE.getTopic().isInstance(container)) {
			EmftextmindmapFuzzyResolveResult<mindmap.Topic> frr = new EmftextmindmapFuzzyResolveResult<mindmap.Topic>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("parent")) {
				topicParentReferenceResolver.resolve(identifier, (mindmap.Topic) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (mindmap.MindmapPackage.eINSTANCE.getRelationship().isInstance(container)) {
			EmftextmindmapFuzzyResolveResult<mindmap.Topic> frr = new EmftextmindmapFuzzyResolveResult<mindmap.Topic>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("source")) {
				relationshipSourceReferenceResolver.resolve(identifier, (mindmap.Relationship) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (mindmap.MindmapPackage.eINSTANCE.getRelationship().isInstance(container)) {
			EmftextmindmapFuzzyResolveResult<mindmap.Topic> frr = new EmftextmindmapFuzzyResolveResult<mindmap.Topic>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("target")) {
				relationshipTargetReferenceResolver.resolve(identifier, (mindmap.Relationship) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
	}
	
	public mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolver<? extends org.eclipse.emf.ecore.EObject, ? extends org.eclipse.emf.ecore.EObject> getResolver(org.eclipse.emf.ecore.EStructuralFeature reference) {
		if (reference == mindmap.MindmapPackage.eINSTANCE.getTopic_Subtopics()) {
			return getResolverChain(reference, topicSubtopicsReferenceResolver);
		}
		if (reference == mindmap.MindmapPackage.eINSTANCE.getTopic_Parent()) {
			return getResolverChain(reference, topicParentReferenceResolver);
		}
		if (reference == mindmap.MindmapPackage.eINSTANCE.getRelationship_Source()) {
			return getResolverChain(reference, relationshipSourceReferenceResolver);
		}
		if (reference == mindmap.MindmapPackage.eINSTANCE.getRelationship_Target()) {
			return getResolverChain(reference, relationshipTargetReferenceResolver);
		}
		return null;
	}
	
	@SuppressWarnings({"rawtypes", "unchecked"})	
	public <ContainerType extends org.eclipse.emf.ecore.EObject, ReferenceType extends org.eclipse.emf.ecore.EObject> mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolver<ContainerType, ReferenceType> getResolverChain(org.eclipse.emf.ecore.EStructuralFeature reference, mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolver<ContainerType, ReferenceType> originalResolver) {
		if (options == null) {
			return originalResolver;
		}
		Object value = options.get(mindmap.resource.emftextmindmap.IEmftextmindmapOptions.ADDITIONAL_REFERENCE_RESOLVERS);
		if (value == null) {
			return originalResolver;
		}
		if (!(value instanceof java.util.Map)) {
			// send this to the error log
			new mindmap.resource.emftextmindmap.util.EmftextmindmapRuntimeUtil().logWarning("Found value with invalid type for option " + mindmap.resource.emftextmindmap.IEmftextmindmapOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + java.util.Map.class.getName() + ", but was " + value.getClass().getName() + ")", null);
			return originalResolver;
		}
		java.util.Map<?,?> resolverMap = (java.util.Map<?,?>) value;
		Object resolverValue = resolverMap.get(reference);
		if (resolverValue == null) {
			return originalResolver;
		}
		if (resolverValue instanceof mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolver) {
			mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolver replacingResolver = (mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolver) resolverValue;
			if (replacingResolver instanceof mindmap.resource.emftextmindmap.IEmftextmindmapDelegatingReferenceResolver) {
				// pass original resolver to the replacing one
				((mindmap.resource.emftextmindmap.IEmftextmindmapDelegatingReferenceResolver) replacingResolver).setDelegate(originalResolver);
			}
			return replacingResolver;
		} else if (resolverValue instanceof java.util.Collection) {
			java.util.Collection replacingResolvers = (java.util.Collection) resolverValue;
			mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolver replacingResolver = originalResolver;
			for (Object next : replacingResolvers) {
				if (next instanceof mindmap.resource.emftextmindmap.IEmftextmindmapReferenceCache) {
					mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolver nextResolver = (mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolver) next;
					if (nextResolver instanceof mindmap.resource.emftextmindmap.IEmftextmindmapDelegatingReferenceResolver) {
						// pass original resolver to the replacing one
						((mindmap.resource.emftextmindmap.IEmftextmindmapDelegatingReferenceResolver) nextResolver).setDelegate(replacingResolver);
					}
					replacingResolver = nextResolver;
				} else {
					// The collection contains a non-resolver. Send a warning to the error log.
					new mindmap.resource.emftextmindmap.util.EmftextmindmapRuntimeUtil().logWarning("Found value with invalid type in value map for option " + mindmap.resource.emftextmindmap.IEmftextmindmapOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + mindmap.resource.emftextmindmap.IEmftextmindmapDelegatingReferenceResolver.class.getName() + ", but was " + next.getClass().getName() + ")", null);
				}
			}
			return replacingResolver;
		} else {
			// The value for the option ADDITIONAL_REFERENCE_RESOLVERS has an unknown type.
			new mindmap.resource.emftextmindmap.util.EmftextmindmapRuntimeUtil().logWarning("Found value with invalid type in value map for option " + mindmap.resource.emftextmindmap.IEmftextmindmapOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + mindmap.resource.emftextmindmap.IEmftextmindmapDelegatingReferenceResolver.class.getName() + ", but was " + resolverValue.getClass().getName() + ")", null);
			return originalResolver;
		}
	}
	
}
