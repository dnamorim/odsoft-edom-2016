/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.mopp;

/**
 * A basic implementation of the
 * mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolveResult interface
 * that collects mappings in a list.
 * 
 * @param <ReferenceType> the type of the references that can be contained in this
 * result
 */
public class EmftextmindmapReferenceResolveResult<ReferenceType> implements mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolveResult<ReferenceType> {
	
	private java.util.Collection<mindmap.resource.emftextmindmap.IEmftextmindmapReferenceMapping<ReferenceType>> mappings;
	private String errorMessage;
	private boolean resolveFuzzy;
	private java.util.Set<mindmap.resource.emftextmindmap.IEmftextmindmapQuickFix> quickFixes;
	
	public EmftextmindmapReferenceResolveResult(boolean resolveFuzzy) {
		super();
		this.resolveFuzzy = resolveFuzzy;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public java.util.Collection<mindmap.resource.emftextmindmap.IEmftextmindmapQuickFix> getQuickFixes() {
		if (quickFixes == null) {
			quickFixes = new java.util.LinkedHashSet<mindmap.resource.emftextmindmap.IEmftextmindmapQuickFix>();
		}
		return java.util.Collections.unmodifiableSet(quickFixes);
	}
	
	public void addQuickFix(mindmap.resource.emftextmindmap.IEmftextmindmapQuickFix quickFix) {
		if (quickFixes == null) {
			quickFixes = new java.util.LinkedHashSet<mindmap.resource.emftextmindmap.IEmftextmindmapQuickFix>();
		}
		quickFixes.add(quickFix);
	}
	
	public java.util.Collection<mindmap.resource.emftextmindmap.IEmftextmindmapReferenceMapping<ReferenceType>> getMappings() {
		return mappings;
	}
	
	public boolean wasResolved() {
		return mappings != null;
	}
	
	public boolean wasResolvedMultiple() {
		return mappings != null && mappings.size() > 1;
	}
	
	public boolean wasResolvedUniquely() {
		return mappings != null && mappings.size() == 1;
	}
	
	public void setErrorMessage(String message) {
		errorMessage = message;
	}
	
	public void addMapping(String identifier, ReferenceType target) {
		if (!resolveFuzzy && target == null) {
			throw new IllegalArgumentException("Mapping references to null is only allowed for fuzzy resolution.");
		}
		addMapping(identifier, target, null);
	}
	
	public void addMapping(String identifier, ReferenceType target, String warning) {
		if (mappings == null) {
			mappings = new java.util.ArrayList<mindmap.resource.emftextmindmap.IEmftextmindmapReferenceMapping<ReferenceType>>(1);
		}
		mappings.add(new mindmap.resource.emftextmindmap.mopp.EmftextmindmapElementMapping<ReferenceType>(identifier, target, warning));
		errorMessage = null;
	}
	
	public void addMapping(String identifier, org.eclipse.emf.common.util.URI uri) {
		addMapping(identifier, uri, null);
	}
	
	public void addMapping(String identifier, org.eclipse.emf.common.util.URI uri, String warning) {
		if (mappings == null) {
			mappings = new java.util.ArrayList<mindmap.resource.emftextmindmap.IEmftextmindmapReferenceMapping<ReferenceType>>(1);
		}
		mappings.add(new mindmap.resource.emftextmindmap.mopp.EmftextmindmapURIMapping<ReferenceType>(identifier, uri, warning));
	}
}
