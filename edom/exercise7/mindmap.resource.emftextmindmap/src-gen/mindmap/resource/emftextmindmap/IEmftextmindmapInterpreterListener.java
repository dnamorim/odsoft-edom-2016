/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap;

public interface IEmftextmindmapInterpreterListener {
	
	public void handleInterpreteObject(org.eclipse.emf.ecore.EObject element);
}
