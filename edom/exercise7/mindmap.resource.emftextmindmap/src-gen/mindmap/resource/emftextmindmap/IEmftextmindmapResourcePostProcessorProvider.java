/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap;

/**
 * Implementors of this interface can provide a post-processor for text resources.
 */
public interface IEmftextmindmapResourcePostProcessorProvider {
	
	/**
	 * Returns the processor that shall be called after text resource are successfully
	 * parsed.
	 */
	public mindmap.resource.emftextmindmap.IEmftextmindmapResourcePostProcessor getResourcePostProcessor();
	
}
