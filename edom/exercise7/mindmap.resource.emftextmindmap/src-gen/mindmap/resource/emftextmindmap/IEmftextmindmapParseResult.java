/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap;

/**
 * An interface used to access the result of parsing a document.
 */
public interface IEmftextmindmapParseResult {
	
	/**
	 * Returns the root object of the document.
	 */
	public org.eclipse.emf.ecore.EObject getRoot();
	
	/**
	 * Returns a list of commands that must be executed after parsing the document.
	 */
	public java.util.Collection<mindmap.resource.emftextmindmap.IEmftextmindmapCommand<mindmap.resource.emftextmindmap.IEmftextmindmapTextResource>> getPostParseCommands();
	
}
