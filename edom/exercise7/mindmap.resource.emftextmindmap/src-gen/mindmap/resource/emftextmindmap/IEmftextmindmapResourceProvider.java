/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap;

/**
 * Implementors of this interface provide an EMF resource.
 */
public interface IEmftextmindmapResourceProvider {
	
	/**
	 * Returns the resource.
	 */
	public mindmap.resource.emftextmindmap.IEmftextmindmapTextResource getResource();
	
}
