/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap;

public enum EmftextmindmapEProblemSeverity {
	WARNING, ERROR;
}
