/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap;

/**
 * A delegating reference resolver is an extension of a normal reference resolver
 * that can be configured with another resolver that it may delegate method calls
 * to. This interface can be implemented by additional resolvers to customize
 * resolving using the load option ADDITIONAL_REFERENCE_RESOLVERS.
 * 
 * @see mindmap.resource.emftextmindmap.IEmftextmindmapOptions
 */
public interface IEmftextmindmapDelegatingReferenceResolver<ContainerType extends org.eclipse.emf.ecore.EObject, ReferenceType extends org.eclipse.emf.ecore.EObject> extends mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolver<ContainerType, ReferenceType> {
	
	/**
	 * Sets the delegate for this resolver.
	 */
	public void setDelegate(mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolver<ContainerType, ReferenceType> delegate);
	
}
