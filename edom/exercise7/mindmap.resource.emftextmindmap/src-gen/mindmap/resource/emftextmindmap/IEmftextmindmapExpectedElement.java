/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap;

/**
 * An element that is expected at a given position in a resource stream.
 */
public interface IEmftextmindmapExpectedElement {
	
	/**
	 * Returns the names of all tokens that are expected at the given position.
	 */
	public java.util.Set<String> getTokenNames();
	
	/**
	 * Returns the metaclass of the rule that contains the expected element.
	 */
	public org.eclipse.emf.ecore.EClass getRuleMetaclass();
	
	/**
	 * Returns the syntax element that is expected.
	 */
	public mindmap.resource.emftextmindmap.grammar.EmftextmindmapSyntaxElement getSymtaxElement();
	
	/**
	 * Adds an element that is a valid follower for this element.
	 */
	public void addFollower(mindmap.resource.emftextmindmap.IEmftextmindmapExpectedElement follower, mindmap.resource.emftextmindmap.mopp.EmftextmindmapContainedFeature[] path);
	
	/**
	 * Returns all valid followers for this element. Each follower is represented by a
	 * pair of an expected elements and the containment trace that leads from the
	 * current element to the follower.
	 */
	public java.util.Collection<mindmap.resource.emftextmindmap.util.EmftextmindmapPair<mindmap.resource.emftextmindmap.IEmftextmindmapExpectedElement, mindmap.resource.emftextmindmap.mopp.EmftextmindmapContainedFeature[]>> getFollowers();
	
}
