/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.util;

/**
 * Class EmftextmindmapTextResourceUtil can be used to perform common tasks on
 * text resources, such as loading and saving resources, as well as, checking them
 * for errors. This class is deprecated and has been replaced by
 * mindmap.resource.emftextmindmap.util.EmftextmindmapResourceUtil.
 */
public class EmftextmindmapTextResourceUtil {
	
	/**
	 * Use
	 * mindmap.resource.emftextmindmap.util.EmftextmindmapResourceUtil.getResource()
	 * instead.
	 */
	@Deprecated	
	public static mindmap.resource.emftextmindmap.mopp.EmftextmindmapResource getResource(org.eclipse.core.resources.IFile file) {
		return new mindmap.resource.emftextmindmap.util.EmftextmindmapEclipseProxy().getResource(file);
	}
	
	/**
	 * Use
	 * mindmap.resource.emftextmindmap.util.EmftextmindmapResourceUtil.getResource()
	 * instead.
	 */
	@Deprecated	
	public static mindmap.resource.emftextmindmap.mopp.EmftextmindmapResource getResource(java.io.File file, java.util.Map<?,?> options) {
		return mindmap.resource.emftextmindmap.util.EmftextmindmapResourceUtil.getResource(file, options);
	}
	
	/**
	 * Use
	 * mindmap.resource.emftextmindmap.util.EmftextmindmapResourceUtil.getResource()
	 * instead.
	 */
	@Deprecated	
	public static mindmap.resource.emftextmindmap.mopp.EmftextmindmapResource getResource(org.eclipse.emf.common.util.URI uri) {
		return mindmap.resource.emftextmindmap.util.EmftextmindmapResourceUtil.getResource(uri);
	}
	
	/**
	 * Use
	 * mindmap.resource.emftextmindmap.util.EmftextmindmapResourceUtil.getResource()
	 * instead.
	 */
	@Deprecated	
	public static mindmap.resource.emftextmindmap.mopp.EmftextmindmapResource getResource(org.eclipse.emf.common.util.URI uri, java.util.Map<?,?> options) {
		return mindmap.resource.emftextmindmap.util.EmftextmindmapResourceUtil.getResource(uri, options);
	}
	
}
