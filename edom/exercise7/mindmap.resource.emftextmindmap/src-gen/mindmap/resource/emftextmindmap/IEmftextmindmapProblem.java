/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap;

public interface IEmftextmindmapProblem {
	public String getMessage();
	public mindmap.resource.emftextmindmap.EmftextmindmapEProblemSeverity getSeverity();
	public mindmap.resource.emftextmindmap.EmftextmindmapEProblemType getType();
	public java.util.Collection<mindmap.resource.emftextmindmap.IEmftextmindmapQuickFix> getQuickFixes();
}
