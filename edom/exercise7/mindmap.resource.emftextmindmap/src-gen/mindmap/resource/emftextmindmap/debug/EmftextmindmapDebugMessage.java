/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.debug;

/**
 * DebugMessages are exchanged between the debug server (the Eclipse debug
 * framework) and the debug client (a running process or interpreter). To exchange
 * messages they are serialized and sent over sockets.
 */
public class EmftextmindmapDebugMessage {
	
	private static final char DELIMITER = ':';
	private mindmap.resource.emftextmindmap.debug.EEmftextmindmapDebugMessageTypes messageType;
	private String[] arguments;
	
	public EmftextmindmapDebugMessage(mindmap.resource.emftextmindmap.debug.EEmftextmindmapDebugMessageTypes messageType, String[] arguments) {
		super();
		this.messageType = messageType;
		this.arguments = arguments;
	}
	
	public EmftextmindmapDebugMessage(mindmap.resource.emftextmindmap.debug.EEmftextmindmapDebugMessageTypes messageType, java.util.List<String> arguments) {
		super();
		this.messageType = messageType;
		this.arguments = new String[arguments.size()];
		for (int i = 0; i < arguments.size(); i++) {
			this.arguments[i] = arguments.get(i);
		}
	}
	
	public mindmap.resource.emftextmindmap.debug.EEmftextmindmapDebugMessageTypes getMessageType() {
		return messageType;
	}
	
	public String[] getArguments() {
		return arguments;
	}
	
	public String serialize() {
		java.util.List<String> parts = new java.util.ArrayList<String>();
		parts.add(messageType.name());
		for (String argument : arguments) {
			parts.add(argument);
		}
		return mindmap.resource.emftextmindmap.util.EmftextmindmapStringUtil.encode(DELIMITER, parts);
	}
	
	public static EmftextmindmapDebugMessage deserialize(String response) {
		java.util.List<String> parts = mindmap.resource.emftextmindmap.util.EmftextmindmapStringUtil.decode(response, DELIMITER);
		String messageType = parts.get(0);
		String[] arguments = new String[parts.size() - 1];
		for (int i = 1; i < parts.size(); i++) {
			arguments[i - 1] = parts.get(i);
		}
		mindmap.resource.emftextmindmap.debug.EEmftextmindmapDebugMessageTypes type = mindmap.resource.emftextmindmap.debug.EEmftextmindmapDebugMessageTypes.valueOf(messageType);
		EmftextmindmapDebugMessage message = new EmftextmindmapDebugMessage(type, arguments);
		return message;
	}
	
	public boolean hasType(mindmap.resource.emftextmindmap.debug.EEmftextmindmapDebugMessageTypes type) {
		return this.messageType == type;
	}
	
	public String getArgument(int index) {
		return getArguments()[index];
	}
	
	public String toString() {
		return this.getClass().getSimpleName() + "[" + messageType.name() + ": " + mindmap.resource.emftextmindmap.util.EmftextmindmapStringUtil.explode(arguments, ", ") + "]";
	}
	
}
