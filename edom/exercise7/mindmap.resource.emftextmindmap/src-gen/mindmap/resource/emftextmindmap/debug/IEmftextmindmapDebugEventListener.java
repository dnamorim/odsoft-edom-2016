/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.debug;

public interface IEmftextmindmapDebugEventListener {
	
	/**
	 * Notification that the given event occurred in the while debugging.
	 */
	public void handleMessage(mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugMessage message);
}
