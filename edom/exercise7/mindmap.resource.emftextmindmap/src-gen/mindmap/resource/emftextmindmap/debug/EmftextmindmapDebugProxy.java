/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.debug;

/**
 * The DebugProxy allows to communicate between the interpreter, which runs in a
 * separate thread or process and the Eclipse Debug framework (i.e., the
 * DebugTarget class).
 */
public class EmftextmindmapDebugProxy {
	
	public static final int STARTUP_DELAY = 1000;
	
	private java.io.PrintStream output;
	
	private java.io.BufferedReader reader;
	
	private mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugTarget debugTarget;
	
	private mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugCommunicationHelper communicationHelper = new mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugCommunicationHelper();
	
	public EmftextmindmapDebugProxy(mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugTarget debugTarget, int requestPort) throws java.net.UnknownHostException, java.io.IOException {
		this.debugTarget = debugTarget;
		// give interpreter a chance to start
		try {
			Thread.sleep(STARTUP_DELAY);
		} catch (InterruptedException e) {
		}
		startSocket(requestPort);
	}
	
	private void startSocket(int requestPort) throws java.net.UnknownHostException, java.io.IOException {
		// creating client proxy socket (trying to connect)...
		java.net.Socket client = new java.net.Socket("localhost", requestPort);
		// creating client proxy socket - done. (connected)
		try {
			java.io.BufferedInputStream input = new java.io.BufferedInputStream(client.getInputStream());
			reader = new java.io.BufferedReader(new java.io.InputStreamReader(input));
		} catch (java.io.IOException e) {
			System.out.println(e);
		}
		try {
			output = new java.io.PrintStream(client.getOutputStream());
		} catch (java.io.IOException e) {
			System.out.println(e);
		}
	}
	
	public void resume() {
		sendCommand(mindmap.resource.emftextmindmap.debug.EEmftextmindmapDebugMessageTypes.RESUME);
	}
	
	public void stepOver() {
		sendCommand(mindmap.resource.emftextmindmap.debug.EEmftextmindmapDebugMessageTypes.STEP_OVER);
	}
	
	public void stepInto() {
		sendCommand(mindmap.resource.emftextmindmap.debug.EEmftextmindmapDebugMessageTypes.STEP_INTO);
	}
	
	public void stepReturn() {
		sendCommand(mindmap.resource.emftextmindmap.debug.EEmftextmindmapDebugMessageTypes.STEP_RETURN);
	}
	
	public void terminate() {
		sendCommand(mindmap.resource.emftextmindmap.debug.EEmftextmindmapDebugMessageTypes.EXIT);
	}
	
	public mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugMessage getStack() {
		return sendCommandAndRead(mindmap.resource.emftextmindmap.debug.EEmftextmindmapDebugMessageTypes.GET_STACK);
	}
	
	public void addLineBreakpoint(String location, int line) {
		mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugMessage message = new mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugMessage(mindmap.resource.emftextmindmap.debug.EEmftextmindmapDebugMessageTypes.ADD_LINE_BREAKPOINT, new String[] {location, Integer.toString(line)});
		communicationHelper.sendEvent(message, output);
	}
	
	public void removeLineBreakpoint(String location, int line) {
		mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugMessage message = new mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugMessage(mindmap.resource.emftextmindmap.debug.EEmftextmindmapDebugMessageTypes.REMOVE_LINE_BREAKPOINT, new String[] {location, Integer.toString(line)});
		communicationHelper.sendEvent(message, output);
	}
	
	public org.eclipse.debug.core.model.IVariable[] getStackVariables(String stackFrame) {
		mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugMessage response = sendCommandAndRead(mindmap.resource.emftextmindmap.debug.EEmftextmindmapDebugMessageTypes.GET_FRAME_VARIABLES, new String[] {stackFrame});
		String[] ids = response.getArguments();
		// fetch all variables
		org.eclipse.debug.core.model.IVariable[] variables = getVariables(ids);
		return variables;
	}
	
	public org.eclipse.debug.core.model.IVariable[] getVariables(String... requestedIDs) {
		mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugMessage response = sendCommandAndRead(mindmap.resource.emftextmindmap.debug.EEmftextmindmapDebugMessageTypes.GET_VARIABLES, requestedIDs);
		String[] varStrings = response.getArguments();
		mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugVariable[] variables  = new mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugVariable[varStrings.length];
		int i = 0;
		for (String varString : varStrings) {
			java.util.Map<String, String> properties = mindmap.resource.emftextmindmap.util.EmftextmindmapStringUtil.convertFromString(varString);
			
			// convert varString to variables and values
			String valueString = properties.get("!valueString");
			String valueRefType = "valueRefType";
			java.util.Map<String, Long> childVariables = new java.util.TreeMap<String, Long>(new java.util.Comparator<String>() {
				public int compare(String s1, String s2) {
					return s1.compareToIgnoreCase(s2);
				}
			});
			for (String property : properties.keySet()) {
				// ignore special properties - they are not children
				if (property.startsWith("!")) {
					continue;
				}
				childVariables.put(property, Long.parseLong(properties.get(property)));
			}
			String id = properties.get("!id");
			org.eclipse.debug.core.model.IValue value = new mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugValue(debugTarget, id, valueString, valueRefType, childVariables);
			
			String variableName = properties.get("!name");
			String variableRefType = properties.get("!type");
			
			mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugVariable variable = new mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugVariable(debugTarget, variableName, value, variableRefType);
			variables[i++] = variable;
		}
		return variables;
	}
	
	private void sendCommand(mindmap.resource.emftextmindmap.debug.EEmftextmindmapDebugMessageTypes command, String... parameters) {
		mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugMessage message = new mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugMessage(command, parameters);
		communicationHelper.sendEvent(message, output);
	}
	
	private mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugMessage sendCommandAndRead(mindmap.resource.emftextmindmap.debug.EEmftextmindmapDebugMessageTypes command, String... parameters) {
		mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugMessage message = new mindmap.resource.emftextmindmap.debug.EmftextmindmapDebugMessage(command, parameters);
		return communicationHelper.sendAndReceive(message, output, reader);
	}
	
}
