/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.debug;

public class EmftextmindmapSourceLookupParticipant extends org.eclipse.debug.core.sourcelookup.AbstractSourceLookupParticipant {
	
	public String getSourceName(Object object) throws org.eclipse.core.runtime.CoreException {
		if (object instanceof mindmap.resource.emftextmindmap.debug.EmftextmindmapStackFrame) {
			mindmap.resource.emftextmindmap.debug.EmftextmindmapStackFrame frame = (mindmap.resource.emftextmindmap.debug.EmftextmindmapStackFrame) object;
			return frame.getResourceURI();
		}
		return null;
	}
	
}
