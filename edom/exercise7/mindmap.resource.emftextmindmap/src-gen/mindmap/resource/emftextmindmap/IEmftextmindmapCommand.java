/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap;

/**
 * A simple interface for commands that can be executed and that return
 * information about the success of their execution.
 */
public interface IEmftextmindmapCommand<ContextType> {
	
	public boolean execute(ContextType context);
}
