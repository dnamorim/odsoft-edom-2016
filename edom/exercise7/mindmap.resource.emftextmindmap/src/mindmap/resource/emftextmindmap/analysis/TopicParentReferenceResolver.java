/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.analysis;

public class TopicParentReferenceResolver implements mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolver<mindmap.Topic, mindmap.Topic> {
	
	private mindmap.resource.emftextmindmap.analysis.EmftextmindmapDefaultResolverDelegate<mindmap.Topic, mindmap.Topic> delegate = new mindmap.resource.emftextmindmap.analysis.EmftextmindmapDefaultResolverDelegate<mindmap.Topic, mindmap.Topic>();
	
	public void resolve(String identifier, mindmap.Topic container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final mindmap.resource.emftextmindmap.IEmftextmindmapReferenceResolveResult<mindmap.Topic> result) {
		delegate.resolve(identifier, container, reference, position, resolveFuzzy, result);
	}
	
	public String deResolve(mindmap.Topic element, mindmap.Topic container, org.eclipse.emf.ecore.EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
