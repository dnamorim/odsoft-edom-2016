/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.ui;

public class EmftextmindmapUIMetaInformation extends mindmap.resource.emftextmindmap.mopp.EmftextmindmapMetaInformation {
	
	public mindmap.resource.emftextmindmap.IEmftextmindmapHoverTextProvider getHoverTextProvider() {
		return new mindmap.resource.emftextmindmap.ui.EmftextmindmapHoverTextProvider();
	}
	
	public mindmap.resource.emftextmindmap.ui.EmftextmindmapImageProvider getImageProvider() {
		return mindmap.resource.emftextmindmap.ui.EmftextmindmapImageProvider.INSTANCE;
	}
	
	public mindmap.resource.emftextmindmap.ui.EmftextmindmapColorManager createColorManager() {
		return new mindmap.resource.emftextmindmap.ui.EmftextmindmapColorManager();
	}
	
	/**
	 * @deprecated this method is only provided to preserve API compatibility. Use
	 * createTokenScanner(mindmap.resource.emftextmindmap.IEmftextmindmapTextResource,
	 * mindmap.resource.emftextmindmap.ui.EmftextmindmapColorManager) instead.
	 */
	public mindmap.resource.emftextmindmap.ui.EmftextmindmapTokenScanner createTokenScanner(mindmap.resource.emftextmindmap.ui.EmftextmindmapColorManager colorManager) {
		return createTokenScanner(null, colorManager);
	}
	
	public mindmap.resource.emftextmindmap.ui.EmftextmindmapTokenScanner createTokenScanner(mindmap.resource.emftextmindmap.IEmftextmindmapTextResource resource, mindmap.resource.emftextmindmap.ui.EmftextmindmapColorManager colorManager) {
		return new mindmap.resource.emftextmindmap.ui.EmftextmindmapTokenScanner(resource, colorManager);
	}
	
	public mindmap.resource.emftextmindmap.ui.EmftextmindmapCodeCompletionHelper createCodeCompletionHelper() {
		return new mindmap.resource.emftextmindmap.ui.EmftextmindmapCodeCompletionHelper();
	}
	
}
