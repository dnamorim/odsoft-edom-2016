/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.ui;

/**
 * An enumeration of all position categories.
 */
public enum EmftextmindmapPositionCategory {
	BRACKET, DEFINTION, PROXY;
}
