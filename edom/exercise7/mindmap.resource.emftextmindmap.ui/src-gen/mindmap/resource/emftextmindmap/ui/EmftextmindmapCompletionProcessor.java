/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.ui;

public class EmftextmindmapCompletionProcessor implements org.eclipse.jface.text.contentassist.IContentAssistProcessor {
	
	private mindmap.resource.emftextmindmap.IEmftextmindmapResourceProvider resourceProvider;
	private mindmap.resource.emftextmindmap.ui.IEmftextmindmapBracketHandlerProvider bracketHandlerProvider;
	
	public EmftextmindmapCompletionProcessor(mindmap.resource.emftextmindmap.IEmftextmindmapResourceProvider resourceProvider, mindmap.resource.emftextmindmap.ui.IEmftextmindmapBracketHandlerProvider bracketHandlerProvider) {
		this.resourceProvider = resourceProvider;
		this.bracketHandlerProvider = bracketHandlerProvider;
	}
	
	public org.eclipse.jface.text.contentassist.ICompletionProposal[] computeCompletionProposals(org.eclipse.jface.text.ITextViewer viewer, int offset) {
		mindmap.resource.emftextmindmap.IEmftextmindmapTextResource textResource = resourceProvider.getResource();
		if (textResource == null) {
			return new org.eclipse.jface.text.contentassist.ICompletionProposal[0];
		}
		String content = viewer.getDocument().get();
		mindmap.resource.emftextmindmap.ui.EmftextmindmapCodeCompletionHelper helper = new mindmap.resource.emftextmindmap.ui.EmftextmindmapCodeCompletionHelper();
		mindmap.resource.emftextmindmap.ui.EmftextmindmapCompletionProposal[] computedProposals = helper.computeCompletionProposals(textResource, content, offset);
		
		// call completion proposal post processor to allow for customizing the proposals
		mindmap.resource.emftextmindmap.ui.EmftextmindmapProposalPostProcessor proposalPostProcessor = new mindmap.resource.emftextmindmap.ui.EmftextmindmapProposalPostProcessor();
		java.util.List<mindmap.resource.emftextmindmap.ui.EmftextmindmapCompletionProposal> computedProposalList = java.util.Arrays.asList(computedProposals);
		java.util.List<mindmap.resource.emftextmindmap.ui.EmftextmindmapCompletionProposal> extendedProposalList = proposalPostProcessor.process(computedProposalList);
		if (extendedProposalList == null) {
			extendedProposalList = java.util.Collections.emptyList();
		}
		java.util.List<mindmap.resource.emftextmindmap.ui.EmftextmindmapCompletionProposal> finalProposalList = new java.util.ArrayList<mindmap.resource.emftextmindmap.ui.EmftextmindmapCompletionProposal>();
		for (mindmap.resource.emftextmindmap.ui.EmftextmindmapCompletionProposal proposal : extendedProposalList) {
			if (proposal.getMatchesPrefix()) {
				finalProposalList.add(proposal);
			}
		}
		org.eclipse.jface.text.contentassist.ICompletionProposal[] result = new org.eclipse.jface.text.contentassist.ICompletionProposal[finalProposalList.size()];
		int i = 0;
		for (mindmap.resource.emftextmindmap.ui.EmftextmindmapCompletionProposal proposal : finalProposalList) {
			String proposalString = proposal.getInsertString();
			String displayString = proposal.getDisplayString();
			String prefix = proposal.getPrefix();
			org.eclipse.swt.graphics.Image image = proposal.getImage();
			org.eclipse.jface.text.contentassist.IContextInformation info;
			info = new org.eclipse.jface.text.contentassist.ContextInformation(image, proposalString, proposalString);
			int begin = offset - prefix.length();
			int replacementLength = prefix.length();
			// if a closing bracket was automatically inserted right before, we enlarge the
			// replacement length in order to overwrite the bracket.
			mindmap.resource.emftextmindmap.ui.IEmftextmindmapBracketHandler bracketHandler = bracketHandlerProvider.getBracketHandler();
			String closingBracket = bracketHandler.getClosingBracket();
			if (bracketHandler.addedClosingBracket() && proposalString.endsWith(closingBracket)) {
				replacementLength += closingBracket.length();
			}
			result[i++] = new org.eclipse.jface.text.contentassist.CompletionProposal(proposalString, begin, replacementLength, proposalString.length(), image, displayString, info, proposalString);
		}
		return result;
	}
	
	public org.eclipse.jface.text.contentassist.IContextInformation[] computeContextInformation(org.eclipse.jface.text.ITextViewer viewer, int offset) {
		return null;
	}
	
	public char[] getCompletionProposalAutoActivationCharacters() {
		return null;
	}
	
	public char[] getContextInformationAutoActivationCharacters() {
		return null;
	}
	
	public org.eclipse.jface.text.contentassist.IContextInformationValidator getContextInformationValidator() {
		return null;
	}
	
	public String getErrorMessage() {
		return null;
	}
}
