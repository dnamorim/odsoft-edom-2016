/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.ui;

/**
 * A class which can be overridden to customize code completion proposals.
 */
public class EmftextmindmapProposalPostProcessor {
	
	public java.util.List<mindmap.resource.emftextmindmap.ui.EmftextmindmapCompletionProposal> process(java.util.List<mindmap.resource.emftextmindmap.ui.EmftextmindmapCompletionProposal> proposals) {
		// the default implementation does returns the proposals as they are
		return proposals;
	}
	
}
