/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.ui;

/**
 * This class is deprecated and not used as of EMFText 1.4.1. The original
 * contents of this class have been moved to
 * mindmap.resource.emftextmindmap.ui.EmftextmindmapSourceViewerConfiguration.
 * This class is only generated to avoid compile errors with existing versions of
 * this class.
 */
@Deprecated
public class EmftextmindmapEditorConfiguration {
	
}
