/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.ui;

/**
 * This class is only generated for backwards compatiblity. The original contents
 * of this class have been moved to class
 * mindmap.resource.emftextmindmap.mopp.EmftextmindmapAntlrTokenHelper.
 */
public class EmftextmindmapAntlrTokenHelper {
	// This class is intentionally left empty.
}
