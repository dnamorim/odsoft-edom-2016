/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.ui;

public abstract class AbstractEmftextmindmapOutlinePageAction extends org.eclipse.jface.action.Action {
	
	private String preferenceKey = this.getClass().getSimpleName() + ".isChecked";
	
	private mindmap.resource.emftextmindmap.ui.EmftextmindmapOutlinePageTreeViewer treeViewer;
	
	public AbstractEmftextmindmapOutlinePageAction(mindmap.resource.emftextmindmap.ui.EmftextmindmapOutlinePageTreeViewer treeViewer, String text, int style) {
		super(text, style);
		this.treeViewer = treeViewer;
	}
	
	public void initialize(String imagePath) {
		org.eclipse.jface.resource.ImageDescriptor descriptor = mindmap.resource.emftextmindmap.ui.EmftextmindmapImageProvider.INSTANCE.getImageDescriptor(imagePath);
		setDisabledImageDescriptor(descriptor);
		setImageDescriptor(descriptor);
		setHoverImageDescriptor(descriptor);
		boolean checked = mindmap.resource.emftextmindmap.ui.EmftextmindmapUIPlugin.getDefault().getPreferenceStore().getBoolean(preferenceKey);
		valueChanged(checked, false);
	}
	
	@Override	
	public void run() {
		if (keepState()) {
			valueChanged(isChecked(), true);
		} else {
			runBusy(true);
		}
	}
	
	public void runBusy(final boolean on) {
		org.eclipse.swt.custom.BusyIndicator.showWhile(org.eclipse.swt.widgets.Display.getCurrent(), new Runnable() {
			public void run() {
				runInternal(on);
			}
		});
	}
	
	public abstract void runInternal(boolean on);
	
	private void valueChanged(boolean on, boolean store) {
		setChecked(on);
		runBusy(on);
		if (store) {
			mindmap.resource.emftextmindmap.ui.EmftextmindmapUIPlugin.getDefault().getPreferenceStore().setValue(preferenceKey, on);
		}
	}
	
	public boolean keepState() {
		return true;
	}
	
	public mindmap.resource.emftextmindmap.ui.EmftextmindmapOutlinePageTreeViewer getTreeViewer() {
		return treeViewer;
	}
	
	public mindmap.resource.emftextmindmap.ui.EmftextmindmapOutlinePageTreeViewerComparator getTreeViewerComparator() {
		return (mindmap.resource.emftextmindmap.ui.EmftextmindmapOutlinePageTreeViewerComparator) treeViewer.getComparator();
	}
	
}
