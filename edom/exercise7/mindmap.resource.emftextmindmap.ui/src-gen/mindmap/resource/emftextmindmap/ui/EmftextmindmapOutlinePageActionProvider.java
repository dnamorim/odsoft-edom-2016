/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.ui;

public class EmftextmindmapOutlinePageActionProvider {
	
	public java.util.List<org.eclipse.jface.action.IAction> getActions(mindmap.resource.emftextmindmap.ui.EmftextmindmapOutlinePageTreeViewer treeViewer) {
		// To add custom actions to the outline view, set the
		// 'overrideOutlinePageActionProvider' option to <code>false</code> and modify
		// this method.
		java.util.List<org.eclipse.jface.action.IAction> defaultActions = new java.util.ArrayList<org.eclipse.jface.action.IAction>();
		defaultActions.add(new mindmap.resource.emftextmindmap.ui.EmftextmindmapOutlinePageLinkWithEditorAction(treeViewer));
		defaultActions.add(new mindmap.resource.emftextmindmap.ui.EmftextmindmapOutlinePageCollapseAllAction(treeViewer));
		defaultActions.add(new mindmap.resource.emftextmindmap.ui.EmftextmindmapOutlinePageExpandAllAction(treeViewer));
		defaultActions.add(new mindmap.resource.emftextmindmap.ui.EmftextmindmapOutlinePageAutoExpandAction(treeViewer));
		defaultActions.add(new mindmap.resource.emftextmindmap.ui.EmftextmindmapOutlinePageLexicalSortingAction(treeViewer));
		defaultActions.add(new mindmap.resource.emftextmindmap.ui.EmftextmindmapOutlinePageTypeSortingAction(treeViewer));
		return defaultActions;
	}
	
}
