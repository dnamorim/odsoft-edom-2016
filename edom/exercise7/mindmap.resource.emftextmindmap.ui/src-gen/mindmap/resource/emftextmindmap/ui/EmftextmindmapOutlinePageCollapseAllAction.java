/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.ui;

public class EmftextmindmapOutlinePageCollapseAllAction extends mindmap.resource.emftextmindmap.ui.AbstractEmftextmindmapOutlinePageAction {
	
	public EmftextmindmapOutlinePageCollapseAllAction(mindmap.resource.emftextmindmap.ui.EmftextmindmapOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Collapse all", org.eclipse.jface.action.IAction.AS_PUSH_BUTTON);
		initialize("icons/collapse_all_icon.gif");
	}
	
	public void runInternal(boolean on) {
		if (on) {
			getTreeViewer().collapseAll();
		}
	}
	
	public boolean keepState() {
		return false;
	}
	
}
