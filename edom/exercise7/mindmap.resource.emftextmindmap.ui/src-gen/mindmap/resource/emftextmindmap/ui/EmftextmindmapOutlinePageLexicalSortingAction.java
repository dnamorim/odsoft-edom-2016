/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.ui;

public class EmftextmindmapOutlinePageLexicalSortingAction extends mindmap.resource.emftextmindmap.ui.AbstractEmftextmindmapOutlinePageAction {
	
	public EmftextmindmapOutlinePageLexicalSortingAction(mindmap.resource.emftextmindmap.ui.EmftextmindmapOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Sort alphabetically", org.eclipse.jface.action.IAction.AS_CHECK_BOX);
		initialize("icons/sort_lexically_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewerComparator().setSortLexically(on);
		getTreeViewer().refresh();
	}
	
}
