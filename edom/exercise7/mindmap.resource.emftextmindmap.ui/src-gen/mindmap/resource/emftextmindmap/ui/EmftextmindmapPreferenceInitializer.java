/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.ui;

/**
 * A class used to initialize default preference values.
 */
public class EmftextmindmapPreferenceInitializer extends org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer {
	
	public void initializeDefaultPreferences() {
		
		initializeDefaultSyntaxHighlighting();
		initializeDefaultBrackets();
		
		org.eclipse.jface.preference.IPreferenceStore store = mindmap.resource.emftextmindmap.ui.EmftextmindmapUIPlugin.getDefault().getPreferenceStore();
		// Set default value for matching brackets
		store.setDefault(mindmap.resource.emftextmindmap.ui.EmftextmindmapPreferenceConstants.EDITOR_MATCHING_BRACKETS_COLOR, "192,192,192");
		store.setDefault(mindmap.resource.emftextmindmap.ui.EmftextmindmapPreferenceConstants.EDITOR_MATCHING_BRACKETS_CHECKBOX, true);
		
	}
	
	private void initializeDefaultBrackets() {
		org.eclipse.jface.preference.IPreferenceStore store = mindmap.resource.emftextmindmap.ui.EmftextmindmapUIPlugin.getDefault().getPreferenceStore();
		initializeDefaultBrackets(store, new mindmap.resource.emftextmindmap.mopp.EmftextmindmapMetaInformation());
	}
	
	public void initializeDefaultSyntaxHighlighting() {
		org.eclipse.jface.preference.IPreferenceStore store = mindmap.resource.emftextmindmap.ui.EmftextmindmapUIPlugin.getDefault().getPreferenceStore();
		initializeDefaultSyntaxHighlighting(store, new mindmap.resource.emftextmindmap.mopp.EmftextmindmapMetaInformation());
	}
	
	private void initializeDefaultBrackets(org.eclipse.jface.preference.IPreferenceStore store, mindmap.resource.emftextmindmap.IEmftextmindmapMetaInformation metaInformation) {
		String languageId = metaInformation.getSyntaxName();
		// set default brackets for ITextResource bracket set
		mindmap.resource.emftextmindmap.ui.EmftextmindmapBracketSet bracketSet = new mindmap.resource.emftextmindmap.ui.EmftextmindmapBracketSet(null, null);
		final java.util.Collection<mindmap.resource.emftextmindmap.IEmftextmindmapBracketPair> bracketPairs = metaInformation.getBracketPairs();
		if (bracketPairs != null) {
			for (mindmap.resource.emftextmindmap.IEmftextmindmapBracketPair bracketPair : bracketPairs) {
				bracketSet.addBracketPair(bracketPair.getOpeningBracket(), bracketPair.getClosingBracket(), bracketPair.isClosingEnabledInside());
			}
		}
		store.setDefault(languageId + mindmap.resource.emftextmindmap.ui.EmftextmindmapPreferenceConstants.EDITOR_BRACKETS_SUFFIX, bracketSet.getBracketString());
	}
	
	private void initializeDefaultSyntaxHighlighting(org.eclipse.jface.preference.IPreferenceStore store, mindmap.resource.emftextmindmap.mopp.EmftextmindmapMetaInformation metaInformation) {
		String languageId = metaInformation.getSyntaxName();
		String[] tokenNames = metaInformation.getSyntaxHighlightableTokenNames();
		if (tokenNames == null) {
			return;
		}
		for (int i = 0; i < tokenNames.length; i++) {
			String tokenName = tokenNames[i];
			mindmap.resource.emftextmindmap.IEmftextmindmapTokenStyle style = metaInformation.getDefaultTokenStyle(tokenName);
			if (style != null) {
				String color = getColorString(style.getColorAsRGB());
				setProperties(store, languageId, tokenName, color, style.isBold(), true, style.isItalic(), style.isStrikethrough(), style.isUnderline());
			} else {
				setProperties(store, languageId, tokenName, "0,0,0", false, false, false, false, false);
			}
		}
	}
	
	private void setProperties(org.eclipse.jface.preference.IPreferenceStore store, String languageID, String tokenName, String color, boolean bold, boolean enable, boolean italic, boolean strikethrough, boolean underline) {
		store.setDefault(mindmap.resource.emftextmindmap.ui.EmftextmindmapSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, mindmap.resource.emftextmindmap.ui.EmftextmindmapSyntaxColoringHelper.StyleProperty.BOLD), bold);
		store.setDefault(mindmap.resource.emftextmindmap.ui.EmftextmindmapSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, mindmap.resource.emftextmindmap.ui.EmftextmindmapSyntaxColoringHelper.StyleProperty.COLOR), color);
		store.setDefault(mindmap.resource.emftextmindmap.ui.EmftextmindmapSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, mindmap.resource.emftextmindmap.ui.EmftextmindmapSyntaxColoringHelper.StyleProperty.ENABLE), enable);
		store.setDefault(mindmap.resource.emftextmindmap.ui.EmftextmindmapSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, mindmap.resource.emftextmindmap.ui.EmftextmindmapSyntaxColoringHelper.StyleProperty.ITALIC), italic);
		store.setDefault(mindmap.resource.emftextmindmap.ui.EmftextmindmapSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, mindmap.resource.emftextmindmap.ui.EmftextmindmapSyntaxColoringHelper.StyleProperty.STRIKETHROUGH), strikethrough);
		store.setDefault(mindmap.resource.emftextmindmap.ui.EmftextmindmapSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, mindmap.resource.emftextmindmap.ui.EmftextmindmapSyntaxColoringHelper.StyleProperty.UNDERLINE), underline);
	}
	
	private String getColorString(int[] colorAsRGB) {
		if (colorAsRGB == null) {
			return "0,0,0";
		}
		if (colorAsRGB.length != 3) {
			return "0,0,0";
		}
		return colorAsRGB[0] + "," +colorAsRGB[1] + ","+ colorAsRGB[2];
	}
}
