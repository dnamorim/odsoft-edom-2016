/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.ui;

public class EmftextmindmapOutlinePageAutoExpandAction extends mindmap.resource.emftextmindmap.ui.AbstractEmftextmindmapOutlinePageAction {
	
	public EmftextmindmapOutlinePageAutoExpandAction(mindmap.resource.emftextmindmap.ui.EmftextmindmapOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Auto expand", org.eclipse.jface.action.IAction.AS_CHECK_BOX);
		initialize("icons/auto_expand_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewer().setAutoExpand(on);
		getTreeViewer().refresh();
	}
	
}
