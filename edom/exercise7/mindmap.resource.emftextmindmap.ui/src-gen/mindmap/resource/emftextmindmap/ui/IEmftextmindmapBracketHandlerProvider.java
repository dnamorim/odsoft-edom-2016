/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.ui;

/**
 * A provider for BracketHandler objects.
 */
public interface IEmftextmindmapBracketHandlerProvider {
	
	/**
	 * Returns the bracket handler.
	 */
	public mindmap.resource.emftextmindmap.ui.IEmftextmindmapBracketHandler getBracketHandler();
	
}
