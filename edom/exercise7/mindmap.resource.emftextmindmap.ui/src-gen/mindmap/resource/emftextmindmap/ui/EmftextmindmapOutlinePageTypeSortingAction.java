/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package mindmap.resource.emftextmindmap.ui;

public class EmftextmindmapOutlinePageTypeSortingAction extends mindmap.resource.emftextmindmap.ui.AbstractEmftextmindmapOutlinePageAction {
	
	public EmftextmindmapOutlinePageTypeSortingAction(mindmap.resource.emftextmindmap.ui.EmftextmindmapOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Group types", org.eclipse.jface.action.IAction.AS_CHECK_BOX);
		initialize("icons/group_types_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewerComparator().setGroupTypes(on);
		getTreeViewer().refresh();
	}
	
}
