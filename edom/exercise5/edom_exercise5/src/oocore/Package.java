/**
 */
package oocore;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Package</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link oocore.Package#getContents <em>Contents</em>}</li>
 * </ul>
 *
 * @see oocore.OocorePackage#getPackage()
 * @model
 * @generated
 */
public interface Package extends PackageableElement {
	/**
	 * Returns the value of the '<em><b>Contents</b></em>' containment reference list.
	 * The list contents are of type {@link oocore.PackageableElement}.
	 * It is bidirectional and its opposite is '{@link oocore.PackageableElement#getPackage <em>Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contents</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contents</em>' containment reference list.
	 * @see oocore.OocorePackage#getPackage_Contents()
	 * @see oocore.PackageableElement#getPackage
	 * @model opposite="package" containment="true"
	 * @generated
	 */
	EList<PackageableElement> getContents();

} // Package
