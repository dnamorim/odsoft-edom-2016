package restcomponent;

import org.apache.ofbiz.entity.GenericDelegator;

/**
 * Contains dummy information for the unit tests.
 * @author Ricardo Mota.
 *
 */
public class GenericDelegatorDummy extends GenericDelegator {
	
	public GenericDelegatorDummy() {
		super();
	}
	
	@Override
	public String getDelegatorName() {
		return "GenericDelegatorDummy";
	}
	
}
