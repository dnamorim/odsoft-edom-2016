package restcomponent;

import org.apache.ofbiz.entity.Delegator;
import org.apache.ofbiz.entity.GenericValue;

/**
 * Dummy values for the delegator class.
 * @author Ricardo Mota.
 *
 */
public class GenericValueDummy extends GenericValue {
	
	private static final long serialVersionUID = 1L;
	private GenericDelegatorDummy delegatorDummy=null;
	
	public GenericValueDummy() {
		delegatorDummy=new GenericDelegatorDummy();
	}
	
	@Override
	public String getEntityName() { 
		return "EntityDummy"; 
	}
	
	@Override
	public String toString() { 
		return "[null-entity]"; 
	}
	
	@Override
	public Delegator getDelegator() { 
		return delegatorDummy; 
	}
	
}