package restcomponent;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.ofbiz.base.util.Debug;
import org.apache.ofbiz.base.util.UtilMisc;
import org.apache.ofbiz.entity.DelegatorFactory;
import org.apache.ofbiz.entity.GenericDelegator;
import org.apache.ofbiz.service.GenericServiceException;
import org.apache.ofbiz.service.LocalDispatcher;

import javolution.util.FastMap;

/**
 * Contains all the REST methods to perform
 * user account operations (login, register).
 */
@Path("/account")
public class AccountResource {
	
	@Context
	HttpServletRequest _httpRequest;

	@POST
	@Produces("application/json")
	@Path("login")
	public Response loginUser(@FormParam("username") String username, @FormParam("password") String password) {
		
		GenericDelegator delegator = (GenericDelegator) DelegatorFactory.getDelegator("default");
		
		LocalDispatcher dispatcher = org.apache.ofbiz.service.ServiceDispatcher
				.getLocalDispatcher("default", delegator);

		Map<String, String> paramMap = UtilMisc.toMap(
				"login.username", username, 
				"login.password", password);

		Map<String, Object> result = FastMap.newInstance();
		
		try {
			result = dispatcher.runSync("userLogin", paramMap);
		} catch (GenericServiceException e1) {
			Debug.logError(e1, PingResource.class.getName());
			return Response.serverError().entity(e1.toString()).build();
		}

		
		if(result.containsKey("responseMessage")){
			if(result.get("responseMessage").equals("success"))
			{				
				return Response.ok().type("application/json").build();				
			}
			else
			{
				if(result.containsKey("errorMessage"))
				{
					return Response.serverError().entity(result.get("errorMessage")).type("application/json").build();
				}else{
					return Response.serverError().entity("An error occurred, try again.").type("application/json").build();
				}
			}
		}
		
		// Should never get here as the responseMessage key is present.
		// If not, an error should be thrown.
		return Response.serverError().type("application/json").build();

	}
	
	@POST
	@Produces("application/json")
	@Path("register")
	public Response registerUser(@FormParam("username") String username, @FormParam("password") String password){
		
		// Admin creedentials are necessary to change permission.
		String adminUsername = "admin";
		String adminPassword = "ofbiz";
		
		GenericDelegator delegator = (GenericDelegator) DelegatorFactory.getDelegator("default");
		
		LocalDispatcher dispatcher = org.apache.ofbiz.service.ServiceDispatcher
				.getLocalDispatcher("default", delegator);

		Map<String, String> paramMap = UtilMisc.toMap(
				"userLoginId", username, "currentPassword", password, "currentPasswordVerify", password);

		Map<String, Object> result = FastMap.newInstance();
		
		try {
			result = dispatcher.runSync("createUserLogin", paramMap);
		} catch (GenericServiceException e1) {
			Debug.logError(e1, PingResource.class.getName());
			return Response.serverError().entity(e1.toString()).build();
		}
	
		if(result.containsKey("responseMessage")){
			if(result.get("responseMessage").equals("success"))
			{
				
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				
				MultivaluedMap<String, Object> headerMap = new MultivaluedHashMap<String, Object>();
				
				headerMap.put("login.username", Arrays.asList(new Object[] { "admin" }));
				headerMap.put("login.password", Arrays.asList(new Object[] { "ofbiz" }));
				
				Map<String, Object> paramMapSecurity = UtilMisc.toMap(
						"fromDate", timestamp, 
						"userLoginId", username,
						"login.username",adminUsername,
						"login.password",adminPassword,
						"groupId", "FULLADMIN");
				
				Map<String, Object> securityResult = FastMap.newInstance();

				try {
					securityResult = dispatcher.runSync("updateUserLoginToSecurityGroup", paramMapSecurity);
				} catch (GenericServiceException e1) {
					Debug.logError(e1, PingResource.class.getName());
					return Response.serverError().entity(e1.toString()).build();
				}
				
				return Response.ok().type("application/json").build();
			}
			else
			{
				if(result.containsKey("errorMessageList"))
				{
					return Response.serverError().entity(result.get("errorMessage")).type("application/json").build();
				}else{
					return Response.serverError().entity("An error occurred, try again.").type("application/json").build();
				}
			}
		}
			
		return Response.serverError().type("application/json").build();
		
	}
	
}
