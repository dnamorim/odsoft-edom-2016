package restcomponent;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.ofbiz.base.conversion.ConversionException;
import org.apache.ofbiz.base.lang.JSON;
import org.apache.ofbiz.base.util.Debug;
import org.apache.ofbiz.base.util.UtilMisc;
import org.apache.ofbiz.entity.DelegatorFactory;
import org.apache.ofbiz.entity.GenericDelegator;
import org.apache.ofbiz.entity.GenericEntityException;
import org.apache.ofbiz.entity.GenericValue;
import org.apache.ofbiz.entity.condition.EntityCondition;
import org.apache.ofbiz.entity.condition.EntityOperator;
import org.apache.ofbiz.entity.util.Converters.GenericValueToJSON;
import org.apache.ofbiz.service.GenericServiceException;
import org.apache.ofbiz.service.LocalDispatcher;

import javolution.util.FastMap;


/**
 * Defines all the recipe REST actions.
 * The server path for this class is set as /recipe.
 *
 */
@Path("/recipe")
public class RecipeResource {
	
	
	@Context
	HttpServletRequest httpRequest;
	
	
	/**
	 * [POST] Create a new recipe using the createRecipe service.
	 * @param recipeName - Recipe name.
	 * @param notes - Recipe notes.
	 * @param recipeTypeId - Recipe type Id.
	 * @return The created recipe information as a JSON object.
	 */
	@POST
	@Produces("application/json")
	public Response createRecipe(@FormParam("recipeName") String recipeName, @FormParam("imageUrl") String imageUrl,
			@FormParam("notes") String notes, @FormParam("recipeTypeId") String recipeTypeId) {
		
		String username = null;
		String password = null;

		try {
			username = httpRequest.getHeader("login.username");
			password = httpRequest.getHeader("login.password");
		} catch (NullPointerException e) {
			return Response.serverError().entity("Problem reading http header(s): login.username or login.password")
					.build();
		}

		if (username == null || password == null) {
			return Response.serverError().entity("Problem reading http header(s): login.username or login.password")
					.build();
		}


		// Lets now invoke the ofbiz service that creates a product
		GenericDelegator delegator = (GenericDelegator) DelegatorFactory.getDelegator("default");
		LocalDispatcher dispatcher = org.apache.ofbiz.service.ServiceDispatcher.getLocalDispatcher("default",
				delegator);

		Map<String, String> paramMap = UtilMisc.toMap(
				"recipeName",recipeName, 
				"notes", notes, 
				"recipeTypeId",recipeTypeId,
				"userId", username, 
				"imageUrl", imageUrl);

		Map<String, Object> result = FastMap.newInstance();
		try {
			// As requested, the create re
			result = dispatcher.runSync("createRecipe", paramMap);
		} catch (GenericServiceException e1) {
			Debug.logError(e1, PingResource.class.getName());
			return Response.serverError().entity(e1.toString()).build();
		}

		// Get the recipe from the Id and return its information trough a JSON object.
		
		String recipeId = result.get("recipeId").toString();
		String recipe = Util.getRecipe(recipeId);
		
		if (recipe != null) {
			return Response.ok(recipe).type("application/json").build();
		} else {
			return Response.serverError().entity("Problem reading the new recipe after created!").build();
		}
	}
		
	/**
	 * [GET] Get all recipes.
	 * @return All the recipes.
	 */
	@GET
	@Produces("aplication/json")
	public Response getRecipeList() {
		
		String username = null;
		String password = null;

		try {
			username = httpRequest.getHeader("login.username");
			password = httpRequest.getHeader("login.password");
		} catch (NullPointerException e) {
			return Response.serverError().entity("Problem reading http header(s): login.username or login.password")
					.build();
		}

		if (username == null || password == null) {
			return Response.serverError().entity("Problem reading http header(s): login.username or login.password")
					.build();
		}

		GenericDelegator delegator = (GenericDelegator) DelegatorFactory.getDelegator("default");
		
		List<GenericValue> recipes = null;

		try {
			// Get all the recipes (no matter what order or fields).
			recipes = delegator.findAll("Recipe",false);
		} catch (GenericEntityException e) {
			return Response.serverError().entity(e.toString()).build();
		}

		if (recipes != null) {
			String response = Util.convertListGenericValueToJSON(recipes);
			
			if (response == null) {
				return Response.serverError().entity("Error on JSON conversion.").build();
			}

			return Response.ok(response).type("application/json").build();
		}

		throw new RuntimeException("Invalid ");
	}

	/**
	 * Gets all the Uom.
	 * @return
	 */
	@GET
	@Produces("aplication/json")
	@Path("/uom")
	public Response getUomList() {
		
		String username = null;
		String password = null;

		try {
			username = httpRequest.getHeader("login.username");
			password = httpRequest.getHeader("login.password");
		} catch (NullPointerException e) {
			return Response.serverError().entity("Problem reading http header(s): login.username or login.password")
					.build();
		}

		if (username == null || password == null) {
			return Response.serverError().entity("Problem reading http header(s): login.username or login.password")
					.build();
		}

		GenericDelegator delegator = (GenericDelegator) DelegatorFactory.getDelegator("default");
		
		List<GenericValue> recipes = null;
		
		EntityCondition condition = EntityCondition.makeCondition(
				EntityOperator.OR,EntityCondition.makeCondition("uomTypeId","RECIPE"));

		Set<String> fieldsToSelect = new HashSet<String>();
		fieldsToSelect.add("uomId");
		fieldsToSelect.add("uomTypeId");
			
		try {
			// Get all the Uom (no matter what order or fields).
			//recipes = delegator.findAll("Uom",false);
			recipes = delegator.findList("Uom", condition, fieldsToSelect, null, null, false);
		} catch (GenericEntityException e) {
			return Response.serverError().entity(e.toString()).build();
		}

		if (recipes != null) {
			String response = Util.convertListGenericValueToJSON(recipes);
			
			if (response == null) {
				return Response.serverError().entity("Error on JSON conversion.").build();
			}

			return Response.ok(response).type("application/json").build();
		}

		throw new RuntimeException("Invalid ");
	}
	

	/**
	 * [GET] Get a recipe by its id.
	 * @param recipeId - Recipe Id.
	 * @return - Recipe information if a recipe with the given id is found.
	 */
	@GET
	@Produces("application/json")
	@Path("{id}")
	public Response getRecipeById(@PathParam("id") String recipeId) {

		String username = null;
		String password = null;

		try {
			username = httpRequest.getHeader("login.username");
			password = httpRequest.getHeader("login.password");
		} catch (NullPointerException e) {
			return Response.serverError().entity("Problem reading http header(s): login.username or login.password")
					.build();
		}

		if (username == null || password == null) {
			return Response.serverError().entity("Problem reading http header(s): login.username or login.password")
					.build();
		}

		GenericDelegator delegator = (GenericDelegator) DelegatorFactory.getDelegator("default");
		GenericValue recipe = null;

		try {
			recipe = delegator.findOne("Recipe", UtilMisc.toMap("recipeId", recipeId), false);
		} catch (GenericEntityException e) {
			return Response.serverError().entity(e.toString()).build();
		}

		if (recipe != null) {

			JsonObject object = null;

			JSON json = null;
			
			try {
				json = new GenericValueToJSON().convert(recipe);
			} catch (ConversionException e) {
				return Response.serverError().entity("Problem converting the recipe to json!").build();
			}

			JsonReader jsonReader = Json.createReader(new StringReader(json.toString()));
			object = jsonReader.readObject();
			jsonReader.close();

			return Response.ok(object.toString()).type("application/json").build();
		}

		throw new RuntimeException("Invalid ");
		
	}
	
	
	@PUT
	@Produces("application/json")
	@Path("{id}")
	public Response updateRecipe(@PathParam("id") String recipeId, @FormParam("recipeName") String recipeName,
			@FormParam("notes") String notes, @FormParam("recipeTypeId") String recipeTypeId) {
		
		String username = null;
		String password = null;

		try {
			username = httpRequest.getHeader("login.username");
			password = httpRequest.getHeader("login.password");
		} catch (NullPointerException e) {
			return Response.serverError().entity("Problem reading http header(s): login.username or login.password")
					.build();
		}

		if (username == null || password == null) {
			return Response.serverError().entity("Problem reading http header(s): login.username or login.password")
					.build();
		}

		GenericDelegator delegator = (GenericDelegator) DelegatorFactory.getDelegator("default");
		GenericValue recipe = null;

				
		try {
			// Check if recipe exists.
			recipe = delegator.findOne("Recipe", UtilMisc.toMap("recipeId", recipeId), false);	
		} catch (GenericEntityException e1) {
			Debug.logError(e1, RecipeResource.class.getName());
			return Response.serverError().entity(e1.toString()).build();
		}
			
			if (recipe != null) {

				// Recipe exists. Can update.				
				if(recipeName != null){
					recipe.set("recipeName", recipeName);
				}
				
				if(notes != null){
					recipe.set("notes", notes);
				}
				
				if(recipeTypeId != null){
					recipe.set("recipeTypeId", recipeTypeId);
				}
				
				
				try{	
					// Try and store the updated recipe.
					delegator.store(recipe);
				}catch(Exception Ex){
					return Response.serverError().entity("Problem updating the recipe.").build();
				}
				
				
				JsonObject object = null;
				
				try{
					JSON json = new GenericValueToJSON().convert(recipe);
					JsonReader jsonReader = Json.createReader(new StringReader(json.toString()));
					object = jsonReader.readObject();
					jsonReader.close();
				
				}catch(Exception Ex){
					return Response.serverError().entity("Problem converting the updated recipe to JSON.").build();
				}
							
				// Return the updated recipe.
				return Response.ok(object.toString()).type("application/json").build();
				
			} else {
				
				return Response.serverError().entity("Problem reading received recipe.").build();
			}
			
	}
	
}
