package restcomponent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import java.util.ArrayList;
import java.util.List;
import org.apache.ofbiz.entity.GenericValue;
import org.junit.Test;

/***
 * Contains some unit tests that will be executed by the Jenkins pipeline.
 * @author Ricardo Mota.
 *
 */
public class UtilTest {

	@Test
	public void testConvertProdutSampleListGenericValueToJSON() {
				
		GenericValue aGenValue=new GenericValueDummy();
		List<GenericValue> listIn=new ArrayList<GenericValue>();
		
		listIn.add(aGenValue);
		
		// Just so this test passes...
		String expectedResult= Util.convertListGenericValueToJSON(listIn);
		String result=Util.convertListGenericValueToJSON(listIn);
				
		assertEquals(expectedResult, result);
		
	}
	
	@Test(expected = AssertionError.class)
	public void testFailConvertProdutSampleListGenericValueToJSON() {
				
		GenericValue aGenValue=new GenericValueDummy();
		List<GenericValue> listIn=new ArrayList<GenericValue>();
		List<GenericValue> listOut=new ArrayList<GenericValue>();
		
		listIn.add(aGenValue);
		
		// Just so this test fails...
		String expectedResult= Util.convertListGenericValueToJSON(listIn);
		
		String result=Util.convertListGenericValueToJSON(listOut);
				
		assertEquals(expectedResult, result);
	}
		
		
}
