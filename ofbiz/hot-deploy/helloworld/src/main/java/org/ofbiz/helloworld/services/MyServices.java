package org.ofbiz.helloworld.services;

import java.util.List;
import java.util.Map;

import org.apache.ofbiz.base.util.UtilMisc;
import org.apache.ofbiz.entity.Delegator;
import org.apache.ofbiz.entity.GenericEntityException;
import org.apache.ofbiz.entity.GenericValue;
import org.apache.ofbiz.service.DispatchContext;
import org.apache.ofbiz.service.ServiceUtil;

public class MyServices {
	
public static Map<String, Object> createRecipe(DispatchContext dctx, Map<String, Object> context){
		
		String recipeName = (String)context.get("recipeName");
		String notes = (String)context.get("notes");
		String recipeTypeId = (String)context.get("recipeTypeId");
		String imageUrl = (String)context.get("imageUrl");
		String userId = (String)context.get("userId");
	
				
        Delegator delegator = dctx.getDelegator();
        
        String recipeId = delegator.getNextSeqId("Recipe");
   
        GenericValue recipeContent = delegator.makeValue(
        		"Recipe", UtilMisc.toMap("recipeId", recipeId));
        

        recipeContent.set("name",recipeName, false);
        recipeContent.set("notes", notes, false);
        recipeContent.set("recipeTypeId",recipeTypeId, false);
        recipeContent.set("imageUrl", imageUrl);
        recipeContent.set("userId", userId);
        
        try {
        	recipeContent.create();
        } catch (GenericEntityException e) {
            return ServiceUtil.returnError(e.getMessage());
        }

        
		Map<String, Object> result = ServiceUtil.returnSuccess() ;
		
		
        result.put("recipeId", recipeId);
	
        
		return result ;
		
	}
}
